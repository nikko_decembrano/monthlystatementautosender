using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;
using System.Diagnostics;


namespace NewTnCPrinter
{
    public partial class NewTnCPrinter : Form
    {
        private CustomerContactList contacts;

        public NewTnCPrinter()
        {
            InitializeComponent();

            contacts = new CustomerContactList(new ActionTypeList(new ActionType[] { ActionType.Print }), AutoSendType.NewTermsAndConditions2012, -1, 30);

            lstNewTnCCustomers.Items.AddRange(contacts.getListOfCustomers());
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            EmailBody letter = EmailBody.GetEmailBodyFromDB("NewConditionsRequest");

            string customerLetter = letter.HtmlBody;
            customerLetter.Replace("@ExactNo", "1000");
            customerLetter.Replace("@CusNo", "ABC");

            Process printjob = new Process();

            printjob.StartInfo.FileName = "";
            printjob.StartInfo.UseShellExecute = true;
            printjob.StartInfo.Verb = "print";
            printjob.StartInfo.CreateNoWindow = true;
            printjob.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            printjob.Start();
        }
    }
}