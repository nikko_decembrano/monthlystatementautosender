using System;
using NUnit.Framework;
using NUnit.Core;
using AutoSenderLibrary;

namespace AutoSenderNUnitTest
{
    #region Test Suite
    public class AutoSenderLibraryTestSuite
    {
        [Suite]
        public static TestSuite suite
        {
            get 
            {
                TestSuite suite = new TestSuite("AutoSenderLibraryTests");
                suite.Add(new ActionTypeListTests());
                suite.Add(new CustomerContactListTests());
                return suite;
            }
        }
    }
    #endregion

    #region ActionTypeList tests
    [TestFixture]
    public class ActionTypeListTests
    {
        ActionTypeList allActions, noActions, faxAction, twoActions;

        [SetUp]
        public void ActionTypeListTestsSetup()
        {
            // Common setup
            allActions = new ActionTypeList(ActionTypeList.getAllPossibleActions());
            noActions  = new ActionTypeList();
            faxAction  = new ActionTypeList();
            faxAction.addAction(ActionType.Fax);
            twoActions = new ActionTypeList(new ActionType[] { ActionType.Review, ActionType.Print });
        }

        #region long testConstructorsAndgetAllPossibleActions Test
        [Test]
        public void testConstructorsAndgetAllPossibleActions()
        {
            // Test allActions
            Assert.IsTrue(allActions.containsAction(ActionType.Email));
            Assert.IsTrue(allActions.containsAction(ActionType.Fax));
            Assert.IsTrue(allActions.containsAction(ActionType.Print));
            Assert.IsTrue(allActions.containsAction(ActionType.Review));
            Assert.IsFalse(allActions.containsAction(ActionType.Sent));
            Assert.IsFalse(allActions.containsAction(ActionType.Unknown));
            Assert.AreEqual(allActions.getActions().Length, 4);

            // testnoActions
            Assert.IsFalse(noActions.containsAction(ActionType.Email));
            Assert.IsFalse(noActions.containsAction(ActionType.Fax));
            Assert.IsFalse(noActions.containsAction(ActionType.Print));
            Assert.IsFalse(noActions.containsAction(ActionType.Review));
            Assert.IsFalse(allActions.containsAction(ActionType.Sent));
            Assert.IsFalse(noActions.containsAction(ActionType.Unknown));
            Assert.AreEqual(noActions.getActions().Length, 0);

            // testfaxAction
            Assert.IsFalse(faxAction.containsAction(ActionType.Email));
            Assert.IsTrue(faxAction.containsAction(ActionType.Fax));
            Assert.IsFalse(faxAction.containsAction(ActionType.Print));
            Assert.IsFalse(faxAction.containsAction(ActionType.Review));
            Assert.IsFalse(allActions.containsAction(ActionType.Sent));
            Assert.IsFalse(faxAction.containsAction(ActionType.Unknown));
            Assert.AreEqual(faxAction.getActions().Length, 1);

            // testtwoActions (print and review)
            Assert.IsFalse(twoActions.containsAction(ActionType.Email));
            Assert.IsFalse(twoActions.containsAction(ActionType.Fax));
            Assert.IsTrue(twoActions.containsAction(ActionType.Print));
            Assert.IsTrue(twoActions.containsAction(ActionType.Review));
            Assert.IsFalse(allActions.containsAction(ActionType.Sent));
            Assert.IsFalse(twoActions.containsAction(ActionType.Unknown));
            Assert.AreEqual(twoActions.getActions().Length, 2);
        }
        #endregion

        [Test]
        public void testGetStringFromActionType()
        {
            Assert.AreEqual("Email",  ActionTypeList.getStringFromActionType(ActionType.Email));
            Assert.AreEqual("Fax",    ActionTypeList.getStringFromActionType(ActionType.Fax));
            Assert.AreEqual("Print",  ActionTypeList.getStringFromActionType(ActionType.Print));
            Assert.AreEqual("Review", ActionTypeList.getStringFromActionType(ActionType.Review));
            Assert.AreEqual("Unknown", ActionTypeList.getStringFromActionType(ActionType.Unknown));
            Assert.AreEqual("Sent", ActionTypeList.getStringFromActionType(ActionType.Sent));

        }

        [Test]
        public void testGetActionTypeFromString()
        {
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("Email"), ActionType.Email);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("Fax"),     ActionType.Fax);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("Print"),   ActionType.Print);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("Review"),  ActionType.Review);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("Unknown"), ActionType.Unknown);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("Foo"),     ActionType.Unknown);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("Bar"),     ActionType.Unknown);
        }

        [Test]
        public void testGetActionTypeFromStringCaseTest()
        {
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("EMail"), ActionType.Email);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("EmAil"), ActionType.Email);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("faX"), ActionType.Fax);
            Assert.AreEqual(ActionTypeList.getActionTypeFromString("review"), ActionType.Review);
        }

        [Test]
        public void testAddAction()
        {
            //Add 2 actions
            Assert.IsTrue(noActions.addAction(ActionType.Email));
            Assert.IsTrue(noActions.containsAction(ActionType.Email));
            Assert.AreEqual(noActions.getActions().Length, 1);

            Assert.IsTrue(noActions.addAction(ActionType.Fax));
            Assert.IsTrue(noActions.containsAction(ActionType.Email));
            Assert.IsTrue(noActions.containsAction(ActionType.Fax));
            Assert.AreEqual(noActions.getActions().Length, 2);

            // Add duplicate actions
            Assert.IsFalse(noActions.addAction(ActionType.Email));
            Assert.IsTrue(noActions.containsAction(ActionType.Email));
            Assert.IsTrue(noActions.containsAction(ActionType.Fax));
            Assert.AreEqual(noActions.getActions().Length, 2);

        }

        [Test]
        public void testRemoveAction()
        {
            Assert.IsTrue(allActions.removeAction(ActionType.Print));
            Assert.IsTrue(allActions.containsAction(ActionType.Email));
            Assert.IsTrue(allActions.containsAction(ActionType.Fax));
            Assert.IsFalse(allActions.containsAction(ActionType.Print));
            Assert.IsTrue(allActions.containsAction(ActionType.Review));
            Assert.AreEqual(allActions.getActions().Length, 3);

            Assert.IsTrue(allActions.removeAction(ActionType.Email));
            Assert.IsFalse(allActions.containsAction(ActionType.Email));
            Assert.IsTrue(allActions.containsAction(ActionType.Fax));
            Assert.IsFalse(allActions.containsAction(ActionType.Print));
            Assert.IsTrue(allActions.containsAction(ActionType.Review));
            Assert.AreEqual(allActions.getActions().Length, 2);

            // Remove non-existing action
            Assert.IsFalse(allActions.removeAction(ActionType.Print));
            Assert.IsFalse(allActions.containsAction(ActionType.Email));
            Assert.IsTrue(allActions.containsAction(ActionType.Fax));
            Assert.IsFalse(allActions.containsAction(ActionType.Print));
            Assert.IsTrue(allActions.containsAction(ActionType.Review));
            Assert.AreEqual(allActions.getActions().Length, 2);
        }

        [Test]
        public void testClearActions()
        {
            allActions.clearActions();
            Assert.IsFalse(allActions.containsAction(ActionType.Email));
            Assert.IsFalse(allActions.containsAction(ActionType.Fax));
            Assert.IsFalse(allActions.containsAction(ActionType.Print));
            Assert.IsFalse(allActions.containsAction(ActionType.Review));
            Assert.AreEqual(allActions.getActions().Length, 0);
        }

        [TearDown]
        public void ActionTypeListTestsTearDown()
        {
            // Common teardowns (None at the moment)
        }
    }
    #endregion

    #region CustomerContact Tests
    [TestFixture]
    public class CustomerContactTests
    {
        CustomerContact empty000, email001, fax002, print003, review004;

        [SetUp]
        public void CustomerContactListTestsSetup()
        {
            empty000 = new CustomerContact("empty000", "emptycode000", ActionType.Email, "a");
            email001    = new CustomerContact("email001", "emailcode001",  ActionType.Email, "b");
            email001.addDetail("a@abc.com");
            fax002      = new CustomerContact("fax002", "faxcode001",    ActionType.Fax, "c");
            fax002.addDetails(new string[] { "0292020202", "90202020" });
            print003 = new CustomerContact("print003", "printcode003", ActionType.Print, "d",new string[] { "a@bbc.com", "b@bbc.com", "c@bbc.com" });
            review004   = new CustomerContact("review004", "reviewcode004", ActionType.Review, "e");
        }

        [Test]
        public void ConstructorTest()
        {
            Assert.AreEqual(empty000.getDetails().Length, 0);
            Assert.AreEqual(email001.getDetails()[0], "a@abc.com");
            Assert.AreEqual(email001.getDetails().Length, 1);

            Assert.AreEqual(fax002.getDetails()[0], "0292020202");
            Assert.AreEqual(fax002.getDetails()[1], "90202020");
            Assert.AreEqual(fax002.getDetails().Length, 2);

            Assert.AreEqual(print003.getDetails()[0], "a@bbc.com");
            Assert.AreEqual(print003.getDetails()[1], "b@bbc.com");
            Assert.AreEqual(print003.getDetails()[2], "c@bbc.com");
            Assert.AreEqual(print003.getDetails().Length, 3);
        }

        [Test]
        public void CustomerNumberCodeTest()
        {
            Assert.AreEqual(empty000.getCustomerNumber(), "empty000");
            Assert.AreEqual(empty000.getCustomerCode(), "emptycode000");
            
        }

        [Test]
        public void AddAndAddDuplicateDetailsTest()
        {
            Assert.IsTrue(empty000.addDetail("a.abc.com"));
            Assert.IsTrue(empty000.addDetail("b.abc.com"));
            string[] details = empty000.getDetails();
            Assert.AreEqual("a.abc.com", details[0]);
            Assert.AreEqual("b.abc.com", details[1]);
            Assert.AreEqual(2, details.Length);

            // Add possible duplicate details
            Assert.IsFalse(empty000.addDetail("a.abc.com"));
            Assert.IsFalse(empty000.addDetail("b.abc.com"));
            Assert.IsTrue(empty000.addDetail("c.abc.com"));
            details = empty000.getDetails();
            Assert.AreEqual("a.abc.com", details[0]);
            Assert.AreEqual("b.abc.com", details[1]);
            Assert.AreEqual("c.abc.com", details[2]);
            Assert.AreEqual(3, details.Length); // a and b should be ignored

            //AddDetails (2 of 3 should be added)
            empty000.addDetails(new string[] { "c.abc.com", "d.abc.com", "e.abc.com" });
            details = empty000.getDetails();
            Assert.AreEqual("a.abc.com", details[0]);
            Assert.AreEqual("b.abc.com", details[1]);
            Assert.AreEqual("c.abc.com", details[2]);
            Assert.AreEqual("d.abc.com", details[3]);
            Assert.AreEqual("e.abc.com", details[4]);
            Assert.AreEqual(5, details.Length); // a and b should be ignored

        }

        [Test]
        public void RemoveDetailsTest()
        {
            // remove existing
            Assert.IsTrue(print003.removeDetail("a@bbc.com"));
            string[] details = print003.getDetails();
            Assert.AreEqual("b@bbc.com", details[0]);
            Assert.AreEqual("c@bbc.com", details[1]);
            Assert.AreEqual(2, details.Length);

            // remove non existing
            Assert.IsFalse(print003.removeDetail("d@bbc.com"));
            Assert.IsFalse(print003.removeDetail("a@bbc.com"));
            details = print003.getDetails();
            Assert.AreEqual("b@bbc.com", details[0]);
            Assert.AreEqual("c@bbc.com", details[1]);
            Assert.AreEqual(2, details.Length);

            // remove range
            print003.removeDetails(new string[] { "x@bbc.com", "c@bbc.com", "b@bbc.com" });
            details = print003.getDetails();
            Assert.AreEqual(0, details.Length);
        }

        [Test]
        public void ClearDetailsTest()
        {
            Assert.AreEqual(print003.getDetails()[0], "a@bbc.com");
            Assert.AreEqual(print003.getDetails()[1], "b@bbc.com");
            Assert.AreEqual(print003.getDetails()[2], "c@bbc.com");
            Assert.AreEqual(print003.getDetails().Length, 3);

            print003.clearDetails();

            Assert.AreEqual(print003.getDetails().Length, 0);
        }

        [Test]
        public void GetActionTypeTest()
        {
            Assert.AreEqual(ActionType.Email,  empty000.getActionType());
            Assert.AreEqual(ActionType.Email,  email001.getActionType());
            Assert.AreEqual(ActionType.Fax,    fax002.getActionType());
            Assert.AreEqual(ActionType.Print,  print003.getActionType());
            Assert.AreEqual(ActionType.Review, review004.getActionType());
        }

        [Test]
        public void InvalidContactTest()
        { 
            Assert.IsFalse(empty000.addDetail(""));
            empty000.addDetails(new string[] { "", null, "a@b.net" });
            String[] details = empty000.getDetails();
            Assert.AreEqual("a@b.net", details[0]);
            Assert.AreEqual(1, details.Length);
        }

        [Test]
        public void upszoneTest()
        {
            Assert.AreEqual("a", empty000.getUpszone());
            Assert.AreEqual("b", email001.getUpszone());
            Assert.AreEqual("c", fax002.getUpszone());
            Assert.AreEqual("d", print003.getUpszone());
            Assert.AreEqual("e", review004.getUpszone());
        }

        [TearDown]
        public void CustomerContactListTearDown()
        {
        }
    }
    #endregion

    #region CustomerContactList Tests
    [TestFixture]
    public class CustomerContactListTests
    {
        CustomerContactList customers;

        [SetUp]
        public void CustomerContactListTestsSetup()
        {
            customers = new CustomerContactList();
        }

        /// <summary>If this test fails, database access did not succeed</summary>
        [Test]
        public void IsRetrievalSuccessfulTest()
        {
            Assert.IsTrue(customers.isRetrievalSuccessful());
        }

        [TearDown]
        public void CustomerContactListTearDown()
        {
        }
    }
    #endregion

    #region CustomerContactList Tests (HK)
    [TestFixture]
    public class CustomerContactListTestsHK
    {
        CustomerContactList customers;

        [SetUp]
        public void CustomerContactListTestsSetup()
        {
            customers = new CustomerContactList(DatabaseEnvironment.HK);
        }

        /// <summary>If this test fails, database access did not succeed</summary>
        [Test]
        public void IsRetrievalSuccessfulTest()
        {
            Assert.IsTrue(customers.isRetrievalSuccessful());
        }

        [TearDown]
        public void CustomerContactListTearDown()
        {
        }
    }
    #endregion

    #region CustomerContactList Tests
    [TestFixture]
    public class EStatementFormatsTest
    {
        EStatementFormats _eStatements;

        [SetUp]
        public void CustomerContactListTestsSetup()
        {
            _eStatements = new EStatementFormats();
        }

        /// <summary>Check Adding Format</summary>
        [Test]
        public void AddFormat()
        {
            Assert.IsTrue(_eStatements.AddFormat(EStatementFormat.PDF));
            Assert.IsFalse(_eStatements.AddFormat(EStatementFormat.PDF));
            Assert.IsTrue(_eStatements.HasFormat(EStatementFormat.PDF));
            Assert.IsFalse(_eStatements.HasFormat(EStatementFormat.EXCEL2003));

            EStatementFormat[] formats = _eStatements.GetFormats();
            Assert.IsTrue(formats.Length == 1);
            Assert.IsTrue(formats[0] == EStatementFormat.PDF);
        }

        /// <summary>Check Adding Formats</summary>
        [Test]
        public void AddFormats()
        {
            EStatementFormat[] failedFormats = _eStatements.AddFormats(new EStatementFormat[] { EStatementFormat.PDF, EStatementFormat.EXCEL2003 });

            Assert.IsTrue(failedFormats.Length == 0);
            Assert.IsFalse(_eStatements.AddFormat(EStatementFormat.PDF));

            EStatementFormat[] formats = _eStatements.GetFormats();
            Assert.IsTrue(formats.Length == 2);
            Assert.IsTrue(formats[0] == EStatementFormat.PDF);
            Assert.IsTrue(formats[1] == EStatementFormat.EXCEL2003);
        }

        /// <summary>Check removing Formats</summary>
        [Test]
        public void RemoveFormat()
        {
            EStatementFormat[] failedFormats = _eStatements.AddFormats(new EStatementFormat[] { EStatementFormat.PDF, EStatementFormat.EXCEL2003 });

            Assert.IsTrue(_eStatements.RemoveFormat(EStatementFormat.PDF));

            EStatementFormat[] formats = _eStatements.GetFormats();
            Assert.IsTrue(formats.Length == 1);

            Assert.IsTrue(_eStatements.HasFormat(EStatementFormat.EXCEL2003));
            Assert.IsFalse(_eStatements.HasFormat(EStatementFormat.PDF));
        }

        /// <summary>Check removing Formats</summary>
        [Test]
        public void RemoveFormats()
        {
            EStatementFormat[] failedFormats = _eStatements.AddFormats(new EStatementFormat[] { EStatementFormat.PDF, EStatementFormat.EXCEL2003 });

            EStatementFormat[] failedformats = _eStatements.RemoveFormats(new EStatementFormat[] { EStatementFormat.PDF, EStatementFormat.EXCEL2003 });
            Assert.IsTrue(failedformats.Length == 0);

            failedformats = _eStatements.RemoveFormats(new EStatementFormat[] { EStatementFormat.PDF, EStatementFormat.EXCEL2003 });
            Assert.IsTrue(failedformats.Length == 2);

            EStatementFormat[] formats = _eStatements.GetFormats();
            Assert.IsTrue(formats.Length == 0);

            Assert.IsFalse(_eStatements.HasFormat(EStatementFormat.EXCEL2003));
            Assert.IsFalse(_eStatements.HasFormat(EStatementFormat.PDF));
        }

        /// <summary>Check removing Formats</summary>
        [Test]
        public void ClearFormats()
        {
            EStatementFormat[] failedFormats = _eStatements.AddFormats(new EStatementFormat[] { EStatementFormat.PDF, EStatementFormat.EXCEL2003 });

            _eStatements.ClearFormats();

            EStatementFormat[] formats = _eStatements.GetFormats();
            Assert.IsTrue(formats.Length == 0);

            Assert.IsFalse(_eStatements.HasFormat(EStatementFormat.EXCEL2003));
            Assert.IsFalse(_eStatements.HasFormat(EStatementFormat.PDF));
        }

        [TearDown]
        public void CustomerContactListTearDown()
        {
        }
    }
    #endregion

    #region DatabaseEnvironments
    [TestFixture]
    public class DatabaseEnvironmentsTests
    {
        DatabaseEnvironments _defaultEnvironment;
        DatabaseEnvironments _auEnvironment;
        DatabaseEnvironments _hkEnvironment;
        DatabaseEnvironments _phEnvironment;

        private readonly string _auNetcrmComm = "user id=dev;Pooling=false;Data source=NETCRMAU;Initial catalog=NETCRM;";
        private readonly string _auExactComm = "user id=dev;Pooling=false;Data source=LIVEXACTAUS;Initial catalog=135;";
        private readonly string _auMonthlyReportPath = "N:\\CompiledReports\\statement_test\\statement_summary_individual_neg_daterange.rpt";


        private readonly string _hkNetcrmComm = "user id=dev;Pooling=false;Data source=LIVMACOLA;Initial catalog=NETCRM;";
        private readonly string _hkExactComm = "user id=dev;Pooling=false;Data source=LIVEXACT3\\LIVEHK;Initial catalog=135;";
        private readonly string _hkMonthlyReportPath = "N:\\CompiledReportsHK\\statement_test\\statement_summary_individual_neg_daterange.rpt";

        private readonly string _phNetcrmComm = "user id=dev;Pooling=false;Data source=EXACTPH;Initial catalog=NETCRM;";
        private readonly string _phExactComm = "user id=dev;Pooling=false;Data source=livexact2;Initial catalog=135;";
        private readonly string _phMonthlyReportPath = "N:\\CompiledReportsPH\\statement_test\\statement_summary_individual_neg_daterange.rpt";

        [SetUp]
        public void DatabaseEnvironmentsSetup()
        {
            _defaultEnvironment = new DatabaseEnvironments("randomString");
            _auEnvironment = new DatabaseEnvironments(DatabaseEnvironment.AU);
            _hkEnvironment = new DatabaseEnvironments(DatabaseEnvironment.HK);
            _phEnvironment = new DatabaseEnvironments(DatabaseEnvironment.PH);
        }

        /// <summary>Test if AU values are correct</summary>
        [Test]
        public void TestExpectedStringValueAU()
        {
            Assert.IsTrue(_auEnvironment.GetNetCRMConnection() == _auNetcrmComm);
            Assert.IsTrue(_auEnvironment.GetExactConnection() == _auExactComm);
            Assert.IsTrue(_auEnvironment.GetMonthlyReportPath() == _auMonthlyReportPath);
        }

        /// <summary>Test if AU values are correct</summary>
        [Test]
        public void TestExpectedStringValueHK()
        {
            Assert.IsTrue(_hkEnvironment.GetNetCRMConnection() == _hkNetcrmComm);
            Assert.IsTrue(_hkEnvironment.GetExactConnection() == _hkExactComm);
            Assert.IsTrue(_hkEnvironment.GetMonthlyReportPath() == _hkMonthlyReportPath);
        }

        /// <summary>Test if AU values are correct</summary>
        [Test]
        public void TestExpectedStringValuePH()
        {
            Assert.IsTrue(_phEnvironment.GetNetCRMConnection() == _phNetcrmComm);
            Assert.IsTrue(_phEnvironment.GetExactConnection() == _phExactComm);
            Assert.IsTrue(_phEnvironment.GetMonthlyReportPath() == _phMonthlyReportPath);
        }

        /// <summary>Test if default environment defaults to AU</summary>
        [Test]
        public void TestDefaultEnvironment()
        {
            Assert.IsTrue(DatabaseEnvironment.DefaultEnvironment == _defaultEnvironment.GetDatabaseEnvironment());
            Assert.IsTrue(_auEnvironment.GetNetCRMConnection() == _defaultEnvironment.GetNetCRMConnection());
            Assert.IsTrue(_auEnvironment.GetExactConnection() == _defaultEnvironment.GetExactConnection());
            Assert.IsTrue(_auEnvironment.GetMonthlyReportPath() == _defaultEnvironment.GetMonthlyReportPath());
        }

        /// <summary>Check if the string version </summary>
        [Test]
        public void StringVersionCorrect()
        {
            // AU
            DatabaseEnvironments auEnvironment = new DatabaseEnvironments("aU");
            Assert.IsTrue(_auEnvironment.GetDatabaseEnvironment() == auEnvironment.GetDatabaseEnvironment());
            Assert.IsTrue(_auEnvironment.GetNetCRMConnection() == auEnvironment.GetNetCRMConnection());
            Assert.IsTrue(_auEnvironment.GetExactConnection() == auEnvironment.GetExactConnection());
            Assert.IsTrue(_auEnvironment.GetMonthlyReportPath() == auEnvironment.GetMonthlyReportPath());

            //HK
            DatabaseEnvironments hkEnvironment = new DatabaseEnvironments("Hk");
            Assert.IsTrue(_hkEnvironment.GetDatabaseEnvironment() == hkEnvironment.GetDatabaseEnvironment());
            Assert.IsTrue(_hkEnvironment.GetNetCRMConnection() == hkEnvironment.GetNetCRMConnection());
            Assert.IsTrue(_hkEnvironment.GetExactConnection() == hkEnvironment.GetExactConnection());
            Assert.IsTrue(_hkEnvironment.GetMonthlyReportPath() == hkEnvironment.GetMonthlyReportPath());

            //PH
            DatabaseEnvironments phEnvironment = new DatabaseEnvironments("ph");
            Assert.IsTrue(_phEnvironment.GetDatabaseEnvironment() == phEnvironment.GetDatabaseEnvironment());
            Assert.IsTrue(_phEnvironment.GetNetCRMConnection() == phEnvironment.GetNetCRMConnection());
            Assert.IsTrue(_phEnvironment.GetExactConnection() == phEnvironment.GetExactConnection());
            Assert.IsTrue(_phEnvironment.GetMonthlyReportPath() == phEnvironment.GetMonthlyReportPath());
        }

        /// <summary>Check if static version is correct </summary>
        [Test]
        public void StaticVersionCorrect()
        {
            // AU
            Assert.IsTrue(DatabaseEnvironments.GetNetCRMConnection(DatabaseEnvironment.AU) == _auNetcrmComm);
            Assert.IsTrue(DatabaseEnvironments.GetExactConnection(DatabaseEnvironment.AU) == _auExactComm);
            Assert.IsTrue(DatabaseEnvironments.GetMonthlyReportPath(DatabaseEnvironment.AU) == _auMonthlyReportPath);

            //HK
            Assert.IsTrue(DatabaseEnvironments.GetNetCRMConnection(DatabaseEnvironment.HK) == _hkNetcrmComm);
            Assert.IsTrue(DatabaseEnvironments.GetExactConnection(DatabaseEnvironment.HK) == _hkExactComm);
            Assert.IsTrue(DatabaseEnvironments.GetMonthlyReportPath(DatabaseEnvironment.HK) == _hkMonthlyReportPath);

            //PH
            Assert.IsTrue(DatabaseEnvironments.GetNetCRMConnection(DatabaseEnvironment.PH) == _phNetcrmComm);
            Assert.IsTrue(DatabaseEnvironments.GetExactConnection(DatabaseEnvironment.PH) == _phExactComm);
            Assert.IsTrue(DatabaseEnvironments.GetMonthlyReportPath(DatabaseEnvironment.PH) == _phMonthlyReportPath);
        }

        [TearDown]
        public void DatabaseEnvironmentsTearDown()
        {
        }
    }
    #endregion
}