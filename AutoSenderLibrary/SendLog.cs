using System;
using System.Collections.Generic;
using System.Text;
using System.Data; // for SQL
using System.Data.SqlClient;


namespace AutoSenderLibrary
{
    public class SendLog
    {
        // Connection
        private static string _connection = "user id=dev;Pooling=false;Data source=NETCRMAU;Initial catalog=NETCRM;"; // default
        private static readonly string _connectionAU = "user id=dev;Pooling=false;Data source=NETCRMAU;Initial catalog=NETCRM;";
        private static readonly string _connectionHK = "user id=dev;Pooling=false;Data source=LIVMACOLA;Initial catalog=NETCRM;";
        private static readonly string _connectionPH = "user id=dev;Pooling=false;Data source=EXACTPH;Initial catalog=NETCRM;";

        // Query
        private static readonly string _query =
            @"
                select h.sendId,h.exact_no,h.[month], h.sendType, h.sendAddress, h.failed, h.internalerror, h.reminder, h.netcrm_activity, h.lastprinted as timestamp,
                c.cus_no, c.cus_type_cd as affinity, c.cus_name as company
                ,rl.resultdescription,rl.received,rl.processed,rl.LogUpdated,rl.AdditionalInformation
                from tblAutomaticStatementHistory h
                inner join arcusfil_sql c
                    on h.exact_no = c.exact_no
				left join tblAutomaticStatementResultLog rl
					on h.sendid = rl.sendid and h.sendaddress = rl.sendaddress
                where (h.sendid = @sendId or (@sendId = -1 and @sendAddress is not null and @sendAddress <> '') )
                and   (h.exact_no = @exactNo or @exactNo is null or @exactNo = '')
                and   (h.[month] = @month or @month is null  or @month = '')
                and   (h.sendType = @sendType or @sendType is null or @sendType = '')
                and   (h.sendAddress = @sendAddress or ((@sendAddress is null or @sendAddress = '') and @sendId <> -1))
                and   (h.failed = @failed or @failed = -1)
                and   (h.internalerror = @internalError or @internalError = -1)
                and   (h.reminder = @reminder or (reminder >= 1 and @reminder = 1) or @reminder = -1)
            ";

        /// <summary>Get Sendlog entry Based on SendID</summary>
        /// <param name="sendId">Send ID  of entry</param>
        /// <returns>Returns sendlog entry corresponding to that sendlog id, if none exists, returns null</returns>
        public static SendLogEntry GetFromSendId(string sendId)
        {
            return GetFromSendId(HexToInt(sendId));
        }

        /// <summary>Get Sendlog entry Based on SendID</summary>
        /// <param name="sendId">Send ID  of entry</param>
        /// <param name="sendId">Database to use</param>
        /// <returns>Returns sendlog entry corresponding to that sendlog id, if none exists, returns null</returns>
        public static SendLogEntry GetFromSendId(string sendId, string dataBase)
        {
            return GetFromSendId(HexToInt(sendId), dataBase);
        }

        /// <summary>Get Sendlog entry Based on SendID</summary>
        /// <param name="sendId">Send ID  of entry</param>
        /// <returns>Returns sendlog entry corresponding to that sendlog id, if none exists, returns null</returns>
        public static SendLogEntry GetFromSendId(int sendId)
        {
            return GetFromSendId(sendId, "AU");
        }

        /// <summary>Get Sendlog entry Based on SendID</summary>
        /// <param name="sendId">Send ID  of entry</param>
        /// <param name="DataBase">Database to use</param>
        /// <returns>Returns sendlog entry corresponding to that sendlog id, if none exists, returns null</returns>
        public static SendLogEntry GetFromSendId(int sendId, string dataBase)
        {
            SendLogEntry[] entries = GetLog(sendId, "", "","", "", "", "", "", -1, -1, -1, dataBase);

            if (entries.Length > 0)
                return entries[0];
            else
                return null;
        }

                /// <summary>
        /// Get list of sends according with following search parameters
        /// </summary>
        /// <param name="sendId">SendId in integer format</param>
        /// <param name="exactNo">Exact No of sender</param>
        /// <param name="month">send month in YYYYMM format</param>
        /// <param name="sendType">Type of send. leave blank or null if not important</param>
        /// <param name="email">Email/fax address sent to, leave blank or null if not important</param>
        /// <param name="failed">1 for sends that failed, 0 if not, -1 if both wanted</param>
        /// <param name="internalerror">1 for sends with internal errors, 0 if not, -1 if both wanted</param>
        /// <param name="reminder">1 if reminder, 0 if monthlystatement, -1 if both desired</param>
        /// <returns>Sendlog results</returns>
        private static SendLogEntry[] GetLog(int sendId, string exactNo, string cusNo, string companyName, string affinity, string month,
            string sendType, string email, int failed, int internalerror, int reminder)
        {
            return GetLog(sendId, exactNo, cusNo, companyName, affinity, month, sendType, email, failed, internalerror, reminder, "AU");
        }

        /// <summary>
        /// Get list of sends according with following search parameters
        /// </summary>
        /// <param name="sendId">SendId in integer format</param>
        /// <param name="exactNo">Exact No of sender</param>
        /// <param name="month">send month in YYYYMM format</param>
        /// <param name="sendType">Type of send. leave blank or null if not important</param>
        /// <param name="email">Email/fax address sent to, leave blank or null if not important</param>
        /// <param name="failed">1 for sends that failed, 0 if not, -1 if both wanted</param>
        /// <param name="internalerror">1 for sends with internal errors, 0 if not, -1 if both wanted</param>
        /// <param name="reminder">1 if reminder, 0 if monthlystatement, -1 if both desired</param>
        /// <param name="DataBase">Database to use</param>
        /// <returns>Sendlog results</returns>
        private static SendLogEntry[] GetLog(int sendId, string exactNo, string cusNo, string companyName, string affinity, string month, 
            string sendType, string email, int failed, int internalerror, int reminder, string dataBase)
        {
            setConnection(dataBase);

            Dictionary<int, SendLogEntry> sendLogs = new Dictionary<int, SendLogEntry>();
            DataTable dbResult = null;

            // Parameters
            SqlParameter sendIdParam = new SqlParameter("sendId",SqlDbType.Int);
            SqlParameter exactNoParam = new SqlParameter("exactNo", SqlDbType.VarChar);
            SqlParameter monthParam = new SqlParameter("month", SqlDbType.VarChar);
            SqlParameter sendTypeParam = new SqlParameter("sendType", SqlDbType.VarChar);
            SqlParameter emailParam = new SqlParameter("sendAddress", SqlDbType.VarChar);
            SqlParameter failedParam = new SqlParameter("failed", SqlDbType.Int);
            SqlParameter internalErrorParam = new SqlParameter("internalError", SqlDbType.Int);
            SqlParameter reminderParam = new SqlParameter("reminder", SqlDbType.Int);
            
            sendIdParam.Value = sendId;
            exactNoParam.Value = exactNo;
            monthParam.Value = month;
            sendTypeParam.Value = sendType;
            emailParam.Value = email;
            failedParam.Value = (failed >= 0 && failed <= 1) ? failed : -1;
            internalErrorParam.Value = (internalerror >= 0 && internalerror <= 1) ? internalerror : -1;
            reminderParam.Value = (reminder >= 0 && reminder <= 1) ? reminder : -1;

            SqlParameter[] searchParameters = new SqlParameter[] { sendIdParam, exactNoParam, monthParam, sendTypeParam, emailParam,  
                                                        failedParam, internalErrorParam, reminderParam };

            // Create SQL command
            SqlCommand comm = new SqlCommand(_query);
            comm.Connection = new SqlConnection(_connection);
            comm.Parameters.AddRange(searchParameters);

            //select command into dataadapter
            SqlDataAdapter adapt = new SqlDataAdapter();
            adapt.SelectCommand = comm;

            // create dataset for storing tables
            DataSet dataset = new DataSet();

            try
            {
                // open connection
                comm.Connection.Open();

                // fill data
                adapt.Fill(dataset);

                // close connection
                comm.Connection.Close();

                // Get table values
                dbResult = dataset.Tables[0];

                // Format entries into SendLogEntry and return
                foreach (DataRow dr in dbResult.Rows)
                {
                    int sendIdEntry = int.Parse(dr["SendId"].ToString() == "" ? "-1" : dr["SendId"].ToString());
                    string sendAddressEntry = dr["sendAddress"].ToString();
                    bool failedEntry = int.Parse(dr["failed"].ToString()) == 1 ? true : false;
                    bool internalErrorEntry = int.Parse(dr["internalerror"].ToString()) == 1 ? true : false;

                    // resultlog
                    string resultDescriptionEntry = dr["ResultDescription"].ToString();
                    SendLogAddressResultLog resultlog = null;
                    if (resultDescriptionEntry != "")
                    {
                        DateTime receivedDate = DateTime.Parse(dr["Received"].ToString());
                        DateTime processedDate = DateTime.Parse(dr["Processed"].ToString());
                        int logUpdated = int.Parse(dr["LogUpdated"].ToString() == "" ? "-1" : dr["LogUpdated"].ToString());
                        string AdditionalInformation = dr["AdditionalInformation"].ToString();

                        resultlog = new SendLogAddressResultLog(sendIdEntry, sendAddressEntry, resultDescriptionEntry, receivedDate, processedDate, logUpdated, AdditionalInformation );
                    }

                    // check if sendlog exists
                    SendLogEntry sendlog;
                    if (sendLogs.TryGetValue(sendIdEntry, out sendlog))
                    {
                        sendlog.AddSendAddress(sendAddressEntry, failedEntry, internalErrorEntry, resultlog);
                    }
                    else
                    {
                        string exactNoEntry = dr["exact_no"].ToString();
                        string monthEntry = dr["month"].ToString();
                        ActionType sendTypeEntry = ActionTypeList.getActionTypeFromString(dr["SendType"].ToString());
                        string netcrmactivityEntry = dr["netcrm_activity"].ToString();
                        DateTime timeStampEntry; 
                        DateTime.TryParse(dr["TimeStamp"].ToString(), out timeStampEntry);
                        string cusNoEntry = dr["cus_no"].ToString();
                        string companyNameEntry = dr["company"].ToString();
                        string affinityEntry = dr["affinity"].ToString();

                        sendlog = new SendLogEntry(sendIdEntry, exactNoEntry,cusNoEntry, companyNameEntry, affinityEntry, monthEntry, sendTypeEntry, timeStampEntry, 
                                                        netcrmactivityEntry, sendAddressEntry, failedEntry, internalErrorEntry, resultlog);

                        sendLogs.Add(sendIdEntry ,sendlog);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            SendLogEntry[] logs = new SendLogEntry[sendLogs.Count];
            sendLogs.Values.CopyTo(logs, 0);

            return logs;
        }

        /// <summary>
        /// Converts Hexadecimal representation of value to int
        /// Returns -1 if error converting value
        /// </summary>
        /// <param name="hexValue"></param>
        /// <returns></returns>
        public static int HexToInt(string hexValue)
        {
            int intValue;

            try
            {
                intValue = int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
            }
            catch
            {
                // set IntValue to -1
                intValue = -1;
            }

            return intValue;
        }

        private static void setConnection(string dataBase)
        {
            switch (dataBase.ToUpper())
            {
                //case "AU":
                //    connection = connectionAU;
                //    break;
                case "PH":
                    _connection = _connectionAU;
                    break;
                case "HK":
                    _connection = _connectionHK;
                    break;
                default:
                    _connection = _connectionPH; // set to AU by default
                    break;
            }
        }
    }
}
