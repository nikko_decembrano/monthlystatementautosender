using System;
using System.Collections.Generic;
using System.Text;
using System.Data; // for SQL
using System.Data.SqlClient;

namespace AutoSenderLibrary
{
    public class EmailBody
    {
        private static readonly string sqlGet = "select * from MonthlyStatementEmails where name = @name";
        private static readonly string sqlGetList = "select name from MonthlyStatementEmails";
        private static readonly string sqlInsertOrUpdate =
            @"
            if (select count(name) from MonthlyStatementEmails where name = @name) = 0
                insert into MonthlyStatementEmails(name, subj, body, htmlbody) values (@name, @subj, @body, @htmlbody)
            else
                update MonthlyStatementEmails set name = @name, subj = @subj, body = @body, htmlbody = @htmlbody where name = @name
            ";

        private static readonly string sqlDelete = "delete from MonthlyStatementEmails where name = @name";
        //private static readonly string connection = "user id=dev;Pooling=false;Data source=NETCRMAU;Initial catalog=NETCRM;";

        private DatabaseEnvironments _dbEnvironment;

        private string name;
        private string subj;
        private string body;
        private string htmlbody;

        #region Constructors
        public EmailBody() 
            : this("","","", "", DatabaseEnvironment.DefaultEnvironment)
        {
        }

        public EmailBody(DatabaseEnvironment dataBase)
            : this("", "", "", "", dataBase)
        {
        }

        public EmailBody(string name, string subj, string body, string htmlbody)
            : this (name, subj, body, htmlbody, DatabaseEnvironment.DefaultEnvironment)
        {
        }

        public EmailBody(string name, string subj, string body, string htmlbody, DatabaseEnvironment dataBase)
        {
            this.name = name;
            this.subj = subj;
            this.body = body;
            this.htmlbody = htmlbody;
            this._dbEnvironment = new DatabaseEnvironments(dataBase);
        }
        #endregion

        #region Get/Set
        /// <summary>Information of Email</summary>
        public string Name
        {
            get
            {
                return name;
            }
            set 
            {
                name = value;
            }
        }

        /// <summary>Subject line of Email</summary>
        public string Subj
        {
            get
            {
                return subj;
            }
            set
            {
                subj = value;
            }
        }

        /// <summary>Information of Email</summary>
        public string Body
        {
            get
            {
                return body;
            }
            set
            {
                body = value;
            }
        }

        /// <summary>HTML formatted Information of Email</summary>
        public string HtmlBody
        {
            get
            {
                return htmlbody;
            }
            set
            {
                htmlbody = value;
            }
        }
        #endregion

        public bool InsertOrUpdateToDB()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("name", name);
            parameters.Add("subj", subj);
            parameters.Add("body", body);
            parameters.Add("htmlbody", htmlbody);

            return SendDataToDB(sqlInsertOrUpdate, parameters, _dbEnvironment.GetDatabaseEnvironment());
        }

        public bool DeleteFromDB()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("name", name);

            return SendDataToDB(sqlDelete, parameters, _dbEnvironment.GetDatabaseEnvironment());
        }

        /// <summary>Get EmailBody data from DB, returns null if not exists</summary>
        /// <param name="name">Name of entry</param>
        /// <returns>EmailBody data</returns>
        public static EmailBody GetEmailBodyFromDB(string name)
        {
            return GetEmailBodyFromDB(name, DatabaseEnvironment.DefaultEnvironment);
        }

        /// <summary>Get EmailBody data from DB, returns null if not exists</summary>
        /// <param name="name">Name of entry</param>
        /// <param name="environment">Database environment(AU/HK/PH)</param>
        /// <returns>EmailBody data</returns>
        public static EmailBody GetEmailBodyFromDB(string name, DatabaseEnvironment environment)
        {
            EmailBody eb = null;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("name", name);

            // Get and store data
            DataSet ds = GetDataFromDB(sqlGet, parameters, environment);
            DataTable dt = ds.Tables[0];
            
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                eb = new EmailBody(dr["name"].ToString(), dr["subj"].ToString(), dr["body"].ToString(), dr["htmlbody"].ToString());
            }

            return eb;
        }

        /// <summary>Gets list of emails stored in Database for use with monthly statements</summary>
        /// <returns>Names in list, empty list if empty</returns>
        public static string[] GetListOfEmailBodiesFromDB()
        {
            return GetListOfEmailBodiesFromDB(DatabaseEnvironment.DefaultEnvironment);
        }

        /// <summary>Gets list of emails stored in Database for use with monthly statements</summary>
        /// <param name="environment">Database environment(AU/HK/PH)</param>
        /// <returns>Names in list, empty list if empty</returns>
        public static string[] GetListOfEmailBodiesFromDB(DatabaseEnvironment environment)
        {
            List<string> emailbodies = new List<string>();

            Dictionary<string, string> parameters = new Dictionary<string, string>(); // empty parameters
            DataSet ds = GetDataFromDB(sqlGetList, parameters, environment);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                emailbodies.Add(dr["name"].ToString());
            }

            return emailbodies.ToArray();
        }

        private static bool SendDataToDB(string command, Dictionary<string, string> parameters, DatabaseEnvironment connection)
        {
            bool result = false;

            // Create connection
            SqlConnection conn = new SqlConnection(DatabaseEnvironments.GetNetCRMConnection(connection));

            // Create command
            SqlCommand com = conn.CreateCommand();
            com.CommandText = command;

            // Add parameters to command
            foreach (string parameterName in parameters.Keys)
            {
                string value = "";
                parameters.TryGetValue(parameterName, out value);
                com.Parameters.Add(new SqlParameter(parameterName, value));
            }

            try
            {
                conn.Open();
                com.ExecuteNonQuery();
                result = true;
                conn.Close();
            }
            catch
            { // Do nothing for now
            }

            return result;
        }

        /// <summary>Helper for retrieving data from DB</summary>
        /// <param name="command">Command</param>
        /// <param name="parameters">List of parameters (parameter name, value)</param>
        /// <returns>DataSet from database</returns>
        private static DataSet GetDataFromDB(string command, Dictionary<string, string> parameters, DatabaseEnvironment connection)
        {
            DataSet ds = null;

            // Create connection
            SqlConnection conn = new SqlConnection(DatabaseEnvironments.GetNetCRMConnection(connection));

            // Create command
            SqlCommand com = conn.CreateCommand();
            com.CommandText = command;

            // Add parameters to command
            foreach (string parameterName in parameters.Keys)
            {
                string value = "";
                parameters.TryGetValue(parameterName, out value);
                com.Parameters.Add(new SqlParameter(parameterName, value));
            }

            //select command into dataadapter
            SqlDataAdapter adapt = new SqlDataAdapter();
            adapt.SelectCommand = com;
            adapt.SelectCommand.CommandType = CommandType.Text;

            //Connect to Database
            try
            {
                ds = new DataSet();
                conn.Open();
                adapt.Fill(ds);
                conn.Close();
            }
            catch
            { // Do nothing for now
            }

            return ds;

        }

    }
}
