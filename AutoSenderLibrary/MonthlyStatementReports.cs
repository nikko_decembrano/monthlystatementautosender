using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    /// <summary>Modified MonthlyStatementReport with multiple customer number support</summary>
    public class MonthlyStatementReports : MonthlyStatementReport
    {
        #region Global variables and constructors
        private string[] exactnos;

        public MonthlyStatementReports(string[] customernos)
            : this(customernos, DateTime.Now)
        { 
        }

        public MonthlyStatementReports(string[] customernos, DatabaseEnvironment dataBase)
            : this(customernos, DateTime.Now, dataBase)
        {
        }

        public MonthlyStatementReports(string[] customernos, DateTime reportDate)
            : this(customernos, reportDate, DatabaseEnvironment.DefaultEnvironment)
        {
        }

        /// <summary>Generates a crystal report of Monthly Statement for specified customers on specified report date</summary>
        /// <param name="customerno">Exact numbers each each customer required</param>
        /// <param name="reportDate">Date of report</param>
        /// <param name="customerno">Database to use (AU/HK/PH)</param>
        public MonthlyStatementReports(string[] customernos, DateTime reportDate, DatabaseEnvironment dataBase)
            : base(customernos.Length == 0 ? "" : customernos[0], reportDate, dataBase)
        {
            exactnos = new string[customernos.Length];

            customernos.CopyTo(exactnos, 0);

            // buffer 11 spaces (new selection formula without left trim)
            for (int i = 0; i < exactnos.Length; i++)
            {
                exactnos[i] = exactnobuffer + exactnos[i];
            }

            cryRpt.SetParameterValue("customer code", exactnos);
        }
        #endregion

        

    }
}
