using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    public class InvoiceLine
    {
        //Globals
        private string applyto;
        private string type;
        private string docno;
        private DateTime date;
        private string currency;
        private double invoiceamount;
        private string pono;
        //private float po_balance;

        //constructor
        public InvoiceLine() : this("","","",DateTime.Now,"",0,"")
        {
        }

        public InvoiceLine(string applyto, string type, string docno, DateTime date, string currency, float invoiceamount, string pono)
        {
            this.applyto         = applyto;
            this.type            = type;
            this.docno           = docno;
            this.date            = date;
            this.currency        = currency;
            this.invoiceamount   = invoiceamount;
            this.pono            = pono;
        }

        //get & sets
        /// <summary>Invoice transaction is applied to</summary>
        public string ApplyTo
        {
            get
            {
                return applyto;
            }
            set
            {
                applyto = value;
            }
        }

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public string DocNo
        {
            get
            {
                return docno;
            }
            set
            {
                docno = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        public string Currency
        {
            get
            {
                return currency;
            }
            set
            {
                currency = value;
            }
        }

        public double InvoiceAmount
        {
            get
            {
                return invoiceamount;
            }
            set
            {
                invoiceamount = value;
            }
        }

        public string PoNo
        {
            get
            {
                return pono;
            }
            set
            {
                pono = value;
            }
        }
    }
}
