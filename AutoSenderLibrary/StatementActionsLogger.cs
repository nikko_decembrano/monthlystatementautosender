using System;
using System.Collections.Generic;
using System.Text;
using System.Data; // for SQL
using System.Data.SqlClient;

namespace AutoSenderLibrary
{
    /// <summary>This class will help log information about email and faxes auto sent</summary>
    public class StatementActionsLogger
    {
        #region v4

        #region globals
        // userid
        private string userid = "";
        private static readonly string defaultuserid = "AR-Auto";
        #endregion

        #region Database access parameters
        // Connection
        private DatabaseEnvironments _dbEnvironment;

        // Commands
        // Get
        private readonly string getSendId ="select max(sendid) from tblAutomaticStatementHistory;";

        // Insertion
        private readonly static string insertLog = "insert into tblAutomaticStatementHistory (sendid,Exact_no, Month, SendType, SendAddress, LastPrinted, Failed, InternalError, Reminder, userid) ";
        private readonly static string insertLogParameters =
        @"
            select 
            case 
                when max(sendid) is null
                      then 1
                else
                      max(sendid)+1
                end, 
            @Exact_no, @Month, @SendType, @SendAddress, @LastPrinted, @Failed, @InternalError,@Reminder, @userid from tblAutomaticStatementHistory
        ";
        private readonly static string insertLogParametersRepeated = "select max(sendid), @Exact_no, @Month, @SendType, @SendAddress, @LastPrinted, @Failed, @InternalError,@Reminder, @userid from tblAutomaticStatementHistory";

        private readonly static string insertPrintLog = "values (null, @Exact_no, @Month, @SendType, @SendAddress, @LastPrinted, @Failed, @InternalError,@Reminder, @userid)";

        // Required: @cus_no, @datetime, @userid, @notewritten, @sendId, @notelookup
        private readonly static string insertNetCrmLog =
            @"
                declare @date varchar(8)
                set @date = convert(varchar, year(@datetime)) + right('0' + rtrim(month(@datetime)), 2) + right('0' + rtrim(day(@datetime)), 2)

                declare @time varchar(6)
                set @time = right('0' + rtrim(datepart(hh, @datetime)), 2) + right('0' + rtrim(datepart(Mi, @datetime)), 2) + right('0' + rtrim(datepart(Ss, @datetime)), 2)

                declare @cont_no varchar (10)
                set @cont_no = (select top 1 cont_no
                from contact 
                where debcode = @cus_no)

                insert into memo (cont_no,last_Update_date,last_update_time,last_update_user,note_text,followup_text,assign_by) 
                values (@cont_no,@date,@time,@userid, @notewritten, @notelookup, @userid)
                
                insert into act (User_id, Cont_no, Cus_no, Activity_Date,Activity_time,Activity_type,Note_address,Aftersale,send_email) 
                values (@userid,@cont_no,@cus_no,@date,@time,'ACCOUT',@@identity,0,0)

                update tblAutomaticStatementHistory
                set
                    netcrm_activity = @@identity
                where sendId = @sendId
            ";

        // Update
        // required @SendId,@SendAddress, @Failed, @internalError
        private readonly static string updateSendLog =
        @"
            update tblAutomaticStatementHistory 
            set 
                failed = @Failed, 
                internalerror = @InternalError
            where sendid = @SendId
        ";

        private readonly static string updateSendLogAddress = "and SendAddress = @SendAddress";

        // required @sendId (gets number of successful sends)
        private readonly static string sendIdSuccessCount = "select count(*) from tblAutomaticStatementHistory where sendId = @sendId and failed = 0 and internalerror = 0";

        // required @sendId
        private readonly static string getActNoteID =
        @"
            declare @activity_no numeric(18)
            declare @note_address numeric(18)

            select 
                @note_address = a.note_address,
                @activity_no = a.activity_no
            from tblAutomaticStatementHistory h
            inner join act a 
                on h.netcrm_activity = a.activity_no
            where sendid = @SendId

        ";

        // required @sendId
        private readonly static string updateNetCRMLogFailed =
        @"
            -- If referencing a note address
            if @note_address is not null and @note_address <> 0
            begin
                -- update if not already updated
                if ((select count (*) from memo where note_address = @note_address and note_text like '(Failed) %') = 0) 
                begin
                    DECLARE @ptrval binary(16)
                    SELECT @ptrval = TEXTPTR(note_text) 
                    from memo
                    where Note_Address = @note_address

                    UPDATETEXT memo.Note_text @ptrval 0 0 '(Failed) ' 
                end
            end
        ";

        private readonly static string updateNetCRMLogNonFailed =
        @"
            -- If referencing a note address
            if @note_address is not null and @note_address <> 0
            begin
                -- update if not already updated
                if ((select count (*) from memo where note_address = @note_address and note_text like '(Failed) %') >= 1) 
                begin
                    DECLARE @ptrval binary(16)
                    SELECT @ptrval = TEXTPTR(note_text) 
                    from memo
                    where Note_Address = @note_address

                    UPDATETEXT memo.Note_text @ptrval 0 9  
                end
            end
        ";

        private readonly static string deleteNetCRMLog =
        @"
            -- Delete entry from activity and memo
            delete from act where Activity_no = @activity_no
            delete from memo where Note_Address = @note_address

            -- Delete reference to activity
            update tblAutomaticStatementHistory
            set
                NetCrm_Activity = null
            where sendId = @sendId
        ";

        private readonly static string updateCustomerAccFlags2012 =
        @"
            update CustomerAccFlags
                set TAC2012_sent = @send_status
            where exact_no = @exact_no
        ";

        private readonly static string updateCustomerAccFlags2018 =
        @"
            update CustomerAccFlags
                set TAC2018_sent = @send_status
            where exact_no = @exact_no
        ";

        #endregion

        #region constructors
        /// <summary>Constructor for EmailFaxLogger using default NO-REPLY userid</summary>
        public StatementActionsLogger()
            : this(defaultuserid, DatabaseEnvironment.DefaultEnvironment)
        {
        }

        public StatementActionsLogger(DatabaseEnvironment dataBase)
            : this(defaultuserid, dataBase)
        {
        }

        /// <summary>Constructor for EmailFaxLogger</summary>
        /// <param name="userid">user id desired</param>
        public StatementActionsLogger(string userid)
            : this (userid, DatabaseEnvironment.DefaultEnvironment)
        {
        }

        /// <summary>Constructor for EmailFaxLogger</summary>
        /// <param name="userid">user id desired</param>
        /// <param name="dataBase">Database environment to use</param>
        public StatementActionsLogger(string userid, DatabaseEnvironment dataBase)
        {
            this.userid = userid;
            _dbEnvironment = new DatabaseEnvironments(dataBase);
        }
        #endregion

        #region Helper functions
        /// <summary>Helper to get correct SQL date to insert into Log</summary>
        /// <param name="date">date you want</param>
        /// <returns>correctly formatted date for insertion into tblAutomaticStatementHistory</returns>
        private string GetDbStatementDate(DateTime date)
        {
            return string.Format("{0:0000}{1:00}", date.Year, date.Month);
        }

        /// <summary>Format of the note to be inserted into Memo.Note_text</summary>
        /// <param name="reminder">0 if monthly statement, number if reminder</param>
        /// <param name="customer">customer sent to</param>
        /// <param name="firstContact">first contact to send to</param>
        /// <param name="lastContact">last contact to send to</param>
        /// <param name="sendType">Type of send</param>
        /// <param name="sendId">ID of transaction in integer format</param>
        /// <returns>formatted string of result</returns>
        private string MemoNote(int reminder, CustomerContact customer, int firstContact, int lastContact, int sendId)
        {
            string note = "";

            if (reminder > 0)
                note += "Reminder ";
            else if (reminder == 0)
                note += "Statement ";
            else if (reminder < 0)
                note += "New Terms & Conditions ";

            note += customer.getActionType().ToString() + "ed to ";

            bool entered = false;
            foreach (string contact in customer.getDetails(firstContact, lastContact))
            {
                if (!entered)
                    note += "\"";
                else
                    note += ", ";

                note += contact;
            }

            note += string.Format("\" SendID: {0}", sendId.ToString("X"));

            CustomerInvoices invoices = customer.Invoices;
            if (invoices != null)
            {
                note += string.Format(" Amount: {0:0.00}", invoices.GetBalance());
            }

            return note;
        }
        #endregion

        #region Insert Log
        private int InsertLog(CustomerContact customer, int firstContact, int lastContact, bool failed, bool internalerror, int reminder, bool addToStatementHistory, bool addToNetCRM)
        {
            // return -1 if not inserted into log
            int result = -1;

            // Create Connection
            SqlConnection conn = new SqlConnection(_dbEnvironment.GetNetCRMConnection());

            // Create command
            SqlCommand com = new SqlCommand();
            com.Connection = conn;

            // create transaction
            SqlTransaction transaction = null;

            // Access database
            try
            {
                // open connection
                conn.Open();

                // Log transaction time
                DateTime transactiontime = DateTime.Now;

                // start transaction
                transaction = conn.BeginTransaction(IsolationLevel.Serializable); // prevent inserts from other users until complete
                com.Transaction = transaction;

                bool repeated = false;
                bool entered = false;

                // Add required entries to Statement History Table if flagged true
                if (addToStatementHistory)
                {
                    // enter log details for each contact
                    foreach (string contact in customer.getDetails(firstContact, lastContact))
                    {
                        string sqlstring = "";
                        entered = true;

                        if (customer.getActionType() == ActionType.Print) // no send id version if print
                        {
                            sqlstring = insertLog + insertPrintLog;
                            result = 0;
                        }
                        else if (!repeated)
                        {
                            repeated = true;
                            sqlstring = insertLog + insertLogParameters;
                        }
                        else
                        {
                            sqlstring = insertLog + insertLogParametersRepeated;
                        }

                        com.CommandText = sqlstring;

                        // Parameters
                        com.Parameters.Clear();
                        com.Parameters.Add(new SqlParameter("@Exact_no", customer.getCustomerNumber()));
                        if (reminder == 0) // use last full month if statement
                            com.Parameters.Add(new SqlParameter("@Month", GetDbStatementDate(MonthlyStatementReport.getLatestMonthlyStatementDate())));
                        else  // use this month if reminder
                            com.Parameters.Add(new SqlParameter("@Month", GetDbStatementDate(DateTime.Now)));
                        com.Parameters.Add(new SqlParameter("@SendType", ActionTypeList.getStringFromActionType(customer.getActionType())));
                        com.Parameters.Add(new SqlParameter("@SendAddress", contact));
                        com.Parameters.Add(new SqlParameter("@LastPrinted", transactiontime));
                        com.Parameters.Add(new SqlParameter("@Failed", failed));
                        com.Parameters.Add(new SqlParameter("@InternalError", internalerror));
                        com.Parameters.Add(new SqlParameter("@Reminder", reminder));
                        com.Parameters.Add(new SqlParameter("@userid", userid));

                        com.ExecuteNonQuery();
                    }
                }
                else // unlogged
                {
                    result = 0;
                }

                // get send id number if entered into log
                if (entered && customer.getActionType() != ActionType.Print)
                {
                    com.CommandText = getSendId;
                    result = int.Parse(com.ExecuteScalar().ToString());
                }

                // Add entry to NetCRM if needed
                if (addToNetCRM)
                {
                    // Add entry to activity and memo for NetCRM
                    // Required: @cus_no, @datetime, @userid, @notewritten
                    com.CommandText = insertNetCrmLog;
                    com.Parameters.Clear();
                    com.Parameters.Add(new SqlParameter("@cus_no", customer.getCustomerNumber()));
                    com.Parameters.Add(new SqlParameter("@datetime", transactiontime));
                    com.Parameters.Add(new SqlParameter("@userid", userid));
                    com.Parameters.Add(new SqlParameter("@notewritten", MemoNote(reminder, customer, firstContact, lastContact, result)));
                    com.Parameters.Add(new SqlParameter("@sendId", result));
                    com.Parameters.Add(new SqlParameter("@notelookup", result.ToString()));

                    com.ExecuteNonQuery();
                }

                transaction.Commit();

                conn.Close();
            }
            catch //(SqlException e)
            {
                // Do nothing for now
            }
            finally
            {
                // close connection
                if (conn.State == ConnectionState.Open)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    conn.Close();

                    result = -1;
                }
            }

            return result;
        }

        /// <summary>Inserts a print log into the database</summary>
        /// <param name="customer">Customer Contact printed</param>
        /// <returns>Send ID of insertion, -1 if failed, 0 if unlogged</returns>
        public int InsertPrintLog(CustomerContact customer)
        {
            bool addToStatementHistory = true;
            bool addToNetCRM = false;

            // add print detail
            CustomerContact printcontact = new CustomerContact(customer.getCustomerNumber(), customer.getCustomerCode(), ActionType.Print, customer.getUpszone());
            printcontact.addDetail("PRINT");

            // Adds a print log to statementhistory and/or NetCRM Activity List
            return InsertLog(printcontact, 0, 1, false, false, 0, addToStatementHistory, addToNetCRM);
        }

        /// <summary>Inserts a send log into the database</summary>
        /// <param name="customer">Customer contact sent</param>
        /// <param name="firstContact">first contact sent</param>
        /// <param name="lastContact">last contact sent</param>
        /// <param name="reminder">0 if monthly statement, value if reminder</param>
        /// <returns>Send ID of insertion, -1 if failed, 0 if unlogged</returns>
        public int InsertSendLog(CustomerContact customer, int firstContact, int lastContact, int reminder)
        {
            bool addToStatementHistory = true;
            bool addToNetCRM = true;

            return InsertLog(customer, firstContact, lastContact, false, false, reminder, addToStatementHistory, addToNetCRM);
        }
        #endregion

        #region Update Customer Account Flags
        public void UpdateCustomerAccFlags(string customer, bool sent, int year)
        {
            // Create Connection
            SqlConnection conn = new SqlConnection(_dbEnvironment.GetNetCRMConnection());

            // Create command
            SqlCommand com = new SqlCommand();
            com.Connection = conn;

            // create transaction
            SqlTransaction transaction = null;

            // Access database
            try
            {
                // open connection
                conn.Open();

                // start transaction
                transaction = conn.BeginTransaction(IsolationLevel.Serializable); // prevent inserts from other users until complete
                com.Transaction = transaction;

                // start command
                switch (year)
                {
                    case 2012:
                        com.CommandText = updateCustomerAccFlags2012;
                        break;
                    case 2018:
                        com.CommandText = updateCustomerAccFlags2018;
                        break;
                }
                com.Parameters.Clear();

                // parameters @SendId,@SendAddress, @Failed, @internalError
                com.Parameters.Add(new SqlParameter("@exact_no", customer));
                com.Parameters.Add(new SqlParameter("@send_status", sent));

                com.ExecuteNonQuery();

                transaction.Commit();

                conn.Close();
            }
            catch //(SqlException e)
            {
                // Do nothing for now
            }
            finally
            {
                // close connection
                if (conn.State == ConnectionState.Open)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    conn.Close();
                }
            }
        }
        #endregion

        #region Update Log
        public void UpdateSendLog(int sendId, bool failed, bool internalerror)
        {
            UpdateSendLog(sendId, null, failed, internalerror);
        }

        public void UpdateSendLog(int sendId, string address, bool failed, bool internalerror)
        {
            // Create Connection
            SqlConnection conn = new SqlConnection(_dbEnvironment.GetNetCRMConnection());

            // Create command
            SqlCommand com = new SqlCommand();
            com.Connection = conn;

            // create transaction
            SqlTransaction transaction = null;

            // Access database
            try
            {
                // open connection
                conn.Open();

                // start transaction
                transaction = conn.BeginTransaction(IsolationLevel.Serializable); // prevent inserts from other users until complete
                com.Transaction = transaction;

                // start command
                com.CommandText = updateSendLog;
                com.Parameters.Clear();

                if (address != null)
                {
                    com.CommandText += updateSendLogAddress;
                    com.Parameters.Add(new SqlParameter("@SendAddress", address));
                }

                // parameters @SendId,@SendAddress, @Failed, @internalError
                com.Parameters.Add(new SqlParameter("@SendId", sendId));
                com.Parameters.Add(new SqlParameter("@Failed", failed));
                com.Parameters.Add(new SqlParameter("@internalError", internalerror));

                com.ExecuteNonQuery();

                // Check if any more successes
                com.CommandText = sendIdSuccessCount;
                com.Parameters.Clear();
                com.Parameters.Add(new SqlParameter("@SendId", sendId));

                int successfulSendIds = int.Parse(com.ExecuteScalar().ToString());

                // if no more successful sendids
                if (successfulSendIds == 0)
                {
                    // If Updating to failed or internal error
                    if (failed)
                    {
                        com.CommandText = getActNoteID + updateNetCRMLogFailed;

                        com.Parameters.Clear();
                        com.Parameters.Add(new SqlParameter("@SendId", sendId));

                        com.ExecuteNonQuery();
                    }
                    else if (internalerror)
                    {
                        com.CommandText = getActNoteID + updateNetCRMLogFailed;

                        com.Parameters.Clear();
                        com.Parameters.Add(new SqlParameter("@SendId", sendId));

                        com.ExecuteNonQuery();
                    }
                }
                else 
                {
                    com.CommandText = getActNoteID + updateNetCRMLogNonFailed;

                    com.Parameters.Clear();
                    com.Parameters.Add(new SqlParameter("@SendId", sendId));

                    com.ExecuteNonQuery();
                }

                transaction.Commit();

                conn.Close();
            }
            catch //(SqlException e)
            {
                // Do nothing for now
            }
            finally
            {
                // close connection
                if (conn.State == ConnectionState.Open)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    conn.Close();
                }
            }
        }
        #endregion
        #endregion

        #region old code 2 (pre v4)
        /*
        #region Database access parameters and Globals
        // SQL
        // test table: tblAutomaticStatementHistory_IdentityTest
        // Actual table tblAutomaticStatementHistory
        private readonly string connection = "user id=dev;Pooling=false;Data source=NETCRMAU;Initial catalog=NETCRM;";
        private readonly string getSendId =
        @"
            select max(sendid) from tblAutomaticStatementHistory;
        ";
        private readonly static string insertLog = "insert into tblAutomaticStatementHistory (sendid,Exact_no, Month, SendType, SendAddress, LastPrinted, Failed, InternalError, Reminder) ";
        private readonly static string insertLogParameters =
        //select max(sendid)+1
        @"
            select 
            case 
                when max(sendid) is null
                      then 1
                else
                      max(sendid)+1
                end, 
            '@Exact_no', @Month, '@SendType', '@SendAddress', @LastPrinted, @Failed, @InternalError,@Reminder from tblAutomaticStatementHistory
        ";
        private readonly static string insertLogParametersRepeated = "select max(sendid), '@Exact_no', @Month, '@SendType', '@SendAddress', @LastPrinted, @Failed, @InternalError,@Reminder from tblAutomaticStatementHistory";
        private readonly static string insertPrintLogParameters = "select null, '@Exact_no', @Month, '@SendType', '@SendAddress', @LastPrinted, @Failed, @InternalError,@Reminder";
        private readonly static string today = "getdate()";
        private readonly static string updateLog = 
        @"
            update tblAutomaticStatementHistory 
            set 
                failed = @Failed, 
                internalerror = @InternalError
            where sendid = @SendId 
        ";
        private readonly static string updateLogSendDetail = " and SendAddress = '@SendAddress'";

        // Globals
        SqlConnection conn;
        #endregion

        /// <summary>Constructor for EmailFaxLogger</summary>
        public StatementActionsLogger()
        {
            //connection parameters
            conn = new SqlConnection(connection);
        }

        private string SingleToDoubleQuote(string contact)
        {
            return contact.Replace("'", "''");
        }

        /// <summary>Helper to get correct SQL date to insert into Log</summary>
        /// <param name="date">date you want</param>
        /// <returns>correctly formatted date for insertion into tblAutomaticStatementHistory</returns>
        private string GetDbStatementDate(DateTime date)
        {
            return string.Format("{0:0000}{1:00}", date.Year, date.Month);
        }

        /// <summary>Updates all entries of a certain send id into the Automatic statements history log</summary>
        /// <param name="sendId">Send Id of Entry</param>
        /// <param name="failed">Failed flag</param>
        /// <param name="internalerror">Internal Error flag</param>
        /// <returns>number of rows affected</returns>
        public int UpdateLog(int sendId, bool failed, bool internalerror)
        {
            return UpdateLog(sendId, null, failed, internalerror);
        }

        /// <summary>Updates an entry into the Automatic statements history log</summary>
        /// <param name="sendId">Send Id of Entry</param>
        /// <param name="sendAddress">Send Address of Entry</param>
        /// <param name="failed">Failed flag</param>
        /// <param name="internalerror">Internal Error flag</param>
        /// <returns>number of rows affected</returns>
        public int UpdateLog(int sendId, string sendAddress, bool failed, bool internalerror)
        {
            int rowsUpdated = 0;

            string failedsql = getFailedSqlString(failed);
            string internalerrorsql = getInternalErrorSqlString(internalerror);

            // Access database
            try
            {
                // Create command
                SqlCommand com = new SqlCommand();
                com.Connection = conn;
                com.CommandText = updateLog;
                com.CommandText = com.CommandText.Replace("@Failed", failedsql);
                com.CommandText = com.CommandText.Replace("@InternalError", internalerrorsql);
                com.CommandText = com.CommandText.Replace("@SendId", sendId.ToString());
                // also check send address if string is not empty
                if (!string.IsNullOrEmpty(sendAddress))
                {
                    com.CommandText += updateLogSendDetail;
                    com.CommandText = com.CommandText.Replace("@SendAddress", SingleToDoubleQuote(sendAddress));
                }

                // open connection
                conn.Open();

                // execute update
                rowsUpdated = com.ExecuteNonQuery();

                conn.Close();
            }
            catch //(SqlException e)
            {
                // Do nothing for now
            }
            finally
            {
                // close connection
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return rowsUpdated;
        }

        public bool InsertPrintLog(CustomerContact customer, bool failed, bool internalerror)
        {
            bool inserted = false;

            string failedsql = getFailedSqlString(failed);
            string internalerrorsql = getInternalErrorSqlString(internalerror);

            // Access database
            try
            {
                // Create command
                SqlCommand com = new SqlCommand();
                com.Connection = conn;
                com.CommandText = insertLog + insertPrintLogParameters;
                com.CommandText = com.CommandText.Replace("@Exact_no", customer.getCustomerNumber());
                com.CommandText = com.CommandText.Replace("@Month", GetDbStatementDate(MonthlyStatementReport.getLatestMonthlyStatementDate()));
                com.CommandText = com.CommandText.Replace("@SendType", ActionTypeList.getStringFromActionType(customer.getActionType()));
                com.CommandText = com.CommandText.Replace("@SendAddress", "PRINT");
                com.CommandText = com.CommandText.Replace("@LastPrinted", today);
                com.CommandText = com.CommandText.Replace("@Failed", failedsql);
                com.CommandText = com.CommandText.Replace("@InternalError", internalerrorsql);
                com.CommandText = com.CommandText.Replace("@Reminder", "0"); // monthly statement

                // open connection
                conn.Open();

                // execute insert
                if (com.ExecuteNonQuery() == 1)
                    inserted = true;

                conn.Close();
            }
            catch //(SqlException e)
            {
                // Do nothing for now
            }
            finally
            {
                // close connection
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return inserted;
        }

        /// <summary>Inserts an entry into the Automatic statements history log</summary>
        /// <param name="customer">Customer</param>
        /// <param name="firstContact">First contact sent to</param>
        /// <param name="lastContact">Last Contact sent to</param>
        /// <param name="failed">Flag if insertion failed</param>
        /// <param name="internalerror">Flag if caused internal error</param>
        /// <returns>SendID of insertion made to database</returns>
        public int InsertLog(CustomerContact customer, int firstContact, int lastContact, bool failed, bool internalerror)
        {
            return InsertLog(customer, firstContact, lastContact, failed, internalerror, 0);
        }

        public int InsertLog(CustomerContact customer, int firstContact, int lastContact, bool failed, bool internalerror, int reminder)
        {
            // return -1 if not inserted into log
            int result = -1;

            string failedsql = getFailedSqlString(failed);
            string internalerrorsql = getInternalErrorSqlString(internalerror);

            // Create command
            SqlCommand com = new SqlCommand();
            com.Connection = conn;

            // create transaction
            SqlTransaction transaction = null;

            // Access database
            try
            {
                // open connection
                conn.Open();

                // start transaction
                transaction = conn.BeginTransaction(IsolationLevel.Serializable);
                com.Transaction = transaction;

                bool repeated = false;
                bool entered = false;

                // enter log details for each contact
                foreach (string contact in customer.getDetails(firstContact, lastContact))
                {
                    entered = true;
                    if (!repeated)
                    {
                        repeated = true;
                        com.CommandText = insertLog + insertLogParameters;
                    }
                    else
                    {
                        com.CommandText = insertLog + insertLogParametersRepeated;
                    }

                    com.CommandText = com.CommandText.Replace("@Exact_no", customer.getCustomerNumber());
                    if (reminder == 0) // use last full month if statement
                        com.CommandText = com.CommandText.Replace("@Month", GetDbStatementDate(MonthlyStatementReport.getLatestMonthlyStatementDate()));
                    else  // use this month if reminder
                        com.CommandText = com.CommandText.Replace("@Month", GetDbStatementDate(DateTime.Now));
                    com.CommandText = com.CommandText.Replace("@SendType", ActionTypeList.getStringFromActionType(customer.getActionType()));
                    com.CommandText = com.CommandText.Replace("@SendAddress", SingleToDoubleQuote(contact));
                    com.CommandText = com.CommandText.Replace("@LastPrinted", today);
                    com.CommandText = com.CommandText.Replace("@Failed", failedsql);
                    com.CommandText = com.CommandText.Replace("@InternalError", internalerrorsql);
                    com.CommandText = com.CommandText.Replace("@Reminder", reminder.ToString());

                    com.ExecuteNonQuery();

                }

                // get send id number if entered into log
                if (entered)
                {
                    com.CommandText = getSendId;
                    result = int.Parse(com.ExecuteScalar().ToString());
                }

                transaction.Commit();

                conn.Close();
            }
            catch// (SqlException e)
            {
                // Do nothing for now
            }
            finally
            {
                // close connection
                if (conn.State == ConnectionState.Open)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    conn.Close();
                }
            }

            return result;
        }

        private static string getInternalErrorSqlString(bool internalerror)
        {
            string internalerrorsql;
            // check if internal error is flagged true
            if (internalerror)
                internalerrorsql = "1";
            else
                internalerrorsql = "0";
            return internalerrorsql;
        }

        private static string getFailedSqlString(bool failed)
        {
            string failedsql;
            // check if failed is flagged true
            if (failed)
                failedsql = "1";
            else
                failedsql = "0";
            return failedsql;
        }

        */
        #endregion

        #region old code
        /*
        public int InsertPrintLog(CustomerContact customer, bool failed, bool internalerror)
        {
            return InsertPrintLog(customer.getCustomerNumber(), failed, internalerror);
        }

        public int InsertPrintLog(string exactno, bool failed, bool internalerror)
        {
            // setup insertion
            string sqlinsert = insertLog;

            // setup statement
            sqlinsert = SetupInsertionStatement(exactno, MonthlyStatementReport.getLatestMonthlyStatementDate(),
                                                            "PRINT", ActionType.Print, sqlinsert, failed, internalerror, false);

            // create command
            return InsertToDb(sqlinsert);
        }

        /// <summary>Inserts autosent customer information into autosend log</summary>
        /// <param name="customer">Customer's exact info</param>
        /// <param name="firstContact">First contact sent to</param>
        /// <param name="lastContact">Last Contact sent to</param>
        /// <param name="failed">Flag if insertion failed</param>
        /// <param name="internalerror">Flag if caused internal error</param>
        /// <returns>number of insertions made into database</returns>
        public int InsertEmailFaxLog(CustomerContact customer, int firstContact, int lastContact, bool failed, bool internalerror)
        {
            // create insert sql statements
            string customerno = customer.getCustomerNumber();

            string sqlinsert = insertLog;
            bool repeated = false;

            // insert contacts
            foreach (string contact in customer.getDetails(firstContact, lastContact))
            {
                sqlinsert = SetupInsertionStatement(customerno, MonthlyStatementReport.getLatestMonthlyStatementDate(),
                                                            contact, customer.getActionType(), sqlinsert, failed, internalerror, repeated);
                repeated = true;
            }

            // create command
            return InsertToDb(sqlinsert);
        }

        // helper to set up insertion SQL
        private string SetupInsertionStatement(string customerno, DateTime date, string contact, ActionType actiontype, string sqlinsert, bool failed, bool internalerror, bool repeated)
        {
            string failedsql, internalerrorsql;

            string sendtype = ActionTypeList.getStringFromActionType(actiontype);

            // check if failed is flagged true
            if (failed)
                failedsql = "1";
            else
                failedsql = "0";

            // check if internal error is flagged true
            if (internalerror)
                internalerrorsql = "1";
            else
                internalerrorsql = "0";

            // check if repeated sql
            if (!repeated)
                repeated = true;
            else
                sqlinsert = sqlinsert + unionall;

            sqlinsert = sqlinsert + insertLogParameters;
            sqlinsert = sqlinsert.Replace("@Exact_no", customerno);
            sqlinsert = sqlinsert.Replace("@Month", GetDbStatementDate(date));
            sqlinsert = sqlinsert.Replace("@SendType", sendtype);
            sqlinsert = sqlinsert.Replace("@SendAddress", SingleToDoubleQuote(contact));
            sqlinsert = sqlinsert.Replace("@LastPrinted", today);
            sqlinsert = sqlinsert.Replace("@Failed", failedsql);
            sqlinsert = sqlinsert.Replace("@InternalError", internalerrorsql);

            return sqlinsert;
        }

        /// <summary>Private method to do insertion into DB</summary>
        /// <param name="sqlinsert">SQL statement</param>
        /// <returns>Number of rows affected, 0 if none or error</returns>
        private int InsertToDb(string sqlinsert)
        {
            // Affected Rows
            int affectedRows = 0;

            // Create command
            SqlCommand com = new SqlCommand();
            com.CommandText = sqlinsert;
            com.Connection = conn;

            // Access database
            try
            {
                conn.Open();

                affectedRows = com.ExecuteNonQuery();
                //affectedRows = com.ExecuteScalar();

                conn.Close();
            }
            catch //(SqlException e)
            {
                // Do nothing for now
            }
            finally
            {
                // close connection
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            // return affected rows
            return affectedRows;
        }
*/
        #endregion
    }
}
