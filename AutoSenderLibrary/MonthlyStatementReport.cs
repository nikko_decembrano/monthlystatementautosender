using System;
using System.Collections.Generic;
using System.Text;
using CrystalDecisions.CrystalReports.Engine; // for crystal report (add crystal report dlls to project's list of references)
using CrystalDecisions.Shared;
using System.IO; // for saving to file

namespace AutoSenderLibrary
{

    /// <summary>Class for compiling individual monthly statements</summary>
    public class MonthlyStatementReport
    {
        #region global variables
        private string exactno;
        protected string pdffilepath;
        protected bool isSuccessfullyGenerated = false;
        protected string errorreport;
        protected string _dataBase;

        // crystal report
        protected ReportDocument cryRpt;
        protected DatabaseEnvironments _dbEnvironment;
        //protected readonly string reportPath = "N:\\CompiledReports\\statement_test\\statement_summary_individual_neg_daterange_testInvalidTable.rpt";

        // 11 spaces before exact no (new selection formula)
        protected readonly string exactnobuffer = "           ";
        //protected readonly string exactnobuffer = "";
        #endregion

        #region static methods
        /// <summary>Static method to determine monthly report date</summary>
        /// <returns>Latest Monthly Report Date</returns>
        public static DateTime getLatestMonthlyStatementDate()
        {
            // Today's date
            DateTime date = DateTime.Now;

/*          // For sending from last day of the month  
            // Last day of the month
            int lastDayOfMonth = DateTime.DaysInMonth(date.Year, date.Month);

            // If last day of the month, return date
            if (date.Day == lastDayOfMonth)
            {
                return date;
            }
            // 
            else
            {
                // get last day of last month
                return date.AddDays(-date.Day);
            }
*/
            
            // For sending from first day of the next month
            // get last day of last month
            return date.AddDays(-date.Day);
        }
        #endregion


        #region constructor
        /// <summary>Generates a crystal report of Monthly Statement for specified customer till today</summary>
        /// <param name="customerno">Exact number of customer</param>
        public MonthlyStatementReport(string customerno) : this (customerno, DateTime.Now)
        {
        }
        /// <summary>Generates a crystal report of Monthly Statement for specified customer till today</summary>
        /// <param name="customerno">Exact number of customer</param>
        /// <param name="customerno">Database to use (AU/HK/PH)</param>
        public MonthlyStatementReport(string customerno, DatabaseEnvironment dataBase)
            : this(customerno, DateTime.Now, dataBase)
        {
        }

        /// <summary>Generates a crystal report of Monthly Statement for specified customer on specified report date</summary>
        /// <param name="customerno">Exact number of customer</param>
        /// <param name="reportDate">Date of report</param>
        public MonthlyStatementReport(string customerno, DateTime reportDate)
            : this (customerno,reportDate, DatabaseEnvironment.DefaultEnvironment)
        {
        }

        /// <summary>Generates a crystal report of Monthly Statement for specified customer on specified report date</summary>
        /// <param name="customerno">Exact number of customer</param>
        /// <param name="reportDate">Date of report</param>
        public MonthlyStatementReport(string customerno, DateTime reportDate, DatabaseEnvironment dataBase)
        {
            /* 
             * This constructor will go through the process of loading the crystal report of the monthly statement
             * as well as setting up default print options for the report
             */
            _dbEnvironment = new DatabaseEnvironments(dataBase); // set report's path according to the database it is working on

            try
            {
                // set customer number
                exactno = customerno;

                // create new report document
                cryRpt = new ReportDocument();

                // Load Crystal Report
                cryRpt.Load(_dbEnvironment.GetMonthlyReportPath());

                #region selection formula test
                /*                cryRpt.RecordSelectionFormula = cryRpt.RecordSelectionFormula +
    @" and
trimleft({Liv_Vu_cicmpy.cmp_code}) = {?Customer code} and 
{Liv_Vu_cicmpy.cmp_type} = 'C' and
left(trimleft({Liv_Vu_cicmpy.cmp_code}),2) <> '60' "
+ "and toText(  {Liv_Statement.Date}, \"yyyyMMdd\")  <= '20100817'";*/
                #endregion

                // Set Parameter
                cryRpt.SetParameterValue("customer code", exactnobuffer + exactno);
                cryRpt.SetParameterValue("statement date", reportDate.Date);

                // set printer options
                cryRpt.PrintOptions.PaperSize = PaperSize.PaperA4;

                isSuccessfullyGenerated = true;
            }
            catch (Exception ex)
            {
                errorreport = ex.Message;
                isSuccessfullyGenerated = false;
            }
        }

        public void Dispose()
        {
            if (cryRpt != null)
            {
                cryRpt.Dispose();
                cryRpt = null;
            }
        }

        #endregion

        #region Successful creation checks
        /// <summary>Reports if the monthly statement report is successfully generated.</summary>
        /// <returns>True if successful</returns>
        public bool isReportSuccessfullyGenerated()
        {
            return isSuccessfullyGenerated;
        }

        /// <summary>Returns error message if report is not successfully created</summary>
        /// <returns>Error message, null if successfully created</returns>
        public string getErrorReport()
        {
            return errorreport;
        }
        #endregion

        #region get and set methods

        /// <summary>Gets report for printing to screen using CrystalDecisions.Windows.Form.CrystalReportViewer</summary>
        /// <returns>ReportDocument</returns>
        public ReportDocument getCrystalReport()
        {
            return cryRpt;
        }

        /// <summary>prints report to printer name specified in setPrinterName or Ricoh-Up by default</summary>
        /// <returns>try if printed without error</returns>
        public bool printReportToPrinter()
        {
            try
            {
                cryRpt.PrintToPrinter(1, false, 0, 0);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return false;
            }

            return true;    
        }

        /// <summary>Gets or sets Printer for Crystal report to print report to</summary>
        public string PrinterName
        {
            get
            {
                return cryRpt.PrintOptions.PrinterName;
            }
            set 
            {
                cryRpt.PrintOptions.PrinterName = value;
                cryRpt.PrintOptions.PaperSize = PaperSize.PaperA4;
            }
        }

        /// <summary>Generates a temporary PDF file for emailing or faxing if not created, return location of temporary PDF if already created</summary>
        /// <returns>file location of temporary PDF, null if not created</returns>
        public string exportToPDF()
        {
            // check if file exists
            if (pdffilepath == null || !File.Exists(pdffilepath))
            {
                // Generate temporary PDF file name using Path.GetTempFileName and store into pdffilepath
                // If file of same name already exists, try again (just in case).
                while (File.Exists(pdffilepath = Path.ChangeExtension(Path.GetTempFileName(), "pdf"))) ;

                //Set PDF export options
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                CrDiskFileDestinationOptions.DiskFileName = pdffilepath;

                ExportOptions CrExportOptions;
                CrExportOptions = cryRpt.ExportOptions;
                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile; // send to disk
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat; //PDF
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions; // save to filename set above
                    CrExportOptions.FormatOptions = CrFormatTypeOptions; // default PdfRtfWordFormatOptions
                }

                try
                {
                    // export to pdf
                    cryRpt.Export();
                }
                catch
                {
                    pdffilepath = null;
                }
            }

            return pdffilepath;
        }
        #endregion

    }

}
