using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    /// <summary>
    /// Addresses in SendLogEntry
    /// To be used inside SendLogEntry
    /// </summary>
    public class SendLogAddress
    {
        private string _sendAddress;
        private bool _failed;
        private bool _internalError;
        private List<SendLogAddressResultLog> _resultLog;

        public SendLogAddress(string sendAddress, bool failed, bool internalError)
        {
            _sendAddress = sendAddress;
            _failed = failed;
            _internalError = internalError;
            _resultLog = new List<SendLogAddressResultLog>();
        }

        public string SendAddress
        {
            get { return _sendAddress; }
            set { _sendAddress = value; }
        }

        public bool Failed
        {
            get { return _failed; }
            set { _failed = value; }
        }

        public bool InternalError
        {
            get { return _internalError; }
            set { _internalError = value; }
        }

        public void AddResultLog(SendLogAddressResultLog resultlog)
        {
            if (resultlog != null)
                _resultLog.Add(resultlog);
        }

        public void ClearResultLog()
        {
            _resultLog.Clear();
        }

        public SendLogAddressResultLog[] GetResultLogs()
        {
            return _resultLog.ToArray();
        }
    }
}
