using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    public enum EStatementFormat { PDF, EXCEL2003 };

    public class EStatementFormats
    {
        HashSet<EStatementFormat> _formatList;

        public EStatementFormats()
        {
            _formatList = new HashSet<EStatementFormat>();
        }

        public EStatementFormats(EStatementFormat format)
            :this ()
        {
            _formatList.Add(format);
        }

        public EStatementFormats(EStatementFormat[] formats)
            :this ()
        {
            if (formats != null)
            {
                foreach (EStatementFormat format in formats)
                {
                    _formatList.Add(format);
                }
            }
        }

        // Get/Sets
        public bool HasFormat(EStatementFormat format)
        {
            return _formatList.Contains(format);
        }

        public EStatementFormat[] GetFormats()
        {
            EStatementFormat[] formats = new EStatementFormat[_formatList.Count];
            _formatList.CopyTo(formats);

            return formats;
        }

        public bool AddFormat(EStatementFormat format)
        {
            return _formatList.Add(format);
        }

        public EStatementFormat[] AddFormats(EStatementFormat[] formats)
        {
            List<EStatementFormat> fails = new List<EStatementFormat>();

            foreach (EStatementFormat format in formats)
            {
                if (!_formatList.Add(format))
                    fails.Add(format);
            }

            return fails.ToArray();
        }

        public bool RemoveFormat(EStatementFormat format)
        {
            return _formatList.Remove(format);
        }

        public EStatementFormat[] RemoveFormats(EStatementFormat[] formats)
        {
            List<EStatementFormat> fails = new List<EStatementFormat>();

            foreach (EStatementFormat format in formats)
            {
                if (!_formatList.Remove(format))
                    fails.Add(format);
            }

            return fails.ToArray();
        }

        public void ClearFormats()
        {
            _formatList.Clear();
        }
    }
}
