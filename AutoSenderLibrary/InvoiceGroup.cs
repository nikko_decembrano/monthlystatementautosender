using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    /// <summary>
    /// Status of invoice group
    /// Overdue, invoices still owing, over overdue date(30 days by default)
    /// Current, invoices still owing, under overdue date(30 days by default)
    /// OverAllocatedOverdue, invoices Overpaid, over overdue date(30 days by default)
    /// OverAllocatedCurrent, invoices Overpaid, under overdue date(30 days by default)
    /// Unallocated, transactions that have not been allocated to any invoice (Apply to = 0)
    /// Misallocated, No corresponding sales invoices
    /// </summary>
    public enum InvoiceGroupStatus {Overdue,Current,OverAllocatedOverdue,OverAllocatedCurrent,Unallocated,Misallocated,None}

    public class InvoiceGroup
    {
        // globals
        private string applyto;
        private List<InvoiceLine> invoices;

        /// <summary>Constructor</summary>
        /// <param name="applyto">Sales invoice to which invoice group is appplied to</param>
        public InvoiceGroup(string applyto)
        {
            this.applyto = applyto;
            invoices = new List<InvoiceLine>();
        }

        /// <summary>Add line to invoice</summary>
        /// <param name="line">line to be added</param>
        public void AddInvoice (InvoiceLine line)
        {
            invoices.Add(line);
        }

        /// <summary>Get list of invoices</summary>
        /// <returns>All debit and credit entries of invoice</returns>
        public InvoiceLine[] GetInvoices()
        {
            return invoices.ToArray();
        }

        /// <summary>Get total amount of debits in invoice</summary>
        /// <returns>Debit total</returns>
        public double GetDebitTotal()
        {
            double debits = 0.0;

            foreach (InvoiceLine line in invoices)
            {
                if (line.InvoiceAmount > 0)
                    debits += line.InvoiceAmount;
            }

            return debits;
        }

        /// <summary>Get total amount of credits in invoice (absolute value)</summary>
        /// <returns>Credit total</returns>
        public double GetCreditTotal()
        {
            double credits = 0.0;

            foreach (InvoiceLine line in invoices)
            {
                if (line.InvoiceAmount < 0)
                    credits -= line.InvoiceAmount;
            }

            return credits;
        }

        /// <summary>Gets invoice total</summary>
        /// <returns>Total</returns>
        public double GetTotalForInvoice()
        {
            double total = 0.0;

            foreach (InvoiceLine line in invoices)
            {
                total += line.InvoiceAmount;
            }

            return total;
        }

        /// <summary>Determines if the invoice is overdue (30 days)</summary>
        /// <returns>True if overdue</returns>
        public bool IsOverdue()
        {
            // use default value of 30 days
            return IsOverXDays(30);
        }

        /// <summary>Deterimines if this invoice is over the indicated number of days</summary>
        /// <param name="days">number of days</param>
        /// <returns>true if over number of days</returns>
        public bool IsOverXDays(int days)
        {
            DateTime date = GetInvoiceDate().AddDays(days);

            // overdue if over specified days
            if (date.Date < DateTime.Now.Date)
                return true;

            return false;
        }

        /// <summary>Deterimines if this invoice has been overallocated</summary>
        /// <returns>True if allocated</returns>
        public bool IsOverAllocated()
        {
            if (GetTotalForInvoice() < 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>Determines if this is an invoice entry</summary>
        /// <returns>True if invoice entry</returns>
        public bool IsInvoiceEntry()
        {
            foreach (InvoiceLine line in invoices)
            {
                if (line.Type.ToLower() == "sales invoice")
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>Gets date of sales invoice</summary>
        /// <returns>Date of invoice (or today if not an invoice)</returns>
        public DateTime GetInvoiceDate ()
        {
            foreach (InvoiceLine line in invoices)
            {
                if (line.Type.ToLower() == "sales invoice")
                {
                    return line.Date;
                }
            }

            return DateTime.Now;
        }

        public string GetApplyTo()
        {
            return applyto;
        }

        public string GetPoNo()
        {
            return invoices[0].PoNo;
        }

        /// <summary>Checks and returns status of invoice group</summary>
        /// <returns>Status of invoice group</returns>
        public InvoiceGroupStatus StatusOfInvoice()
        {
            InvoiceGroupStatus status = InvoiceGroupStatus.None;

            // Unallocated invoice if applyto is 0
            if (applyto == "0" || applyto == "")
            {
                status = InvoiceGroupStatus.Unallocated;
            }
            // Does not have a corresponding sales invoice entry
            else if (!IsInvoiceEntry())
            {
                status = InvoiceGroupStatus.Misallocated;
            }
            // overdue invoices
            else if (IsOverdue())
            {
                if (IsOverAllocated())
                {
                    status = InvoiceGroupStatus.OverAllocatedOverdue;
                }
                else 
                {
                    status = InvoiceGroupStatus.Overdue;
                }
            }
            // Current invoices (!IsOverdue)
            else 
            {
                if (IsOverAllocated())
                {
                    status = InvoiceGroupStatus.OverAllocatedCurrent;
                }
                else
                {
                    status = InvoiceGroupStatus.Current;
                }
            }

            return status;
        }

        public bool HasStatus(InvoiceGroupStatus[] groups)
        {
            // Get Invoice Group's status
            InvoiceGroupStatus status = StatusOfInvoice();

            // Check if any of the invoice group statuses matches
            foreach (InvoiceGroupStatus group in groups)
            {
                if ( group == status )
                {
                    return true;
                }
            }
            
            return false;
        }
    }
}
