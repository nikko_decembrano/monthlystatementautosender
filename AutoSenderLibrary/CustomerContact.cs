using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    public enum AccountType { Livingstone, UniversalChoice, Other }

    public class CustomerContact
    {
        #region Global variables
        string customer = null;
        string customercode = null;
        ActionType action;
        string upszone;
        HashSet<string> details = null;
        CustomerInvoices invoices = null;
        EStatementFormats sendFormats;
        string terms;
        string termsType;
        #endregion

        #region constructors
        /// <summary>Create new list of contacts for customer</summary>
        /// <param name="customer">customer's exact no</param>
        /// <param name="customer">customer's netcrm no</param>
        /// <param name="action">action type of this contact (email,fax,etc..)</param>
        public CustomerContact(string customer, string customercode, ActionType action, string upszone)
        {
            this.customer = customer;
            this.customercode = customercode;
            this.action = action;
            this.upszone = upszone;
            details = new HashSet<string>();
            sendFormats = new EStatementFormats();
        }

        /// <summary>Create new list of contacts for customer</summary>
        /// <param name="customer">customer's exact no</param>
        /// <param name="customer">customer's netcrm no</param>
        /// <param name="action">action type of this contact (email,fax,etc..)</param>
        /// <param name="details">Array of customer contact details you will like to add</param>
        public CustomerContact(string customer, string customercode, ActionType action, string upszone, string[] details)
            : this(customer, customercode, action, upszone)
        {
            foreach (string detail in details)
            {
                this.details.Add(detail);
            }
        }

        /// <summary>Create new list of contacts for customer</summary>
        /// <param name="customer">customer's exact no</param>
        /// <param name="action">action type of this contact (email,fax,etc..)</param>
        /// <param name="details">Array of customer contact details you will like to add</param>
        public CustomerContact(string customer, string customercode, ActionType action, string upszone, string[] details, EStatementFormats formats)
            : this(customer, customercode,action, upszone, details)
        {
            sendFormats.AddFormats(formats.GetFormats());
        }
        #endregion

        #region add methods
        /// <summary>Add a contact detail to customer list</summary>
        /// <param name="detail">Email or fax detail of customer</param>
        /// <returns>true if addition is successful</returns>
        public bool addDetail(string detail)
        {
            // check if detail is a valid or empty string
            if (detail != null && detail != string.Empty)
                return details.Add(detail);
            return false;
        }

        /// <summary>Add a list of details to customer list</summary>
        /// <param name="details">Array of email or fax details of customer</param>
        public void addDetails(string[] details)
        {
            foreach (string detail in details) addDetail(detail);
        }
        #endregion

        #region get methods
        /// <summary>Get/Set customer's invoices</summary>
        public CustomerInvoices Invoices
        {
            get 
            {
                return invoices;
            }
            set 
            {
                invoices = value;
            }
        }

        /// <summary>Returns customer's Exact number</summary>
        /// <returns>Customer's Exact number in string format</returns>
        public string getCustomerNumber()
        {
            return customer;
        }

        /// <summary>Returns customer's NetCRM number</summary>
        /// <returns>Customer's NetCRM number in string format</returns>
        public string getCustomerCode()
        {
            return customercode;
        }

        /// <summary>Get list of customer details</summary>
        /// <returns>An array of customer email or fax details</returns>
        public string[] getDetails()
        {
            string[] details = new string[this.details.Count];
            this.details.CopyTo(details);

            return details;
        }

        /// <summary>Get details of contact between min (starting from 0) and max (to n-1)</summary>
        /// <param name="min">min</param>
        /// <param name="max">max</param>
        /// <returns>
        /// array of contacts, returns null if none in range.
        /// Returns less than required number of contacts if max is above (n-1).
        /// </returns>
        public string[] getDetails(int min, int max)
        {
            // set details as null first
            string[] details = null;

            // check if valid values
            if (min < 0 || max < min)
                return details;

            // set max to at most n-1 if larger than n-1
            if (max > this.details.Count - 1)
                max = this.details.Count - 1;

            // set number to copy
            int numbertocopy = max - min + 1;

            // create string array of details
            details = new string[numbertocopy];
            
            // copy details
            this.details.CopyTo(details, min, numbertocopy);

            return details;
        }

        /// <summary>Returns action type of customer detail</summary>
        /// <returns>ActionType containing information of action for current customer</returns>
        public ActionType getActionType()
        {
            return action;
        }

        /// <summary>Returns ups zone of customer</summary>
        /// <returns>ups zone of customer</returns>
        public string getUpszone()
        {
            return upszone;
        }

        /// <summary>Returns Type of customer account (Livingstone, UC)</summary>
        /// <returns>Type of customer account</returns>
        public AccountType getAccountType()
        {
            // Get first 2 digits of account
            string initial = customer.Substring(0, 2);
            AccountType accounttype;

            switch (initial)
            {
                case "10":
                    accounttype = AccountType.Livingstone;
                    break;
                case "60":
                    accounttype = AccountType.UniversalChoice;
                    break;
                default:
                    accounttype = AccountType.Other;
                    break;
            }

            return accounttype;
        }

        public EStatementFormats SendFormats
        {
            get { return sendFormats; }
            set { sendFormats = value; }
        }

        /// <summary>
        /// Get/Set Terms
        /// </summary>
        public string Terms
        {
            get { return terms; }
            set { terms = value; }
        }

        /// <summary>
        /// Get/Set Terms
        /// </summary>
        public string TermsType
        {
            get { return termsType; }
            set { termsType = value; }
        }

        /// <summary>
        /// Gets days before an invoice is overdue from customer terms
        /// </summary>
        /// <returns>days before invoice is overdue</returns>
        public int getInvoiceOverdueDay() 
        {
            int daysOverdue;
            if (!int.TryParse(terms, out daysOverdue))
                daysOverdue = 60;

            // Temp set terms type EOM to 60 days
            if (termsType.ToUpper() == "EOM")
                return 60;
            else
                return 30;
        }
        #endregion

        #region remove methods
        /// <summary>Remove a contact detail</summary>
        /// <param name="detail">contact detail to remove</param>
        /// <returns>True if remove is successful</returns>
        public bool removeDetail(string detail)
        {
            return details.Remove(detail);
        }

        /// <summary>Remove a group of contact details</summary>
        /// <param name="details">Array of contant details to be removed</param>
        public void removeDetails(string[] details)
        {
            foreach (string detail in details) removeDetail(detail);
        }

        /// <summary>Clear all contact details from customer</summary>
        public void clearDetails()
        {
            details.Clear();
        }
        #endregion

    }
}
