using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    public enum DatabaseEnvironment { AU, HK, PH, Test, DefaultEnvironment }

    public class DatabaseEnvironments
    {
        #region Environments
        //Environment
        private DatabaseEnvironment _environment;
        private string _connNetcrm;
        private string _connExact;
        private string _reportPath;

        // NetCRM environments
        private static readonly string _connNetcrmAU = "user id=dev;Pooling=false;Data source=NETCRMAU;Initial catalog=NETCRM;";
        private static readonly string _connNetcrmHK = "user id=dev;Pooling=false;Data source=LIVMACOLA;Initial catalog=NETCRM;";
        private static readonly string _connNetcrmPH = "user id=dev;Pooling=false;Data source=EXACTPH;Initial catalog=NETCRM;";

        // Exact environments
        private static readonly string _connExactAU = "user id=dev;Pooling=false;Data source=LIVEXACTAUS;Initial catalog=135;";
        private static readonly string _connExactHK = "user id=dev;Pooling=false;Data source=LIVEXACT3\\LIVEHK;Initial catalog=135;";
        private static readonly string _connExactPH = "user id=dev;Pooling=false;Data source=livexact2;Initial catalog=135;";

        // Monthly statement report path environments
        private static readonly string _reportPathAU = "N:\\CompiledReports\\statement_test\\statement_summary_individual_neg_daterange.rpt";
        private static readonly string _reportPathAU_alt = "N:\\CompiledReports\\statement_test\\statement_summary_sort_seqpage_alternate_individual_statement.rpt";
        private static readonly string _reportPathHK = "N:\\CompiledReportsHK\\statement_test\\statement_summary_individual_neg_daterange.rpt";
        private static readonly string _reportPathPH = "N:\\CompiledReportsPH\\statement_test\\statement_summary_individual_neg_daterange.rpt";
        #endregion

        #region constructors
        /// <summary>constructor</summary>
        /// <param name="environment">Database environment (AU/PH/HK)</param>
        public DatabaseEnvironments(DatabaseEnvironment environment)
        {
            SetEnvironment(environment);
        }

        public DatabaseEnvironments(string environment)
        {
            SetEnvironment(environment);
        }
        #endregion

        #region Get/sets
        public void SetEnvironment(DatabaseEnvironment environment)
        {
            _environment = environment;
            _connNetcrm = GetNetCRMConnection(_environment);
            _connExact = GetExactConnection(_environment);
            _reportPath = GetMonthlyReportPath(_environment);
        }

        public void SetEnvironment(string environment)
        {
            DatabaseEnvironment environmentToUse;
            environmentToUse = GetEnvironmentFromString(environment);

            SetEnvironment(environmentToUse);
        }

        public DatabaseEnvironment GetDatabaseEnvironment()
        {
            return _environment;
        }

        public string GetNetCRMConnection()
        {
            return _connNetcrm;
        }

        public string GetExactConnection()
        {
            return _connExact;
        }

        public string GetMonthlyReportPath()
        {
            return _reportPath;
        }
        #endregion

        #region static methods
        /// <summary>Get NetCRM connection string from type of database</summary>
        /// <param name="dataBase">Database to use</param>
        /// <returns></returns>
        public static string GetNetCRMConnection(DatabaseEnvironment dataBase)
        {
            string connNetcrm = "";

            switch (dataBase)
            {
                case DatabaseEnvironment.HK:
                    connNetcrm = _connNetcrmHK;
                    break;
                case DatabaseEnvironment.PH:
                    connNetcrm = _connNetcrmPH;
                    break;
                default: // set to Australia
                    connNetcrm = _connNetcrmAU;
                    break;
            }

            return connNetcrm;
        }

        /// <summary>Get Exact connection string from type of database</summary>
        /// <param name="dataBase">Database to use</param>
        /// <returns></returns>
        public static string GetExactConnection(DatabaseEnvironment dataBase)
        {
            string connExact = "";

            switch (dataBase)
            {
                case DatabaseEnvironment.HK:
                    connExact = _connExactHK;
                    break;
                case DatabaseEnvironment.PH:
                    connExact = _connExactPH;
                    break;
                default: // set to Australia
                    connExact = _connExactAU;
                    break;
            }

            return connExact;
        }

        /// <summary>Get report path from type of database</summary>
        /// <param name="dataBase">Database to use</param>
        /// <returns></returns>
        public static string GetMonthlyReportPath(DatabaseEnvironment dataBase)
        {
            string reportPath = "";

            switch (dataBase)
            {
                case DatabaseEnvironment.HK:
                    reportPath = _reportPathHK;
                    break;
                case DatabaseEnvironment.PH:
                    reportPath = _reportPathPH;
                    break;
                default: // set to Australia
                    reportPath = _reportPathAU_alt;
                    break;
            }

            return reportPath;
        }

        public static DatabaseEnvironment GetEnvironmentFromString(string environment)
        {
            DatabaseEnvironment environmentToUse;
            switch (environment == null ? "" : environment.ToUpper())
            {
                case "AU":
                    environmentToUse = DatabaseEnvironment.AU;
                    break;
                case "AUSTRALIA":
                    environmentToUse = DatabaseEnvironment.AU;
                    break;
                case "HK":
                    environmentToUse = DatabaseEnvironment.HK;
                    break;
                case "HONG KONG":
                    environmentToUse = DatabaseEnvironment.HK;
                    break;
                case "PH":
                    environmentToUse = DatabaseEnvironment.PH;
                    break;
                case "PHILIPPINES":
                    environmentToUse = DatabaseEnvironment.PH;
                    break;
                case "TEST":
                    environmentToUse = DatabaseEnvironment.Test;
                    break;
                default:
                    environmentToUse = DatabaseEnvironment.DefaultEnvironment;
                    break;
            }
            return environmentToUse;
        }

        /// <summary>
        /// Gets list of environments
        /// </summary>
        /// <returns></returns>
        public static DatabaseEnvironment[] GetEnvironments()
        {
            // Get full list of actions
            List<DatabaseEnvironment> environments = new List<DatabaseEnvironment>((DatabaseEnvironment[])Enum.GetValues(typeof(DatabaseEnvironment)));
            
            // Remove Default and Test environments
            environments.Remove(DatabaseEnvironment.DefaultEnvironment);
            environments.Remove(DatabaseEnvironment.Test);

            return environments.ToArray();
        }
        #endregion
    }
}
