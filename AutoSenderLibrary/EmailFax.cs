/*
 * For setting who to BCC
 */
#define BccSendLog              // Bcc to Sendlog
//#define BccQingyou            // Bcc to Qingyou
//#define BccPatrick            // Bcc to Patrick
//#define BccPeipei             // Bcc to Peipei
//#define BccKaiKai             // Bcc to Kai
//#define PlainTextFormatOnly     // Send plaintext format of email only

using System;
using System.Collections.Generic;
using System.Text;
using System.Net; // Mail
using System.Net.Mail;
using System.Net.Mime;

namespace AutoSenderLibrary
{
    public enum SendStatus {Success, Failure}

    public enum EmailType {Monthly, Reminder1, Reminder2, NewTermsAndConditions}

    public class EmailFax
    {
        #region Globals
        // Email Client
        private SmtpClient emailclient;
        private DatabaseEnvironments _dbEnvironment;

        //for faxing
        private string faxbody = "@faxmaker.com";

        //default credentials
        private static readonly string defaultdomain = "exchau";
        private static readonly string defaultuserid = "no-reply";
        private static readonly string defaultpassword = "Password1";

        //credentials
        private string domain = "";
        private string userid = "";
        private string password = "";

        //Default messagebody
        private static readonly string livingstoneemaildomain = "@livingstone.com.au";
        private static readonly string universalchoiceemaildomain = "@universalchoice.net";
        private static readonly string noreplyalias = "Please do not reply to this email";

        //messagebody
        private string emaildomain = "";
        private string emailalias = "";
        private string defaultsubj = "";
        private string subj = "";
        private string defaultbody = "";
        private string body = "";
        private string defaulthtmlbody = "";
        private string htmlbody = "";

        //savedmessagetypes
        private static readonly string livMonthly   = "LIV-Month";
        private static readonly string livReminder1 = "LIV-Reminder1";
        private static readonly string livReminder2 = "LIV-Reminder2";
        private static readonly string ucMonthly    = "UC-Month";
        private static readonly string ucReminder1  = "UC-Reminder1";
        private static readonly string ucReminder2  = "UC-Reminder2";
        private static readonly string newTermsAndConditions = "NewConditionsRequest";
        #endregion

        #region Constructors
        /// <summary>Constructor that uses default no-reply email account</summary>
        public EmailFax()
            : this(DatabaseEnvironment.DefaultEnvironment)
        {
        }

        public EmailFax(DatabaseEnvironment dataBase)
            : this(EmailFax.defaultuserid, EmailFax.defaultpassword, dataBase)
        {
            emailalias = noreplyalias;
        }

        /// <summary>Constructor requiring credentials for logging into default SMTP server exchau</summary>
        /// <param name="userid">user ID</param>
        /// <param name="password">Password</param>
        public EmailFax(string userid, string password)
            : this(userid, password, EmailFax.defaultdomain)
        {
        }

        /// <summary>Constructor requiring credentials for logging into default SMTP server exchau</summary>
        /// <param name="userid">user ID</param>
        /// <param name="password">Password</param>
        /// <param name="dataBase">Database (AU/HK/PH)</param>
        public EmailFax(string userid, string password, DatabaseEnvironment dataBase)
            : this(userid, password, EmailFax.defaultdomain, dataBase)
        { 
        }

        /// <summary>Constructor requiring credentials for loggin into SMTP server</summary>
        /// <param name="userid">user ID</param>
        /// <param name="password">Password</param>
        /// <param name="domain">SMTP Server Domain</param>
        public EmailFax(string userid, string password, string domain)
            : this(userid, password, domain, DatabaseEnvironment.DefaultEnvironment)
        {
        }

        /// <summary>Constructor requiring credentials for loggin into SMTP server</summary>
        /// <param name="userid">user ID</param>
        /// <param name="password">Password</param>
        /// <param name="domain">SMTP Server Domain</param>
        /// <param name="dataBase">Database (AU/HK/PH)</param>
        public EmailFax(string userid, string password, string domain, DatabaseEnvironment dataBase)
        {
            _dbEnvironment = new DatabaseEnvironments(dataBase);

            emailalias = userid;
            // use defaults if userid not supplied
            if (string.IsNullOrEmpty(userid))
            {
                userid   = EmailFax.defaultuserid;
                password = EmailFax.defaultpassword;
                emailalias = noreplyalias;
            }
            this.userid = userid;
            this.password = password;
            this.domain = domain;

            emailclient = new SmtpClient(domain);
        }
        #endregion

        #region get and set methods

        public string EmailAlias
        {
            get
            {
                return emailalias;
            }
            set
            {
                emailalias = value;
            }
        }

        /// <summary>Gets or sets Email's subject line</summary>
        public string Subject
        {
            get
            {
                return subj;
            }
            set
            {
                subj = value;
            }
        }

        /// <summary>Gets or sets Email's current message body</summary>
        public string MessageBody
        {
            get
            {
                return body;
            }
            set
            {
                body = value;
            }
        }

        /// <summary>Gets or sets Email's html message body</summary>
        public string HtmlMessageBody
        {
            get
            {
                return htmlbody;
            }
            set
            {
                htmlbody = value;
            }
        }

        /// <summary>Gets or sets Email's subject line</summary>
        public string DefaultSubject
        {
            get
            {
                return defaultsubj;
            }
            set
            {
                defaultsubj = value;
            }
        }

        /// <summary>Gets or sets Email's current message body</summary>
        public string DefaultMessageBody
        {
            get
            {
                return defaultbody;
            }
            set
            {
                defaultbody = value;
            }
        }

        /// <summary>Gets or sets Email's html message body</summary>
        public string DefaultHtmlMessageBody
        {
            get
            {
                return defaulthtmlbody;
            }
            set
            {
                defaulthtmlbody = value;
            }
        }


        /// <summary>Sets default email subject and body according to account type</summary>
        /// <param name="accounttype">Livingstone/UC</param>
        /// <param name="emailtype">Monthly/Reminder/NewTnC</param>
        /// <returns>true if successfully set</returns>
        public bool SetEmailBodySettings(AccountType accounttype, EmailType emailtype)
        {
            // Set email domain
            switch (accounttype)
            {
                case AccountType.UniversalChoice:
                    emaildomain = universalchoiceemaildomain;
                    break;
                default:
                    emaildomain = livingstoneemaildomain;
                    break;
            }

            string touse = "";

            switch (emailtype)
            {
                case EmailType.Monthly:
                    if (accounttype == AccountType.Livingstone)
                        touse = livMonthly;
                    else if (accounttype == AccountType.UniversalChoice)
                        touse = ucMonthly;
                    break;
                case EmailType.Reminder1:
                    if (accounttype == AccountType.Livingstone)
                        touse = livReminder1;
                    else if (accounttype == AccountType.UniversalChoice)
                        touse = ucReminder1;
                    break;
                case EmailType.Reminder2:
                    if (accounttype == AccountType.Livingstone)
                        touse = livReminder2;
                    else if (accounttype == AccountType.UniversalChoice)
                        touse = ucReminder2;
                    break;
                case EmailType.NewTermsAndConditions:
                    touse = newTermsAndConditions;
                    break;
            }

            // return if not found
            if (touse == "")
                return false;

            // set email body
            EmailBody eb = EmailBody.GetEmailBodyFromDB(touse, _dbEnvironment.GetDatabaseEnvironment());
            subj = defaultsubj = eb.Subj;
            body = defaultbody = eb.Body;
            htmlbody = defaulthtmlbody = eb.HtmlBody;

            return true;
        }

        /// <summary>Resets message body to default settings</summary>
        public void ResetMessageBody()
        {
            subj = defaultsubj;
            body = defaultbody;
            htmlbody = defaulthtmlbody;
        }

        /// <summary>Appends contact information to message body</summary>
        /// <param name="contact">Customer Contact information</param>
        /// <param name="firstContactToSend">first contact info to append</param>
        /// <param name="maxContactsToSend">maximum contact number to send</param>
        public void AppendContactInfoToMessageBody(CustomerContact customer, int firstContactToSend, int maxContactsToSend)
        {
            body += "\nAccount:[" + customer.getCustomerNumber() + "]";
            // Add fax numbers if fax
            bool repeated = false;
            string[] customerDetails = customer.getDetails(firstContactToSend, maxContactsToSend);

            foreach (string actualcontact in customerDetails)
            {
                if (repeated)
                {
                    body += ",";
                }
                else
                {
                    body += "\n" + ActionTypeList.getStringFromActionType(customer.getActionType()) + ":[";
                    repeated = true;
                }

                // add contact
                body += actualcontact;
            }
            body += "]";
        }
        #endregion

        #region Lists Email Bodies from the MonthlyStatementEmails table that is currently in use by this class
        /// <summary>Lists Email Bodies from the MonthlyStatementEmails table that is currently in use by this class</summary>
        /// <returns>string array of such class</returns>
        public static string[] EmailBodiesInUse()
        {
            List<string> list = new List<string>();
            list.Add(livMonthly);
            list.Add(livReminder1);
            list.Add(livReminder2);
            list.Add(ucMonthly);
            list.Add(ucReminder1);
            list.Add(ucReminder2);
            list.Add(newTermsAndConditions);

            return list.ToArray();
        }

        /// <summary>Checks if email body is in currently in use by this class</summary>
        /// <param name="name">Name if emailbody</param>
        /// <returns>True if in use</returns>
        public static bool IsEmailBodyInUse(string name)
        {
            foreach (string emailname in EmailFax.EmailBodiesInUse())
                if (name.Equals(emailname)) return true;

            return false;
        }
        #endregion

        #region Send email
        public SendStatus SendEmail(CustomerContact contacts, string attachmentPath, int minContact, int maxContact)
        {
            return SendEmail(contacts, new string[] { attachmentPath }, minContact, maxContact);
        }

        /// <summary>Method for sending email to desired contacts</summary>
        /// <param name="contacts">CustomerContact</param>
        /// <param name="attachmentPath">Attachments to send with email</param>
        /// <param name="maxContact">Which first contact to send to</param>
        /// <param name="maxContact">Which last contact to send to (may be less than that number if customer have less contacts than that)</param>
        /// <returns>Status of send email</returns>
        public SendStatus SendEmail(CustomerContact contacts, string[] attachmentPaths, int minContact, int maxContact)
        {
            // Set as success in the beginning
            SendStatus sendstatus = SendStatus.Success;

            try
            {
                //----------------------------------
                // set credentials
                emailclient.Credentials = new NetworkCredential(userid, password);

                // create message
                MailMessage m = new MailMessage();
                m.From = new MailAddress(userid + emaildomain, emailalias);
                m.Subject = subj;
                //m.Body = body;

                // Personalise message
                CustomerInvoices cin = contacts.Invoices;
                string hb = htmlbody;
                hb = hb.Replace("@ExactNo", cin.ExactNo);
                hb = hb.Replace("@CusNo", cin.CusNo);
                hb = hb.Replace("@Invoices", GetInvoiceTable(cin, true)); // html
                AlternateView htmlview = AlternateView.CreateAlternateViewFromString(hb, Encoding.ASCII, "text/html");

                string pb = body;
                pb = pb.Replace("@ExactNo", cin.ExactNo);
                pb = pb.Replace("@CusNo", cin.CusNo);
                pb = pb.Replace("@Invoices", GetInvoiceTable(cin, false)); // non-html
                AlternateView plainview = AlternateView.CreateAlternateViewFromString(pb, Encoding.ASCII, "text/plain");

                // Add images to htmlview
                if (contacts.getAccountType() == AccountType.Livingstone)
                {
                    LinkedResource logo = new LinkedResource("M:\\macsql\\C# Projects\\Print Monthly Statements\\Letterheads\\lh.gif");
                    logo.ContentId = "lh";
                    htmlview.LinkedResources.Add(logo);
                }
                else if (contacts.getAccountType() == AccountType.UniversalChoice)
                {
                    LinkedResource logo = new LinkedResource("M:\\macsql\\C# Projects\\Print Monthly Statements\\Letterheads\\uh.gif");
                    logo.ContentId = "uh";
                    htmlview.LinkedResources.Add(logo);
                }

                LinkedResource ccinfo = new LinkedResource("M:\\macsql\\C# Projects\\Print Monthly Statements\\Letterheads\\ccinfo.gif");
                ccinfo.ContentId = "ccinfo";
                htmlview.LinkedResources.Add(ccinfo);

                // Add views to email
#if(!PlainTextFormatOnly)
                m.AlternateViews.Add(htmlview);
#endif
                m.AlternateViews.Add(plainview);
                //----------------------------------

                #region BCC ourselves Test
                // BCC ourselves
#if (BccQingyou)
                m.Bcc.Add("qingyou_lu@livingstone.com.au");
#endif
#if (BccPatrick)
                m.Bcc.Add("patrick_bezzina@livingstone.com.au");
#endif
#if (BccPeipei)
                m.Bcc.Add("peipei_han@livingstone.com.au");
#endif
#if (BccKaiKai)
                m.Bcc.Add("kai_jiang@livingstone.com.au");
#endif
#if (BccSendLog)
                m.Bcc.Add("send-log@livingstone.com.au");
#endif
                /***Have to be an email address 
                 * which will not auto forward email 
                 * to VP email address. 
                 * Requested by Ivan
                 ***********************************/
                m.Bcc.Add("it_autoemail@livingstone.com.au");

                #endregion

                // Set Attachment
                int i = 0;
                foreach (string attachmentPath in attachmentPaths)
                {
                    if (!string.IsNullOrEmpty(attachmentPath))
                    {
                        Attachment attachment = new Attachment(attachmentPath);
                        m.Attachments.Add(attachment);
                        m.Attachments[i++].Name = "Statement" + System.IO.Path.GetExtension(attachmentPath);
                    }
                }

                // Set mail to contacts
                foreach (string contact in contacts.getDetails(minContact, maxContact))
                {
                    string mailcontact = contact;

                    // if fax contact, append @faxmaker.com
                    if (contacts.getActionType() == ActionType.Fax)
                    {
                        mailcontact += faxbody;
                    }

                    m.To.Add(new MailAddress(mailcontact));
                }

                // Send email
                emailclient.Send(m);
            }
            catch (Exception e)
            {
                sendstatus = SendStatus.Failure;
                string message = e.Message;
            }

            return sendstatus;
        }
        #endregion

        #region Send new Terms and Conditions
        public SendStatus SendNewTermsandConditions(CustomerContact contacts, string[] attachmentPaths, int minContact, int maxContact)
        {
            SendStatus sendstatus = SendStatus.Success;

            try
            {
                // set credentials
                emailclient.Credentials = new NetworkCredential(userid, password);

                #region plaintext only
                /*
                string mailBody = body;

                mailBody = mailBody.Replace("@ExactNo", contacts.getCustomerNumber());
                mailBody = mailBody.Replace("@CusNo", contacts.getCustomerCode());

                // create message
                MailMessage m = new MailMessage();
                m.From = new MailAddress(userid + emaildomain, emailalias);
                m.Subject = subj;
                m.Body = mailBody;
 
                */
                #endregion

                // Create HTML Body
                // create message
                MailMessage m = new MailMessage();
                m.From = new MailAddress(userid + emaildomain, emailalias);
                m.Subject = subj;
                m.IsBodyHtml = true;

                string hb = htmlbody;
                string letterhead  = @"<tr>
<td>
<img src = 'cid:lh' width = 700 alt = ""Livingstone International Pty Ltd."">
<br/><br/>
</td>
</tr>";
                hb = contacts.getActionType() == ActionType.Email ? hb.Replace("@Letterhead", letterhead) : hb.Replace("@Letterhead", "");
                hb = hb.Replace("@ExactNo", contacts.getCustomerNumber());
                hb = hb.Replace("@CusNo", contacts.getCustomerCode());
                AlternateView htmlview = AlternateView.CreateAlternateViewFromString(hb, Encoding.ASCII, "text/html");

                // Create Plaintext body
                string pb = body;
                pb = pb.Replace("@ExactNo", contacts.getCustomerNumber());
                pb = pb.Replace("@CusNo", contacts.getCustomerCode());
                AlternateView plainview = AlternateView.CreateAlternateViewFromString(pb, Encoding.ASCII, "text/plain");

                // Add images to htmlview
                if (contacts.getAccountType() == AccountType.Livingstone)
                {
                    LinkedResource logo = new LinkedResource(@"\\exactdoc\macapps\macsql\C# Projects\Print Monthly Statements\Letterheads\lh-20190226-b.png");
                    logo.ContentId = "lh";
                    logo.TransferEncoding = TransferEncoding.Base64;
                    logo.ContentType.Name = logo.ContentId;
                    logo.ContentLink = new Uri("cid:" + logo);
                    htmlview.LinkedResources.Add(logo);
                }
                else if (contacts.getAccountType() == AccountType.UniversalChoice)
                {
                    LinkedResource logo = new LinkedResource(@"\\exactdoc\macapps\macsql\C# Projects\Print Monthly Statements\Letterheads\uh-20190226-b.png");
                    logo.ContentId = "uh";
                    logo.TransferEncoding = TransferEncoding.Base64;
                    logo.ContentType.Name = logo.ContentId;
                    logo.ContentLink = new Uri("cid:" + logo);
                    htmlview.LinkedResources.Add(logo);
                }

                // Add views to email
                
#if(!PlainTextFormatOnly)
                m.AlternateViews.Add(htmlview);
                m.Body = hb;
#endif
                m.AlternateViews.Add(plainview);

                #region BCC ourselves Test
                m.Bcc.Add("it_autoemail@livingstone.com.au");
                // BCC ourselves
#if (BccQingyou)
                m.Bcc.Add("qingyou_lu@livingstone.com.au");
#endif
#if (BccPatrick)
                m.Bcc.Add("patrick_bezzina@livingstone.com.au");
#endif
#if (BccPeipei)
                m.Bcc.Add("peipei_han@livingstone.com.au");
#endif
#if (BccKaiKai)
                m.Bcc.Add("kai_jiang@livingstone.com.au");
#endif
#if (BccSendLog)
                m.Bcc.Add("send-log@livingstone.com.au");
#endif
                #endregion

                // Set Attachment
                int i = 0;
                foreach (string attachmentPath in attachmentPaths)
                {
                    if (!string.IsNullOrEmpty(attachmentPath))
                    {
                        Attachment attachment = new Attachment(attachmentPath);
                        m.Attachments.Add(attachment);
                        m.Attachments[i++].Name = "TermsAndConditions" + System.IO.Path.GetExtension(attachmentPath);
                    }
                }

                // Set mail to contacts
                foreach (string contact in contacts.getDetails(minContact, maxContact))
                {
                    string mailcontact = contact;

                    // if fax contact, append @faxmaker.com
                    if (contacts.getActionType() == ActionType.Fax)
                    {
                        mailcontact += faxbody;
                    }

                    m.To.Add(new MailAddress(mailcontact));
                }

                // Send email
                emailclient.Send(m);
            }
            catch (Exception e)
            {
                sendstatus = SendStatus.Failure;
                string message = e.Message;
            }

            return sendstatus;
        }
        #endregion

        #region SendReminder
        public SendStatus SendReminder(CustomerContact contacts, int minContact, int maxContact)
        {
            return SendReminder(contacts, null, minContact, maxContact);
        }

        public SendStatus SendReminder(CustomerContact contacts, string attachmentPath, int minContact, int maxContact)
        {
            // Set sendstatus
            SendStatus sendstatus = SendStatus.Success;

            try
            {
                // set credentials
                emailclient.Credentials = new NetworkCredential(userid, password);

                // create message
                MailMessage m = new MailMessage();
                m.From = new MailAddress(userid + emaildomain, emailalias);
                m.Subject = subj;
                //m.Body = body;

                // Personalise message
                CustomerInvoices cin = contacts.Invoices;
                string hb = htmlbody;
                hb = hb.Replace("@ExactNo", cin.ExactNo);
                hb = hb.Replace("@CusNo", cin.CusNo);
                hb = hb.Replace("@Invoices", GetInvoiceTable(cin, true)); // html
                AlternateView htmlview = AlternateView.CreateAlternateViewFromString(hb, Encoding.ASCII, "text/html");

                string pb = body;
                pb = pb.Replace("@ExactNo", cin.ExactNo);
                pb = pb.Replace("@CusNo", cin.CusNo);
                pb = pb.Replace("@Invoices", GetInvoiceTable(cin, false)); // non-html
                AlternateView plainview = AlternateView.CreateAlternateViewFromString(pb, Encoding.ASCII, "text/plain");

                // Add images to htmlview
                LinkedResource logo = new LinkedResource("M:\\macsql\\C# Projects\\Print Monthly Statements\\Letterheads\\lh.gif");
                logo.ContentId = "lh";
                htmlview.LinkedResources.Add(logo);

                LinkedResource ccinfo = new LinkedResource("M:\\macsql\\C# Projects\\Print Monthly Statements\\Letterheads\\ccinfo.gif");
                ccinfo.ContentId = "ccinfo";
                htmlview.LinkedResources.Add(ccinfo);

                // Add views to email
#if(!PlainTextFormatOnly)
                m.AlternateViews.Add(htmlview);
#endif
                m.AlternateViews.Add(plainview);

                #region BCC ourselves Test
                // BCC ourselves
#if (BccQingyou)
                m.Bcc.Add("qingyou_lu@livingstone.com.au");
#endif
#if (BccPatrick)
                m.Bcc.Add("patrick_bezzina@livingstone.com.au");
#endif
#if (BccPeipei)
                m.Bcc.Add("peipei_han@livingstone.com.au");
#endif
#if (BccKaiKai)
                m.Bcc.Add("kai_jiang@livingstone.com.au");
#endif
#if (BccSendLog)
                m.Bcc.Add("send-log@livingstone.com.au");
#endif

                m.Bcc.Add("danan_thilakanathan@livingstone.com.au");

                #endregion

                // Set Attachment
                if (!string.IsNullOrEmpty(attachmentPath))
                {
                    Attachment attachment = new Attachment(attachmentPath);
                    m.Attachments.Add(attachment);
                    m.Attachments[0].Name = "Statement.pdf";
                }

                // Set mail to contacts
                foreach (string contact in contacts.getDetails(minContact, maxContact))
                {
                    string mailcontact = contact;

                    // if fax contact, append @faxmaker.com
                    if (contacts.getActionType() == ActionType.Fax)
                    {
                        mailcontact += faxbody;
                    }

                    m.To.Add(new MailAddress(mailcontact));
                }

                // Send email
                emailclient.Send(m);
            }
            catch (Exception e)
            {
                sendstatus = SendStatus.Failure;
                string message = e.Message;
            }


            // not implemented yet
            return sendstatus;
        }

        /// <summary>Private method for creating invoice table</summary>
        /// <param name="cin">Customer's invoices</param>
        /// <param name="useHTML">Set to true if HTML, false if not</param>
        /// <returns>String containing formatted invoice tables</returns>
        private string GetInvoiceTable(CustomerInvoices cin, bool useHTML)
        {
            string s = "";

            if (!useHTML)
            {
                // Non HTML formatted code                
                s += "Date        Details                 Numbers     Cust Po No                     Amount\n";
                foreach (InvoiceGroup invoice in cin.GetOverdueUnallocatedMisallocatedInvoiceGroups())
                {
                    foreach (InvoiceLine line in invoice.GetInvoices())
                    {
                        s += string.Format("{0,-12:dd/MM/yyyy}{1,-24}{2,-12}{3,-25}{4,12:0.00}\n"
                        , line.Date, line.Type, line.ApplyTo, line.PoNo, line.InvoiceAmount);
                    }
                }
                s += string.Format("            Total        {0,60:0.00}\n", cin.GetOverdueUnallocatedMisallocatedBalance());
            }
            else
            {
                // HTML formatted table
                s += "<td align=\"center\">";

                s += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:600px;border-color:black;border-width:1px;\">";
                s += "<tr>";
                s += "<td style=\"border-color:black\">&nbsp;Date";
                s += "</td>";
                s += "<td style=\"border-color:black\">&nbsp;Details";
                s += "</td>";
                s += "<td style=\"border-color:black\">&nbsp;Apply To";
                s += "</td>";
                s += "<td style=\"border-color:black\">&nbsp;Doc Number";
                s += "</td>";
                s += "<td style=\"border-color:black\">&nbsp;Cust PO No";
                s += "</td>";
                s += "<td style=\"border-color:black\" align=\"right\">Amount&nbsp";
                s += "</td>";
                s += "</tr>";

                foreach (InvoiceGroup invoice in cin.GetOverdueUnallocatedMisallocatedInvoiceGroups())
                {
                    foreach (InvoiceLine line in invoice.GetInvoices())
                    {
                        s += "<tr>";
                        s += string.Format("<td style=\"border-color:black\"><span style=\"color:black\">&nbsp;{0:dd/MM/yyyy}</span>", line.Date);
                        s += "</td>";
                        s += string.Format("<td style=\"border-color:black\"><span style=\"color:black\">&nbsp;{0}</span>", line.Type);
                        s += "</td>";
                        s += string.Format("<td style=\"border-color:black\"><span style=\"color:black\">&nbsp;{0}</span>", line.ApplyTo);
                        s += "</td>";
                        s += string.Format("<td style=\"border-color:black\"><span style=\"color:black\">&nbsp;{0}</span>", line.DocNo);
                        s += "</td>";
                        s += string.Format("<td style=\"border-color:black\"><span style=\"color:black\">&nbsp;{0}</span>", line.PoNo);
                        s += "</td>";
                        s += string.Format("<td style=\"border-color:black\" align=\"right\"><span style=\"color:black\">{0:0.00}&nbsp;</span>", line.InvoiceAmount);
                        s += "</td>";
                        s += "</tr>";
                    }
                }

                s += "<tr>";
                s += "<td style=\"border-color:black\">&nbsp;";
                s += "</td>";
                s += "<td style=\"border-color:black\">&nbsp;";
                s += "</td>";
                s += "<td style=\"border-color:black\">&nbsp;";
                s += "</td>";
                s += "<td style=\"border-color:black\">&nbsp;Total";
                s += "</td>";
                s += string.Format("<td style=\"border-color:black\" align=\"right\"><span style=\"color:black\">{0:0.00}&nbsp;</span>", cin.GetOverdueUnallocatedMisallocatedBalance());
                s += "</td>";
                s += "</tr>";

                s += "</table>";
                s += "<br/>";
                s += "</td>";
            }

            return s;
        }
        #endregion

    }
}
