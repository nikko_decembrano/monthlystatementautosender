/*
 * CONDITIONALS (For debugging)
 * JAN2011 will check for statements sent in tblAutomaticStatementHisory which are marked month 201101
 *               First statements sent are in 201102, 201101 and earlier are used only for debugging
 * 
 * FULLLIST will ignore list of customers already sent and extract the full list (Debugging only)
 * 
 * Please also set System time to somewhere around Feburary 2011
 * Will be ignored in release mode
 */
//#define JAN2011
//#define FULLLIST

using System;
using System.Collections.Generic;
using System.Text;
using System.Data; // for SQL
using System.Data.SqlClient;
using System.Collections; // for collections
using System.Diagnostics; // for debugging

namespace AutoSenderLibrary
{

    public enum AutoSendType { MonthlyStatement, Reminder, NewTermsAndConditions2012, NewTermsAndConditions2018 }

    /// <summary>
    /// This class gets and stores the list of customers and their contacts
    /// from the database.
    /// Makes used of stored procedure spCustomerForAutoSend
    /// </summary>
    public class CustomerContactList
    {
        #region Global variables
        DataTable _debug_dt = null; // for debugging
        private DatabaseEnvironments _dbConnection;

        public static readonly AutoSendType defaultSendType = AutoSendType.MonthlyStatement;
        public static readonly int defaultReminderDay = 0;
        private static readonly int monthlyStatementVal = 0;
        public static readonly int defaultOverdueReminderDay = 30;

        // Contact tables
        Hashtable emailList;
        Hashtable faxList;
        Hashtable printList;
        Hashtable reviewList;
        Hashtable unknownList;
        Hashtable sentList;

        // indicator to see if data is successfully retrieved from server
        bool _retrievalsuccess = false;

        // For reminders
        OutstandingInvoiceList outstandinginvoices;
        int _reminderDay; // 0 if monthly statement
        int _overdueReminderDay; // 0 if monthly statement
        AutoSendType _autoSendType;
        #endregion

        #region Database access parameters
        private readonly string procedureContactForAutosend = "spContactForAutoSend";
        private readonly string procedureContactForAutosendNewTnc = "spContactForAutoSend_NewTNC_WithYear";
        private readonly string procedureContactForAutosendByContNo = "spContactForAutoSend_ByContNo";
        private string connection;
        #endregion

        #region Constructors
        /// <summary>
        /// Gets list of customers and their associated contacts that require sending of monthly/reminder statements
        /// This default constructor calls all ActionTypes for sending of MONTHLY statements
        /// Sets this as a monthly statement
        /// default overdue reminder day is ignored
        /// </summary>
        public CustomerContactList()
            : this(ActionTypeList.getAllPossibleActions(), defaultSendType, defaultReminderDay, defaultOverdueReminderDay)
        {
        }
        public CustomerContactList(DatabaseEnvironment dataBase)
            : this(ActionTypeList.getAllPossibleActions(), defaultSendType, defaultReminderDay, defaultOverdueReminderDay, dataBase, "")
        {
        }
        
        /// <summary>
        /// Gets list of customers and their associated contacts that require sending of monthly/reminder statements
        /// This constructor calls all ActionTypes
        /// Gets contacts for monthly statement if 0 or reminder statements if >= 1
        /// default overdue reminder day is 60 (Only customers with overdues > than this period will be selected for friendly reminders)
        /// </summary>
        /// <param name="reminder">0: monthly statement >=1: reminder statement</param>
        public CustomerContactList(AutoSendType autoSendType, int reminder)
            : this(ActionTypeList.getAllPossibleActions(), autoSendType, reminder, defaultOverdueReminderDay)
        {
        }
        public CustomerContactList(AutoSendType autoSendType, int reminder, DatabaseEnvironment dataBase)
            : this(ActionTypeList.getAllPossibleActions(), autoSendType, reminder, defaultOverdueReminderDay, dataBase, "")
        {
        }

        /// <summary>
        /// Gets list of customers and their associated contacts that require sending of monthly/reminder statements
        /// This constructor calls all ActionTypes
        /// Gets contacts for monthly statement if 0 or reminder statements if >= 1
        /// customers with overdues > than period stated in overdueReminderDay will be selected for friendly reminders
        /// </summary>
        /// <param name="reminder">Reminder day</param>
        /// <param name="overdueReminderDay">customers with overdues over that period will be reminded</param>
        public CustomerContactList(AutoSendType autoSendType, int reminder, int overdueReminderDay)
            : this(ActionTypeList.getAllPossibleActions(), autoSendType, reminder, overdueReminderDay)
        {
        }
        public CustomerContactList(AutoSendType autoSendType, int reminder, int overdueReminderDay, DatabaseEnvironment dataBase)
            : this(autoSendType, reminder, overdueReminderDay, dataBase, "")
        {
        }

        public CustomerContactList(AutoSendType autoSendType, int reminder, int overdueReminderDay, DatabaseEnvironment dataBase, string cont_no)
            : this(ActionTypeList.getAllPossibleActions(), autoSendType, reminder, overdueReminderDay, dataBase, cont_no)
        {
        }

        /// <summary>Gets list of customers and their associated contacts that require sending of monthly statements</summary>
        /// <param name="delimiters">Enter the types of actions you are interested in (Email, Fax, etc)
        /// use the enum ActionType</param>
        public CustomerContactList(ActionTypeList actions)
            : this(actions, defaultSendType, defaultReminderDay, defaultOverdueReminderDay)
        {
        }
        public CustomerContactList(ActionTypeList actions, DatabaseEnvironment dataBase)
            : this(actions, defaultSendType, defaultReminderDay, defaultOverdueReminderDay, dataBase, "")
        {
        }

        public CustomerContactList(ActionTypeList actions, AutoSendType autoSendType, int reminder)
            : this(actions, autoSendType, reminder, defaultOverdueReminderDay)
        {
        }
        public CustomerContactList(ActionTypeList actions, AutoSendType autoSendType, int reminder, DatabaseEnvironment dataBase)
            : this(actions, autoSendType, reminder, defaultOverdueReminderDay, dataBase, "")
        {
        }

        public CustomerContactList(ActionTypeList actions, AutoSendType autoSendType, int reminder, int overdueReminderDay)
            : this(actions, autoSendType, reminder, defaultOverdueReminderDay, DatabaseEnvironment.DefaultEnvironment, "")
        {
        }

        public CustomerContactList(ActionTypeList actions, AutoSendType autoSendType, int reminder, int overdueReminderDay, string cont_no)
            : this(actions, autoSendType, reminder, defaultOverdueReminderDay, DatabaseEnvironment.DefaultEnvironment, cont_no)
        {
        }

        public CustomerContactList(ActionTypeList actions, AutoSendType autoSendType, int reminder, int overdueReminderDay, DatabaseEnvironment dataBase)
            : this(actions, autoSendType, reminder, overdueReminderDay, dataBase, "")
        {
        }
            
        public CustomerContactList (ActionTypeList actions, AutoSendType autoSendType, int reminder, int overdueReminderDay, DatabaseEnvironment dataBase, string cont_no)
        {
            _dbConnection = new DatabaseEnvironments(dataBase);
            connection = _dbConnection.GetNetCRMConnection();
            _autoSendType = autoSendType;
            _reminderDay = reminder;
            _overdueReminderDay = overdueReminderDay;

            #region Get Customer Invoices
            // Get outstanding invoices
            if (_autoSendType == AutoSendType.Reminder) // Reminders
            {
                // Get outstanding invoices up to today
                outstandinginvoices = new OutstandingInvoiceList(DateTime.Now, _dbConnection.GetDatabaseEnvironment());
            }
            else if (_autoSendType == AutoSendType.MonthlyStatement)
            {
                // Get outstanding invoices up to last full month
                outstandinginvoices = new OutstandingInvoiceList(MonthlyStatementReport.getLatestMonthlyStatementDate(), _dbConnection.GetDatabaseEnvironment());
            }

            // check if retrieval of outstanding invoices is successful if monthly statements or reminders
            if (new List<AutoSendType>(new AutoSendType[] { AutoSendType.MonthlyStatement, AutoSendType.Reminder }).Contains(_autoSendType))
            {
                _retrievalsuccess = outstandinginvoices.isRetrievalSuccessful();
                if (!_retrievalsuccess)
                {
                    // Do not continue, return failure if retrieval of outstanding invoices fails
                    return;
                }
            }
            #endregion

            //Set up tables
            emailList   = new Hashtable();
            faxList     = new Hashtable();
            reviewList  = new Hashtable();
            printList   = new Hashtable();
            unknownList = new Hashtable();
            sentList    = new Hashtable();

            #region Database access setup
            //connection parameters
            SqlConnection conn = new SqlConnection(connection);

            //command parameters and attach connection
            SqlCommand com = new SqlCommand();

            if (cont_no != "")
            {
                com.CommandText = procedureContactForAutosendByContNo;
            }
            else if (_autoSendType == AutoSendType.NewTermsAndConditions2012 ||
                _autoSendType == AutoSendType.NewTermsAndConditions2018)
            {
                com.CommandText = procedureContactForAutosendNewTnc;
            }
            else
            {
                com.CommandText = procedureContactForAutosend;
            }
            com.CommandTimeout = 0;
            com.Connection = conn;

            //select command into dataadapter
            SqlDataAdapter adapt = new SqlDataAdapter();
            adapt.SelectCommand = com;
            adapt.SelectCommand.CommandType = CommandType.StoredProcedure;

            // convert action types into string for
            string actionlist = "";
            foreach (ActionType action in actions.getActions())
                actionlist += ActionTypeList.getStringFromActionType(action) + ",";
            adapt.SelectCommand.Parameters.Add(new SqlParameter("@ActionType",SqlDbType.VarChar,100));
            adapt.SelectCommand.Parameters["@ActionType"].Value = actionlist;

            if (cont_no != "")
            {
                adapt.SelectCommand.Parameters.Add(new SqlParameter("@cont_no", SqlDbType.VarChar, 20));
                adapt.SelectCommand.Parameters["@cont_no"].Value = cont_no;
            }

            // Set Reminder value
            if (_autoSendType != AutoSendType.NewTermsAndConditions2012
                && _autoSendType != AutoSendType.NewTermsAndConditions2018)
            {
                adapt.SelectCommand.Parameters.Add(new SqlParameter("@Reminder", SqlDbType.Int));
                adapt.SelectCommand.Parameters["@Reminder"].Value = reminder;
            

#if (JAN2011 && DEBUG)  // use 201101 statement month
                adapt.SelectCommand.Parameters.Add(new SqlParameter("@StatementMonth", SqlDbType.Char, 6));
                adapt.SelectCommand.Parameters["@StatementMonth"].Value = "201101";
#endif
            }

#if (FULLLIST && DEBUG)
            adapt.SelectCommand.Parameters.Add(new SqlParameter("@FullList", SqlDbType.Bit));
            adapt.SelectCommand.Parameters["@FullList"].Value = true;
#endif

            if (cont_no == "")
            {
                if (_autoSendType == AutoSendType.NewTermsAndConditions2012)
                {
                    adapt.SelectCommand.Parameters.Add(new SqlParameter("@Year", SqlDbType.Int));
                    adapt.SelectCommand.Parameters["@Year"].Value = 2012;
                }
                else if (_autoSendType == AutoSendType.NewTermsAndConditions2018)
                {
                    adapt.SelectCommand.Parameters.Add(new SqlParameter("@Year", SqlDbType.Int));
                    adapt.SelectCommand.Parameters["@Year"].Value = 2018;
                }
            }

            #endregion

            #region Database access and arranging of data into customerlists
            // create dataset for storing tables
            DataSet dataset = new DataSet();

            try
            {
                // open connection
                conn.Open();

                // fill data
                adapt.Fill(dataset);

                // Debugging use only (Save results of search into a datatable)
                debugSetCustomerTable(dataset.Tables[0]);

                // close connection
                conn.Close();

                // : Arrange customer and contact information into data
                foreach (DataRow dr in dataset.Tables[0].Rows)
                {
                    string customer = dr["exact_no"].ToString().Trim();
                    string customercode = dr["cus_no"] == null ? "" : dr["cus_no"].ToString().Trim();
                    string contactdetail = dr["details"].ToString().Trim();
                    ActionType action = ActionTypeList.getActionTypeFromString(dr["action"].ToString().Trim());
                    string upszone = dr["ups_zone"].ToString().Trim();
                    bool alreadySent = dr["done"].ToString() == "False" ? false : true;
                    string terms = dr["Terms"].ToString() == null ? "" : dr["Terms"].ToString().Trim();
                    string termsType = dr["TermsType"].ToString() == null ? "" : dr["TermsType"].ToString().Trim();

                    // Statement formats
                    EStatementFormats formats = new EStatementFormats();
                    if (bool.Parse(dr["estatement_pdf"].ToString()))
                        formats.AddFormat(EStatementFormat.PDF);
                    if (bool.Parse(dr["estatement_xls"].ToString()))
                        formats.AddFormat(EStatementFormat.EXCEL2003);

                    // Add contact detail to appropriate list
                    if (alreadySent)
                    {
                        AddDetailToList(sentList, customer, customercode, contactdetail, action, upszone, formats, terms, termsType);
                    }
                    else
                    {
                        switch (action)
                        {
                            case ActionType.Email:
                                AddDetailToList(emailList, customer, customercode, contactdetail, action, upszone, formats, terms, termsType);
                                break;
                            case ActionType.Fax:
                                AddDetailToList(faxList, customer, customercode, contactdetail, action, upszone, formats, terms, termsType);
                                break;
                            case ActionType.Print:
                                AddDetailToList(printList, customer, customercode, contactdetail, action, upszone, formats, terms, termsType);
                                break;
                            case ActionType.Review:
                                AddDetailToList(reviewList, customer, customercode, contactdetail, action, upszone, formats, terms, termsType);
                                break;
                            case ActionType.Unknown:
                                AddDetailToList(unknownList, customer, customercode, contactdetail, action, upszone, formats, terms, termsType);
                                break;

                        }
                    }
                }

                _retrievalsuccess = true;
            }
            catch (Exception ex)
            {
                _retrievalsuccess = false;
            }

            #endregion


        }

        private void AddDetailToList(Hashtable list, string customer, string customercode, string contactdetail, ActionType action, string upszone, EStatementFormats estatementFormats, string terms, string termsType)
        {
            if (!list.ContainsKey(customer))
            {
                // Create new customer contact list and add to Hashtable
                CustomerContact customercontactlist = new CustomerContact(customer, customercode, action, upszone);
                customercontactlist.Terms = terms;
                customercontactlist.TermsType = termsType;
                customercontactlist.addDetail(contactdetail);

                // Add customer estatementformats
                customercontactlist.SendFormats = estatementFormats;

                // reminder
                if (_autoSendType == AutoSendType.Reminder)
                {
                    // Get customer invoice and only add if overdues are over days specified
                    CustomerInvoices c = outstandinginvoices.GetCustomerInvoice(customer);
                    //if (c != null && c.HasInvoicesOverXDays(_overdueReminderDay))
                    if (c != null && c.HasInvoicesOverXDays(customercontactlist.getInvoiceOverdueDay()))
                    {
                        customercontactlist.Invoices = c;
                        list.Add(customer, customercontactlist);
                    }
                }
                else if (_autoSendType == AutoSendType.MonthlyStatement)
                {
                    // Get customer invoice and add invoice list to customer contact
                    CustomerInvoices c = outstandinginvoices.GetCustomerInvoice(customer);
                    customercontactlist.Invoices = c;

                    list.Add(customer, customercontactlist);
                }
                else // New T & C
                {
                    list.Add(customer, customercontactlist);
                }
                
            }
            else
            {
                // Get customer contact list and add detail
                CustomerContact c = (CustomerContact)list[customer];
                c.addDetail(contactdetail);
            }
        }
        #endregion

        #region get set methods

        /// <summary>Indicates if customer contact data is successfully retrieved from server</summary>
        /// <returns>true if data is successfully retrieved</returns>
        public bool isRetrievalSuccessful()
        {
            return _retrievalsuccess;
        }

        /// <summary>Gets list of customer numbers existing in list. (Returns all action types)</summary>
        /// <returns>String array containing customer numbers in list</returns>
        public string[] getListOfCustomers()
        {
            return getListOfCustomers(ActionTypeList.getAllPossibleActions());
        }

        /// <summary>Gets list of customer numbers  to be e-sent </summary>
        /// <returns>String array containing customer numbers in list</returns>
        public string[] getListOfEsendCustomers()
        {
            return getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Email, ActionType.Fax }));
        }

        /// <summary>Gets list of customer numbers to be printed.</summary>
        /// <returns>String array containing customer numbers in list</returns>
        public string[] getListOfPrintCustomers()
        {
            return getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Print }));
        }

        /// <summary>Gets list of customer numbers to be reviewed</summary>
        /// <returns>String array containing customer numbers in list</returns>
        public string[] getListOfReviewCustomers()
        {
            return getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Print }));
        }

        /// <summary>Gets list of customer numbers already sent</summary>
        /// <returns>String array containing customer numbers in list</returns>
        public string[] getListOfSentCustomers()
        {
            return getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Sent }));
        }

        /// <summary>Gets list of customer numbers existing in list. Delimited by the ActionTypes you are interested in.</summary>
        /// <param name="actions">ActionTypes you are interested in.</param>
        /// <returns>String array containing customer numbers in list, Empty array if none in list</returns>
        public string[] getListOfCustomers(ActionTypeList actions)
        {
            // Initialise list of Customer Numbers
            List<string> keys = new List<string>();

            // Check if ActionType is needed
            if (actions.containsAction(ActionType.Email))
            {
                // Add customer numbers of action type
                foreach (string key in emailList.Keys) keys.Add(key);
            }
            if (actions.containsAction(ActionType.Fax))
            {
                foreach (string key in faxList.Keys) keys.Add(key);
            }
            if (actions.containsAction(ActionType.Print))
            {
                foreach (string key in printList.Keys) keys.Add(key);
            }
            if (actions.containsAction(ActionType.Review))
            {
                foreach (string key in reviewList.Keys) keys.Add(key);
            }
            if (actions.containsAction(ActionType.Sent))
            {
                foreach (string key in sentList.Keys) keys.Add(key);
            }

            // sort keys
            keys.Sort();

            return keys.ToArray();
        }

        /// <summary>Gets the customer contacts of customer whose Exact No matches</summary>
        /// <param name="contactno">Customer's Exact No in String format</param>
        /// <returns>Contacts of customer in CustomerContact, returns null if does not exist</returns>
        public CustomerContact getCustomerContact (string contactno)
        {
            CustomerContact contact = null;

            if (emailList.ContainsKey(contactno))
                contact = (CustomerContact)emailList[contactno];
            else if (faxList.ContainsKey(contactno))
                contact = (CustomerContact)faxList[contactno];
            else if (printList.ContainsKey(contactno))
                contact = (CustomerContact)printList[contactno];
            else if (reviewList.ContainsKey(contactno))
                contact = (CustomerContact)reviewList[contactno];
            else if (sentList.ContainsKey(contactno))
                contact = (CustomerContact)sentList[contactno];

            return contact;
        }
        #endregion

        /// <summary>Gets the customer contacts of customers listed in Exact Nos provided</summary>
        /// <param name="contactno">Customers' Exact Nos in String format</param>
        /// <returns>Contacts of customer in CustomerContact, returns null if does not exist</returns>
        public CustomerContact[] GetCustomersContacts(string[] customers)
        { 
            List<CustomerContact> customercontacts = new List<CustomerContact>();

            foreach (string customer in customers)
            {
                CustomerContact customercontact = getCustomerContact(customer);

                if (customercontact != null)
                    customercontacts.Add(customercontact);
            }

            return customercontacts.ToArray();
        }

        #region Debug classes
        /// <summary>Returns customer table for debugging uses. (Will return null in release versions)</summary>
        /// <returns>DataTable containing data retrieved when CustomerContactList is first initialised</returns>
        public DataTable debugGetCustomerTable()
        {
            return _debug_dt;
        }

        /// <summary>For saving the datatable returned in spCustomerForAutosend Stored procedure (For Debugging only)
        /// Will not be called in release builds [Conditional("DEBUG")]</summary>
        /// <param name="data">Data</param>
        [Conditional("DEBUG")]
        private void debugSetCustomerTable(DataTable data)
        {
            _debug_dt = data;
        }
        #endregion

    }
}
