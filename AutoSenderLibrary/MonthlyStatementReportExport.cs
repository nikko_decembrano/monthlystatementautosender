using System;
using System.Collections.Generic;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace AutoSenderLibrary
{
    public class MonthlyStatementReportExport
    {
        // Readonlys
        static readonly int cusCodeCol = 1;
        static readonly string cusCodeText = "Cust Code";
        static readonly int dateCol = 2;
        static readonly string dateText = "Date";
        static readonly int typeCol = 3;
        static readonly string typeText = "Type";
        static readonly int applyToCol = 4;
        static readonly string applyToText = "Apply To";
        static readonly int poNoCol = 5;
        static readonly string poNoText = "PO No";
        static readonly int invoiceAmtCol = 6;
        static readonly string invoiceAmtText = "Invoice Amt";
        static readonly int accTotalCol = 7;
        static readonly string accTotalText = "Acc. Total";

        public static string ExportToExcel(CustomerInvoices customerInvoices)
        {
            string filePath = null;

            if (customerInvoices != null)
            {
                // Get a tempfile location to save to
                while (File.Exists(filePath = Path.ChangeExtension(Path.GetTempFileName(), "xls"))) ;

                // Excel
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value; // for missing values

                // New Excel worksheet
                xlApp = new Excel.ApplicationClass();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                int rowNum = 1;

                // Cell Headers
                xlWorkSheet.Cells[rowNum, cusCodeCol] = cusCodeText;
                xlWorkSheet.Cells[rowNum, dateCol] = dateText;
                xlWorkSheet.Cells[rowNum, typeCol] = typeText;
                xlWorkSheet.Cells[rowNum, applyToCol] = applyToText;
                xlWorkSheet.Cells[rowNum, poNoCol] = poNoText;
                xlWorkSheet.Cells[rowNum, invoiceAmtCol] = invoiceAmtText;
                xlWorkSheet.Cells[rowNum++, accTotalCol] = accTotalText;
                double accTotal = 0.0;

                foreach (InvoiceGroup invoice in customerInvoices.GetInvoiceGroups())
                {
                    foreach (InvoiceLine invoiceLine in invoice.GetInvoices())
                    {
                        // Invoice Lines
                        xlWorkSheet.Cells[rowNum, cusCodeCol] = customerInvoices.ExactNo;
                        xlWorkSheet.Cells[rowNum, dateCol] = invoiceLine.Date;
                        xlWorkSheet.Cells[rowNum, typeCol] = invoiceLine.Type;
                        xlWorkSheet.Cells[rowNum, applyToCol] = invoiceLine.ApplyTo;
                        xlWorkSheet.Cells[rowNum, poNoCol] = invoiceLine.PoNo;
                        xlWorkSheet.Cells[rowNum, invoiceAmtCol] = invoiceLine.InvoiceAmount;
                        accTotal += invoiceLine.InvoiceAmount;
                        xlWorkSheet.Cells[rowNum++, accTotalCol] = accTotal;
                    }
                }

                // Total Summary
                //xlWorkSheet.Cells[rowNum, poNoCol] = "Total:";
                //xlWorkSheet.Cells[rowNum, invoiceAmtCol] = customerInvoices.GetBalance();

                // Format
                Excel.Range colRange = (Excel.Range)xlWorkSheet.get_Range(xlWorkSheet.Cells[1, dateCol], xlWorkSheet.Cells[rowNum, dateCol]);
                colRange.NumberFormat = "dd/MM/yyyy";
                //colRange.AutoFit();
                colRange.Columns.AutoFit();
                colRange = (Excel.Range)xlWorkSheet.get_Range(xlWorkSheet.Cells[1, typeCol], xlWorkSheet.Cells[rowNum, typeCol]);
                colRange.Columns.AutoFit();
                colRange = (Excel.Range)xlWorkSheet.get_Range(xlWorkSheet.Cells[1, applyToCol], xlWorkSheet.Cells[rowNum, applyToCol]);
                colRange.Columns.AutoFit();
                colRange = (Excel.Range)xlWorkSheet.get_Range(xlWorkSheet.Cells[1, poNoCol], xlWorkSheet.Cells[rowNum, poNoCol]);
                colRange.Columns.AutoFit();
                colRange = (Excel.Range)xlWorkSheet.get_Range(xlWorkSheet.Cells[1, invoiceAmtCol], xlWorkSheet.Cells[rowNum, invoiceAmtCol]);
                colRange.NumberFormat = "$0.00";
                colRange.Columns.AutoFit();
                colRange = (Excel.Range)xlWorkSheet.get_Range(xlWorkSheet.Cells[1, cusCodeCol], xlWorkSheet.Cells[rowNum, cusCodeCol]);
                colRange.Columns.AutoFit();
                colRange = (Excel.Range)xlWorkSheet.get_Range(xlWorkSheet.Cells[1, accTotalCol], xlWorkSheet.Cells[rowNum, accTotalCol]);
                colRange.NumberFormat = "$0.00";
                colRange.Columns.AutoFit();

                // Save workbook
                xlWorkBook.SaveAs(filePath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                // Release COM objects
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);
            }


            return filePath;
        }

        /// <summary>
        /// Helper for releasing COM objects
        /// </summary>
        /// <param name="obj">object to be released</param>
        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
