using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    public class SendLogAddressResultLog
    {
        private int _sendId;
        private string _sendAddress;
        private string _resultDescription;
        private DateTime _received;
        private DateTime _processed;
        private int _logUpdated;
        private string _additionalInformation;

        public SendLogAddressResultLog(int sendId, string sendAddress, string resultDescription, DateTime received, DateTime processed, int logUpdated, string additionalInformation)
        {
            _sendId = sendId;
            _sendAddress = sendAddress;
            _resultDescription = resultDescription;
            _received = received;
            _processed = processed;
            _additionalInformation = additionalInformation;
            _logUpdated = logUpdated;
        }

        public int SendId
        {
            get { return _sendId; }
        }

        public string SendAddress
        {
            get { return _sendAddress; }
        }

        public string ResultDescription
        {
            get { return _resultDescription; }
            set { _resultDescription = value; }
        }

        public DateTime DateReceived
        {
            get { return _received; }
            set { _received = value; }
        }

        public DateTime DateProcessed
        {
            get { return _processed; }
            set { _processed = value; }
        }

        public int LogUpdated
        {
            get { return _logUpdated; }
            set { _logUpdated = value; }
        }

        public string AdditionalInformation
        {
            get { return _additionalInformation; }
            set { _additionalInformation = value; }
        }
    }
}
