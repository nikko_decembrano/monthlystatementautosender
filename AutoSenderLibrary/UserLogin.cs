using System;
using System.Collections.Generic;
using System.Text;
using System.Data; // for SQL
using System.Data.SqlClient;

namespace AutoSenderLibrary
{
    /// <summary>
    /// User login audit
    /// </summary>
    public class UserLogin
    {

        private static readonly string printStatementSql = @"
            select count(*) [Result]
            from [user] u
            inner join userrole ur on u.usr = ur.usr
            inner join rolepermission rp on ur.[role] = rp.[role]
            where u.usr = @user and rp.permission = @permission
        ";
        private static readonly string userPasswordAuthentication = "select count(usr) [Result] from [user] where usr = @user and pass = @password";

        private static readonly string printStatementPermission = "MonthlyStatementPrint";
        private static readonly string sendStatementPermission  = "MonthlyStatementEmailFax";
        private static readonly string AdministratorStatementPermission = "MonthlyStatementAdministrator";

        /// <summary>Checks if user has permission to print monthly statements</summary>
        /// <param name="user">username</param>
        /// <param name="password">password</param>
        /// <returns></returns>
        public static bool HasPrintStatementPermission(string user, DatabaseEnvironment db)
        {
            return HasStatementPermission(user, printStatementPermission, db);
        }

        /// <summary>Checks if user has permission to E-Send (email/fax) monthly statements</summary>
        /// <param name="user">username</param>
        /// <param name="password">password</param>
        /// <returns></returns>
        public static bool HasESendStatementPermission(string user, DatabaseEnvironment db)
        {
            return HasStatementPermission(user, sendStatementPermission, db);
        }

        /// <summary>Checks if user has permission to E-Send (email/fax) monthly statements</summary>
        /// <param name="user">username</param>
        /// <param name="password">password</param>
        /// <returns></returns>
        public static bool HasAdministratorPermission(string user, DatabaseEnvironment db)
        {
            return HasStatementPermission(user, AdministratorStatementPermission, db);
        }

        /// <summary>Checks if user password is correct</summary>
        /// <param name="user">username</param>
        /// <param name="password">password</param>
        /// <param name="database">Database to check with</param>
        /// <returns>true if authenticated</returns>
        public static bool CheckPassword(string user, string password, string database)
        {
            return PermissionHelper(user, password, "", DatabaseEnvironments.GetEnvironmentFromString(database), userPasswordAuthentication);
        }

        #region helper functions
        /// <summary>Helper for HasPrintStatementPermission, HasESendStatementPermission and HasAdministratorPermission</summary>
        /// <param name="user">username</param>
        /// <param name="password">password</param>
        /// <param name="statementPermission">permission</param>
        /// <param name="DatabaseEnvironment">Database to check</param>
        /// <returns></returns>
        private static bool HasStatementPermission(string user, string statementPermission, DatabaseEnvironment environment)
        {
            return PermissionHelper(user, "", statementPermission, environment, printStatementSql);
        }

        /// <summary>
        /// Base function for SQL access
        /// </summary>
        /// <param name="user">username</param>
        /// <param name="password">password (not used in certain sql statements)</param>
        /// <param name="permission">permission to check</param>
        /// <param name="environment">Database to check</param>
        /// <param name="sql">sql statement</param>
        /// <returns>true of correct</returns>
        private static bool PermissionHelper (string user, string password, string permission, DatabaseEnvironment environment, string sql)
        {
            bool hasPermission = false;

            #region Database access setup
            //command parameters
            SqlCommand com = new SqlCommand();
            com.CommandText = sql;
            com.CommandTimeout = 0;

            //connection parameters
            SqlConnection conn = new SqlConnection(DatabaseEnvironments.GetNetCRMConnection(environment));
            com.Connection = conn;

            //select command into dataadapter
            SqlDataAdapter adapt = new SqlDataAdapter();
            adapt.SelectCommand = com;
            
            adapt.SelectCommand.Parameters.Add(new SqlParameter("@user", SqlDbType.VarChar, 100));
            adapt.SelectCommand.Parameters["@user"].Value = user;
            adapt.SelectCommand.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar, 100));
            adapt.SelectCommand.Parameters["@password"].Value = password;
            adapt.SelectCommand.Parameters.Add(new SqlParameter("@permission", SqlDbType.VarChar, 100));
            adapt.SelectCommand.Parameters["@permission"].Value = permission;
            #endregion

            #region Database access and arranging of data into customerlists
            // create dataset for storing tables
            DataSet dataset = new DataSet();

            try
            {
                // open connection
                conn.Open();

                // fill data
                adapt.Fill(dataset);

                // close connection
                conn.Close();

                // : get permission
                hasPermission = int.Parse(dataset.Tables[0].Rows[0]["Result"].ToString()) > 0 ? true : false;

            }
            catch (Exception ex)
            {
                hasPermission = false;
            }

            #endregion

            return hasPermission;
        }
        #endregion
    }
}
