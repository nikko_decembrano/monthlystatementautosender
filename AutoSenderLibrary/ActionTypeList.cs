using System;
using System.Collections.Generic;
using System.Text;
using System.Collections; // for collections

namespace AutoSenderLibrary
{

    /// <summary>
    /// Enum of the type of actions associated with the customer (Unknown is output if of unknown actiontype default)
    /// </summary>
    public enum ActionType { Email, Fax, Print, Review, Sent, Unknown }

    /// <summary>
    /// Class containing a list of ActionTypes
    /// </summary>
    public class ActionTypeList
    {
        // Contains list of actions
        // Added Reference to System.Core to get Hash Set
        HashSet<ActionType> actionlist;

        #region Constructors (1 empty, 1 with ActionType[], 1 with ActionTypeList)
        /// <summary>
        /// Constructor which returns an empty ActionType list
        /// </summary>
        public ActionTypeList()
        {
            actionlist = new HashSet<ActionType>();
        }

        /// <summary>
        /// Constructor which returns an ActionTypeList
        /// containing ActionTypes specified in parameter
        /// </summary>
        /// <param name="actions">Array of ActionTypes to be added into ActionTypeList</param>
        public ActionTypeList(ActionType[] actions)
            : this()
        {
            foreach (ActionType action in actions)
                actionlist.Add(action);
        }

        /// <summary>
        /// Constructor which returns an ActionTypeList
        /// containing ActionTypes specified in parameter
        /// </summary>
        /// <param name="actionlist"></param>
        public ActionTypeList(ActionTypeList actionlist)
            : this(actionlist.getActions())
        {
        }
        #endregion

        #region Static methods
        /// <summary>Static method for getting all possible ActionTypes</summary>
        /// <returns>An ActionTypeList with all possible ActionTypes</returns>
        public static ActionTypeList getAllPossibleActions()
        {
            ActionTypeList actions = new ActionTypeList((ActionType[])Enum.GetValues(typeof(ActionType)));
            actions.removeAction(ActionType.Sent); // remove semt actiontype
            actions.removeAction(ActionType.Unknown); // remove default unknown actiontype

            return actions;
        }

        /// <summary>Static method for getting all possible ActionTypes</summary>
        /// <returns>An ActionTypeList with all possible ActionTypes</returns>
        public static ActionTypeList getAllPossibleActionsWithoutSent()
        {
            ActionTypeList actions = getAllPossibleActions();

            actions.removeAction(ActionType.Sent); // Remove Sent

            return actions;
        }

        /// <summary>Static helper method to get a string description of an ActionType</summary>
        /// <param name="action">Desired ActionType</param>
        /// <returns>String description of ActionType</returns>
        public static string getStringFromActionType(ActionType action)
        {
            return Enum.GetName(typeof(ActionType), action);
        }

        /// <summary>Get an Actiontype from string. returns unknown if not a valid ActionType</summary>
        /// <param name="action">String description of Actiontype</param>
        /// <returns>Specified ActionType (ActionType.Unknown if not a valid ActionType)</returns>
        public static ActionType getActionTypeFromString(string action)
        {
            ActionType actiontype;
            try
            {
                actiontype = (ActionType)Enum.Parse(typeof(ActionType), action, true);
            }
            catch (Exception)
            {
                actiontype = ActionType.Unknown;
            }

            return actiontype;
        }
        #endregion

        #region add get remove methods
        /// <summary>Adds an ActionType to ActionTypeList</summary>
        /// <param name="action">ActionType to be added</param>
        /// <returns>true if successfully added</returns>
        public bool addAction(ActionType action)
        {
            return actionlist.Add(action);
        }

        /// <summary>Removes an ActionType from ActionTypeList</summary>
        /// <param name="action">ActionType to be removed</param>
        /// <returns>true if successfully removed</returns>
        public bool removeAction(ActionType action)
        {
            return actionlist.Remove(action);
        }

        /// <summary>Clears all actions from ActionTypeList</summary>
        public void clearActions()
        {
            actionlist.Clear();
        }

        /// <summary>Gets list of actions in ActionTypeList</summary>
        /// <returns>Array of ActionTypes in list</returns>
        public ActionType[] getActions()
        {
            ActionType[] actions = new ActionType[actionlist.Count];
            actionlist.CopyTo(actions);

            return actions;
        }

        /// <summary>Checks if ActionType is in ActionTypeList</summary>
        /// <param name="action">ActionType you want to query</param>
        /// <returns>True if action is in ActionList</returns>
        public bool containsAction(ActionType action)
        {
            return actionlist.Contains(action);
        }
        #endregion


    }
}