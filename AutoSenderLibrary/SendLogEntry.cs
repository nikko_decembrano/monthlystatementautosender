using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    public class SendLogEntry
    {
        private int sendId;
        private string exactNo;
        private string month;
        private ActionType sendType;
        private Dictionary<string, SendLogAddress> sendAddresses;
        private DateTime timeStamp;
        private string netCrmActivityNo;
        private string cusNo;
        private string companyName;
        private string affinity;

        #region Constructors
        public SendLogEntry(int sendId, string exactNo, string cusNo, string companyName, string affinity, string month, ActionType sendType, DateTime timeStamp, string netCrmActivityNo, 
                                string sendAddress, bool failed, bool internalError, SendLogAddressResultLog resultLog)
        {
            this.sendAddresses = new Dictionary<string, SendLogAddress>();

            this.sendId = sendId;
            this.exactNo = exactNo;
            this.cusNo = cusNo;
            this.companyName = companyName;
            this.affinity = affinity;
            this.month = month;
            this.sendType = sendType;
            SendLogAddress logEntry = new SendLogAddress(sendAddress, failed, internalError);
            if (resultLog != null)
                logEntry.AddResultLog(resultLog);
            this.sendAddresses.Add(sendAddress, logEntry);
        }
        #endregion

        #region get and set methods
        public int SendId
        {
            get { return sendId; }
            set { sendId = value; }
        }

        public string ExactNo
        {
            get { return exactNo; }
            set { exactNo = value; }
        }

        public string Month
        {
            get { return month; }
            set { month = value; }
        }

        public ActionType SendType
        {
            get { return sendType; }
            set { sendType = value; }
        }

        public DateTime TimeStamp
        {
            get { return timeStamp; }
            set { timeStamp = value; }
        }

        public string NetCrmActivityNo
        {
            get { return netCrmActivityNo; }
            set { netCrmActivityNo = value; }
        }

        public string CusNo
        {
            get { return cusNo; }
            set { cusNo = value; }
        }

        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        public string Affinity
        {
            get { return affinity; }
            set { affinity = value; }
        }

        public SendLogAddress[] SendAddresses
        {
            get 
            { 
                SendLogAddress[] addresses = new SendLogAddress[sendAddresses.Count];
                sendAddresses.Values.CopyTo(addresses, 0);

                return addresses;
            }
        }

        /// <summary>Add new send address to SendLogEntry</summary>
        /// <param name="sendAddress">Email/Fax address to add</param>
        /// <returns>true if added</returns>
        public bool AddSendAddress(string sendAddress, bool failed, bool internalError, SendLogAddressResultLog log)
        {
            bool result= false;

            if (!sendAddresses.ContainsKey(sendAddress))
                sendAddresses.Add(sendAddress, new SendLogAddress (sendAddress, failed, internalError));

            SendLogAddress sendlogaddress;
            if (sendAddresses.TryGetValue(sendAddress, out sendlogaddress))
            {
                sendlogaddress.AddResultLog(log);
                result = true;
            }

            return result;
        }

        public void ClearSendAddresses()
        {
            sendAddresses.Clear();
        }

        public bool HasFailed()
        {
            foreach (SendLogAddress sendAddress in sendAddresses.Values)
            {
                if (sendAddress.Failed == false)
                    return false;
            }
            
            return true;
        }

        public bool HasInternalError()
        {
            foreach (SendLogAddress sendAddress in sendAddresses.Values)
            {
                if (sendAddress.InternalError == false)
                    return false;
            }

            return true;
        }

        public bool hasNetCRMActivity()
        {
            if (NetCrmActivityNo != "")
                return true;
            else
                return false;
        }
        #endregion
    }
}
