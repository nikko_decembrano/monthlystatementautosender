using System;
using System.Collections.Generic;
using System.Text;
using System.Data; // for SQL
using System.Data.SqlClient;
using System.Diagnostics; // for debugging

namespace AutoSenderLibrary
{
    public class OutstandingInvoiceList
    {
        #region Global variables
        DataTable _debug_dt = null; // for debugging

        // indicator to see if data is successfully retrieved from server
        bool retrievalsuccess = false;
        Dictionary<string, CustomerInvoices> customerinvoices;
        #endregion

        #region Database access parameters
        private DatabaseEnvironments _dbEnvironment;
        //private readonly string customerInvoiceSql = "select * from vCustomerReminderInfo order by date asc";
        private readonly string customerInvoiceSql = "spCustomerReminderInfo";
        #endregion

        #region constructor
        public OutstandingInvoiceList(DateTime dateTo)
            : this(dateTo, DatabaseEnvironment.DefaultEnvironment)
        {
        }

        public OutstandingInvoiceList(DateTime dateTo, DatabaseEnvironment dataBase)
        {
            // set up Dictionary
            customerinvoices = new Dictionary<string, CustomerInvoices>();

            #region Database access setup
            _dbEnvironment = new DatabaseEnvironments(dataBase);

            //connection parameters
            SqlConnection conn = new SqlConnection(_dbEnvironment.GetExactConnection());

            //command parameters and attach connection
            SqlCommand com = new SqlCommand();
            com.CommandText = customerInvoiceSql;
            com.CommandTimeout = 0;
            com.CommandType = CommandType.StoredProcedure;
            com.Connection = conn;

            //select command into dataadapter
            SqlDataAdapter adapt = new SqlDataAdapter();
            adapt.SelectCommand = com;

            adapt.SelectCommand.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.DateTime));
            adapt.SelectCommand.Parameters["@DateTo"].Value = dateTo;
            #endregion

            #region Database access
            // create dataset for storing tables
            DataSet dataset = new DataSet();

            try
            {
                // open connection
                conn.Open();

                // fill data
                adapt.Fill(dataset);

                // Debugging use only (Save results of search into a datatable)
                debugSetInvoiceTable(dataset.Tables[0]);

                // close connection
                conn.Close();

                // Arrange data according to customer and Invoice group
                foreach (DataRow dr in dataset.Tables[0].Rows)
                {
                    // Get exact and cus no
                    string exactno = dr["exact_no"].ToString();
                    string cusno   = dr["cus_no"].ToString();

                    // create customerinvoice entry if not exists
                    if (!customerinvoices.ContainsKey(exactno))
                    {
                        customerinvoices.Add(exactno, new CustomerInvoices(exactno, cusno));
                    }

                    // Create invoiceline
                    InvoiceLine line = new InvoiceLine();
                    DateTime date = new DateTime();
                    line.ApplyTo         = dr["apply to"].ToString();
                    line.Type            = dr["type"].ToString();
                    line.DocNo           = dr["doc no"].ToString();
                    DateTime.TryParse(dr["date"].ToString(), out date); 
                    line.Date            = date;
                    line.Currency        = dr["currency"].ToString();
                    line.InvoiceAmount   = double.Parse(dr["InvoiceAmount"].ToString());
                    line.PoNo            = dr["po no"].ToString();

                    // Add invoice line to customer
                    CustomerInvoices c;
                    customerinvoices.TryGetValue(exactno, out c);
                    c.AddInvoiceLine(line);

                }

                retrievalsuccess = true;
            }
            catch (Exception)
            {
                retrievalsuccess = false;
            }
            #endregion
        }
        #endregion


        #region Get Sets

        /// <summary>Gets list of outstanding invoices for customer, returns null if none exists</summary>
        /// <param name="exactno">Customer's exact no</param>
        /// <returns>customer's invoices</returns>
        public CustomerInvoices GetCustomerInvoice (string exactno)
        {
            CustomerInvoices c;
            bool success = customerinvoices.TryGetValue(exactno, out c);

            if (success)
            {
                return c;
            }
            else
            {
                return null;
            }
        }

        public string[] GetCustomerList()
        {
            string[] list = new string[customerinvoices.Count];
            customerinvoices.Keys.CopyTo(list, 0);

            return list;
        }

        /// <summary>Indicates if customer contact data is successfully retrieved from server</summary>
        /// <returns>true if data is successfully retrieved</returns>
        public bool isRetrievalSuccessful()
        {
            return retrievalsuccess;
        }
        #endregion


        #region Debug classes
        /// <summary>Returns customer table for debugging uses. (Will return null in release versions)</summary>
        /// <returns>DataTable containing data retrieved when CustomerContactList is first initialised</returns>
        public DataTable debugGetInvoiceTable()
        {
            return _debug_dt;
        }

        /// <summary>For saving the datatable returned in spCustomerForAutosend Stored procedure (For Debugging only)
        /// Will not be called in release builds [Conditional("DEBUG")]</summary>
        /// <param name="data">Data</param>
        [Conditional("DEBUG")]
        private void debugSetInvoiceTable(DataTable data)
        {
            _debug_dt = data;
        }
        #endregion


    }
}
