using System;
using System.Collections.Generic;
using System.Text;

namespace AutoSenderLibrary
{
    public class CustomerInvoices
    {
        // globals
        private string exactno;
        private string cusno;
        private int overduedays = 30; // Default number of days after which an invoice is overdue
        Dictionary<string, InvoiceGroup> invoices;

        #region contructors
        public CustomerInvoices(string exactno)
            : this(exactno, "")
        {
        }

        public CustomerInvoices(string exactno, string cusno)
        {
            this.exactno = exactno;
            this.cusno = cusno;

            invoices = new Dictionary<string,InvoiceGroup>();
        }

        public CustomerInvoices(string exactno, string cusno, int overduedays)
        {
            this.exactno = exactno;
            this.cusno = cusno;
            this.overduedays = overduedays;

            invoices = new Dictionary<string, InvoiceGroup>();
        }
        #endregion

        public void AddInvoiceLine (InvoiceLine line)
        {
            // add invoice line to applicable invoice group
            string applyto = line.ApplyTo;

            if (!invoices.ContainsKey(applyto))
            { 
                invoices.Add(applyto, new InvoiceGroup(applyto));
            }

            InvoiceGroup invoicegroup;
            invoices.TryGetValue(applyto, out invoicegroup);

            invoicegroup.AddInvoice(line);
        }

        /// <summary>Determines if customer has overdues</summary>
        /// <returns>true if there are overdues</returns>
        public bool HasOverdues()
        {
            // Check each invoice to see if it is overdue
            foreach (InvoiceGroup g in invoices.Values)
            {
                // return true if at least one invoice is overdue
                if (g.IsOverdue())
                    return true;
            }

            return false;
        }

        /// <summary>Determines if customer has overdues over a certain number of days</summary>
        /// <param name="days">Days overdue</param>
        /// <returns>true if there are overdues</returns>
        public bool HasInvoicesOverXDays(int days)
        {
            // Check each invoice to see if it is overdue
            foreach (InvoiceGroup g in invoices.Values)
            {
                // return true if at least one invoice is overdue
                if (g.IsOverXDays(days))
                    return true;
            }

            return false;
        }

        #region get sets
        public string ExactNo
        {
            get
            {
                return exactno;
            }
            set
            {
                exactno = value;
            }
        }

        public string CusNo
        {
            get
            {
                return cusno;
            }
            set
            {
                cusno = value;
            }
        }

        public int OverdueDays
        {
            get
            {
                return overduedays;
            }
            set
            {
                overduedays = value;
            }
        }

        public InvoiceGroup[] GetInvoiceGroups()
        {
            InvoiceGroup[] groups = new InvoiceGroup[invoices.Count];
            invoices.Values.CopyTo(groups, 0);

            return groups;
        }

        /// <summary>Get List of invoice groups that are overdue, unallocated or misallocated</summary>
        /// <returns>List of such invoicegroups</returns>
        public InvoiceGroup[] GetOverdueUnallocatedMisallocatedInvoiceGroups()
        {
            return GetInvoiceGroups(new InvoiceGroupStatus[] 
                                        {
                                            InvoiceGroupStatus.Overdue,
                                            InvoiceGroupStatus.OverAllocatedOverdue,
                                            InvoiceGroupStatus.Unallocated, 
                                            InvoiceGroupStatus.Misallocated 
                                        });
        }

        public InvoiceGroup[] GetInvoiceGroups(InvoiceGroupStatus[] groups)
        {
            List<InvoiceGroup> requiredgroups = new List<InvoiceGroup>();

            foreach (InvoiceGroup g in invoices.Values)
            {
                if (g.HasStatus(groups))
                    requiredgroups.Add(g);
            }

            return requiredgroups.ToArray();
        }

        public double GetOverdueDebits()
        {
            return GetDebits(InvoiceGroupStatus.Overdue) + GetDebits(InvoiceGroupStatus.OverAllocatedOverdue);
        }

        public double GetNonOverdueDebits()
        {
            return GetDebits(InvoiceGroupStatus.Current) + GetDebits(InvoiceGroupStatus.OverAllocatedCurrent);
        }

        public double GetMisallocatedDebits()
        {
            return GetDebits(InvoiceGroupStatus.Misallocated);
        }

        public double GetUnallocatedDebits()
        {
            return GetDebits(InvoiceGroupStatus.Unallocated);
        }

        public double GetDebits(InvoiceGroupStatus status)
        {
            double debits = 0.0;

            foreach (InvoiceGroup group in invoices.Values)
            {
                if (group.StatusOfInvoice() == status)
                    debits += group.GetDebitTotal();
            }

            return debits;
        }

        public double GetOverdueCredits()
        {
            return GetCredits(InvoiceGroupStatus.Overdue) + GetDebits(InvoiceGroupStatus.OverAllocatedOverdue);
        }

        public double GetNonOverdueCredits()
        {
            return GetCredits(InvoiceGroupStatus.Current) + GetDebits(InvoiceGroupStatus.OverAllocatedCurrent);
        }

        public double GetMisallocatedCredits()
        {
            return GetCredits(InvoiceGroupStatus.Misallocated);
        }

        public double GetUnallocatedCredits()
        {
            return GetCredits(InvoiceGroupStatus.Unallocated);
        }

        public double GetCredits(InvoiceGroupStatus status)
        {
            double credits = 0.0;

            foreach (InvoiceGroup group in invoices.Values)
            {
                if (group.StatusOfInvoice() == status)
                    credits += group.GetCreditTotal();
            }

            return credits;
        }

        public double GetOverdueUnallocatedMisallocatedBalance()
        {
            return GetBalance(new InvoiceGroupStatus[] 
                                        {
                                            InvoiceGroupStatus.Overdue,
                                            InvoiceGroupStatus.OverAllocatedOverdue,
                                            InvoiceGroupStatus.Unallocated, 
                                            InvoiceGroupStatus.Misallocated 
                                        });
        }

        public double GetBalance(InvoiceGroupStatus[] groups)
        {
            double balance = 0.0;

            foreach (InvoiceGroup group in invoices.Values)
            {
                if (group.HasStatus(groups))
                    balance += group.GetTotalForInvoice();
            }

            return balance;
        }

        public double GetBalance ()
        {
            double balance = 0.0;

            foreach (InvoiceGroup group in invoices.Values)
            {
                balance += group.GetTotalForInvoice();
            }

            return balance;
        }
        #endregion
    }
}
