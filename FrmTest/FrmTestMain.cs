using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;
using CrystalDecisions.Windows.Forms;

namespace FrmTest
{
    /// <summary>
    /// For testing classes in AutoSenderLibrary
    /// Please use in Debug mode when using this program
    /// </summary>
    public partial class FrmTestMain : Form
    {
        // customer contact list
        CustomerContactList cl = null;
        MonthlyStatementReport report;
        MonthlyStatementReports reports;
        OutstandingInvoiceList invoicelist;

        private readonly string formname = "Auto Send Tester ";

        public FrmTestMain()
        {
            InitializeComponent();
            HideGenerateReportButtons();
            HideGenerateReportsButtons();
            HideReportButtons();
            HideReportsButtons();
        }

        #region report button helpers
        private void HideGenerateReportButtons()
        {
            lblGenerateReport.Visible = false;
            txtReportNo.Visible = false;
            btnGenerateReport.Visible = false;
        }

        private void ShowGenerateReportButtons()
        {
            lblGenerateReport.Visible = true;
            txtReportNo.Visible = true;
            btnGenerateReport.Visible = true;
        }

        private void HideGenerateReportsButtons()
        {
            lblGenerateReports.Visible = false;
            btnGenerateReports.Visible = false;
        }

        private void ShowGenerateReportsButtons()
        {
            lblGenerateReports.Visible = true;
            btnGenerateReports.Visible = true;
        }

        private void HideReportButtons()
        {
            btnExportToPDF.Visible = false;
            btnSendEmail.Visible = false;
            btnPrintReport.Visible = false;
            btnToScreen.Visible = false;
        }

        private void ShowReportButtons()
        {
            btnExportToPDF.Visible = true;
            btnSendEmail.Visible = true;
            btnPrintReport.Visible = true;
            btnToScreen.Visible = true;
        }

        private void HideReportsButtons()
        {
            //btnExportToPDFs.Visible = false;
            //btnSendEmails.Visible = false;
            btnPrintReports.Visible = false;
            btnToScreens.Visible = false;
        }

        private void ShowReportsButtons()
        {
            //btnExportToPDFs.Visible = true;
            //btnSendEmails.Visible = true;
            btnPrintReports.Visible = true;
            btnToScreens.Visible = true;
        }

        #endregion

        private void btnGetCustomers_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(Getting Customers)";

            int reminder = 0;

            if (cboReminder.Checked)
            {
                reminder = 1;
            }

            int overduereminderdays = int.Parse(txtOverdueReminderDays.Text);

            // check types of actions wanted
            if (chkDelimit.Checked)
            {
                ActionTypeList actions = new ActionTypeList();
                if (chkEmail.Checked)
                    actions.addAction(ActionType.Email);
                if (chkFax.Checked)
                    actions.addAction(ActionType.Fax); 
                if (chkPrint.Checked)
                    actions.addAction(ActionType.Print);
                if (chkReview.Checked)
                    actions.addAction(ActionType.Review);
                if (chkSent.Checked)
                    actions.addAction(ActionType.Sent);

                cl = new CustomerContactList(actions, reminder > 0 ? AutoSendType.Reminder : AutoSendType.MonthlyStatement, reminder, overduereminderdays, GetDatabase());
            }
            else
            {
                cl = new CustomerContactList(reminder > 0 ? AutoSendType.Reminder : AutoSendType.MonthlyStatement, reminder, overduereminderdays, GetDatabase());
            }

            this.Text = formname + "(Loading Data)";

            // Show information onto screen
            dgrCustomerData.DataSource = cl.debugGetCustomerTable();
            lstCustomers.Items.Clear();


            if (cl.isRetrievalSuccessful())
            {
                lstCustomers.Items.AddRange(cl.getListOfCustomers());

                txtNumOfEntries.Text = cl.getListOfCustomers().Length.ToString();
                txtNumOfEmailEntries.Text = cl.getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Email })).Length.ToString();
                txtNumOfFaxEntries.Text = cl.getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Fax })).Length.ToString();
                txtNumOfPrintEntries.Text = cl.getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Print })).Length.ToString();
                txtNumOfReviewEntries.Text = cl.getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Review })).Length.ToString();
                txtNumAlreadySent.Text = cl.getListOfCustomers(new ActionTypeList(new ActionType[] { ActionType.Sent })).Length.ToString();
                txtNotSent.Text = cl.getListOfCustomers(ActionTypeList.getAllPossibleActionsWithoutSent()).Length.ToString();

                ShowGenerateReportsButtons();
            }
            else
            {
                MessageBox.Show("Failed to connect to Database, possible timeout of command");
            }
            
            this.Text = formname;
        }

        private void lstCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Clear contact list
            lstContacts.Items.Clear();
            txtNumOfContacts.Text = "";

            // If customer contact list is not null and within range, get information
            if (cl != null)
            {
                string cusno = (string)lstCustomers.Items[lstCustomers.SelectedIndex];
                CustomerContact customercontact = cl.getCustomerContact(cusno);
                string[] contacts = customercontact.getDetails();

                txtNumOfContacts.Text = contacts.Length.ToString();
                lstContacts.Items.AddRange(contacts);
                txtReportNo.Text = cusno;
                txtUpszone.Text = customercontact.getUpszone();
                txtActionType.Text = ActionTypeList.getStringFromActionType(customercontact.getActionType());

                // report buttons
                ShowGenerateReportButtons();
                HideReportButtons();

            }
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(Generating report)";
            report = new MonthlyStatementReport(txtReportNo.Text, GetDatabase());
            this.Text = formname;

            if (!report.isReportSuccessfullyGenerated())
            {
                MessageBox.Show("Error Generating report");
                return;
            }

            ShowReportButtons();
        }

        private void btnSendEmail_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(Getting contacts)";

            EmailFax emailer = new EmailFax();

            CustomerContact contact = cl.getCustomerContact(txtReportNo.Text);

            // remove details and add to message body
            string[] mailcontacts = contact.getDetails();
            emailer.MessageBody += "\n\n\nThis message would have been sent to: ";
            foreach (string mailcontact in mailcontacts)
            {
                emailer.MessageBody += "\n\t\t" + mailcontact; 
            }
            contact.clearDetails();

            // send to ourselves
            if (contact.getActionType() == ActionType.Fax)
                contact.addDetails(new string[] { "93137977" });
            else
                contact.addDetails(new string[] { "qingyou_lu@livingstone.com.au", "patrick_bezzina@livingstone.com.au" });

            // export to pdf and send
            this.Text = formname + "(exporting to PDF)";
            report.exportToPDF();

            this.Text = formname + "(sending email)";
            emailer.SendEmail(contact, report.exportToPDF(), 0, 9);

            // clear and reinsert details
            contact.clearDetails();
            contact.addDetails(mailcontacts);

            this.Text = formname;
        }

        private void btnExportToPDF_Click(object sender, EventArgs e)
        {
            // export to pdf
            this.Text = formname + "(exporting to PDF)";
            report.exportToPDF();
            this.Text = formname;
        }

        private void btnPrintReport_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(printing report)";
            report.printReportToPrinter();
            this.Text = formname;
        }

        private void btnToScreen_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(printing to screen)";
            crystalReportViewer1.ReportSource = report.getCrystalReport();
            this.Text = formname;
        }

        private void btnInsertLog_Click(object sender, EventArgs e)
        {
            StatementActionsLogger logger = new StatementActionsLogger();

            CustomerContact customer = cl.getCustomerContact(txtInsertLog.Text);

            if (customer != null)
            {
                //int sendId = logger.InsertLog(customer, 0 , 2, false, false, int.Parse(txtReminder.Text));
                int sendId = logger.InsertSendLog(customer, 0, 2, int.Parse(txtReminder.Text));

                MessageBox.Show(string.Format("Send Id {0}", sendId));
            }
            else
            {
                MessageBox.Show("Invalid customer number");
            }
        }

        private void btnUpdateLog_Click(object sender, EventArgs e)
        {
            StatementActionsLogger logger = new StatementActionsLogger();

            //int affected = logger.UpdateLog(int.Parse(txtSendId.Text), txtSendAddr.Text, chkFailed.Checked, ChkError.Checked);
            //MessageBox.Show(string.Format("{0} rows affected", affected));

            logger.UpdateSendLog(int.Parse(txtSendId.Text), txtSendAddr.Text, chkFailed.Checked, ChkError.Checked);
            MessageBox.Show("Done");
        }

        private void btnGenerateReports_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(Generating reports)";

            string[] customers = new string[lstCustomers.Items.Count];
            int i = 0;
            foreach (string customer in lstCustomers.Items)
            {
                customers[i++] = customer;
            }

            reports = new MonthlyStatementReports(customers, MonthlyStatementReports.getLatestMonthlyStatementDate());
            this.Text = formname;

            if (!reports.isReportSuccessfullyGenerated())
            {
                MessageBox.Show("Error Generating reports");
                return;
            }
            
            ShowReportsButtons();
        }

        private void btnToScreens_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(printing to screen)";
            crystalReportViewer1.ReportSource = reports.getCrystalReport();
            this.Text = formname;
        }

        private void btnPrintReports_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(printing report)";
            reports.printReportToPrinter();
            this.Text = formname;
        }

        private void btnGetTransactionTable_Click(object sender, EventArgs e)
        {
            MessageBox.Show("not implemented yet");
        }

        private void btnGetInvoices_Click(object sender, EventArgs e)
        {
            this.Text = formname + "(Getting invoices)";
            
            invoicelist = new OutstandingInvoiceList(DateTime.Now, GetDatabase());

            lstInvoiceList.Items.Clear();

            lstInvoiceList.Items.AddRange(invoicelist.GetCustomerList());

            dgrInvoiceData.DataSource = invoicelist.debugGetInvoiceTable();

            this.Text = formname;
        }

        private void lstInvoiceList_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lstInvoiceList.SelectedIndex >= 0 && lstInvoiceList.SelectedIndex < lstInvoiceList.Items.Count)
            {
                CustomerInvoices c = invoicelist.GetCustomerInvoice(lstInvoiceList.SelectedItem.ToString());

                string details = "Date        Details                 Numbers     Cust Po No                     Amount" + System.Environment.NewLine;
                foreach (InvoiceGroup invoice in c.GetOverdueUnallocatedMisallocatedInvoiceGroups())
                {
                    foreach (InvoiceLine line in invoice.GetInvoices())
                    {
                        details += string.Format("{0,-12:dd/MM/yyyy}{1,-24}{2,-12}{3,-25}{4,12:0.00}"
                        , line.Date, line.Type, line.ApplyTo, line.PoNo, line.InvoiceAmount) + System.Environment.NewLine;
                    }
                }
                details += string.Format("            Total        {0,60:0.00}", c.GetOverdueUnallocatedMisallocatedBalance()) + System.Environment.NewLine;

                /*
                 
                string details = "Overdues:" + System.Environment.NewLine;
                
                InvoiceGroup[] gs = c.GetOverdueUnallocatedMisallocatedInvoiceGroups();

                foreach (InvoiceGroup g in gs)
                {
                    details += string.Format("{0:dd/MM/yyyy}\tSales Invoice\t{1}\t{2}\t {3:0.00} \t  {4:0.00} \t  {5:0.00} ", g.GetInvoiceDate(), g.GetApplyTo(), g.GetPoNo(), g.GetDebitTotal(), g.GetCreditTotal(), g.GetTotalForInvoice()) 
                        + System.Environment.NewLine;
                }

                details += string.Format("Other Debits : \t\t\t\t\t\t{0:0.00}", c.GetNonOverdueDebits()) + System.Environment.NewLine;
                details += string.Format("Other Credits: \t\t\t\t\t\t\t{0:0.00}", c.GetNonOverdueCredits()) + System.Environment.NewLine;
                details += string.Format("Total        : \t\t\t\t\t\t\t\t{0:0.00}", c.GetBalance()) + System.Environment.NewLine;
                */

                txtInvoiceDetails.Text = details;
            }
        }

        private DatabaseEnvironment GetDatabase()
        {
            return lstDatabase.SelectedItem == null ? DatabaseEnvironment.AU : DatabaseEnvironments.GetEnvironmentFromString(lstDatabase.SelectedItem.ToString());
        }

        private void btnOverdue_Click(object sender, EventArgs e)
        {
            CustomerContact c = new CustomerContact("10000000", "TESTING", ActionType.Email, "X");
            c.Terms = txtTerms.Text;
            c.TermsType = txtTermsType.Text;

            lblOverdue.Text = c.getInvoiceOverdueDay().ToString();
        }
    }
}