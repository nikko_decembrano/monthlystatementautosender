namespace FrmTest
{
    partial class FrmTestMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetCustomers = new System.Windows.Forms.Button();
            this.chkDelimit = new System.Windows.Forms.CheckBox();
            this.chkEmail = new System.Windows.Forms.CheckBox();
            this.chkFax = new System.Windows.Forms.CheckBox();
            this.chkPrint = new System.Windows.Forms.CheckBox();
            this.chkReview = new System.Windows.Forms.CheckBox();
            this.tabContainer = new System.Windows.Forms.TabControl();
            this.tabDbData = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.dgrCustomerData = new System.Windows.Forms.DataGridView();
            this.tabCustomerData = new System.Windows.Forms.TabPage();
            this.txtNotSent = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNumAlreadySent = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnPrintReports = new System.Windows.Forms.Button();
            this.btnToScreens = new System.Windows.Forms.Button();
            this.lblGenerateReports = new System.Windows.Forms.Label();
            this.btnGenerateReports = new System.Windows.Forms.Button();
            this.txtActionType = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUpszone = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnToScreen = new System.Windows.Forms.Button();
            this.btnPrintReport = new System.Windows.Forms.Button();
            this.btnExportToPDF = new System.Windows.Forms.Button();
            this.lblGenerateReport = new System.Windows.Forms.Label();
            this.txtReportNo = new System.Windows.Forms.TextBox();
            this.btnSendEmail = new System.Windows.Forms.Button();
            this.btnGenerateReport = new System.Windows.Forms.Button();
            this.txtNumOfContacts = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lstContacts = new System.Windows.Forms.ListBox();
            this.txtNumOfReviewEntries = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNumOfPrintEntries = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNumOfFaxEntries = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNumOfEmailEntries = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNumOfEntries = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lstCustomers = new System.Windows.Forms.ListBox();
            this.tabCryRpt = new System.Windows.Forms.TabPage();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.tabTransactionLog = new System.Windows.Forms.TabPage();
            this.txtReminder = new System.Windows.Forms.TextBox();
            this.txtSendAddr = new System.Windows.Forms.TextBox();
            this.txtSendId = new System.Windows.Forms.TextBox();
            this.chkFailed = new System.Windows.Forms.CheckBox();
            this.ChkError = new System.Windows.Forms.CheckBox();
            this.btnUpdateLog = new System.Windows.Forms.Button();
            this.btnInsertLog = new System.Windows.Forms.Button();
            this.txtInsertLog = new System.Windows.Forms.TextBox();
            this.btnGetTransactionTable = new System.Windows.Forms.Button();
            this.dgrTransaction = new System.Windows.Forms.DataGridView();
            this.tabInvoices = new System.Windows.Forms.TabPage();
            this.txtInvoiceDetails = new System.Windows.Forms.TextBox();
            this.lstInvoiceList = new System.Windows.Forms.ListBox();
            this.btnGetInvoices = new System.Windows.Forms.Button();
            this.tabInvoiceDb = new System.Windows.Forms.TabPage();
            this.dgrInvoiceData = new System.Windows.Forms.DataGridView();
            this.cboReminder = new System.Windows.Forms.CheckBox();
            this.txtOverdueReminderDays = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lstDatabase = new System.Windows.Forms.ListBox();
            this.chkSent = new System.Windows.Forms.CheckBox();
            this.txtTerms = new System.Windows.Forms.TextBox();
            this.txtTermsType = new System.Windows.Forms.TextBox();
            this.lblOverdue = new System.Windows.Forms.Label();
            this.btnOverdue = new System.Windows.Forms.Button();
            this.tabContainer.SuspendLayout();
            this.tabDbData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrCustomerData)).BeginInit();
            this.tabCustomerData.SuspendLayout();
            this.tabCryRpt.SuspendLayout();
            this.tabTransactionLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTransaction)).BeginInit();
            this.tabInvoices.SuspendLayout();
            this.tabInvoiceDb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrInvoiceData)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGetCustomers
            // 
            this.btnGetCustomers.Location = new System.Drawing.Point(20, 18);
            this.btnGetCustomers.Name = "btnGetCustomers";
            this.btnGetCustomers.Size = new System.Drawing.Size(129, 40);
            this.btnGetCustomers.TabIndex = 0;
            this.btnGetCustomers.Text = "Get Customers";
            this.btnGetCustomers.UseVisualStyleBackColor = true;
            this.btnGetCustomers.Click += new System.EventHandler(this.btnGetCustomers_Click);
            // 
            // chkDelimit
            // 
            this.chkDelimit.AutoSize = true;
            this.chkDelimit.Location = new System.Drawing.Point(155, 31);
            this.chkDelimit.Name = "chkDelimit";
            this.chkDelimit.Size = new System.Drawing.Size(63, 17);
            this.chkDelimit.TabIndex = 1;
            this.chkDelimit.Text = "Delimit?";
            this.chkDelimit.UseVisualStyleBackColor = true;
            // 
            // chkEmail
            // 
            this.chkEmail.AutoSize = true;
            this.chkEmail.Location = new System.Drawing.Point(224, 18);
            this.chkEmail.Name = "chkEmail";
            this.chkEmail.Size = new System.Drawing.Size(51, 17);
            this.chkEmail.TabIndex = 2;
            this.chkEmail.Text = "Email";
            this.chkEmail.UseVisualStyleBackColor = true;
            // 
            // chkFax
            // 
            this.chkFax.AutoSize = true;
            this.chkFax.Location = new System.Drawing.Point(281, 18);
            this.chkFax.Name = "chkFax";
            this.chkFax.Size = new System.Drawing.Size(43, 17);
            this.chkFax.TabIndex = 3;
            this.chkFax.Text = "Fax";
            this.chkFax.UseVisualStyleBackColor = true;
            // 
            // chkPrint
            // 
            this.chkPrint.AutoSize = true;
            this.chkPrint.Location = new System.Drawing.Point(224, 41);
            this.chkPrint.Name = "chkPrint";
            this.chkPrint.Size = new System.Drawing.Size(47, 17);
            this.chkPrint.TabIndex = 4;
            this.chkPrint.Text = "Print";
            this.chkPrint.UseVisualStyleBackColor = true;
            // 
            // chkReview
            // 
            this.chkReview.AutoSize = true;
            this.chkReview.Location = new System.Drawing.Point(281, 41);
            this.chkReview.Name = "chkReview";
            this.chkReview.Size = new System.Drawing.Size(62, 17);
            this.chkReview.TabIndex = 5;
            this.chkReview.Text = "Review";
            this.chkReview.UseVisualStyleBackColor = true;
            // 
            // tabContainer
            // 
            this.tabContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabContainer.Controls.Add(this.tabDbData);
            this.tabContainer.Controls.Add(this.tabCustomerData);
            this.tabContainer.Controls.Add(this.tabCryRpt);
            this.tabContainer.Controls.Add(this.tabTransactionLog);
            this.tabContainer.Controls.Add(this.tabInvoices);
            this.tabContainer.Controls.Add(this.tabInvoiceDb);
            this.tabContainer.Location = new System.Drawing.Point(6, 87);
            this.tabContainer.Name = "tabContainer";
            this.tabContainer.SelectedIndex = 0;
            this.tabContainer.Size = new System.Drawing.Size(1076, 464);
            this.tabContainer.TabIndex = 7;
            // 
            // tabDbData
            // 
            this.tabDbData.Controls.Add(this.label1);
            this.tabDbData.Controls.Add(this.dgrCustomerData);
            this.tabDbData.Location = new System.Drawing.Point(4, 22);
            this.tabDbData.Name = "tabDbData";
            this.tabDbData.Padding = new System.Windows.Forms.Padding(3);
            this.tabDbData.Size = new System.Drawing.Size(1068, 438);
            this.tabDbData.TabIndex = 0;
            this.tabDbData.Text = "Database Data";
            this.tabDbData.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "* Note: Will only work in Debug build";
            // 
            // dgrCustomerData
            // 
            this.dgrCustomerData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrCustomerData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrCustomerData.Location = new System.Drawing.Point(0, 19);
            this.dgrCustomerData.Name = "dgrCustomerData";
            this.dgrCustomerData.Size = new System.Drawing.Size(1068, 419);
            this.dgrCustomerData.TabIndex = 7;
            // 
            // tabCustomerData
            // 
            this.tabCustomerData.Controls.Add(this.txtNotSent);
            this.tabCustomerData.Controls.Add(this.label14);
            this.tabCustomerData.Controls.Add(this.txtNumAlreadySent);
            this.tabCustomerData.Controls.Add(this.label13);
            this.tabCustomerData.Controls.Add(this.btnPrintReports);
            this.tabCustomerData.Controls.Add(this.btnToScreens);
            this.tabCustomerData.Controls.Add(this.lblGenerateReports);
            this.tabCustomerData.Controls.Add(this.btnGenerateReports);
            this.tabCustomerData.Controls.Add(this.txtActionType);
            this.tabCustomerData.Controls.Add(this.label11);
            this.tabCustomerData.Controls.Add(this.txtUpszone);
            this.tabCustomerData.Controls.Add(this.label10);
            this.tabCustomerData.Controls.Add(this.btnToScreen);
            this.tabCustomerData.Controls.Add(this.btnPrintReport);
            this.tabCustomerData.Controls.Add(this.btnExportToPDF);
            this.tabCustomerData.Controls.Add(this.lblGenerateReport);
            this.tabCustomerData.Controls.Add(this.txtReportNo);
            this.tabCustomerData.Controls.Add(this.btnSendEmail);
            this.tabCustomerData.Controls.Add(this.btnGenerateReport);
            this.tabCustomerData.Controls.Add(this.txtNumOfContacts);
            this.tabCustomerData.Controls.Add(this.label8);
            this.tabCustomerData.Controls.Add(this.label7);
            this.tabCustomerData.Controls.Add(this.lstContacts);
            this.tabCustomerData.Controls.Add(this.txtNumOfReviewEntries);
            this.tabCustomerData.Controls.Add(this.label6);
            this.tabCustomerData.Controls.Add(this.txtNumOfPrintEntries);
            this.tabCustomerData.Controls.Add(this.label5);
            this.tabCustomerData.Controls.Add(this.txtNumOfFaxEntries);
            this.tabCustomerData.Controls.Add(this.label4);
            this.tabCustomerData.Controls.Add(this.txtNumOfEmailEntries);
            this.tabCustomerData.Controls.Add(this.label3);
            this.tabCustomerData.Controls.Add(this.txtNumOfEntries);
            this.tabCustomerData.Controls.Add(this.label2);
            this.tabCustomerData.Controls.Add(this.lstCustomers);
            this.tabCustomerData.Location = new System.Drawing.Point(4, 22);
            this.tabCustomerData.Name = "tabCustomerData";
            this.tabCustomerData.Padding = new System.Windows.Forms.Padding(3);
            this.tabCustomerData.Size = new System.Drawing.Size(1068, 438);
            this.tabCustomerData.TabIndex = 1;
            this.tabCustomerData.Text = "Customer Contacts";
            this.tabCustomerData.UseVisualStyleBackColor = true;
            // 
            // txtNotSent
            // 
            this.txtNotSent.Location = new System.Drawing.Point(297, 36);
            this.txtNotSent.Name = "txtNotSent";
            this.txtNotSent.ReadOnly = true;
            this.txtNotSent.Size = new System.Drawing.Size(91, 20);
            this.txtNotSent.TabIndex = 33;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(223, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "Not Sent";
            // 
            // txtNumAlreadySent
            // 
            this.txtNumAlreadySent.Location = new System.Drawing.Point(468, 36);
            this.txtNumAlreadySent.Name = "txtNumAlreadySent";
            this.txtNumAlreadySent.ReadOnly = true;
            this.txtNumAlreadySent.Size = new System.Drawing.Size(91, 20);
            this.txtNumAlreadySent.TabIndex = 31;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(394, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "Already Sent";
            // 
            // btnPrintReports
            // 
            this.btnPrintReports.Location = new System.Drawing.Point(840, 311);
            this.btnPrintReports.Name = "btnPrintReports";
            this.btnPrintReports.Size = new System.Drawing.Size(100, 32);
            this.btnPrintReports.TabIndex = 29;
            this.btnPrintReports.Text = "Print Reports";
            this.btnPrintReports.UseVisualStyleBackColor = true;
            this.btnPrintReports.Click += new System.EventHandler(this.btnPrintReports_Click);
            // 
            // btnToScreens
            // 
            this.btnToScreens.Location = new System.Drawing.Point(840, 349);
            this.btnToScreens.Name = "btnToScreens";
            this.btnToScreens.Size = new System.Drawing.Size(100, 32);
            this.btnToScreens.TabIndex = 28;
            this.btnToScreens.Text = "To Screen";
            this.btnToScreens.UseVisualStyleBackColor = true;
            this.btnToScreens.Click += new System.EventHandler(this.btnToScreens_Click);
            // 
            // lblGenerateReports
            // 
            this.lblGenerateReports.AutoSize = true;
            this.lblGenerateReports.Location = new System.Drawing.Point(837, 256);
            this.lblGenerateReports.Name = "lblGenerateReports";
            this.lblGenerateReports.Size = new System.Drawing.Size(139, 13);
            this.lblGenerateReports.TabIndex = 27;
            this.lblGenerateReports.Text = "Generate Reports for all nos";
            // 
            // btnGenerateReports
            // 
            this.btnGenerateReports.Location = new System.Drawing.Point(840, 272);
            this.btnGenerateReports.Name = "btnGenerateReports";
            this.btnGenerateReports.Size = new System.Drawing.Size(100, 32);
            this.btnGenerateReports.TabIndex = 26;
            this.btnGenerateReports.Text = "Generate Reports";
            this.btnGenerateReports.UseVisualStyleBackColor = true;
            this.btnGenerateReports.Click += new System.EventHandler(this.btnGenerateReports_Click);
            // 
            // txtActionType
            // 
            this.txtActionType.Location = new System.Drawing.Point(650, 207);
            this.txtActionType.Name = "txtActionType";
            this.txtActionType.ReadOnly = true;
            this.txtActionType.Size = new System.Drawing.Size(67, 20);
            this.txtActionType.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(559, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "ActionType";
            // 
            // txtUpszone
            // 
            this.txtUpszone.Location = new System.Drawing.Point(485, 207);
            this.txtUpszone.Name = "txtUpszone";
            this.txtUpszone.ReadOnly = true;
            this.txtUpszone.Size = new System.Drawing.Size(67, 20);
            this.txtUpszone.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(394, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "UPS Zone";
            // 
            // btnToScreen
            // 
            this.btnToScreen.Location = new System.Drawing.Point(724, 387);
            this.btnToScreen.Name = "btnToScreen";
            this.btnToScreen.Size = new System.Drawing.Size(100, 32);
            this.btnToScreen.TabIndex = 21;
            this.btnToScreen.Text = "To Screen";
            this.btnToScreen.UseVisualStyleBackColor = true;
            this.btnToScreen.Click += new System.EventHandler(this.btnToScreen_Click);
            // 
            // btnPrintReport
            // 
            this.btnPrintReport.Location = new System.Drawing.Point(606, 387);
            this.btnPrintReport.Name = "btnPrintReport";
            this.btnPrintReport.Size = new System.Drawing.Size(100, 32);
            this.btnPrintReport.TabIndex = 20;
            this.btnPrintReport.Text = "Print Report";
            this.btnPrintReport.UseVisualStyleBackColor = true;
            this.btnPrintReport.Click += new System.EventHandler(this.btnPrintReport_Click);
            // 
            // btnExportToPDF
            // 
            this.btnExportToPDF.Location = new System.Drawing.Point(606, 311);
            this.btnExportToPDF.Name = "btnExportToPDF";
            this.btnExportToPDF.Size = new System.Drawing.Size(100, 32);
            this.btnExportToPDF.TabIndex = 19;
            this.btnExportToPDF.Text = "Export to PDF";
            this.btnExportToPDF.UseVisualStyleBackColor = true;
            this.btnExportToPDF.Click += new System.EventHandler(this.btnExportToPDF_Click);
            // 
            // lblGenerateReport
            // 
            this.lblGenerateReport.AutoSize = true;
            this.lblGenerateReport.Location = new System.Drawing.Point(410, 256);
            this.lblGenerateReport.Name = "lblGenerateReport";
            this.lblGenerateReport.Size = new System.Drawing.Size(104, 13);
            this.lblGenerateReport.TabIndex = 18;
            this.lblGenerateReport.Text = "Generate Report for:";
            // 
            // txtReportNo
            // 
            this.txtReportNo.Location = new System.Drawing.Point(413, 280);
            this.txtReportNo.Name = "txtReportNo";
            this.txtReportNo.Size = new System.Drawing.Size(169, 20);
            this.txtReportNo.TabIndex = 17;
            // 
            // btnSendEmail
            // 
            this.btnSendEmail.Location = new System.Drawing.Point(606, 349);
            this.btnSendEmail.Name = "btnSendEmail";
            this.btnSendEmail.Size = new System.Drawing.Size(100, 32);
            this.btnSendEmail.TabIndex = 16;
            this.btnSendEmail.Text = "Send to qy";
            this.btnSendEmail.UseVisualStyleBackColor = true;
            this.btnSendEmail.Click += new System.EventHandler(this.btnSendEmail_Click);
            // 
            // btnGenerateReport
            // 
            this.btnGenerateReport.Location = new System.Drawing.Point(606, 273);
            this.btnGenerateReport.Name = "btnGenerateReport";
            this.btnGenerateReport.Size = new System.Drawing.Size(100, 32);
            this.btnGenerateReport.TabIndex = 15;
            this.btnGenerateReport.Text = "Generate Report";
            this.btnGenerateReport.UseVisualStyleBackColor = true;
            this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
            // 
            // txtNumOfContacts
            // 
            this.txtNumOfContacts.Location = new System.Drawing.Point(321, 207);
            this.txtNumOfContacts.Name = "txtNumOfContacts";
            this.txtNumOfContacts.ReadOnly = true;
            this.txtNumOfContacts.Size = new System.Drawing.Size(67, 20);
            this.txtNumOfContacts.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(230, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Num of contacts";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(230, 239);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Contacts";
            // 
            // lstContacts
            // 
            this.lstContacts.FormattingEnabled = true;
            this.lstContacts.Location = new System.Drawing.Point(230, 255);
            this.lstContacts.Name = "lstContacts";
            this.lstContacts.Size = new System.Drawing.Size(157, 160);
            this.lstContacts.TabIndex = 11;
            // 
            // txtNumOfReviewEntries
            // 
            this.txtNumOfReviewEntries.Location = new System.Drawing.Point(840, 8);
            this.txtNumOfReviewEntries.Name = "txtNumOfReviewEntries";
            this.txtNumOfReviewEntries.ReadOnly = true;
            this.txtNumOfReviewEntries.Size = new System.Drawing.Size(91, 20);
            this.txtNumOfReviewEntries.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(756, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Review Entries";
            // 
            // txtNumOfPrintEntries
            // 
            this.txtNumOfPrintEntries.Location = new System.Drawing.Point(650, 8);
            this.txtNumOfPrintEntries.Name = "txtNumOfPrintEntries";
            this.txtNumOfPrintEntries.ReadOnly = true;
            this.txtNumOfPrintEntries.Size = new System.Drawing.Size(91, 20);
            this.txtNumOfPrintEntries.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(576, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Print Entries";
            // 
            // txtNumOfFaxEntries
            // 
            this.txtNumOfFaxEntries.Location = new System.Drawing.Point(468, 8);
            this.txtNumOfFaxEntries.Name = "txtNumOfFaxEntries";
            this.txtNumOfFaxEntries.ReadOnly = true;
            this.txtNumOfFaxEntries.Size = new System.Drawing.Size(91, 20);
            this.txtNumOfFaxEntries.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(394, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Fax Entries";
            // 
            // txtNumOfEmailEntries
            // 
            this.txtNumOfEmailEntries.Location = new System.Drawing.Point(297, 8);
            this.txtNumOfEmailEntries.Name = "txtNumOfEmailEntries";
            this.txtNumOfEmailEntries.ReadOnly = true;
            this.txtNumOfEmailEntries.Size = new System.Drawing.Size(91, 20);
            this.txtNumOfEmailEntries.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(223, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Email Entries";
            // 
            // txtNumOfEntries
            // 
            this.txtNumOfEntries.Location = new System.Drawing.Point(297, 62);
            this.txtNumOfEntries.Name = "txtNumOfEntries";
            this.txtNumOfEntries.ReadOnly = true;
            this.txtNumOfEntries.Size = new System.Drawing.Size(91, 20);
            this.txtNumOfEntries.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(223, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Total Entries";
            // 
            // lstCustomers
            // 
            this.lstCustomers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lstCustomers.FormattingEnabled = true;
            this.lstCustomers.Location = new System.Drawing.Point(11, 11);
            this.lstCustomers.Name = "lstCustomers";
            this.lstCustomers.Size = new System.Drawing.Size(196, 407);
            this.lstCustomers.TabIndex = 0;
            this.lstCustomers.SelectedIndexChanged += new System.EventHandler(this.lstCustomers_SelectedIndexChanged);
            // 
            // tabCryRpt
            // 
            this.tabCryRpt.Controls.Add(this.crystalReportViewer1);
            this.tabCryRpt.Location = new System.Drawing.Point(4, 22);
            this.tabCryRpt.Name = "tabCryRpt";
            this.tabCryRpt.Padding = new System.Windows.Forms.Padding(3);
            this.tabCryRpt.Size = new System.Drawing.Size(1068, 438);
            this.tabCryRpt.TabIndex = 2;
            this.tabCryRpt.Text = "CrystalReport";
            this.tabCryRpt.UseVisualStyleBackColor = true;
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.Size = new System.Drawing.Size(1072, 442);
            this.crystalReportViewer1.TabIndex = 0;
            // 
            // tabTransactionLog
            // 
            this.tabTransactionLog.Controls.Add(this.txtReminder);
            this.tabTransactionLog.Controls.Add(this.txtSendAddr);
            this.tabTransactionLog.Controls.Add(this.txtSendId);
            this.tabTransactionLog.Controls.Add(this.chkFailed);
            this.tabTransactionLog.Controls.Add(this.ChkError);
            this.tabTransactionLog.Controls.Add(this.btnUpdateLog);
            this.tabTransactionLog.Controls.Add(this.btnInsertLog);
            this.tabTransactionLog.Controls.Add(this.txtInsertLog);
            this.tabTransactionLog.Controls.Add(this.btnGetTransactionTable);
            this.tabTransactionLog.Controls.Add(this.dgrTransaction);
            this.tabTransactionLog.Location = new System.Drawing.Point(4, 22);
            this.tabTransactionLog.Name = "tabTransactionLog";
            this.tabTransactionLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabTransactionLog.Size = new System.Drawing.Size(1068, 438);
            this.tabTransactionLog.TabIndex = 3;
            this.tabTransactionLog.Text = "Transaction History";
            this.tabTransactionLog.UseVisualStyleBackColor = true;
            // 
            // txtReminder
            // 
            this.txtReminder.Location = new System.Drawing.Point(1003, 185);
            this.txtReminder.Name = "txtReminder";
            this.txtReminder.Size = new System.Drawing.Size(45, 20);
            this.txtReminder.TabIndex = 19;
            this.txtReminder.Text = "0";
            // 
            // txtSendAddr
            // 
            this.txtSendAddr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSendAddr.Location = new System.Drawing.Point(918, 304);
            this.txtSendAddr.Name = "txtSendAddr";
            this.txtSendAddr.Size = new System.Drawing.Size(130, 20);
            this.txtSendAddr.TabIndex = 18;
            this.txtSendAddr.Text = "a@b.com";
            // 
            // txtSendId
            // 
            this.txtSendId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSendId.Location = new System.Drawing.Point(918, 283);
            this.txtSendId.Name = "txtSendId";
            this.txtSendId.Size = new System.Drawing.Size(130, 20);
            this.txtSendId.TabIndex = 17;
            this.txtSendId.Text = "999999";
            // 
            // chkFailed
            // 
            this.chkFailed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFailed.AutoSize = true;
            this.chkFailed.Location = new System.Drawing.Point(988, 330);
            this.chkFailed.Name = "chkFailed";
            this.chkFailed.Size = new System.Drawing.Size(54, 17);
            this.chkFailed.TabIndex = 16;
            this.chkFailed.Text = "Failed";
            this.chkFailed.UseVisualStyleBackColor = true;
            // 
            // ChkError
            // 
            this.ChkError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkError.AutoSize = true;
            this.ChkError.Location = new System.Drawing.Point(919, 330);
            this.ChkError.Name = "ChkError";
            this.ChkError.Size = new System.Drawing.Size(48, 17);
            this.ChkError.TabIndex = 15;
            this.ChkError.Text = "Error";
            this.ChkError.UseVisualStyleBackColor = true;
            // 
            // btnUpdateLog
            // 
            this.btnUpdateLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateLog.Location = new System.Drawing.Point(918, 353);
            this.btnUpdateLog.Name = "btnUpdateLog";
            this.btnUpdateLog.Size = new System.Drawing.Size(131, 23);
            this.btnUpdateLog.TabIndex = 12;
            this.btnUpdateLog.Text = "Update Log Entry";
            this.btnUpdateLog.UseVisualStyleBackColor = true;
            this.btnUpdateLog.Click += new System.EventHandler(this.btnUpdateLog_Click);
            // 
            // btnInsertLog
            // 
            this.btnInsertLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInsertLog.Location = new System.Drawing.Point(919, 211);
            this.btnInsertLog.Name = "btnInsertLog";
            this.btnInsertLog.Size = new System.Drawing.Size(130, 23);
            this.btnInsertLog.TabIndex = 11;
            this.btnInsertLog.Text = "Insert Log";
            this.btnInsertLog.UseVisualStyleBackColor = true;
            this.btnInsertLog.Click += new System.EventHandler(this.btnInsertLog_Click);
            // 
            // txtInsertLog
            // 
            this.txtInsertLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInsertLog.Location = new System.Drawing.Point(919, 159);
            this.txtInsertLog.Name = "txtInsertLog";
            this.txtInsertLog.Size = new System.Drawing.Size(130, 20);
            this.txtInsertLog.TabIndex = 10;
            this.txtInsertLog.Text = "100000025";
            // 
            // btnGetTransactionTable
            // 
            this.btnGetTransactionTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetTransactionTable.Location = new System.Drawing.Point(919, 10);
            this.btnGetTransactionTable.Name = "btnGetTransactionTable";
            this.btnGetTransactionTable.Size = new System.Drawing.Size(130, 23);
            this.btnGetTransactionTable.TabIndex = 9;
            this.btnGetTransactionTable.Text = "Get Transaction Table";
            this.btnGetTransactionTable.UseVisualStyleBackColor = true;
            this.btnGetTransactionTable.Click += new System.EventHandler(this.btnGetTransactionTable_Click);
            // 
            // dgrTransaction
            // 
            this.dgrTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrTransaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrTransaction.Location = new System.Drawing.Point(0, 10);
            this.dgrTransaction.Name = "dgrTransaction";
            this.dgrTransaction.Size = new System.Drawing.Size(902, 422);
            this.dgrTransaction.TabIndex = 8;
            // 
            // tabInvoices
            // 
            this.tabInvoices.Controls.Add(this.txtInvoiceDetails);
            this.tabInvoices.Controls.Add(this.lstInvoiceList);
            this.tabInvoices.Controls.Add(this.btnGetInvoices);
            this.tabInvoices.Location = new System.Drawing.Point(4, 22);
            this.tabInvoices.Name = "tabInvoices";
            this.tabInvoices.Padding = new System.Windows.Forms.Padding(3);
            this.tabInvoices.Size = new System.Drawing.Size(1068, 438);
            this.tabInvoices.TabIndex = 4;
            this.tabInvoices.Text = "Invoices";
            this.tabInvoices.UseVisualStyleBackColor = true;
            // 
            // txtInvoiceDetails
            // 
            this.txtInvoiceDetails.Location = new System.Drawing.Point(161, 46);
            this.txtInvoiceDetails.Multiline = true;
            this.txtInvoiceDetails.Name = "txtInvoiceDetails";
            this.txtInvoiceDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInvoiceDetails.Size = new System.Drawing.Size(588, 381);
            this.txtInvoiceDetails.TabIndex = 2;
            // 
            // lstInvoiceList
            // 
            this.lstInvoiceList.FormattingEnabled = true;
            this.lstInvoiceList.Location = new System.Drawing.Point(3, 46);
            this.lstInvoiceList.Name = "lstInvoiceList";
            this.lstInvoiceList.Size = new System.Drawing.Size(120, 381);
            this.lstInvoiceList.Sorted = true;
            this.lstInvoiceList.TabIndex = 1;
            this.lstInvoiceList.SelectedIndexChanged += new System.EventHandler(this.lstInvoiceList_SelectedIndexChanged);
            // 
            // btnGetInvoices
            // 
            this.btnGetInvoices.Location = new System.Drawing.Point(6, 17);
            this.btnGetInvoices.Name = "btnGetInvoices";
            this.btnGetInvoices.Size = new System.Drawing.Size(117, 23);
            this.btnGetInvoices.TabIndex = 0;
            this.btnGetInvoices.Text = "Get Invoices";
            this.btnGetInvoices.UseVisualStyleBackColor = true;
            this.btnGetInvoices.Click += new System.EventHandler(this.btnGetInvoices_Click);
            // 
            // tabInvoiceDb
            // 
            this.tabInvoiceDb.Controls.Add(this.dgrInvoiceData);
            this.tabInvoiceDb.Location = new System.Drawing.Point(4, 22);
            this.tabInvoiceDb.Name = "tabInvoiceDb";
            this.tabInvoiceDb.Padding = new System.Windows.Forms.Padding(3);
            this.tabInvoiceDb.Size = new System.Drawing.Size(1068, 438);
            this.tabInvoiceDb.TabIndex = 5;
            this.tabInvoiceDb.Text = "InvoiceDB";
            this.tabInvoiceDb.UseVisualStyleBackColor = true;
            // 
            // dgrInvoiceData
            // 
            this.dgrInvoiceData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrInvoiceData.Location = new System.Drawing.Point(0, 3);
            this.dgrInvoiceData.Name = "dgrInvoiceData";
            this.dgrInvoiceData.Size = new System.Drawing.Size(1068, 429);
            this.dgrInvoiceData.TabIndex = 0;
            // 
            // cboReminder
            // 
            this.cboReminder.AutoSize = true;
            this.cboReminder.Location = new System.Drawing.Point(44, 64);
            this.cboReminder.Name = "cboReminder";
            this.cboReminder.Size = new System.Drawing.Size(71, 17);
            this.cboReminder.TabIndex = 8;
            this.cboReminder.Text = "Reminder";
            this.cboReminder.UseVisualStyleBackColor = true;
            // 
            // txtOverdueReminderDays
            // 
            this.txtOverdueReminderDays.Location = new System.Drawing.Point(542, 18);
            this.txtOverdueReminderDays.Name = "txtOverdueReminderDays";
            this.txtOverdueReminderDays.Size = new System.Drawing.Size(60, 20);
            this.txtOverdueReminderDays.TabIndex = 9;
            this.txtOverdueReminderDays.Text = "60";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(469, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Remind after";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(608, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "days";
            // 
            // lstDatabase
            // 
            this.lstDatabase.FormattingEnabled = true;
            this.lstDatabase.Items.AddRange(new object[] {
            "au",
            "hk",
            "ph"});
            this.lstDatabase.Location = new System.Drawing.Point(388, 12);
            this.lstDatabase.Name = "lstDatabase";
            this.lstDatabase.Size = new System.Drawing.Size(57, 56);
            this.lstDatabase.TabIndex = 12;
            // 
            // chkSent
            // 
            this.chkSent.AutoSize = true;
            this.chkSent.Location = new System.Drawing.Point(240, 64);
            this.chkSent.Name = "chkSent";
            this.chkSent.Size = new System.Drawing.Size(48, 17);
            this.chkSent.TabIndex = 13;
            this.chkSent.Text = "Sent";
            this.chkSent.UseVisualStyleBackColor = true;
            // 
            // txtTerms
            // 
            this.txtTerms.Location = new System.Drawing.Point(786, 32);
            this.txtTerms.Name = "txtTerms";
            this.txtTerms.Size = new System.Drawing.Size(100, 20);
            this.txtTerms.TabIndex = 14;
            // 
            // txtTermsType
            // 
            this.txtTermsType.Location = new System.Drawing.Point(786, 58);
            this.txtTermsType.Name = "txtTermsType";
            this.txtTermsType.Size = new System.Drawing.Size(100, 20);
            this.txtTermsType.TabIndex = 15;
            // 
            // lblOverdue
            // 
            this.lblOverdue.AutoSize = true;
            this.lblOverdue.Location = new System.Drawing.Point(908, 35);
            this.lblOverdue.Name = "lblOverdue";
            this.lblOverdue.Size = new System.Drawing.Size(41, 13);
            this.lblOverdue.TabIndex = 16;
            this.lblOverdue.Text = "label15";
            // 
            // btnOverdue
            // 
            this.btnOverdue.Location = new System.Drawing.Point(892, 55);
            this.btnOverdue.Name = "btnOverdue";
            this.btnOverdue.Size = new System.Drawing.Size(88, 23);
            this.btnOverdue.TabIndex = 17;
            this.btnOverdue.Text = "Calc Overdue";
            this.btnOverdue.UseVisualStyleBackColor = true;
            this.btnOverdue.Click += new System.EventHandler(this.btnOverdue_Click);
            // 
            // FrmTestMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 563);
            this.Controls.Add(this.btnOverdue);
            this.Controls.Add(this.lblOverdue);
            this.Controls.Add(this.txtTermsType);
            this.Controls.Add(this.txtTerms);
            this.Controls.Add(this.chkSent);
            this.Controls.Add(this.lstDatabase);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtOverdueReminderDays);
            this.Controls.Add(this.cboReminder);
            this.Controls.Add(this.tabContainer);
            this.Controls.Add(this.chkReview);
            this.Controls.Add(this.chkPrint);
            this.Controls.Add(this.chkFax);
            this.Controls.Add(this.chkEmail);
            this.Controls.Add(this.chkDelimit);
            this.Controls.Add(this.btnGetCustomers);
            this.Name = "FrmTestMain";
            this.Text = "Auto Send Tester ";
            this.tabContainer.ResumeLayout(false);
            this.tabDbData.ResumeLayout(false);
            this.tabDbData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrCustomerData)).EndInit();
            this.tabCustomerData.ResumeLayout(false);
            this.tabCustomerData.PerformLayout();
            this.tabCryRpt.ResumeLayout(false);
            this.tabTransactionLog.ResumeLayout(false);
            this.tabTransactionLog.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrTransaction)).EndInit();
            this.tabInvoices.ResumeLayout(false);
            this.tabInvoices.PerformLayout();
            this.tabInvoiceDb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrInvoiceData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetCustomers;
        private System.Windows.Forms.CheckBox chkDelimit;
        private System.Windows.Forms.CheckBox chkEmail;
        private System.Windows.Forms.CheckBox chkFax;
        private System.Windows.Forms.CheckBox chkPrint;
        private System.Windows.Forms.CheckBox chkReview;
        private System.Windows.Forms.TabControl tabContainer;
        private System.Windows.Forms.TabPage tabDbData;
        private System.Windows.Forms.DataGridView dgrCustomerData;
        private System.Windows.Forms.TabPage tabCustomerData;
        private System.Windows.Forms.ListBox lstCustomers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNumOfEntries;
        private System.Windows.Forms.TextBox txtNumOfReviewEntries;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNumOfPrintEntries;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNumOfFaxEntries;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNumOfEmailEntries;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstContacts;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNumOfContacts;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSendEmail;
        private System.Windows.Forms.Button btnGenerateReport;
        private System.Windows.Forms.Label lblGenerateReport;
        private System.Windows.Forms.TextBox txtReportNo;
        private System.Windows.Forms.Button btnPrintReport;
        private System.Windows.Forms.Button btnExportToPDF;
        private System.Windows.Forms.Button btnToScreen;
        private System.Windows.Forms.TabPage tabCryRpt;
        private System.Windows.Forms.TextBox txtUpszone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtActionType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabTransactionLog;
        private System.Windows.Forms.DataGridView dgrTransaction;
        private System.Windows.Forms.Button btnGetTransactionTable;
        private System.Windows.Forms.TextBox txtInsertLog;
        private System.Windows.Forms.Button btnInsertLog;
        private System.Windows.Forms.CheckBox chkFailed;
        private System.Windows.Forms.CheckBox ChkError;
        private System.Windows.Forms.Button btnUpdateLog;
        private System.Windows.Forms.TextBox txtSendId;
        private System.Windows.Forms.TextBox txtSendAddr;
        private System.Windows.Forms.Button btnGenerateReports;
        private System.Windows.Forms.Button btnToScreens;
        private System.Windows.Forms.Label lblGenerateReports;
        private System.Windows.Forms.Button btnPrintReports;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.CheckBox cboReminder;
        private System.Windows.Forms.TextBox txtReminder;
        private System.Windows.Forms.TabPage tabInvoices;
        private System.Windows.Forms.Button btnGetInvoices;
        private System.Windows.Forms.ListBox lstInvoiceList;
        private System.Windows.Forms.TextBox txtInvoiceDetails;
        private System.Windows.Forms.TabPage tabInvoiceDb;
        private System.Windows.Forms.DataGridView dgrInvoiceData;
        private System.Windows.Forms.TextBox txtOverdueReminderDays;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListBox lstDatabase;
        private System.Windows.Forms.CheckBox chkSent;
        private System.Windows.Forms.TextBox txtNumAlreadySent;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNotSent;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTerms;
        private System.Windows.Forms.TextBox txtTermsType;
        private System.Windows.Forms.Label lblOverdue;
        private System.Windows.Forms.Button btnOverdue;
    }
}

