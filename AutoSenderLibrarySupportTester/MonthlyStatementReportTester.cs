using System;
using System.Collections.Generic;
using System.Text;
using AutoSenderLibrary;

namespace AutoSenderLibrarySupportTester
{
    #region test crystal report support
    /// <summary>Not all metaframes support crystal reports, please use this to check if crystal report is supported in current metaframe</summary>
    public class MonthlyStatementReportTester
    {

        /// <summary>Not all metaframes support crystal reports, please use this to check if crystal report is supported in current metaframe</summary>
        /// <returns>true if supported</returns>
        public static bool TestCrystalReportSupport()
        {
            bool passed = true;
            try
            {
                MonthlyStatementReport test = new MonthlyStatementReport("0");
            }
            catch
            {
                passed = false;
            }

            return passed;
        }

        /// <summary>Support message</summary>
        /// <returns>Support message</returns>
        public static string TestCrystalReportSupportFailedMessage()
        {
            return "This metaframe does not support the required Crystal Reports tools for it to work correctly.\n"
                 + "Any parts of the program you are running that supports crystal reports will not function.\n"
                 + "Please try running this program on a different metaframe instead.";
        }

        /// <summary>Checks and returns true if the barcode font is installed in this computer</summary>
        public static bool TestBarcodeFontInstalled()
        {
            return CheckForFont("DMSBCode");
        }

        /// <summary>Support message</summary>
        /// <returns>Support message</returns>
        public static string TestBarcodeFontInstalledFailedMessage()
        {
            return "Sorry, this server does not have the barcode font (DMSBCode) installed. \n\nPlease ask IT to install this font on " + System.Environment.MachineName + " or try running this program on a metaframe which has the font installed (eg. M4)";
        }

        /// <summary> This function returns a boolean value indicating whether the specified font is installed on the current computer </summary>
        /// <param name="font">A string containing the name of the font to check (eg. "Times New Roman")</param>
        public static bool CheckForFont(string font)
        {
            //Get a collection of installed fonts
            System.Drawing.Text.InstalledFontCollection fontCollection = new System.Drawing.Text.InstalledFontCollection();

            //Loop through each font and check it against the search term
            foreach (System.Drawing.FontFamily family in fontCollection.Families)
            {
                //Check the curent font against the search term
                if (family.Name == font)
                {
                    //Font Found: Return Successful!
                    return true;
                }
            }

            //If execution reaches this point, the font was not found in the collection. Return Fail Result
            return false;
        }

    }
    #endregion
}
