using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;
using AutoSenderLogin;

namespace InternalErrorUpdater
{
    public partial class frmInternalErrorToolbox : Form
    {
        StatementActionsLogger _logger;

        public frmInternalErrorToolbox()
        {
            // Login (AU only)
            FormLogin loginForm = new FormLogin();
            loginForm.AllowDbChange = false;
            DatabaseEnvironment db = DatabaseEnvironment.AU;
            if (!loginForm.IsAuthenticated(db))
            {
                loginForm.ShowDialog();

                if (loginForm.DialogResult == DialogResult.Cancel)
                {
                    // Exit with failure
                    System.Environment.Exit(1);
                }
            }
            if (!loginForm.HasAdminPermission(db))
            {
                MessageBox.Show("Your account does not give you permission to this form, please contact IT or login as a different user");
                System.Environment.Exit(1);
            }
            //------------------------------------------

            InitializeComponent();

            _logger = new StatementActionsLogger();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvSendID.Rows)
            {
                if (!row.IsNewRow)
                {
                    int i;

                    if (int.TryParse(row.Cells["sendId"].Value.ToString(), out i))
                    {
#if !DEBUG
                        _logger.UpdateSendLog(i, row.Cells["SendAddress"].Value.ToString(), row.Cells["ErrorStatus"].Value.ToString() == "Failed" ? true : false, row.Cells["ErrorStatus"].Value.ToString() == "Internal Error" ? true : false);
#endif
                        row.Cells["updateStatus"].Value = "Success";
                    }
                    else
                    {
                        row.Cells["updateStatus"].Value = "Failed";
                    }
                }
            }

            /*
            int fromId;
            int toId;

            if (int.TryParse(txtFrom.Text, out fromId) == true && int.TryParse(txtTo.Text, out toId) == true)
            {
                int i;
                for (i = fromId; i <= toId; i++)
                {
                    _logger.UpdateSendLog(i, rdoFailed.Checked, rdoInternalError.Checked);
                }
            }
            */
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dgvSendID.Rows.Clear();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int fromId;
            int toId;

            if (int.TryParse(txtFrom.Text, out fromId) == true && int.TryParse(txtTo.Text, out toId) == true)
            {
                int i;
                for (i = fromId; i <= toId; i++)
                {
                    foreach (SendLogAddress sendlogaddressentry in SendLog.GetFromSendId(i).SendAddresses)
                    {
                        int curRow = dgvSendID.Rows.Add();
                        dgvSendID.Rows[curRow].Cells["sendId"].Value = i;
                        dgvSendID.Rows[curRow].Cells["sendAddress"].Value = sendlogaddressentry.SendAddress;

                        SendLogAddressResultLog lastResultLog = new SendLogAddressResultLog(i, sendlogaddressentry.SendAddress, "", DateTime.MinValue, DateTime.MinValue, 0, "");
                        foreach (SendLogAddressResultLog resultLog in sendlogaddressentry.GetResultLogs())
                        {
                            if (resultLog.DateProcessed > lastResultLog.DateProcessed)
                                lastResultLog = resultLog;
                        }

                        string status = "";
                        if (!sendlogaddressentry.InternalError && !sendlogaddressentry.Failed)
                            status = "No Error";
                        else if (sendlogaddressentry.Failed)
                            status = "Failed";
                        else if (sendlogaddressentry.InternalError)
                            status = "Internal Error";

                        dgvSendID.Rows[curRow].Cells["ErrorStatus"].Value = status;
                    }
                }
            }
        }

        private void dgvSendID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                DataObject d = dgvSendID.GetClipboardContent();
                Clipboard.SetDataObject(d);
                e.Handled = true;
            }
            else if (e.Control && e.KeyCode == Keys.V)
            {
                CopyToDatagrid();
            }
        }

        #region Datagridview
        private void ClearItemTable()
        {
            dgvSendID.Rows.Clear();
        }


        private void CopyToDatagrid()
        {
            // Get values from clipboard
            string s = Clipboard.GetText();

            // Split values according to lines
            string[] lines = s.Split('\n');

            // Get current row, columns
            int row = dgvSendID.CurrentCell.RowIndex;

            foreach (string line in lines)
            {
                // In case more than one column is copied, in which case, only the first tab sepearated value is taken and saved
                string[] cells = line.Split('\t');

                // Check if copied cell contains values
                if (!cells[0].Trim().Equals(""))
                {
                    // Add a new row if newrow reached
                    if (dgvSendID["sendId", row].OwningRow.IsNewRow)
                    {
                        dgvSendID.Rows.Add();
                    }

                    // Save itemcode
                    dgvSendID["sendId", row].Value = cells[0].TrimEnd('\r');
                    dgvSendID["sendAddress", row].Value = cells.Length >= 2 ? cells[1].TrimEnd('\r') : "";
                    dgvSendID["ErrorStatus", row].Value = cells.Length >= 3 && (cells[2].TrimEnd('\r') == "Internal Error" || cells[2].TrimEnd('\r') == "Failed" || cells[2].TrimEnd('\r') == "No Error") ? cells[2].TrimEnd('\r') : "";

                    row++;
                }
            }

            dgvSendID.Refresh();
        }
        #endregion

        private void btnUpdateAll_Click(object sender, EventArgs e)
        {
            int currentrow = 0;

            while (!dgvSendID["sendId", currentrow].OwningRow.IsNewRow)
            {
                string newstatus = "";
                if (rdoFailed.Checked)
                    newstatus = "Failed";
                else if (rdoInternalError.Checked)
                    newstatus = "Internal Error";
                else if (rdoNoError.Checked)
                    newstatus = "No Error";

                dgvSendID["ErrorStatus", currentrow].Value = newstatus;
                currentrow++;
            }
        }

    }
}