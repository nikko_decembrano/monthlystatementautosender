namespace InternalErrorUpdater
{
    partial class frmInternalErrorToolbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.rdoInternalError = new System.Windows.Forms.RadioButton();
            this.rdoFailed = new System.Windows.Forms.RadioButton();
            this.txtFrom = new System.Windows.Forms.TextBox();
            this.txtTo = new System.Windows.Forms.TextBox();
            this.dgvSendID = new System.Windows.Forms.DataGridView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.grpUpdateAll = new System.Windows.Forms.GroupBox();
            this.btnUpdateAll = new System.Windows.Forms.Button();
            this.SendId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SendAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ErrorStatus = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.updateStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rdoNoError = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSendID)).BeginInit();
            this.grpUpdateAll.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "To";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(653, 405);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // rdoInternalError
            // 
            this.rdoInternalError.AutoSize = true;
            this.rdoInternalError.Checked = true;
            this.rdoInternalError.Location = new System.Drawing.Point(9, 19);
            this.rdoInternalError.Name = "rdoInternalError";
            this.rdoInternalError.Size = new System.Drawing.Size(85, 17);
            this.rdoInternalError.TabIndex = 4;
            this.rdoInternalError.TabStop = true;
            this.rdoInternalError.Text = "Internal Error";
            this.rdoInternalError.UseVisualStyleBackColor = true;
            // 
            // rdoFailed
            // 
            this.rdoFailed.AutoSize = true;
            this.rdoFailed.Location = new System.Drawing.Point(100, 19);
            this.rdoFailed.Name = "rdoFailed";
            this.rdoFailed.Size = new System.Drawing.Size(53, 17);
            this.rdoFailed.TabIndex = 5;
            this.rdoFailed.TabStop = true;
            this.rdoFailed.Text = "Failed";
            this.rdoFailed.UseVisualStyleBackColor = true;
            // 
            // txtFrom
            // 
            this.txtFrom.Location = new System.Drawing.Point(48, 9);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(100, 20);
            this.txtFrom.TabIndex = 1;
            // 
            // txtTo
            // 
            this.txtTo.Location = new System.Drawing.Point(48, 35);
            this.txtTo.Name = "txtTo";
            this.txtTo.Size = new System.Drawing.Size(100, 20);
            this.txtTo.TabIndex = 2;
            // 
            // dgvSendID
            // 
            this.dgvSendID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSendID.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSendID.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SendId,
            this.SendAddress,
            this.ErrorStatus,
            this.updateStatus});
            this.dgvSendID.Location = new System.Drawing.Point(12, 101);
            this.dgvSendID.Name = "dgvSendID";
            this.dgvSendID.Size = new System.Drawing.Size(700, 283);
            this.dgvSendID.TabIndex = 8;
            this.dgvSendID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSendID_KeyDown);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(48, 63);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(46, 23);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(102, 63);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(46, 23);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // grpUpdateAll
            // 
            this.grpUpdateAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpUpdateAll.Controls.Add(this.rdoNoError);
            this.grpUpdateAll.Controls.Add(this.rdoFailed);
            this.grpUpdateAll.Controls.Add(this.rdoInternalError);
            this.grpUpdateAll.Controls.Add(this.btnUpdateAll);
            this.grpUpdateAll.Location = new System.Drawing.Point(383, 10);
            this.grpUpdateAll.Name = "grpUpdateAll";
            this.grpUpdateAll.Size = new System.Drawing.Size(329, 75);
            this.grpUpdateAll.TabIndex = 16;
            this.grpUpdateAll.TabStop = false;
            this.grpUpdateAll.Text = "Update All";
            // 
            // btnUpdateAll
            // 
            this.btnUpdateAll.Location = new System.Drawing.Point(242, 16);
            this.btnUpdateAll.Name = "btnUpdateAll";
            this.btnUpdateAll.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateAll.TabIndex = 16;
            this.btnUpdateAll.Text = "Update All";
            this.btnUpdateAll.UseVisualStyleBackColor = true;
            this.btnUpdateAll.Click += new System.EventHandler(this.btnUpdateAll_Click);
            // 
            // SendId
            // 
            this.SendId.HeaderText = "Send ID";
            this.SendId.Name = "SendId";
            // 
            // SendAddress
            // 
            this.SendAddress.HeaderText = "Send Address";
            this.SendAddress.Name = "SendAddress";
            this.SendAddress.Width = 350;
            // 
            // ErrorStatus
            // 
            this.ErrorStatus.HeaderText = "ErrorStatus";
            this.ErrorStatus.Items.AddRange(new object[] {
            "Internal Error",
            "Failed",
            "No Error"});
            this.ErrorStatus.Name = "ErrorStatus";
            this.ErrorStatus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ErrorStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // updateStatus
            // 
            this.updateStatus.HeaderText = "Update Status";
            this.updateStatus.Name = "updateStatus";
            this.updateStatus.ReadOnly = true;
            // 
            // rdoNoError
            // 
            this.rdoNoError.AutoSize = true;
            this.rdoNoError.Location = new System.Drawing.Point(172, 19);
            this.rdoNoError.Name = "rdoNoError";
            this.rdoNoError.Size = new System.Drawing.Size(64, 17);
            this.rdoNoError.TabIndex = 17;
            this.rdoNoError.TabStop = true;
            this.rdoNoError.Text = "No Error";
            this.rdoNoError.UseVisualStyleBackColor = true;
            // 
            // frmInternalErrorToolbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 440);
            this.Controls.Add(this.grpUpdateAll);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dgvSendID);
            this.Controls.Add(this.txtTo);
            this.Controls.Add(this.txtFrom);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmInternalErrorToolbox";
            this.Text = "Send History Toolbox";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSendID)).EndInit();
            this.grpUpdateAll.ResumeLayout(false);
            this.grpUpdateAll.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.RadioButton rdoInternalError;
        private System.Windows.Forms.RadioButton rdoFailed;
        private System.Windows.Forms.TextBox txtFrom;
        private System.Windows.Forms.TextBox txtTo;
        private System.Windows.Forms.DataGridView dgvSendID;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.GroupBox grpUpdateAll;
        private System.Windows.Forms.Button btnUpdateAll;
        private System.Windows.Forms.DataGridViewTextBoxColumn SendId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SendAddress;
        private System.Windows.Forms.DataGridViewComboBoxColumn ErrorStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn updateStatus;
        private System.Windows.Forms.RadioButton rdoNoError;
    }
}

