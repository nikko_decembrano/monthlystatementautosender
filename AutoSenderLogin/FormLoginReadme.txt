Add a reference to AutoSenderLogin Project then 
Paste the below codes into the constructor before InitialiseComponent to have the user login before the form opens
or if user log in with environmental values (eg NetCRM with @user @password @Database, will check those and login if needed)

The below code block will keep prompting the user to login until exit or escape is pressed.
-- Qingyou Lu

// Paste into top of form
using AutoSenderLogin;
//------------------------------------------

// Paste into Constructor before 
// Login (AU only)
FormLogin loginForm = new FormLogin();
loginForm.AllowDbChange = false;
DatabaseEnvironment db = DatabaseEnvironment.AU;
if (!loginForm.IsAuthenticated(db))
{
    loginForm.ShowDialog();

    if (loginForm.DialogResult == DialogResult.Cancel)
    {
        // Exit with failure
        System.Environment.Exit(1);
    }
}
if (!loginForm.HasEsendPermission(db))
{
    MessageBox.Show("Your account does not give you permission to print statements, please contact IT or login as a different user");
    System.Environment.Exit(1);
}
//------------------------------------------