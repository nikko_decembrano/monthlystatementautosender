using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;

namespace AutoSenderLogin
{
    /// <summary>
    /// For performing Login functions in classes such as AutoSenderServer and AutoSenderClient
    /// Not to be used by iteself
    /// </summary>
    public partial class FormLogin : Form
    {
        Dictionary<DatabaseEnvironment, string> _logins;
        bool _allowDbChange;

        public FormLogin()
        {
            string username = "";
            string password = "";
            string database = "AU";
            _allowDbChange = true;

            // check for command line args
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length >= 2)
                username = args[1];
            if (args.Length >= 3)
                password = args[2];
            if (args.Length >= 4)
                database = args[3].ToUpper();

            InitEnvironment(username, password, database);
        }

        public FormLogin(string username, string password, string database)
        {
            _allowDbChange = true;
            InitEnvironment(username, password, database);
            CheckLogin(username, password, database);
        }

        private void InitEnvironment(string username, string password, string database)
        {
            InitializeComponent();
            lblError.Text = "";

            _logins = new Dictionary<DatabaseEnvironment, string>();

            // Get list of environments to populate cboDatabase combo box
            DatabaseEnvironment[] dbs = DatabaseEnvironments.GetEnvironments();

            foreach (DatabaseEnvironment db in dbs)
            {
                cboDatabase.Items.Add(db.ToString());
            }

            CheckLogin(username, password, database);

            cboDatabase.SelectedItem = database;
            RefreshText();
        }

        public bool AllowDbChange
        {
            get { return _allowDbChange; }
            set 
            { 
                _allowDbChange = value;
                RefreshText();
            }
        }

        public bool IsAuthenticated(DatabaseEnvironment db)
        {
            return _logins.ContainsKey(db);
        }

        public bool HasAdminPermission(DatabaseEnvironment db)
        {
            bool result = false;

            if (_logins.ContainsKey(db))
            {
                result = UserLogin.HasAdministratorPermission(GetUsername(db), db);
            }

            return result;
        }

        public bool HasEsendPermission(DatabaseEnvironment db)
        {
            bool result = false;

            if (_logins.ContainsKey(db))
            {
                result = UserLogin.HasESendStatementPermission(GetUsername(db), db);
            }

            return result;
        }

        public bool HasPrintPermission(DatabaseEnvironment db)
        {
            bool result = false;

            if (_logins.ContainsKey(db))
            {
                result = UserLogin.HasPrintStatementPermission(GetUsername(db), db);
            }

            return result;
        }

        public DatabaseEnvironment[] AuthenticatedDbs()
        {
            List<DatabaseEnvironment> dbs = new List<DatabaseEnvironment>();
            
            foreach(DatabaseEnvironment db in _logins.Keys)
            {
                dbs.Add(db);
            }

            return dbs.ToArray();
        }

        public string GetUsername(DatabaseEnvironment db)
        {
            string username = "";
            
            if (IsAuthenticated(db))
            {
                _logins.TryGetValue(db, out username);
            }

            return username;
        }

        public void SwitchDb(DatabaseEnvironment db)
        {
            cboDatabase.SelectedItem = db.ToString();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            btnLoginCheck();
        }

        private void btnLoginCheck()
        {
            if (btnLogin.Text == "Login")
            {
                if (!CheckLogin())
                {
                    lblError.Text = "Password Error, please try again";
                    this.DialogResult = DialogResult.None;
                }
            }
            else
            {
                // Logout
                _logins.Remove(DatabaseEnvironments.GetEnvironmentFromString(DbComboBoxGet()));
                RefreshText();
                this.DialogResult = DialogResult.None;
            }
        }

        private bool CheckLogin()
        {
            string username = txtUser.Text;
            string password = txtPass.Text;
            string database = DbComboBoxGet();

            return CheckLogin(username, password, database);
        }

        private bool CheckLogin(string username, string password, string database)
        {
            bool result = false;

            if (database != "")
            {
                if (UserLogin.CheckPassword(username, password, database))
                {
                    _logins.Add(DatabaseEnvironments.GetEnvironmentFromString(database), username);
                    result = true;
                }
            }

            return result;
        }

        private string DbComboBoxGet()
        {
            return cboDatabase.SelectedItem == null ? "" : cboDatabase.SelectedItem.ToString();
        }

        private void cboDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshText();
        }

        private void RefreshText()
        {
            DatabaseEnvironment db = DatabaseEnvironments.GetEnvironmentFromString(DbComboBoxGet());
            cboDatabase.Enabled = _allowDbChange;

            if (IsAuthenticated(db))
            {
                txtUser.Enabled = false;
                txtUser.Text = GetUsername(db);
                txtPass.Enabled = false;
                txtPass.Text = "";
                btnLogin.Text = "Logout";
            }
            else
            {
                txtUser.Enabled = true;
                txtPass.Enabled = true;
                txtPass.Text = "";
                btnLogin.Text = "Login";
            }
        }

        private void FormLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtUser.Text != "" && txtPass.Text != "" && cboDatabase.SelectedItem != null)
            {
                this.DialogResult = DialogResult.OK;
                btnLoginCheck();
            }
            if (e.KeyCode == Keys.Escape)
                this.DialogResult = DialogResult.Cancel;
        }
    }
}