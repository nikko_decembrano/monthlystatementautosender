namespace StatementActionsLoggerTest1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCusNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSendLog = new System.Windows.Forms.Button();
            this.gboType = new System.Windows.Forms.GroupBox();
            this.rdoStatement = new System.Windows.Forms.RadioButton();
            this.rdoReminder = new System.Windows.Forms.RadioButton();
            this.btnPrintLog = new System.Windows.Forms.Button();
            this.btnUpdateFailed = new System.Windows.Forms.Button();
            this.btnInternalError = new System.Windows.Forms.Button();
            this.txtSendId = new System.Windows.Forms.TextBox();
            this.rdoA = new System.Windows.Forms.RadioButton();
            this.rdoB = new System.Windows.Forms.RadioButton();
            this.rdoO = new System.Windows.Forms.RadioButton();
            this.rdoAll = new System.Windows.Forms.RadioButton();
            this.gboType.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCusNo
            // 
            this.txtCusNo.Location = new System.Drawing.Point(82, 25);
            this.txtCusNo.Name = "txtCusNo";
            this.txtCusNo.Size = new System.Drawing.Size(100, 20);
            this.txtCusNo.TabIndex = 0;
            this.txtCusNo.Text = "100073480";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "cus_no";
            // 
            // btnSendLog
            // 
            this.btnSendLog.Location = new System.Drawing.Point(30, 83);
            this.btnSendLog.Name = "btnSendLog";
            this.btnSendLog.Size = new System.Drawing.Size(101, 23);
            this.btnSendLog.TabIndex = 2;
            this.btnSendLog.Text = "create send log";
            this.btnSendLog.UseVisualStyleBackColor = true;
            this.btnSendLog.Click += new System.EventHandler(this.btnSendLog_Click);
            // 
            // gboType
            // 
            this.gboType.Controls.Add(this.rdoReminder);
            this.gboType.Controls.Add(this.rdoStatement);
            this.gboType.Location = new System.Drawing.Point(186, 62);
            this.gboType.Name = "gboType";
            this.gboType.Size = new System.Drawing.Size(102, 61);
            this.gboType.TabIndex = 3;
            this.gboType.TabStop = false;
            this.gboType.Text = "Type";
            // 
            // rdoStatement
            // 
            this.rdoStatement.AutoSize = true;
            this.rdoStatement.Checked = true;
            this.rdoStatement.Location = new System.Drawing.Point(6, 19);
            this.rdoStatement.Name = "rdoStatement";
            this.rdoStatement.Size = new System.Drawing.Size(71, 17);
            this.rdoStatement.TabIndex = 0;
            this.rdoStatement.TabStop = true;
            this.rdoStatement.Text = "statement";
            this.rdoStatement.UseVisualStyleBackColor = true;
            // 
            // rdoReminder
            // 
            this.rdoReminder.AutoSize = true;
            this.rdoReminder.Location = new System.Drawing.Point(6, 38);
            this.rdoReminder.Name = "rdoReminder";
            this.rdoReminder.Size = new System.Drawing.Size(65, 17);
            this.rdoReminder.TabIndex = 1;
            this.rdoReminder.Text = "reminder";
            this.rdoReminder.UseVisualStyleBackColor = true;
            // 
            // btnPrintLog
            // 
            this.btnPrintLog.Location = new System.Drawing.Point(30, 143);
            this.btnPrintLog.Name = "btnPrintLog";
            this.btnPrintLog.Size = new System.Drawing.Size(101, 23);
            this.btnPrintLog.TabIndex = 6;
            this.btnPrintLog.Text = "create print log";
            this.btnPrintLog.UseVisualStyleBackColor = true;
            this.btnPrintLog.Click += new System.EventHandler(this.btnPrintLog_Click);
            // 
            // btnUpdateFailed
            // 
            this.btnUpdateFailed.Location = new System.Drawing.Point(205, 216);
            this.btnUpdateFailed.Name = "btnUpdateFailed";
            this.btnUpdateFailed.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateFailed.TabIndex = 7;
            this.btnUpdateFailed.Text = "Failed";
            this.btnUpdateFailed.UseVisualStyleBackColor = true;
            this.btnUpdateFailed.Click += new System.EventHandler(this.btnUpdateFailed_Click);
            // 
            // btnInternalError
            // 
            this.btnInternalError.Location = new System.Drawing.Point(205, 245);
            this.btnInternalError.Name = "btnInternalError";
            this.btnInternalError.Size = new System.Drawing.Size(75, 23);
            this.btnInternalError.TabIndex = 8;
            this.btnInternalError.Text = "InternalError";
            this.btnInternalError.UseVisualStyleBackColor = true;
            this.btnInternalError.Click += new System.EventHandler(this.btnInternalError_Click);
            // 
            // txtSendId
            // 
            this.txtSendId.Location = new System.Drawing.Point(30, 190);
            this.txtSendId.Name = "txtSendId";
            this.txtSendId.Size = new System.Drawing.Size(100, 20);
            this.txtSendId.TabIndex = 9;
            this.txtSendId.Text = "9177";
            // 
            // rdoA
            // 
            this.rdoA.AutoSize = true;
            this.rdoA.Location = new System.Drawing.Point(30, 216);
            this.rdoA.Name = "rdoA";
            this.rdoA.Size = new System.Drawing.Size(31, 17);
            this.rdoA.TabIndex = 10;
            this.rdoA.TabStop = true;
            this.rdoA.Text = "a";
            this.rdoA.UseVisualStyleBackColor = true;
            // 
            // rdoB
            // 
            this.rdoB.AutoSize = true;
            this.rdoB.Location = new System.Drawing.Point(30, 234);
            this.rdoB.Name = "rdoB";
            this.rdoB.Size = new System.Drawing.Size(31, 17);
            this.rdoB.TabIndex = 11;
            this.rdoB.TabStop = true;
            this.rdoB.Text = "b";
            this.rdoB.UseVisualStyleBackColor = true;
            // 
            // rdoO
            // 
            this.rdoO.AutoSize = true;
            this.rdoO.Location = new System.Drawing.Point(30, 251);
            this.rdoO.Name = "rdoO";
            this.rdoO.Size = new System.Drawing.Size(51, 17);
            this.rdoO.TabIndex = 12;
            this.rdoO.TabStop = true;
            this.rdoO.Text = "o\'neill";
            this.rdoO.UseVisualStyleBackColor = true;
            // 
            // rdoAll
            // 
            this.rdoAll.AutoSize = true;
            this.rdoAll.Location = new System.Drawing.Point(30, 266);
            this.rdoAll.Name = "rdoAll";
            this.rdoAll.Size = new System.Drawing.Size(47, 17);
            this.rdoAll.TabIndex = 13;
            this.rdoAll.TabStop = true;
            this.rdoAll.Text = "<all>";
            this.rdoAll.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 295);
            this.Controls.Add(this.rdoAll);
            this.Controls.Add(this.rdoO);
            this.Controls.Add(this.rdoB);
            this.Controls.Add(this.rdoA);
            this.Controls.Add(this.txtSendId);
            this.Controls.Add(this.btnInternalError);
            this.Controls.Add(this.btnUpdateFailed);
            this.Controls.Add(this.btnPrintLog);
            this.Controls.Add(this.gboType);
            this.Controls.Add(this.btnSendLog);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCusNo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gboType.ResumeLayout(false);
            this.gboType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCusNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSendLog;
        private System.Windows.Forms.GroupBox gboType;
        private System.Windows.Forms.RadioButton rdoReminder;
        private System.Windows.Forms.RadioButton rdoStatement;
        private System.Windows.Forms.Button btnPrintLog;
        private System.Windows.Forms.Button btnUpdateFailed;
        private System.Windows.Forms.Button btnInternalError;
        private System.Windows.Forms.TextBox txtSendId;
        private System.Windows.Forms.RadioButton rdoA;
        private System.Windows.Forms.RadioButton rdoB;
        private System.Windows.Forms.RadioButton rdoO;
        private System.Windows.Forms.RadioButton rdoAll;
    }
}

