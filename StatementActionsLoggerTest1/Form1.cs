using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;

namespace StatementActionsLoggerTest1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSendLog_Click(object sender, EventArgs e)
        {
            StatementActionsLogger log = new StatementActionsLogger();

            CustomerContact c = new CustomerContact(txtCusNo.Text, "", ActionType.Email,"tx");
            c.addDetails(new string[] {"a@livingstone.com.au", "b@livingstone.com.au", "o'neill@livingstone.com.au"});

            int reminder = 0;

            if (rdoReminder.Checked)
            {
                reminder = 1;
            }

            log.InsertSendLog(c, 0, 3, reminder);
        }

        private void btnPrintLog_Click(object sender, EventArgs e)
        {
            StatementActionsLogger log = new StatementActionsLogger();

            CustomerContact c = new CustomerContact(txtCusNo.Text, "", ActionType.Email, "tx");
            c.addDetails(new string[] { "a@livingstone.com.au", "b@livingstone.com.au", "o'neill@livingstone.com.au"});

            log.InsertPrintLog(c);
        }

        private void btnUpdateFailed_Click(object sender, EventArgs e)
        {
            UpdateLog(true, false);
        }
        
        private void btnInternalError_Click(object sender, EventArgs e)
        {
            UpdateLog(false, true);
        }

        private string SendAddress()
        {
            if (rdoA.Checked)
            {
                return "a@livingstone.com.au";
            }
            if (rdoB.Checked)
            {
                return "b@livingstone.com.au";
            }
            if (rdoO.Checked)
            {
                return "o'neill@livingstone.com.au";
            }

            return null;
        }

        private void UpdateLog(bool failed, bool internalerror)
        {
            StatementActionsLogger log = new StatementActionsLogger();

            log.UpdateSendLog(int.Parse(txtSendId.Text), SendAddress(), failed, internalerror);
        }
    }
}