namespace CheckBouncedEmails
{
    partial class FrmDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAnalysisStatus = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.outerBarFaxError = new System.Windows.Forms.Panel();
            this.barFaxError = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.outerBarFaxFail = new System.Windows.Forms.Panel();
            this.barFaxFail = new System.Windows.Forms.Label();
            this.outerBarFaxSuccess = new System.Windows.Forms.Panel();
            this.barFaxSuccess = new System.Windows.Forms.Label();
            this.outerBarEmailFail = new System.Windows.Forms.Panel();
            this.barEmailFail = new System.Windows.Forms.Label();
            this.outerBarEmailSuccess = new System.Windows.Forms.Panel();
            this.barEmailSuccess = new System.Windows.Forms.Label();
            this.outerBarProgress = new System.Windows.Forms.Panel();
            this.barProgress = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnAction1 = new System.Windows.Forms.Button();
            this.txtItemsToProcess = new System.Windows.Forms.TextBox();
            this.cboMailbox = new System.Windows.Forms.ComboBox();
            this.lblReadyToProcess = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.outerBarOther = new System.Windows.Forms.Panel();
            this.barOther = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.outerBarFaxError.SuspendLayout();
            this.outerBarFaxFail.SuspendLayout();
            this.outerBarFaxSuccess.SuspendLayout();
            this.outerBarEmailFail.SuspendLayout();
            this.outerBarEmailSuccess.SuspendLayout();
            this.outerBarProgress.SuspendLayout();
            this.outerBarOther.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAnalysisStatus
            // 
            this.lblAnalysisStatus.AutoSize = true;
            this.lblAnalysisStatus.Location = new System.Drawing.Point(148, 14);
            this.lblAnalysisStatus.Name = "lblAnalysisStatus";
            this.lblAnalysisStatus.Size = new System.Drawing.Size(124, 13);
            this.lblAnalysisStatus.TabIndex = 0;
            this.lblAnalysisStatus.Text = "Inbox Analysis Complete!";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.outerBarOther);
            this.groupBox1.Controls.Add(this.outerBarFaxError);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.outerBarFaxFail);
            this.groupBox1.Controls.Add(this.outerBarFaxSuccess);
            this.groupBox1.Controls.Add(this.outerBarEmailFail);
            this.groupBox1.Controls.Add(this.outerBarEmailSuccess);
            this.groupBox1.Location = new System.Drawing.Point(12, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(406, 204);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Results";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(130)))), ((int)(((byte)(51)))));
            this.label11.Location = new System.Drawing.Point(155, 165);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Success";
            // 
            // outerBarFaxError
            // 
            this.outerBarFaxError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(223)))), ((int)(((byte)(208)))));
            this.outerBarFaxError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outerBarFaxError.Controls.Add(this.barFaxError);
            this.outerBarFaxError.Location = new System.Drawing.Point(209, 55);
            this.outerBarFaxError.Name = "outerBarFaxError";
            this.outerBarFaxError.Size = new System.Drawing.Size(22, 107);
            this.outerBarFaxError.TabIndex = 0;
            // 
            // barFaxError
            // 
            this.barFaxError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.barFaxError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(174)))), ((int)(((byte)(135)))));
            this.barFaxError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barFaxError.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barFaxError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(71)))), ((int)(((byte)(26)))));
            this.barFaxError.Location = new System.Drawing.Point(-1, 38);
            this.barFaxError.Name = "barFaxError";
            this.barFaxError.Size = new System.Drawing.Size(22, 68);
            this.barFaxError.TabIndex = 0;
            this.barFaxError.Text = "0";
            this.barFaxError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.SteelBlue;
            this.label6.Location = new System.Drawing.Point(203, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Faxes";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(58)))), ((int)(((byte)(58)))));
            this.label12.Location = new System.Drawing.Point(238, 165);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Failed";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(108)))), ((int)(((byte)(11)))));
            this.label3.Location = new System.Drawing.Point(198, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "Internal Error";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(58)))), ((int)(((byte)(58)))));
            this.label10.Location = new System.Drawing.Point(76, 165);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Failed";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(130)))), ((int)(((byte)(51)))));
            this.label9.Location = new System.Drawing.Point(27, 165);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Success";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.SteelBlue;
            this.label5.Location = new System.Drawing.Point(57, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Emails";
            // 
            // outerBarFaxFail
            // 
            this.outerBarFaxFail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(213)))), ((int)(((byte)(213)))));
            this.outerBarFaxFail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outerBarFaxFail.Controls.Add(this.barFaxFail);
            this.outerBarFaxFail.Location = new System.Drawing.Point(241, 55);
            this.outerBarFaxFail.Name = "outerBarFaxFail";
            this.outerBarFaxFail.Size = new System.Drawing.Size(22, 107);
            this.outerBarFaxFail.TabIndex = 0;
            // 
            // barFaxFail
            // 
            this.barFaxFail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.barFaxFail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(112)))), ((int)(((byte)(112)))));
            this.barFaxFail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barFaxFail.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barFaxFail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(26)))), ((int)(((byte)(30)))));
            this.barFaxFail.Location = new System.Drawing.Point(-1, 38);
            this.barFaxFail.Name = "barFaxFail";
            this.barFaxFail.Size = new System.Drawing.Size(22, 68);
            this.barFaxFail.TabIndex = 0;
            this.barFaxFail.Text = "0";
            this.barFaxFail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // outerBarFaxSuccess
            // 
            this.outerBarFaxSuccess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(222)))), ((int)(((byte)(190)))));
            this.outerBarFaxSuccess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outerBarFaxSuccess.Controls.Add(this.barFaxSuccess);
            this.outerBarFaxSuccess.Location = new System.Drawing.Point(176, 55);
            this.outerBarFaxSuccess.Name = "outerBarFaxSuccess";
            this.outerBarFaxSuccess.Size = new System.Drawing.Size(22, 107);
            this.outerBarFaxSuccess.TabIndex = 0;
            // 
            // barFaxSuccess
            // 
            this.barFaxSuccess.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.barFaxSuccess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(174)))), ((int)(((byte)(34)))));
            this.barFaxSuccess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barFaxSuccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barFaxSuccess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(71)))), ((int)(((byte)(26)))));
            this.barFaxSuccess.Location = new System.Drawing.Point(-1, 38);
            this.barFaxSuccess.Name = "barFaxSuccess";
            this.barFaxSuccess.Size = new System.Drawing.Size(22, 68);
            this.barFaxSuccess.TabIndex = 0;
            this.barFaxSuccess.Text = "0";
            this.barFaxSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // outerBarEmailFail
            // 
            this.outerBarEmailFail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(213)))), ((int)(((byte)(213)))));
            this.outerBarEmailFail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outerBarEmailFail.Controls.Add(this.barEmailFail);
            this.outerBarEmailFail.Location = new System.Drawing.Point(79, 55);
            this.outerBarEmailFail.Name = "outerBarEmailFail";
            this.outerBarEmailFail.Size = new System.Drawing.Size(22, 107);
            this.outerBarEmailFail.TabIndex = 0;
            // 
            // barEmailFail
            // 
            this.barEmailFail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.barEmailFail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(112)))), ((int)(((byte)(112)))));
            this.barEmailFail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barEmailFail.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barEmailFail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(26)))), ((int)(((byte)(30)))));
            this.barEmailFail.Location = new System.Drawing.Point(-1, 38);
            this.barEmailFail.Name = "barEmailFail";
            this.barEmailFail.Size = new System.Drawing.Size(22, 68);
            this.barEmailFail.TabIndex = 0;
            this.barEmailFail.Text = "0";
            this.barEmailFail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // outerBarEmailSuccess
            // 
            this.outerBarEmailSuccess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(222)))), ((int)(((byte)(190)))));
            this.outerBarEmailSuccess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outerBarEmailSuccess.Controls.Add(this.barEmailSuccess);
            this.outerBarEmailSuccess.Location = new System.Drawing.Point(48, 55);
            this.outerBarEmailSuccess.Name = "outerBarEmailSuccess";
            this.outerBarEmailSuccess.Size = new System.Drawing.Size(22, 107);
            this.outerBarEmailSuccess.TabIndex = 0;
            // 
            // barEmailSuccess
            // 
            this.barEmailSuccess.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.barEmailSuccess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(174)))), ((int)(((byte)(34)))));
            this.barEmailSuccess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barEmailSuccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barEmailSuccess.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(71)))), ((int)(((byte)(26)))));
            this.barEmailSuccess.Location = new System.Drawing.Point(-1, 38);
            this.barEmailSuccess.Name = "barEmailSuccess";
            this.barEmailSuccess.Size = new System.Drawing.Size(22, 68);
            this.barEmailSuccess.TabIndex = 0;
            this.barEmailSuccess.Text = "0";
            this.barEmailSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // outerBarProgress
            // 
            this.outerBarProgress.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.outerBarProgress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(174)))), ((int)(((byte)(181)))), ((int)(((byte)(206)))));
            this.outerBarProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outerBarProgress.Controls.Add(this.barProgress);
            this.outerBarProgress.Location = new System.Drawing.Point(124, 64);
            this.outerBarProgress.Name = "outerBarProgress";
            this.outerBarProgress.Size = new System.Drawing.Size(198, 18);
            this.outerBarProgress.TabIndex = 0;
            // 
            // barProgress
            // 
            this.barProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.barProgress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(142)))));
            this.barProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barProgress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(254)))));
            this.barProgress.Location = new System.Drawing.Point(-1, -1);
            this.barProgress.Name = "barProgress";
            this.barProgress.Size = new System.Drawing.Size(156, 18);
            this.barProgress.TabIndex = 0;
            this.barProgress.Text = "325 / 500";
            this.barProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.SteelBlue;
            this.label13.Location = new System.Drawing.Point(181, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Overall Progress";
            // 
            // btnAction1
            // 
            this.btnAction1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction1.Location = new System.Drawing.Point(350, 311);
            this.btnAction1.Name = "btnAction1";
            this.btnAction1.Size = new System.Drawing.Size(75, 23);
            this.btnAction1.TabIndex = 2;
            this.btnAction1.Text = "Begin";
            this.btnAction1.UseVisualStyleBackColor = true;
            this.btnAction1.Click += new System.EventHandler(this.btnAction1_Click);
            // 
            // txtItemsToProcess
            // 
            this.txtItemsToProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtItemsToProcess.Location = new System.Drawing.Point(360, 9);
            this.txtItemsToProcess.Name = "txtItemsToProcess";
            this.txtItemsToProcess.ReadOnly = true;
            this.txtItemsToProcess.Size = new System.Drawing.Size(58, 20);
            this.txtItemsToProcess.TabIndex = 3;
            this.txtItemsToProcess.Text = "500";
            this.txtItemsToProcess.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cboMailbox
            // 
            this.cboMailbox.FormattingEnabled = true;
            this.cboMailbox.Location = new System.Drawing.Point(8, 10);
            this.cboMailbox.Name = "cboMailbox";
            this.cboMailbox.Size = new System.Drawing.Size(121, 21);
            this.cboMailbox.TabIndex = 4;
            this.cboMailbox.SelectedIndexChanged += new System.EventHandler(this.cboMailbox_SelectedIndexChanged);
            // 
            // lblReadyToProcess
            // 
            this.lblReadyToProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblReadyToProcess.AutoSize = true;
            this.lblReadyToProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReadyToProcess.Location = new System.Drawing.Point(348, 30);
            this.lblReadyToProcess.Name = "lblReadyToProcess";
            this.lblReadyToProcess.Size = new System.Drawing.Size(78, 12);
            this.lblReadyToProcess.TabIndex = 5;
            this.lblReadyToProcess.Text = "Ready to Process";
            this.lblReadyToProcess.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(108)))), ((int)(((byte)(81)))));
            this.label1.Location = new System.Drawing.Point(324, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unknown Mail";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SteelBlue;
            this.label2.Location = new System.Drawing.Point(329, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Other";
            // 
            // outerBarOther
            // 
            this.outerBarOther.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(208)))), ((int)(((byte)(203)))));
            this.outerBarOther.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outerBarOther.Controls.Add(this.barOther);
            this.outerBarOther.Location = new System.Drawing.Point(335, 55);
            this.outerBarOther.Name = "outerBarOther";
            this.outerBarOther.Size = new System.Drawing.Size(22, 107);
            this.outerBarOther.TabIndex = 0;
            // 
            // barOther
            // 
            this.barOther.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.barOther.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(132)))), ((int)(((byte)(119)))));
            this.barOther.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.barOther.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barOther.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(71)))), ((int)(((byte)(26)))));
            this.barOther.Location = new System.Drawing.Point(-1, 38);
            this.barOther.Name = "barOther";
            this.barOther.Size = new System.Drawing.Size(22, 68);
            this.barOther.TabIndex = 0;
            this.barOther.Text = "0";
            this.barOther.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Location = new System.Drawing.Point(266, 311);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset Inbox";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // FrmDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 339);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.lblReadyToProcess);
            this.Controls.Add(this.cboMailbox);
            this.Controls.Add(this.txtItemsToProcess);
            this.Controls.Add(this.btnAction1);
            this.Controls.Add(this.outerBarProgress);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lblAnalysisStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmDisplay";
            this.Text = "Inbox Analysis (Run only when logged in as no-reply)";
            this.Load += new System.EventHandler(this.FrmDisplay_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDisplay_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.outerBarFaxError.ResumeLayout(false);
            this.outerBarFaxFail.ResumeLayout(false);
            this.outerBarFaxSuccess.ResumeLayout(false);
            this.outerBarEmailFail.ResumeLayout(false);
            this.outerBarEmailSuccess.ResumeLayout(false);
            this.outerBarProgress.ResumeLayout(false);
            this.outerBarOther.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAnalysisStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel outerBarEmailSuccess;
        private System.Windows.Forms.Label barEmailSuccess;
        private System.Windows.Forms.Panel outerBarProgress;
        private System.Windows.Forms.Label barProgress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel outerBarEmailFail;
        private System.Windows.Forms.Label barEmailFail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnAction1;
        private System.Windows.Forms.Panel outerBarFaxFail;
        private System.Windows.Forms.Label barFaxFail;
        private System.Windows.Forms.Panel outerBarFaxSuccess;
        private System.Windows.Forms.Label barFaxSuccess;
        private System.Windows.Forms.TextBox txtItemsToProcess;
        private System.Windows.Forms.Panel outerBarFaxError;
        private System.Windows.Forms.Label barFaxError;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboMailbox;
        private System.Windows.Forms.Label lblReadyToProcess;
        private System.Windows.Forms.Panel outerBarOther;
        private System.Windows.Forms.Label barOther;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReset;
    }
}

