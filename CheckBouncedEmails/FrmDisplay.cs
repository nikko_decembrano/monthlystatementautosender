using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using Microsoft.Office.Core
using System.Globalization;
using System.Text.RegularExpressions;
using AutoSenderLibrary;
using AutoSenderLogin;
using System.Diagnostics;


namespace CheckBouncedEmails
{
    public partial class FrmDisplay : Form
    {

        //Enumerated Types
        private enum Result { Success, InternalError, Failure };
        private enum SendType { Email, Fax };

        //Internal Values
        private int totalToProcess = 500;
        private int barScaling = 100;
        private int currentItemIndex = 0;

        //Internal Counts
        private int emailSuccessCount = 0;
        private int emailFailCount = 0;
        private int faxSuccessCount = 0;
        private int faxErrorCount = 0;
        private int faxFailCount = 0;
        private int otherCount = 0;

        //Internal Properties
        private int CurrentItemIndex   {   get { return currentItemIndex;  }   set { currentItemIndex = value;  RedrawBars(); }   }
        private int EmailSuccessCount  {   get { return emailSuccessCount; }   set { emailSuccessCount = value; RedrawBars(); }   }
        private int EmailFailCount     {   get { return emailFailCount;    }   set { emailFailCount = value;    RedrawBars(); }   }
        private int FaxSuccessCount    {   get { return faxSuccessCount;   }   set { faxSuccessCount = value;   RedrawBars(); }   }
        private int FaxErrorCount      {   get { return faxErrorCount;     }   set { faxErrorCount = value;     RedrawBars(); }   }
        private int FaxFailCount       {   get { return faxFailCount;      }   set { faxFailCount = value;      RedrawBars(); }   }
        private int OtherCount         {   get { return otherCount;        }   set { otherCount = value;        RedrawBars(); }   }

        //Internal Outlook References
        Microsoft.Office.Interop.Outlook.Application app = null;
        Microsoft.Office.Interop.Outlook._NameSpace ns = null;
        Microsoft.Office.Interop.Outlook.MAPIFolder inboxFolder = null;


        //-----------------------------
        //Application Settings
        bool useDebugFolder = false;
        bool writeToDatabase = true;
        bool showSQLMessageBox = false;
        bool moveProcessedMail = true;
        bool resetEntireInbox = false;
        //-----------------------------



        //Constructor
        public FrmDisplay()
        {
            // Login (AU only)
            FormLogin loginForm = new FormLogin();
            loginForm.AllowDbChange = false;
            DatabaseEnvironment db = DatabaseEnvironment.AU;
            if (!loginForm.IsAuthenticated(db))
            {
                loginForm.ShowDialog();

                if (loginForm.DialogResult == DialogResult.Cancel)
                {
                    // Exit with failure
                    System.Environment.Exit(1);
                }
            }
            if (!loginForm.HasAdminPermission(db))
            {
                MessageBox.Show("Your account does not give you permission to this form, please contact IT or login as a different user");
                System.Environment.Exit(1);
            }
            //------------------------------------------

            InitializeComponent();
        }

        private void FrmDisplay_Load(object sender, EventArgs e)
        {

            //Check for Debugging Mode
            #if DEBUG
            {
                //Prompt the user
                DialogResult useDebugSettings = MessageBox.Show("The application is running in DEBUG mode. \r\n\r\nClick:\r\n\r\n    YES:  to use the TESTING subfolder in Outlook \r\n             (Inbox\\Test Analaysis) to process e-mails \r\n\r\n    NO:  to use the LIVE e-mails in the Inbox)", "Use Debug Data?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //Handle the user's reponse
                if (useDebugSettings == DialogResult.Yes)
                {
                    //Use Debug Settings
                    useDebugFolder = true;
                    writeToDatabase = false;
                    showSQLMessageBox = false; //showSQLMessageBox = true;
                    moveProcessedMail = true;
                    resetEntireInbox = false;
                    btnReset.Visible = true;

                    //Inform the User
                    MessageBox.Show(@"Using Debug Folder:   Inbox\Test Analaysis");
                }
            }
            #endif

            if (useDebugFolder == false)
            {
                //Inform the User if Live
                //MessageBox.Show("Processing LIVE E-mails");
            }

            // Select mailbox
            cboMailbox.Items.Add("Livingstone");
            cboMailbox.Items.Add("Universal Choice");
            cboMailbox.SelectedIndex = 0;

        }


        private void RedrawBars()
        {

            //Update Label Text
            barEmailSuccess.Text = emailSuccessCount.ToString();
            barEmailFail.Text    = emailFailCount.ToString();
            barFaxSuccess.Text   = faxSuccessCount.ToString();
            barFaxError.Text     = faxErrorCount.ToString();
            barFaxFail.Text      = faxFailCount.ToString();
            barOther.Text        = otherCount.ToString();
            barProgress.Text     = CurrentItemIndex + " / " + totalToProcess;

            //Check if the Total Number of Items has Changed
            if (CurrentItemIndex > totalToProcess) totalToProcess = CurrentItemIndex + inboxFolder.Items.Count;  //this line untested

            //Calculate Scaling
            double scalingLimit = barScaling * 0.7;
            if (barScaling != totalToProcess && (emailSuccessCount > scalingLimit || emailFailCount > scalingLimit || faxSuccessCount > scalingLimit || faxFailCount > scalingLimit || otherCount > scalingLimit))
            {
                //Increase the scaling limit
                barScaling = (int)Math.Floor((double)barScaling * 1.5);
                if (barScaling > totalToProcess) barScaling = totalToProcess;
            }

            //Apply Scaling to Bar Heights
            barEmailSuccess.Height = (int)Math.Ceiling((double)emailSuccessCount / barScaling * outerBarEmailSuccess.Height);
            barEmailFail.Height =    (int)Math.Ceiling((double)emailFailCount    / barScaling * outerBarEmailFail.Height);
            barFaxSuccess.Height =   (int)Math.Ceiling((double)faxSuccessCount   / barScaling * outerBarFaxSuccess.Height);
            barFaxError.Height =     (int)Math.Ceiling((double)faxErrorCount     / barScaling * outerBarFaxError.Height);
            barFaxFail.Height =      (int)Math.Ceiling((double)faxFailCount      / barScaling * outerBarFaxFail.Height);
            barOther.Height =        (int)Math.Ceiling((double)otherCount        / barScaling * outerBarOther.Height);
            barProgress.Width =      (int)((double)CurrentItemIndex  / (double)totalToProcess * outerBarProgress.Width);

            //Correct Bar Positioning
            barEmailSuccess.Top = outerBarEmailSuccess.Height - barEmailSuccess.Height-1;
            barEmailFail.Top =    outerBarEmailFail.Height    - barEmailFail.Height-1;
            barFaxSuccess.Top =   outerBarFaxSuccess.Height   - barFaxSuccess.Height-1;
            barFaxError.Top =     outerBarFaxError.Height     - barFaxError.Height-1;
            barFaxFail.Top =      outerBarFaxFail.Height      - barFaxFail.Height-1;
            barOther.Top =        outerBarOther.Height        - barOther.Height-1;

            //Refresh Form
            this.Refresh();
        }


        private bool InitialiseOutlook(string selectedmailbox)
        {

            //See if we have a previous connection to Outlook (eg. when changing the Drop Down Box
            if (app == null)
            {

                // Check whether there is an Outlook process running.
                if (IsOutlookRunning())
                {
                    // If so, use the GetActiveObject method to obtain the process and cast it to an Application object.     https://msdn.microsoft.com/en-us/library/office/ff462097.aspx?cs-save-lang=1&cs-lang=csharp#code-snippet-2
                    MessageBox.Show("Unable to process E-mails while Outlook is opened. Please close Outlook");
                    System.Environment.Exit(1);

                    //I believe this line of code fails as Marshal.GetActiveObject gets the FIRST instance of Outlook from the ROT table, however this may actually be an Outlook process from another session from another user. To do this properly, I believe this convenient function call cannot be used and you need to retrieve the process from the ROT directly:  http://stackoverflow.com/questions/13432057/how-to-use-marshal-getactiveobject-to-get-2-instance-of-of-a-running-process-t
                    app = (Microsoft.Office.Interop.Outlook.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Outlook.Application");
                }
                else
                {
                    //Outlook not opened, create a new instance and log onto the default profile
                    app = new Microsoft.Office.Interop.Outlook.Application();
                    ns = app.GetNamespace("MAPI");
                    ns.Logon(app.DefaultProfileName, null, false, false);

                }

            }


            //Retrieve Inbox
            Microsoft.Office.Interop.Outlook.Recipient receipient;

            switch (selectedmailbox)
            {
                case "Universal Choice":
                    receipient = ns.CreateRecipient("no-reply@universalchoice.net");
                    break;
                default: // livingstone
                    receipient = ns.CreateRecipient("no-reply@livingstone.com.au");
                    break;
            }

            //Retrieve Inbox
            //inboxFolder = ns.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox);
            inboxFolder = ns.GetSharedDefaultFolder(receipient, Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox);

            //Temp Debug Code
            // (THIS CREATES FOLDERS INSIDE)
            if (useDebugFolder == true)
            {
                inboxFolder = GetSubFolder(inboxFolder, "Test Analaysis");
            }

            //Test Connection
            int emailsFound = 0;
            try
            {
                emailsFound = inboxFolder.Items.Count;
            }
            catch
            {
                MessageBox.Show("Error: Outlook Reached Maximum Limit. \n\nPlease Restart Outlook");
                Application.Exit();
            }

            //Check for Debugging E-mails
            if (useDebugFolder == true && emailsFound == 0)
            {
                MessageBox.Show("No e-mails were found in the debug folder:   Inbox\\Test Analaysis  \r\n\r\nTry copying some bounce back e-mails into the test folder and run this program again.");
            }

            
            //Function Completed
            //   NOTE:  Sometimes this point won't be reached if there is an issue with the Outlook Interop DLL. When an issue occurrs, it may actually break out of this function and continues running the form without even triggering an error. Returning true here proves that the function actually completed running successfully.
            //Return Successful
            return true;
        }

        private bool IsOutlookRunning()
        {
            //Get Current SessionID
            int currentSessionID = Process.GetCurrentProcess().SessionId;

            //Get any open Outlook Processes on this metaframe (not: 
            Process[] processes = Process.GetProcessesByName("OUTLOOK");

            //Check that there are processes returned
            if (processes.GetLength(0) > 0)
            {
                //Loop through each process
                foreach (Process process in processes)
                {
                    //Check if the Session ID is the same as the Current User's Session ID
                    if (process.SessionId == currentSessionID)
                    {
                        //Outlook Found running on this Session
                        return true;
                    }
                }
            }

            //No Outlook Process found running on this Session
            return false;
        }

        private bool IsValidEmail(string strIn)
        {
            //invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            //strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper);
            //if (invalid)
            //    return false;

            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                   RegexOptions.IgnoreCase);
        }

        /*private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }*/

        /// <summary> This takes all the e-mails from the processed subfolders and dunks them back in the inbox</summary>
        private void ResetInbox()
        {
            //Note:  This function should generally only be run on the testing subfolder otherwise you're going to move a lot of emails that have already been written to the database back into the unprocessed inbox folder

            //Disable the Button
            btnReset.Enabled = false;

            //Initialise Components
            Microsoft.Office.Interop.Outlook.MailItem mailItem = null;
            Microsoft.Office.Interop.Outlook.ReportItem reportItem = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder successfulFaxesFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder failedFaxesFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder failedEmailsFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder internalErrorFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder internalErrorUnprocessedFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder otherFolder = null;

            if (useDebugFolder)
            {

                //Check if the Processing Folder Exists
                successfulFaxesFolder =          EnsureFolderExists(inboxFolder, "Successful Faxes");
                failedFaxesFolder =              EnsureFolderExists(inboxFolder, "Failed Faxes");
                failedEmailsFolder =             EnsureFolderExists(inboxFolder, "Failed Emails");
                internalErrorFolder =            EnsureFolderExists(inboxFolder, "Internal Error Reports");
                internalErrorUnprocessedFolder = EnsureFolderExists(inboxFolder, "Internal Error Reports - Unprocessed");
                otherFolder =                    EnsureFolderExists(inboxFolder, "Other");

                //Debug Only:  Reset Entire Inbox
                MoveAllMessages(successfulFaxesFolder, inboxFolder);
                MoveAllMessages(failedFaxesFolder, inboxFolder);
                MoveAllMessages(failedEmailsFolder, inboxFolder);
                MoveAllMessages(internalErrorFolder, inboxFolder);
                MoveAllMessages(internalErrorUnprocessedFolder, inboxFolder);
                MoveAllMessages(otherFolder, inboxFolder);

                //Get latest Item Count and Redraw Screen
                cboMailbox_SelectedIndexChanged(this, new EventArgs());
            }
            else
            {
                MessageBox.Show("This function should only be used when using the debug folder");
            }

            //Enable the Button
            btnReset.Enabled = true;

        }

        private void ProcessInbox()
        {
            //Initialise Components
            Microsoft.Office.Interop.Outlook.MailItem mailItem = null;
            Microsoft.Office.Interop.Outlook.ReportItem reportItem = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder successfulFaxesFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder failedFaxesFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder failedEmailsFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder internalErrorFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder internalErrorUnprocessedFolder = null;
            Microsoft.Office.Interop.Outlook.MAPIFolder otherFolder = null;


            //try
            {

                //Check if the Processing Folder Exists
                successfulFaxesFolder =          EnsureFolderExists(inboxFolder, "Successful Faxes");
                failedFaxesFolder =              EnsureFolderExists(inboxFolder, "Failed Faxes");
                failedEmailsFolder =             EnsureFolderExists(inboxFolder, "Failed Emails");
                internalErrorFolder =            EnsureFolderExists(inboxFolder, "Internal Error Reports");
                internalErrorUnprocessedFolder = EnsureFolderExists(inboxFolder, "Internal Error Reports - Unprocessed");
                otherFolder =                    EnsureFolderExists(inboxFolder, "Other");


                ////Temp Debug Code
                //// (does not create folders inside)
                //if (useDebugFolder == true)
                //{
                //    //inboxFolder = GetSubFolder(inboxFolder, "Other");
                //    //inboxFolder = GetSubFolder(inboxFolder, "Failed Reports");
                //    //inboxFolder = GetSubFolder(inboxFolder, "Test Analaysis");
                //    //inboxFolder = failedEmailsFolder;
                //}


                //Debug Only:  Reset Entire Inbox
                if (resetEntireInbox == true)
                {
                    MoveAllMessages(successfulFaxesFolder, inboxFolder);
                    MoveAllMessages(failedFaxesFolder, inboxFolder);
                    MoveAllMessages(failedEmailsFolder, inboxFolder);
                    MoveAllMessages(internalErrorFolder, inboxFolder);
                    MoveAllMessages(internalErrorUnprocessedFolder, inboxFolder);
                    MoveAllMessages(otherFolder, inboxFolder);
                }


                //Loop through each item in the Inbox
                int count = 1;
                while (count <= inboxFolder.Items.Count)
                {

                    //Initialise
                    string messageClass = "";
                    string sendAddress = "";
                    string timeReceived = "";
                    string receiptNo = "";
                    string reason = "";
                    mailItem = null;
                    reportItem = null;

                    //Get a reference to the Item Type
                    try
                    {
                        messageClass = (string)(string)inboxFolder.Items[count].GetType().InvokeMember("MessageClass", System.Reflection.BindingFlags.GetProperty, null, inboxFolder.Items[count], null);
                    }
                    catch
                    {
                        MessageBox.Show("Error: Outlook Reached Maximum Limit. \n\nPlease Restart Outlook");
                        Application.Exit();
                    }

                    //Check the Item Type
                    switch (messageClass)
                    {

                        //Check if Item is a [Normal Mail Message]
                        case "IPM.Note":

                            //Normal Mail Message
                            mailItem = (Microsoft.Office.Interop.Outlook.MailItem)inboxFolder.Items[count];

                            /* Above line eventually returns an error:
                             Unable to cast COM object of type 'System.__ComObject' to interface type 'Microsoft.Office.Interop.Outlook.MailItem'. This operation failed because the QueryInterface call on the COM component for the interface with IID '{00063034-0000-0000-C000-000000000046}' failed due to the following error: No such interface supported (Exception from HRESULT: 0x80004002 (E_NOINTERFACE)).
                            */

                            //Get Details
                            timeReceived = DateToSQL(mailItem.SentOn);
                            //Example:     "Success: Monthly Account Statement (Fax sent to 93138991) [::resend=s73d71b0]"
                            //Example:     "Failure: Monthly Account Statement (Sent fax failed to 1300729729) [::resend=f1990560]"

                            //Check the type of message received
                            if (mailItem.Subject != null && mailItem.Subject.StartsWith("Success: ") && mailItem.Subject.Contains("Fax sent to "))
                            {
                                //Successful Fax
                                sendAddress = ExtractStringComponent(mailItem.Subject, "(Fax sent to ", ")");
                                
                                //reason = ExtractStringComponent(mailItem.Body, "Description: ", "\r\n\r\n");
                                if (mailItem.HTMLBody.Contains("Additional information</td><td>"))
                                {
                                    reason = ExtractStringComponent(mailItem.HTMLBody, "Additional information</td><td>", "</td>");
                                }
                                else if (mailItem.Body.Contains("Description: "))
                                {
                                    reason = ExtractStringComponent(mailItem.Body, "Description: ", "\n");
                                }
                                
                                if (mailItem.Subject.Contains("Rcpt: ")) receiptNo = ExtractStringComponent(mailItem.Subject, "Rcpt: ", " ");

                                //Record the Success in the Database
                                UpdateDatabase(SendType.Fax, sendAddress, timeReceived, receiptNo, Result.Success, reason);

                                //Move to Successful Folder
                                if (moveProcessedMail)  mailItem.Move(successfulFaxesFolder);   else count++;

                            }
                            else if (mailItem.SenderName == "FAXmaker" && mailItem.Body.Contains("ERROR FAX REPORT") && mailItem.Body.Contains("Attachment conversion failed"))
                            {
                                //Fax Failure (Conversion Failed)

                                if (mailItem.Body.Contains("Fax:["))
                                {
                                    //Easy to detect which fax number failed
                                    sendAddress = ExtractStringComponent(mailItem.Body, "Fax:[", "]");
                                    
                                    //reason = ExtractStringComponent(mailItem.Body, "Description: ", "\r\n\r\n");
                                    if (mailItem.HTMLBody.Contains("Additional information</td><td>"))
                                    {
                                        reason = ExtractStringComponent(mailItem.HTMLBody, "Additional information</td><td>", "</td>");
                                    }
                                    else if (mailItem.Body.Contains("Description: "))
                                    {
                                        reason = ExtractStringComponent(mailItem.Body, "Description: ", "\n");
                                    }

                                    if (mailItem.Subject.Contains("Rcpt: ")) receiptNo = ExtractStringComponent(mailItem.Subject, "Rcpt: ", " ");

                                    //Record the Internal Error into the Database
                                    UpdateDatabase(SendType.Fax, sendAddress, timeReceived, receiptNo, Result.InternalError, reason);

                                    //Move E-mail into the Internal Error Folder
                                    if (moveProcessedMail)  mailItem.Move(internalErrorFolder);   else count++;
                                }
                                else
                                {
                                    //Impossible to detect which fax number failed
                                    //Move E-mail into the Internal Error Folder
                                    if (moveProcessedMail)  mailItem.Move(internalErrorUnprocessedFolder);   else count++;

                                }
                                
                            }
                            else if (mailItem.Subject != null && mailItem.Subject.StartsWith("Failure: ") && mailItem.Subject.Contains("Sent fax failed to ") && mailItem.Body.Contains("Description: Failed to send fax : No dialtone"))
                            {
                                //Fax Failure (Livingstone FaxMaker Issue: No Dialtone)
                                sendAddress = ExtractStringComponent(mailItem.Subject, "Sent fax failed to ", ")");
                                
                                //reason = ExtractStringComponent(mailItem.Body, "Description: ", "\r\n\r\n");
                                if (mailItem.HTMLBody.Contains("Additional information</td><td>"))
                                {
                                    reason = ExtractStringComponent(mailItem.HTMLBody, "Additional information</td><td>", "</td>");
                                }
                                else if (mailItem.Body.Contains("Description: "))
                                {
                                    reason = ExtractStringComponent(mailItem.Body, "Description: ", "\n");
                                }

                                if (mailItem.Subject.Contains("Rcpt: ")) receiptNo = ExtractStringComponent(mailItem.Subject, "Rcpt: ", " ");

                                //Record the Internal Error into the Database
                                UpdateDatabase(SendType.Fax, sendAddress, timeReceived, receiptNo, Result.InternalError, reason);

                                //Move E-mail into the Internal Error Folder
                                if (moveProcessedMail)  mailItem.Move(internalErrorFolder);   else count++;
                            }
                            else if (mailItem.Subject != null && mailItem.Subject.StartsWith("Failure: ") && mailItem.Subject.Contains("Sent fax failed to ") && mailItem.ReceivedTime < new DateTime(2011,3,1,12,0,0) && mailItem.Body.Contains("Line number: 0"))
                            {
                                //Fax Failure (Livingstone FaxMaker Issue: Line Zero Issue before the 1/3/2011 12PM
                                sendAddress = ExtractStringComponent(mailItem.Body, "Sent fax failed to ", ")");
                                reason = "Line Zero Issue";
                                if (mailItem.Subject.Contains("Rcpt: ")) receiptNo = ExtractStringComponent(mailItem.Subject, "Rcpt: ", " ");

                                //Record the Internal Error into the Database
                                UpdateDatabase(SendType.Fax, sendAddress, timeReceived, receiptNo, Result.InternalError, reason);

                                //Move E-mail into the Internal Error Folder
                                if (moveProcessedMail)  mailItem.Move(internalErrorFolder);   else count++;
                            }
                            else if (mailItem.Subject != null && mailItem.Subject.StartsWith("Failure: ") && mailItem.Subject.Contains("Sent fax failed to "))
                            {
                                //Fax Failure (Bad Fax Number or No Answer at USERS end)
                                sendAddress = ExtractStringComponent(mailItem.Subject, "Sent fax failed to ", ")");

                                //reason = ExtractStringComponent(mailItem.Body, "Description: ", "\n");
                                if (mailItem.HTMLBody.Contains("Additional information</td><td>"))
                                {
                                    reason = ExtractStringComponent(mailItem.HTMLBody, "Additional information</td><td>", "</td>");
                                }
                                else if (mailItem.Body.Contains("Description: "))
                                {
                                    reason = ExtractStringComponent(mailItem.Body, "Description: ", "\n");
                                }

                                if (mailItem.Subject.Contains("Rcpt: ")) receiptNo = ExtractStringComponent(mailItem.Subject, "Rcpt: ", " ");
                                
                                //Record the Failure into the Database
                                UpdateDatabase(SendType.Fax, sendAddress, timeReceived, receiptNo, Result.Failure, reason);

                                //Move E-mail into the Failed Faxes Folder
                                if (moveProcessedMail)  mailItem.Move(failedFaxesFolder);   else count++;
                            }
                            // return from mailmarshal@netcomdata.com.au
                            else if (mailItem.Subject != null && (mailItem.Subject.StartsWith("Undeliverable Mail:") || mailItem.Subject.StartsWith("Timed Out Mail:")))
                            {
                                //E-mail Failure
                                reason = "";
                                string sendAddressGroupString = "";
                                string[] sendAddressGroupList = new string[] { };
                                string additionalInformation = "";

                                if (mailItem.Subject.StartsWith("Undeliverable Mail:"))
                                {
                                    reason = ExtractStringComponent(mailItem.Body, "Could not be delivered because", "The following recipients were affected:");
                                    reason = ExtractStringComponent(reason, "\r\n", null); // remove first line
                                    sendAddressGroupString = ExtractStringComponent(mailItem.Body, "The following recipients were affected:", "Additional Information");
                                    sendAddressGroupList = sendAddressGroupString.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                                    additionalInformation = ExtractStringComponent(mailItem.Body, "Additional Information", null);
                                }
                                else if (mailItem.Subject.StartsWith("Timed Out Mail:"))
                                {
                                    reason = ExtractStringComponent(mailItem.Body, "Delivery of a message from you ", "\r\n");
                                    sendAddressGroupString = ExtractStringComponent(mailItem.Body, "The following recipients could not be delivered:", "Additional Information");
                                    sendAddressGroupList = sendAddressGroupString.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                                    additionalInformation = ExtractStringComponent(mailItem.Body, "Additional Information", null);
                                }

                                List<string> sendAddresses = new List<string>();
                                if (mailItem.Subject.Contains("Rcpt: "))
                                {
                                    receiptNo = ExtractStringComponent(mailItem.Subject, "Rcpt: ", " ");
                                    receiptNo = receiptNo.Replace("\"", "");
                                }

                                foreach (string emailFailure in sendAddressGroupList)
                                {
                                    sendAddress = emailFailure.Trim();

                                    //Record the Failure into the Database
                                    //MessageBox.Show(string.Format("{0}\n\n({1})\n\n{2}\n\n{3}", sendAddress, receiptNo,timeReceived, reason));
                                    UpdateDatabase(SendType.Email, sendAddress, timeReceived, receiptNo, Result.Failure, reason, additionalInformation);
                                    
                                }

                                //Move E-mail into the Failed Emails Folder
                                if (moveProcessedMail) mailItem.Move(failedEmailsFolder); else count++;
                            }
                            else
                            {
                                //Mail Message not in correct format: Skip Past
                                //count++;
                                if (moveProcessedMail)  mailItem.Move(otherFolder);   else count++;

                                //Increase the Number Processed (normally this is done by the UpdateDatabase function)
                                CurrentItemIndex++;
                                otherCount++;
                            }
                            break;


                        //Check if Item is a [Non Delivery Report Item]
                        case "REPORT.IPM.Note.NDR":

                            //Non Deliverable Report
                            reportItem = (Microsoft.Office.Interop.Outlook.ReportItem)inboxFolder.Items[count];

                            if (reportItem.Subject != null && reportItem.Subject.StartsWith("Undeliverable:"))
                            {
                                //E-mail Failure
                                string sendAddressGroupString = ExtractStringComponent(reportItem.Body, "The following recipient(s) cannot be reached:\r\n\r\n      ", null);
                                string[] sendAddressGroupList = sendAddressGroupString.Split(new string[] { "\r\n\r\n      " }, StringSplitOptions.RemoveEmptyEntries);

                                foreach (string emailFailure in sendAddressGroupList)
                                {

                                    //Extract Details
                                    sendAddress = ExtractStringComponent(emailFailure, null, " on ");
                                    timeReceived = DateToSQL(reportItem.CreationTime);
                                    reason = ExtractStringComponent(emailFailure, "\r\n            ", "\r\n            ");
                                    
                                    if (reportItem.Subject.Contains("Rcpt: ")) receiptNo = ExtractStringComponent(reportItem.Subject, "Rcpt: ", " ");

                                    //Record the Failure into the Database
                                    UpdateDatabase(SendType.Email, sendAddress, timeReceived, receiptNo, Result.Failure, reason);

                                }

                                //Move E-mail into the Failed Emails Folder
                                if (moveProcessedMail)  reportItem.Move(failedEmailsFolder);   else count++;
                            }
                            else
                            {
                                //Report Item Not in Correct Format: SKIP PAST
                                if (moveProcessedMail)  reportItem.Move(otherFolder);   else count++;

                                //Increase the Number Processed (normally this is done by the UpdateDatabase function)
                                CurrentItemIndex++;
                                otherCount++;
                            }
                            break;

                        default:
                            //Type could not be identified. Do not process this item for now.
                            sendAddress = "";
                            count++;
                            break;
                    }

                    

                    
                    //Move the Mail Item to a Sub Folder

                }
            }
            //catch (System.Runtime.InteropServices.COMException ex)
            //{
            //    //Console.WriteLine(ex.ToString());
            //    MessageBox.Show(ex.ToString());
            //}
            //finally
            //{

            //    ns = null;
            //    app.Quit();
            //    app = null;
            //    inboxFolder = null;
            //}

            //Update Total
            totalToProcess = CurrentItemIndex;  //Force the total (which was read when the program loaded) to be the total number processed.. since we may have processed a few extra e-mails that were received after the program launched but it would like funny to say something like 420/415.
            RedrawBars();

            //Show Complete
            lblAnalysisStatus.Text = "Inbox Analysis Complete!";

            //Enable Button
            btnAction1.Text = "Close";
            btnAction1.Enabled = true;

            //Clean Up
            //inboxFolder = null;
            //ns = null;
            //app.Quit();
            //app = null;
        }

        private static string ExtractStringComponent(string text, string trimBefore, string trimAfter)
        {
            //Initialise
            string result = "";

            try
            {

                //Remove text BEFORE the desired element
                // Check if the parameter is valid
                if (trimBefore != null && trimBefore != "")
                {
                    //Check if the trim point exists
                    if (text.Contains(trimBefore))
                        result = text.Remove(0, text.IndexOf(trimBefore) + trimBefore.Length);
                    else
                        MessageBox.Show("Warning: The desired trim point: '" + trimBefore + "' does not exist in the string:\n'" + text + "'");
                }
                else
                    result = text;

                //Remove text AFTER the desired element
                if (trimAfter != null && trimAfter != "")
                {
                    //Check if the trim point exists
                    if (result.Contains(trimAfter))
                        result = result.Remove(result.IndexOf(trimAfter));
                    else
                        if (trimAfter != " ") MessageBox.Show("Warning: The desired trim point: '" + trimAfter + "' does not exist in the string:\n'" + text + "'");
                }

                //Trim Result
                result = result.Trim();

            }
            catch
            {
                MessageBox.Show("Error Extracting String:\n   Text: '" + text + "'\n   trimBefore: '" + trimBefore + "'\n   trimAfter: '" + trimAfter + "'");
                //Clipboard.Clear();
                //Clipboard.SetText("Error Extracting String:\n   Text: '" + text + "'\n   trimBefore: '" + trimBefore + "'\n   trimAfter: '" + trimAfter + "'");
            }

            //Return the result
            return result;
        }


        private bool UpdateDatabase(SendType sendType, string sendAddress, string timeReceived, string receiptNo, Result result, string resultDescription)
        {
            return UpdateDatabase(sendType, sendAddress, timeReceived, receiptNo, result, resultDescription, "");
        }

        private bool UpdateDatabase(SendType sendType, string sendAddress, string timeReceived, string receiptNo, Result result, string resultDescription, string additionalInfo)
        {

            sendAddress = sendAddress.Replace("'", "''");
            resultDescription = resultDescription.Replace("'", "''");

            //return false;
            
            //Initialise
            int rowsAffected = 0;
            string SQL = "";

            //Check if we need to update the main table
            if (result == Result.Failure)
            {
                //Initialise SQL
                if (receiptNo != null && receiptNo != "")
                {
                    SQL = @"
                        update tblAutomaticStatementHistory
                        set Failed = 1
                        where SendId = '" + Convert.ToInt32(receiptNo, 16) + @"'
                        and SendAddress = '" + sendAddress + @"'
                        and abs(datediff(d, cast('" + timeReceived + @"' as datetime), LastPrinted)) <= 6

                        select @@rowcount as result

                        --Added 2011/6/1 (For updating NetCRM Activity List if all sends for that sendid fail)
                        IF (select count(*) from tblAutomaticStatementHistory where sendId = " + Convert.ToInt32(receiptNo, 16) + @" and failed = 0 and internalerror = 0) = 0
                        begin
                            declare @activity_no numeric(18)
                            declare @note_address numeric(18)

                            select 
                                @note_address = a.note_address,
                                @activity_no = a.activity_no
                            from tblAutomaticStatementHistory h
                            inner join act a 
                                on h.netcrm_activity = a.activity_no
                            where sendid = " + Convert.ToInt32(receiptNo, 16) + @"

                            -- If referencing a note address
                            if @note_address is not null and @note_address <> 0
                            begin
                                -- update if not already updated
                                if ((select count (*) from memo where note_address = @note_address and note_text like '(Failed) %') = 0) 
                                begin
                                    DECLARE @ptrval binary(16)
                                    SELECT @ptrval = TEXTPTR(note_text) 
                                    from memo
                                    where Note_Address = @note_address

                                    UPDATETEXT memo.Note_text @ptrval 0 0 '(Failed) ' 
                                end
                            end
                        end
                    ";
                }
                else
                {
                    SQL = @"
                        update tblAutomaticStatementHistory
                        set Failed = 1
                        where SendAddress = '" + sendAddress + @"'
                        and abs(datediff(d, cast('" + timeReceived + @"' as datetime), LastPrinted)) <= 2

                        select @@rowcount as result
                    ";
                }

                //Initialise Results
                string rowsAffectedStr = "";

                //Perform Database Update
                if (writeToDatabase == true)
                {
                    using (DatabaseDLL.DBConnection NetCRM = new DatabaseDLL.DBConnection(DatabaseDLL.DB.NetCRM))
                    {
                        rowsAffectedStr = NetCRM.Read_SingleCell(SQL);
                    }
                }

                //Convert Result to an Integer
                int.TryParse(rowsAffectedStr, out rowsAffected);
            }
            else if (result == Result.InternalError)
            {
                //Initialise SQL
                if (receiptNo != null && receiptNo != "")
                {
                    SQL = @"
                        update tblAutomaticStatementHistory
                        set InternalError = 1
                        where SendId = '" + Convert.ToInt32(receiptNo, 16) + @"'
                        and SendAddress = '" + sendAddress + @"'
                        and abs(datediff(d, cast('" + timeReceived + @"' as datetime), LastPrinted)) <= 6

                        select @@rowcount as result

                        --Added 2011/6/1 (For updating NetCRM Activity List if all sends for that sendid fail)
                        IF (select count(*) from tblAutomaticStatementHistory where sendId = " + Convert.ToInt32(receiptNo, 16) + @" and failed = 0 and internalerror = 0) = 0
                        begin
                            declare @activity_no numeric(18)
                            declare @note_address numeric(18)

                            select 
                                @note_address = a.note_address,
                                @activity_no = a.activity_no
                            from tblAutomaticStatementHistory h
                            inner join act a 
                                on h.netcrm_activity = a.activity_no
                            where sendid = " + Convert.ToInt32(receiptNo, 16) + @"

                            -- If referencing a note address
                            if @note_address is not null and @note_address <> 0
                            begin
                                -- update if not already updated
                                if ((select count (*) from memo where note_address = @note_address and note_text like '(Error) %') = 0) 
                                begin
                                    DECLARE @ptrval binary(16)
                                    SELECT @ptrval = TEXTPTR(note_text) 
                                    from memo
                                    where Note_Address = @note_address

                                    UPDATETEXT memo.Note_text @ptrval 0 0 '(Error) ' 
                                end
                            end
                        end
                    ";
                }
                else
                {
                    SQL = @"
                        update tblAutomaticStatementHistory
                        set InternalError = 1
                        where SendAddress = '" + sendAddress + @"'
                        and abs(datediff(d, cast('" + timeReceived + @"' as datetime), LastPrinted)) <= 2

                        select @@rowcount as result
                    ";
                }

                //Initialise Results
                string rowsAffectedStr = "";

                //Perform Database Update
                if (writeToDatabase == true)
                {
                    using (DatabaseDLL.DBConnection NetCRM = new DatabaseDLL.DBConnection(DatabaseDLL.DB.NetCRM))
                    {
                        rowsAffectedStr = NetCRM.Read_SingleCell(SQL);
                    }
                }

                //Convert Result to an Integer
                int.TryParse(rowsAffectedStr, out rowsAffected);
            }


            //MessageBox.Show(sendType.ToString());

            //Insert Log
            if (receiptNo != null && receiptNo != "")
            {
                SQL = @"
                    insert into tblAutomaticStatementResultLog (SendId, SendType, SendAddress, Received, Result, ResultDescription, Processed, LogUpdated, AdditionalInformation)
                    values ('" + Convert.ToInt32(receiptNo, 16) + "', '" + sendType.ToString() + "', '" + sendAddress + "', cast('" + timeReceived + "' as datetime), '" + result.ToString() + "', '" + resultDescription + "', getdate(), " + rowsAffected + ", '"+ additionalInfo + @"')
                ";
            }
            else
            {
                SQL = @"
                    insert into tblAutomaticStatementResultLog (SendId, SendType, SendAddress, Received, Result, ResultDescription, Processed, LogUpdated, AdditionalInformation)
                    values (null, '" + sendType.ToString() + "', '" + sendAddress + "', cast('" + timeReceived + "' as datetime), '" + result.ToString() + "', '" + resultDescription + "', getdate(), " + rowsAffected + ", '" + additionalInfo + @"')
                ";
            }

            //DEBUG
            if (showSQLMessageBox)
            {
                MessageBox.Show(SQL);
                Clipboard.Clear();
                Clipboard.SetText(SQL);
            }

            //Perform Database Insertion
            if (writeToDatabase == true)
            {
                using (DatabaseDLL.DBConnection NetCRM = new DatabaseDLL.DBConnection(DatabaseDLL.DB.NetCRM))
                {
                    NetCRM.Execute(SQL);
                }
            }

            //Update Internal Count
            if (sendType == SendType.Email)
            {
                if       (result == Result.Success)  EmailSuccessCount++;
                else if  (result == Result.Failure)  EmailFailCount++;
            }
            else if (sendType == SendType.Fax)
            {
                if       (result == Result.Success)        FaxSuccessCount++;
                else if  (result == Result.InternalError)  FaxErrorCount++;
                else if  (result == Result.Failure)        FaxFailCount++;
            }

            //Increase Item Process Count
            CurrentItemIndex++;


            //Return a Boolean Value if Update Succeeded
            return (rowsAffected >= 1);
        }










        /// <summary> This function searches the current folder for a subfolder with the specified name </summary>
        /// <param name="rootFolder">A reference to the Folder in which you want to perform the search</param>
        /// <param name="searchFolder">The name of the subfolder you wish to search for (as a string)</param>
        /// <returns>Returns a reference to the subfolder that was requested</returns>
        private static Microsoft.Office.Interop.Outlook.MAPIFolder GetSubFolder(Microsoft.Office.Interop.Outlook.MAPIFolder rootFolder, string searchFolder)
        {
            //Loop through all Subfolders in the specified Folder
            for (int count = 1; count <= rootFolder.Folders.Count; count++)
            {
                //Check if the Current Subfolder matches the Desired Subfolder
                if (rootFolder.Folders[count].Name == searchFolder)
                {
                    //Current Subfolder matches the Search Term! Return the result
                    return rootFolder.Folders[count];
                }
            }

            //If execution reaches here, no subfolder was found matching the search term. Return nothing
            return null;

        }


        /// <summary> This function moves all the e-mails OUT from the Source Folder into the Destination Folder</summary>
        private static void MoveAllMessages(Microsoft.Office.Interop.Outlook.MAPIFolder sourceFolder, Microsoft.Office.Interop.Outlook.MAPIFolder destinationFolder)
        {
            int count = 1;
            while (count <= sourceFolder.Items.Count)
            {
                //Get a reference to the Item Type
                object item = sourceFolder.Items[count];
                string messageClass = (string)(string)item.GetType().InvokeMember("MessageClass", System.Reflection.BindingFlags.GetProperty, null, item, null);

                //Check the Item Type
                switch (messageClass)
                {

                    //Check if Item is a [Normal Mail Message]
                    case "IPM.Note":
                        ((Microsoft.Office.Interop.Outlook.MailItem)item).Move(destinationFolder);
                        break;
                    case "REPORT.IPM.Note.NDR":
                        ((Microsoft.Office.Interop.Outlook.ReportItem)item).Move(destinationFolder);
                        break;
                    default:
                        count++;
                        break;
                }
            }
        }


        private Microsoft.Office.Interop.Outlook.MAPIFolder EnsureFolderExists(Microsoft.Office.Interop.Outlook.MAPIFolder searchIn, string searchFor)
        {
            //Loop through the existing sub-folders to see if the desired folder name can be located
            foreach (Microsoft.Office.Interop.Outlook.MAPIFolder subFolder in searchIn.Folders)
            {
                //Check if current folder matches the folder we're searching for
                if (subFolder.Name == searchFor)
                {
                    //Folder successfully found! Exit Function
                    return subFolder;
                }
            }

            //Folder was not located, create it now
            return searchIn.Folders.Add(searchFor, Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderInbox);

        }

        /// <summary> Returns a safe string which can be easily cast in SQL as an [SQL] DateTime </summary>
        private static string DateToSQL(DateTime date)
        {
            return date.Year + "-" + date.Month.ToString().PadLeft(2, '0') + "-" + date.Day.ToString().PadLeft(2, '0') + " " + date.Hour.ToString().PadLeft(2, '0') + ":" + date.Minute.ToString().PadLeft(2, '0') + ":" + date.Second.ToString().PadLeft(2, '0');
        }

        private void btnAction1_Click(object sender, EventArgs e)
        {
            if (btnAction1.Text == "Begin" || btnAction1.Text == "Process")
            {
                //Update Button
                btnAction1.Enabled = false;

                //Update Text
                lblAnalysisStatus.Text = "Analysing Inbox..";

                //Process Inbox
                ProcessInbox();
            }
            else if (btnAction1.Text == "Close")
            {
                //Exit Application
                Application.Exit();
            }
        }






        private void cboMailbox_SelectedIndexChanged(object sender, EventArgs e)
        {

            //Reset Counters
            emailSuccessCount = 0;
            emailFailCount = 0;
            faxSuccessCount = 0;
            faxErrorCount = 0;
            faxFailCount = 0;
            otherCount = 0;
            CurrentItemIndex = 0;



            //Initialise Default Values
            RedrawBars();

            //Reset Form Items (in case the Initialise Outlook process fails)
            txtItemsToProcess.Text = "";
            lblAnalysisStatus.Text = "Attempting to Connect to Outlook";

            //Reset Buttons
            btnAction1.Text = "Begin";
            btnAction1.Enabled = false;

            //Initialise Outlook
            bool initialiseSuccess = false;
            initialiseSuccess = InitialiseOutlook(cboMailbox.SelectedItem.ToString());



            //Check that Outlook Initialised Successfully
            if (initialiseSuccess == false)
            {
                //FAILED TO CONNECT

                //Warn the user
                MessageBox.Show("Unable to establish a connection to the Outlook Folders for the No-Reply Account. You will will NOT be able to use this form normally. Please contact IT for assistance");

                //Disable the button
                btnAction1.Enabled = false;

                //Update the text
                lblAnalysisStatus.Text = "ERROR: Failed to connect to Outlook";
            }
            else
            {
                //CONNECTED SUCCESSFULLY

                //Capture the total
                totalToProcess = inboxFolder.Items.Count;
                txtItemsToProcess.Text = totalToProcess.ToString();

                //Check that E-mails are visible
                if (inboxFolder.Items.Count > 0)
                {
                    //Ready to process

                    //Update the Screen
                    lblReadyToProcess.Visible = true;
                    btnAction1.Text = "Begin";
                    btnAction1.Enabled = true;

                    //Update the Text
                    lblAnalysisStatus.Text = "Press the  Begin  button..";
                }
                else
                {
                    //No E-mails to process

                    //Update the Screen
                    lblReadyToProcess.Visible = false;
                    btnAction1.Enabled = false;

                    //Update the Text
                    lblAnalysisStatus.Text = "NOTE:  No unprocessed E-mails";
                }
            }


            //Refresh Form
            this.Refresh();

            //Begin Processing
            //ProcessInbox();
        }

       
        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetInbox();
        }

        private void FrmDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Clean Up
            inboxFolder = null;
            ns = null;

            //Tell the Connection to Outlook to Close (prevents keeping a lock on the program)
            app.Quit();
            app = null;

        }


    }
}