using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;
using AutoSenderLogin;

namespace EmailBodyEditor
{
    public partial class frmEmailBody : Form
    {
        public frmEmailBody()
        {
            // Login (AU only)
            FormLogin loginForm = new FormLogin();
            loginForm.AllowDbChange = false;
            DatabaseEnvironment db = DatabaseEnvironment.AU;
            if (!loginForm.IsAuthenticated(db))
            {
                loginForm.ShowDialog();

                if (loginForm.DialogResult == DialogResult.Cancel)
                {
                    // Exit with failure
                    System.Environment.Exit(1);
                }
            }
            if (!loginForm.HasAdminPermission(db))
            {
                MessageBox.Show("Your account does not give you permission to this form, please contact IT or login as a different user");
                System.Environment.Exit(1);
            }
            //------------------------------------------

            InitializeComponent();

            string[] bodies = EmailBody.GetListOfEmailBodiesFromDB();

            foreach (string body in bodies)
            {
                if (EmailFax.IsEmailBodyInUse(body))
                {
                    cboActiveName.Items.Add(body);
                }
                else 
                {
                    cboBackupName.Items.Add(body);
                }
            }
        }

        private void cboBackupName_SelectedIndexChanged(object sender, EventArgs e)
        {
            EmailBody b = EmailBody.GetEmailBodyFromDB(cboBackupName.SelectedItem.ToString());
            txtBackupSubj.Text = b.Subj;
            txtBackupBody.Text = b.Body;
            txtBackupHtmlBody.Text = b.HtmlBody;
        }

        private void cboActiveName_SelectedIndexChanged(object sender, EventArgs e)
        {
            EmailBody b = EmailBody.GetEmailBodyFromDB(cboActiveName.SelectedItem.ToString());
            txtActiveSubj.Text = b.Subj;
            txtActiveBody.Text = b.Body;
            txtActiveHtmlBody.Text = b.HtmlBody;
        }

        private void btnBackupSave_Click(object sender, EventArgs e)
        {
            SaveEmailToDB(cboBackupName.SelectedItem.ToString(), txtBackupSubj.Text, txtBackupBody.Text, txtBackupHtmlBody.Text);
        }

        private void btnActiveSave_Click(object sender, EventArgs e)
        {
            SaveEmailToDB(cboActiveName.SelectedItem.ToString(), txtActiveSubj.Text, txtActiveBody.Text, txtActiveHtmlBody.Text);
        }

        // Helper for both save buttons
        private void SaveEmailToDB(string name, string subj, string body, string htmlbody)
        {
            EmailBody b = new EmailBody(name, subj, body, htmlbody);
            b.InsertOrUpdateToDB();
        }

    }
}