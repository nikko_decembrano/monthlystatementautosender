namespace EmailBodyEditor
{
    partial class frmEmailBody
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gboActive = new System.Windows.Forms.GroupBox();
            this.btnActiveSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtActiveBody = new System.Windows.Forms.TextBox();
            this.txtActiveSubj = new System.Windows.Forms.TextBox();
            this.cboActiveName = new System.Windows.Forms.ComboBox();
            this.gboBackup = new System.Windows.Forms.GroupBox();
            this.btnBackupSave = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBackupBody = new System.Windows.Forms.TextBox();
            this.txtBackupSubj = new System.Windows.Forms.TextBox();
            this.cboBackupName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtActiveHtmlBody = new System.Windows.Forms.TextBox();
            this.txtBackupHtmlBody = new System.Windows.Forms.TextBox();
            this.gboActive.SuspendLayout();
            this.gboBackup.SuspendLayout();
            this.SuspendLayout();
            // 
            // gboActive
            // 
            this.gboActive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gboActive.Controls.Add(this.txtActiveHtmlBody);
            this.gboActive.Controls.Add(this.label7);
            this.gboActive.Controls.Add(this.btnActiveSave);
            this.gboActive.Controls.Add(this.label3);
            this.gboActive.Controls.Add(this.label2);
            this.gboActive.Controls.Add(this.label1);
            this.gboActive.Controls.Add(this.txtActiveBody);
            this.gboActive.Controls.Add(this.txtActiveSubj);
            this.gboActive.Controls.Add(this.cboActiveName);
            this.gboActive.Location = new System.Drawing.Point(19, 12);
            this.gboActive.Name = "gboActive";
            this.gboActive.Size = new System.Drawing.Size(342, 541);
            this.gboActive.TabIndex = 0;
            this.gboActive.TabStop = false;
            this.gboActive.Text = "In Active Use";
            // 
            // btnActiveSave
            // 
            this.btnActiveSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActiveSave.Location = new System.Drawing.Point(261, 508);
            this.btnActiveSave.Name = "btnActiveSave";
            this.btnActiveSave.Size = new System.Drawing.Size(75, 23);
            this.btnActiveSave.TabIndex = 6;
            this.btnActiveSave.Text = "Save";
            this.btnActiveSave.UseVisualStyleBackColor = true;
            this.btnActiveSave.Click += new System.EventHandler(this.btnActiveSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Body";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Subject";
            // 
            // txtActiveBody
            // 
            this.txtActiveBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtActiveBody.Location = new System.Drawing.Point(6, 121);
            this.txtActiveBody.Multiline = true;
            this.txtActiveBody.Name = "txtActiveBody";
            this.txtActiveBody.Size = new System.Drawing.Size(330, 164);
            this.txtActiveBody.TabIndex = 2;
            // 
            // txtActiveSubj
            // 
            this.txtActiveSubj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtActiveSubj.Location = new System.Drawing.Point(6, 81);
            this.txtActiveSubj.Name = "txtActiveSubj";
            this.txtActiveSubj.Size = new System.Drawing.Size(330, 20);
            this.txtActiveSubj.TabIndex = 1;
            // 
            // cboActiveName
            // 
            this.cboActiveName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboActiveName.FormattingEnabled = true;
            this.cboActiveName.Location = new System.Drawing.Point(6, 41);
            this.cboActiveName.Name = "cboActiveName";
            this.cboActiveName.Size = new System.Drawing.Size(330, 21);
            this.cboActiveName.TabIndex = 0;
            this.cboActiveName.SelectedIndexChanged += new System.EventHandler(this.cboActiveName_SelectedIndexChanged);
            // 
            // gboBackup
            // 
            this.gboBackup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gboBackup.Controls.Add(this.txtBackupHtmlBody);
            this.gboBackup.Controls.Add(this.label8);
            this.gboBackup.Controls.Add(this.btnBackupSave);
            this.gboBackup.Controls.Add(this.label4);
            this.gboBackup.Controls.Add(this.label5);
            this.gboBackup.Controls.Add(this.label6);
            this.gboBackup.Controls.Add(this.txtBackupBody);
            this.gboBackup.Controls.Add(this.txtBackupSubj);
            this.gboBackup.Controls.Add(this.cboBackupName);
            this.gboBackup.Location = new System.Drawing.Point(424, 12);
            this.gboBackup.Name = "gboBackup";
            this.gboBackup.Size = new System.Drawing.Size(342, 541);
            this.gboBackup.TabIndex = 1;
            this.gboBackup.TabStop = false;
            this.gboBackup.Text = "Backups";
            // 
            // btnBackupSave
            // 
            this.btnBackupSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBackupSave.Location = new System.Drawing.Point(261, 508);
            this.btnBackupSave.Name = "btnBackupSave";
            this.btnBackupSave.Size = new System.Drawing.Size(75, 23);
            this.btnBackupSave.TabIndex = 6;
            this.btnBackupSave.Text = "Save";
            this.btnBackupSave.UseVisualStyleBackColor = true;
            this.btnBackupSave.Click += new System.EventHandler(this.btnBackupSave_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "File";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Body";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Subject";
            // 
            // txtBackupBody
            // 
            this.txtBackupBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBackupBody.Location = new System.Drawing.Point(6, 121);
            this.txtBackupBody.Multiline = true;
            this.txtBackupBody.Name = "txtBackupBody";
            this.txtBackupBody.Size = new System.Drawing.Size(330, 164);
            this.txtBackupBody.TabIndex = 2;
            // 
            // txtBackupSubj
            // 
            this.txtBackupSubj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBackupSubj.Location = new System.Drawing.Point(6, 81);
            this.txtBackupSubj.Name = "txtBackupSubj";
            this.txtBackupSubj.Size = new System.Drawing.Size(330, 20);
            this.txtBackupSubj.TabIndex = 1;
            // 
            // cboBackupName
            // 
            this.cboBackupName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboBackupName.FormattingEnabled = true;
            this.cboBackupName.Location = new System.Drawing.Point(6, 41);
            this.cboBackupName.Name = "cboBackupName";
            this.cboBackupName.Size = new System.Drawing.Size(330, 21);
            this.cboBackupName.TabIndex = 0;
            this.cboBackupName.SelectedIndexChanged += new System.EventHandler(this.cboBackupName_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 288);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "HtmlBody";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 288);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "HtmlBody";
            // 
            // txtActiveHtmlBody
            // 
            this.txtActiveHtmlBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtActiveHtmlBody.Location = new System.Drawing.Point(9, 304);
            this.txtActiveHtmlBody.Multiline = true;
            this.txtActiveHtmlBody.Name = "txtActiveHtmlBody";
            this.txtActiveHtmlBody.Size = new System.Drawing.Size(330, 164);
            this.txtActiveHtmlBody.TabIndex = 3;
            // 
            // txtBackupHtmlBody
            // 
            this.txtBackupHtmlBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBackupHtmlBody.Location = new System.Drawing.Point(6, 304);
            this.txtBackupHtmlBody.Multiline = true;
            this.txtBackupHtmlBody.Name = "txtBackupHtmlBody";
            this.txtBackupHtmlBody.Size = new System.Drawing.Size(330, 164);
            this.txtBackupHtmlBody.TabIndex = 9;
            // 
            // frmEmailBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 574);
            this.Controls.Add(this.gboBackup);
            this.Controls.Add(this.gboActive);
            this.Name = "frmEmailBody";
            this.Text = "Stored Email Bodies";
            this.gboActive.ResumeLayout(false);
            this.gboActive.PerformLayout();
            this.gboBackup.ResumeLayout(false);
            this.gboBackup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gboActive;
        private System.Windows.Forms.ComboBox cboActiveName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtActiveBody;
        private System.Windows.Forms.TextBox txtActiveSubj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnActiveSave;
        private System.Windows.Forms.GroupBox gboBackup;
        private System.Windows.Forms.Button btnBackupSave;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBackupBody;
        private System.Windows.Forms.TextBox txtBackupSubj;
        private System.Windows.Forms.ComboBox cboBackupName;
        private System.Windows.Forms.TextBox txtActiveHtmlBody;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBackupHtmlBody;
        private System.Windows.Forms.Label label8;
    }
}

