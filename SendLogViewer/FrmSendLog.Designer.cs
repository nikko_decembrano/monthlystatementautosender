namespace SendLogViewer
{
    partial class FrmSendLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtHex = new System.Windows.Forms.TextBox();
            this.txtInteger = new System.Windows.Forms.TextBox();
            this.BtnHexInt = new System.Windows.Forms.Button();
            this.gboDebug = new System.Windows.Forms.GroupBox();
            this.lblSendId = new System.Windows.Forms.Label();
            this.txtSendId = new System.Windows.Forms.TextBox();
            this.gboResults = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtActivityLog = new System.Windows.Forms.TextBox();
            this.txtSendAddresses = new System.Windows.Forms.TextBox();
            this.txtAffinity = new System.Windows.Forms.TextBox();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.txtCusNo = new System.Windows.Forms.TextBox();
            this.txtExact = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.gboDebug.SuspendLayout();
            this.gboResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtHex
            // 
            this.txtHex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtHex.Location = new System.Drawing.Point(6, 21);
            this.txtHex.Name = "txtHex";
            this.txtHex.Size = new System.Drawing.Size(100, 20);
            this.txtHex.TabIndex = 0;
            // 
            // txtInteger
            // 
            this.txtInteger.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtInteger.Enabled = false;
            this.txtInteger.Location = new System.Drawing.Point(189, 21);
            this.txtInteger.Name = "txtInteger";
            this.txtInteger.Size = new System.Drawing.Size(100, 20);
            this.txtInteger.TabIndex = 1;
            // 
            // BtnHexInt
            // 
            this.BtnHexInt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnHexInt.Location = new System.Drawing.Point(112, 21);
            this.BtnHexInt.Name = "BtnHexInt";
            this.BtnHexInt.Size = new System.Drawing.Size(75, 23);
            this.BtnHexInt.TabIndex = 2;
            this.BtnHexInt.Text = "Hex to Int";
            this.BtnHexInt.UseVisualStyleBackColor = true;
            this.BtnHexInt.Click += new System.EventHandler(this.BtnHexInt_Click);
            // 
            // gboDebug
            // 
            this.gboDebug.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gboDebug.Controls.Add(this.txtHex);
            this.gboDebug.Controls.Add(this.BtnHexInt);
            this.gboDebug.Controls.Add(this.txtInteger);
            this.gboDebug.Location = new System.Drawing.Point(0, 345);
            this.gboDebug.Name = "gboDebug";
            this.gboDebug.Size = new System.Drawing.Size(306, 50);
            this.gboDebug.TabIndex = 3;
            this.gboDebug.TabStop = false;
            this.gboDebug.Text = "Debug";
            // 
            // lblSendId
            // 
            this.lblSendId.AutoSize = true;
            this.lblSendId.Location = new System.Drawing.Point(3, 15);
            this.lblSendId.Name = "lblSendId";
            this.lblSendId.Size = new System.Drawing.Size(46, 13);
            this.lblSendId.TabIndex = 4;
            this.lblSendId.Text = "Send ID";
            // 
            // txtSendId
            // 
            this.txtSendId.Location = new System.Drawing.Point(55, 12);
            this.txtSendId.Name = "txtSendId";
            this.txtSendId.Size = new System.Drawing.Size(100, 20);
            this.txtSendId.TabIndex = 5;
            this.txtSendId.TextChanged += new System.EventHandler(this.txtSendId_TextChanged);
            // 
            // gboResults
            // 
            this.gboResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gboResults.Controls.Add(this.label6);
            this.gboResults.Controls.Add(this.txtActivityLog);
            this.gboResults.Controls.Add(this.txtSendAddresses);
            this.gboResults.Controls.Add(this.txtAffinity);
            this.gboResults.Controls.Add(this.txtCompanyName);
            this.gboResults.Controls.Add(this.txtCusNo);
            this.gboResults.Controls.Add(this.txtExact);
            this.gboResults.Controls.Add(this.label5);
            this.gboResults.Controls.Add(this.label4);
            this.gboResults.Controls.Add(this.label3);
            this.gboResults.Controls.Add(this.label2);
            this.gboResults.Controls.Add(this.label1);
            this.gboResults.Location = new System.Drawing.Point(0, 41);
            this.gboResults.Name = "gboResults";
            this.gboResults.Size = new System.Drawing.Size(306, 306);
            this.gboResults.TabIndex = 6;
            this.gboResults.TabStop = false;
            this.gboResults.Text = "Result";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Activity Logged";
            // 
            // txtActivityLog
            // 
            this.txtActivityLog.Location = new System.Drawing.Point(91, 127);
            this.txtActivityLog.Name = "txtActivityLog";
            this.txtActivityLog.ReadOnly = true;
            this.txtActivityLog.Size = new System.Drawing.Size(198, 20);
            this.txtActivityLog.TabIndex = 11;
            // 
            // txtSendAddresses
            // 
            this.txtSendAddresses.Location = new System.Drawing.Point(91, 153);
            this.txtSendAddresses.Multiline = true;
            this.txtSendAddresses.Name = "txtSendAddresses";
            this.txtSendAddresses.ReadOnly = true;
            this.txtSendAddresses.Size = new System.Drawing.Size(198, 135);
            this.txtSendAddresses.TabIndex = 10;
            // 
            // txtAffinity
            // 
            this.txtAffinity.Location = new System.Drawing.Point(91, 101);
            this.txtAffinity.Name = "txtAffinity";
            this.txtAffinity.ReadOnly = true;
            this.txtAffinity.Size = new System.Drawing.Size(198, 20);
            this.txtAffinity.TabIndex = 9;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(91, 75);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.ReadOnly = true;
            this.txtCompanyName.Size = new System.Drawing.Size(198, 20);
            this.txtCompanyName.TabIndex = 8;
            // 
            // txtCusNo
            // 
            this.txtCusNo.Location = new System.Drawing.Point(91, 49);
            this.txtCusNo.Name = "txtCusNo";
            this.txtCusNo.ReadOnly = true;
            this.txtCusNo.Size = new System.Drawing.Size(198, 20);
            this.txtCusNo.TabIndex = 7;
            // 
            // txtExact
            // 
            this.txtExact.Location = new System.Drawing.Point(91, 23);
            this.txtExact.Name = "txtExact";
            this.txtExact.ReadOnly = true;
            this.txtExact.Size = new System.Drawing.Size(198, 20);
            this.txtExact.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Company Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Affinity";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Send Addr";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cus No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Exact No";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(161, 12);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // FrmSendLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 392);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.gboResults);
            this.Controls.Add(this.txtSendId);
            this.Controls.Add(this.lblSendId);
            this.Controls.Add(this.gboDebug);
            this.Name = "FrmSendLog";
            this.Text = "SendId Quick Search";
            this.gboDebug.ResumeLayout(false);
            this.gboDebug.PerformLayout();
            this.gboResults.ResumeLayout(false);
            this.gboResults.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtHex;
        private System.Windows.Forms.TextBox txtInteger;
        private System.Windows.Forms.Button BtnHexInt;
        private System.Windows.Forms.GroupBox gboDebug;
        private System.Windows.Forms.Label lblSendId;
        private System.Windows.Forms.TextBox txtSendId;
        private System.Windows.Forms.GroupBox gboResults;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAffinity;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.TextBox txtCusNo;
        private System.Windows.Forms.TextBox txtExact;
        private System.Windows.Forms.TextBox txtSendAddresses;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtActivityLog;
    }
}

