using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;


namespace SendLogViewer
{
    public partial class FrmSendLog : Form
    {
        public FrmSendLog()
        {
            InitializeComponent();

            // Only show debug box in debug version 
#if !DEBUG
            gboDebug.Visible = false;
#endif
        }

        private void BtnHexInt_Click(object sender, EventArgs e)
        {
            txtInteger.Text = string.Format("{0}", SendLog.HexToInt(txtHex.Text));
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            GetLogEntry();
        }

        private void txtSendId_TextChanged(object sender, EventArgs e)
        {
            GetLogEntry();
        }

        private void GetLogEntry()
        {
            SendLogEntry entry = SendLog.GetFromSendId(txtSendId.Text);

            if (entry != null)
            {
                txtExact.Text = entry.ExactNo;
                txtCusNo.Text = entry.CusNo;
                txtCompanyName.Text = entry.CompanyName;
                txtAffinity.Text = entry.Affinity;
                txtActivityLog.Text = entry.hasNetCRMActivity() ? "YES" : "NO";
                txtSendAddresses.Text = "";

                foreach (SendLogAddress address in entry.SendAddresses)
                {
                    txtSendAddresses.Text += address.SendAddress + System.Environment.NewLine;
                }
            }
            else
            {
                txtExact.Text = "";
                txtCusNo.Text = "";
                txtCompanyName.Text = "";
                txtAffinity.Text = "";
                txtActivityLog.Text = "";
                txtSendAddresses.Text = "";
            }
        }
    }
}