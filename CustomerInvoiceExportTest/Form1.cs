using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;
using System.Threading;

namespace CustomerInvoiceExportTest
{
    public delegate void ContactListRetrievedEventHandler();

    public partial class Form1 : Form
    {
        CustomerContactList contactList;
        public event ContactListRetrievedEventHandler UpdateCustomerComboBoxEvent;

        public Form1()
        {
            InitializeComponent();

            Thread thread = new Thread(new ThreadStart(GetContacts));
            thread.Start();

        }

        private void GetContacts()
        {
            UpdateCustomerComboBoxEvent += new ContactListRetrievedEventHandler(UpdateCustomerComboBox);

            contactList = new CustomerContactList(new ActionTypeList(new ActionType[] { ActionType.Email, ActionType.Fax, ActionType.Print }));
            contactList.getListOfCustomers();
            //cboCustomer.Items.AddRange(contactList.getListOfCustomers());

            UpdateCustomerComboBoxEvent();
        }

        private void UpdateCustomerComboBox()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new ContactListRetrievedEventHandler(GetContacts), new object[] { });
            }
            else
            {
                cboCustomer.Items.AddRange(contactList.getListOfCustomers());
                lblLoading.Visible = false;
                UpdateCustomerComboBoxEvent -= new ContactListRetrievedEventHandler(UpdateCustomerComboBox);
            }
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            InvoiceExport("EXCEL");
        }

        private void btnPdf_Click(object sender, EventArgs e)
        {
            InvoiceExport("PDF");
        }

        private void InvoiceExport(string type)
        {
            string selectedCustomer = (string)cboCustomer.SelectedItem;

            if (selectedCustomer != null)
            {
                string path = "";
                
                switch (type)
                {
                    case "EXCEL":
                        path = MonthlyStatementReportExport.ExportToExcel(contactList.getCustomerContact(selectedCustomer).Invoices);
                        break;
                    case "PDF":
                        MonthlyStatementReport cReport = new MonthlyStatementReport(selectedCustomer, MonthlyStatementReport.getLatestMonthlyStatementDate());
                        path = cReport.exportToPDF();
                        break;
                }

                if (path != "") System.Diagnostics.Process.Start(path);
            }
        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedCustomer = (string)cboCustomer.SelectedItem;

            if (selectedCustomer != null)
            {
                //dataGridView1.DataSource = contactList.getCustomerContact(selectedCustomer).Invoices;
                MessageBox.Show(contactList.getCustomerContact(selectedCustomer).getUpszone());
            }
        }
    }
}