namespace AutoSenderClient
{
    partial class FormUpsSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkSelectedUps = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chkUnselectedUps = new System.Windows.Forms.CheckedListBox();
            this.btnActivate = new System.Windows.Forms.Button();
            this.btnDeactivate = new System.Windows.Forms.Button();
            this.btnActivateAll = new System.Windows.Forms.Button();
            this.btnDeactivateAll = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkSelectedUps
            // 
            this.chkSelectedUps.CheckOnClick = true;
            this.chkSelectedUps.FormattingEnabled = true;
            this.chkSelectedUps.Location = new System.Drawing.Point(236, 62);
            this.chkSelectedUps.Name = "chkSelectedUps";
            this.chkSelectedUps.Size = new System.Drawing.Size(156, 319);
            this.chkSelectedUps.Sorted = true;
            this.chkSelectedUps.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Please select desired UPS zones:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(236, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Active";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Inactive";
            // 
            // chkUnselectedUps
            // 
            this.chkUnselectedUps.CheckOnClick = true;
            this.chkUnselectedUps.FormattingEnabled = true;
            this.chkUnselectedUps.Location = new System.Drawing.Point(12, 62);
            this.chkUnselectedUps.Name = "chkUnselectedUps";
            this.chkUnselectedUps.Size = new System.Drawing.Size(156, 319);
            this.chkUnselectedUps.Sorted = true;
            this.chkUnselectedUps.TabIndex = 3;
            // 
            // btnActivate
            // 
            this.btnActivate.Location = new System.Drawing.Point(183, 152);
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Size = new System.Drawing.Size(38, 35);
            this.btnActivate.TabIndex = 5;
            this.btnActivate.Text = "Add";
            this.btnActivate.UseVisualStyleBackColor = true;
            this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
            // 
            // btnDeactivate
            // 
            this.btnDeactivate.Location = new System.Drawing.Point(183, 220);
            this.btnDeactivate.Name = "btnDeactivate";
            this.btnDeactivate.Size = new System.Drawing.Size(38, 35);
            this.btnDeactivate.TabIndex = 6;
            this.btnDeactivate.Text = "Del";
            this.btnDeactivate.UseVisualStyleBackColor = true;
            this.btnDeactivate.Click += new System.EventHandler(this.btnDeactivate_Click);
            // 
            // btnActivateAll
            // 
            this.btnActivateAll.Location = new System.Drawing.Point(183, 111);
            this.btnActivateAll.Name = "btnActivateAll";
            this.btnActivateAll.Size = new System.Drawing.Size(38, 35);
            this.btnActivateAll.TabIndex = 7;
            this.btnActivateAll.Text = "Add All";
            this.btnActivateAll.UseVisualStyleBackColor = true;
            this.btnActivateAll.Click += new System.EventHandler(this.btnActivateAll_Click);
            // 
            // btnDeactivateAll
            // 
            this.btnDeactivateAll.Location = new System.Drawing.Point(183, 261);
            this.btnDeactivateAll.Name = "btnDeactivateAll";
            this.btnDeactivateAll.Size = new System.Drawing.Size(38, 35);
            this.btnDeactivateAll.TabIndex = 8;
            this.btnDeactivateAll.Text = "Del All";
            this.btnDeactivateAll.UseVisualStyleBackColor = true;
            this.btnDeactivateAll.Click += new System.EventHandler(this.btnDeactivateAll_Click);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(416, 36);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // BtnCancel
            // 
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancel.Location = new System.Drawing.Point(416, 65);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 10;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // FormUpsSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 410);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnDeactivateAll);
            this.Controls.Add(this.btnActivateAll);
            this.Controls.Add(this.btnDeactivate);
            this.Controls.Add(this.btnActivate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chkUnselectedUps);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkSelectedUps);
            this.Name = "FormUpsSelection";
            this.Text = "Select UPS Zones";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox chkSelectedUps;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox chkUnselectedUps;
        private System.Windows.Forms.Button btnActivate;
        private System.Windows.Forms.Button btnDeactivate;
        private System.Windows.Forms.Button btnActivateAll;
        private System.Windows.Forms.Button btnDeactivateAll;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button BtnCancel;
    }
}