using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;

namespace AutoSenderClient
{
    public partial class FormUpsSelection : Form
    {
        CustomerContact[] customercontacts;

        public FormUpsSelection(CustomerContact[] customerscontacts)
        {
            InitializeComponent();

            // keep copy of customer contacts
            this.customercontacts = customerscontacts;

            // Loop through list of customers
            foreach (CustomerContact customercontact in customerscontacts)
            {
                string upszone = customercontact.getUpszone();
                int index = chkUnselectedUps.Items.IndexOf(upszone);

                // Add ups zone to list if not found in customer list
                if (index < 0 || index >= chkUnselectedUps.Items.Count)
                {
                    chkUnselectedUps.Items.Add(upszone);
                }
            }
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            // get all zones and move to other list
            string[] zones = GetListOfZones(chkUnselectedUps.CheckedItems);
            MoveZones(chkUnselectedUps, chkSelectedUps, zones);
        }

        private void btnDeactivate_Click(object sender, EventArgs e)
        {
            // get all zones and move to other list
            string[] zones = GetListOfZones(chkSelectedUps.CheckedItems);
            MoveZones(chkSelectedUps, chkUnselectedUps, zones);
        }

        private void btnActivateAll_Click(object sender, EventArgs e)
        {
            // get all zones and move to other list
            string[] zones = GetListOfZones(chkUnselectedUps.Items);
            MoveZones(chkUnselectedUps, chkSelectedUps, zones);
        }

        private void btnDeactivateAll_Click(object sender, EventArgs e)
        {
            // get all zones and move to other list
            string[] zones = GetListOfZones(chkSelectedUps.Items);
            MoveZones(chkSelectedUps, chkUnselectedUps, zones);
        }

        // helper for getting list of zones in checkedlistbox collection
        private string[] GetListOfZones(CheckedListBox.CheckedItemCollection items)
        {
            string[] zones = new string[items.Count];

            int i = 0;
            foreach (string zone in items)
            {
                zones[i++] = zone;
            }

            return zones;
        }

        // helper for getting list of zones in checkedlistbox collection
        private string[] GetListOfZones(CheckedListBox.ObjectCollection items)
        {
            string[] zones = new string[items.Count];

            int i = 0;
            foreach (string zone in items)
            {
                zones[i++] = zone;
            }

            return zones;
        }

        // helper for moving selected zones from one list to another
        private void MoveZones(CheckedListBox from, CheckedListBox to, string[] zones)
        {
            foreach (string zone in zones)
            {
                to.Items.Add(zone);
                from.Items.Remove(zone);
            }
        }


        public List<string> GetSelectedCustomerNos()
        {
            List<string> selectedCustomerNos = new List<string>();

            // foreach customer
            foreach (CustomerContact customercontact in customercontacts)
            {
                // If upszone is in selected upszone list
                int index = chkSelectedUps.Items.IndexOf(customercontact.getUpszone());
                if (index >= 0 && index < chkSelectedUps.Items.Count)
                {
                    // Add to selected customer numbers
                    selectedCustomerNos.Add(customercontact.getCustomerNumber());
                }
            }

            return selectedCustomerNos;
        }
    }
}