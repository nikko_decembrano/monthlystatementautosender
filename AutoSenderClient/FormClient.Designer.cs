namespace AutoSenderClient
{
    partial class FormClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSetPrinter = new System.Windows.Forms.Button();
            this.panTop = new System.Windows.Forms.Panel();
            this.panCount = new System.Windows.Forms.Panel();
            this.txtReview = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPrint = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panSetPrinter = new System.Windows.Forms.Panel();
            this.txtPrinterName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panSetSelection = new System.Windows.Forms.Panel();
            this.btnNone = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.btnModifySelection = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaxExact = new System.Windows.Forms.TextBox();
            this.txtMinExact = new System.Windows.Forms.TextBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.tabControlListBoxes = new System.Windows.Forms.TabControl();
            this.tabPrint = new System.Windows.Forms.TabPage();
            this.lstPrint = new System.Windows.Forms.CheckedListBox();
            this.tabReview = new System.Windows.Forms.TabPage();
            this.lstReview = new System.Windows.Forms.CheckedListBox();
            this.tabDone = new System.Windows.Forms.TabPage();
            this.lstDone = new System.Windows.Forms.CheckedListBox();
            this.panItemInfo = new System.Windows.Forms.Panel();
            this.txtRptDate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtActionType = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnBackup = new System.Windows.Forms.Button();
            this.txtUpszone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtExact = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPreviewReport = new System.Windows.Forms.Button();
            this.btnPreviewReports = new System.Windows.Forms.Button();
            this.panCryRpt = new System.Windows.Forms.Panel();
            this.cryRptViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.lblSelectedItems = new System.Windows.Forms.Label();
            this.btnSelectByUps = new System.Windows.Forms.Button();
            this.saveLivPdfBackupDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveUcPdfBackupDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveOtherspdfBackupDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveReviewpdfbackupDialog = new System.Windows.Forms.SaveFileDialog();
            this.label10 = new System.Windows.Forms.Label();
            this.cboDatabase = new System.Windows.Forms.ComboBox();
            this.btnGet = new System.Windows.Forms.Button();
            this.btnBackupSelected = new System.Windows.Forms.Button();
            this.saveSelectedFilesDialog = new System.Windows.Forms.SaveFileDialog();
            this.panLeft = new System.Windows.Forms.Panel();
            this.panDbSelect = new System.Windows.Forms.Panel();
            this.panBackup = new System.Windows.Forms.Panel();
            this.panTop.SuspendLayout();
            this.panCount.SuspendLayout();
            this.panSetPrinter.SuspendLayout();
            this.panSetSelection.SuspendLayout();
            this.tabControlListBoxes.SuspendLayout();
            this.tabPrint.SuspendLayout();
            this.tabReview.SuspendLayout();
            this.tabDone.SuspendLayout();
            this.panItemInfo.SuspendLayout();
            this.panCryRpt.SuspendLayout();
            this.panLeft.SuspendLayout();
            this.panDbSelect.SuspendLayout();
            this.panBackup.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSetPrinter
            // 
            this.btnSetPrinter.Location = new System.Drawing.Point(6, 32);
            this.btnSetPrinter.Name = "btnSetPrinter";
            this.btnSetPrinter.Size = new System.Drawing.Size(156, 23);
            this.btnSetPrinter.TabIndex = 2;
            this.btnSetPrinter.Text = "Set Printer";
            this.btnSetPrinter.UseVisualStyleBackColor = true;
            this.btnSetPrinter.Click += new System.EventHandler(this.btnSetPrinter_Click);
            // 
            // panTop
            // 
            this.panTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panTop.Controls.Add(this.panCount);
            this.panTop.Controls.Add(this.panSetPrinter);
            this.panTop.Controls.Add(this.panSetSelection);
            this.panTop.Controls.Add(this.btnPrint);
            this.panTop.Location = new System.Drawing.Point(176, -1);
            this.panTop.Name = "panTop";
            this.panTop.Size = new System.Drawing.Size(887, 74);
            this.panTop.TabIndex = 0;
            // 
            // panCount
            // 
            this.panCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panCount.Controls.Add(this.txtReview);
            this.panCount.Controls.Add(this.label2);
            this.panCount.Controls.Add(this.txtPrint);
            this.panCount.Controls.Add(this.label1);
            this.panCount.Location = new System.Drawing.Point(4, 6);
            this.panCount.Name = "panCount";
            this.panCount.Size = new System.Drawing.Size(212, 65);
            this.panCount.TabIndex = 14;
            // 
            // txtReview
            // 
            this.txtReview.Enabled = false;
            this.txtReview.Location = new System.Drawing.Point(104, 28);
            this.txtReview.Name = "txtReview";
            this.txtReview.Size = new System.Drawing.Size(100, 20);
            this.txtReview.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Number to review:";
            // 
            // txtPrint
            // 
            this.txtPrint.Enabled = false;
            this.txtPrint.Location = new System.Drawing.Point(104, 3);
            this.txtPrint.Name = "txtPrint";
            this.txtPrint.Size = new System.Drawing.Size(100, 20);
            this.txtPrint.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Number of Prints:";
            // 
            // panSetPrinter
            // 
            this.panSetPrinter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panSetPrinter.Controls.Add(this.txtPrinterName);
            this.panSetPrinter.Controls.Add(this.label8);
            this.panSetPrinter.Controls.Add(this.btnSetPrinter);
            this.panSetPrinter.Location = new System.Drawing.Point(584, 6);
            this.panSetPrinter.Name = "panSetPrinter";
            this.panSetPrinter.Size = new System.Drawing.Size(175, 65);
            this.panSetPrinter.TabIndex = 13;
            // 
            // txtPrinterName
            // 
            this.txtPrinterName.Enabled = false;
            this.txtPrinterName.Location = new System.Drawing.Point(62, 7);
            this.txtPrinterName.Name = "txtPrinterName";
            this.txtPrinterName.Size = new System.Drawing.Size(100, 20);
            this.txtPrinterName.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Printing to:";
            // 
            // panSetSelection
            // 
            this.panSetSelection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panSetSelection.Controls.Add(this.btnNone);
            this.panSetSelection.Controls.Add(this.btnAll);
            this.panSetSelection.Controls.Add(this.btnModifySelection);
            this.panSetSelection.Controls.Add(this.label7);
            this.panSetSelection.Controls.Add(this.label6);
            this.panSetSelection.Controls.Add(this.txtMaxExact);
            this.panSetSelection.Controls.Add(this.txtMinExact);
            this.panSetSelection.Location = new System.Drawing.Point(235, 6);
            this.panSetSelection.Name = "panSetSelection";
            this.panSetSelection.Size = new System.Drawing.Size(324, 65);
            this.panSetSelection.TabIndex = 10;
            // 
            // btnNone
            // 
            this.btnNone.Location = new System.Drawing.Point(207, 42);
            this.btnNone.Name = "btnNone";
            this.btnNone.Size = new System.Drawing.Size(47, 20);
            this.btnNone.TabIndex = 13;
            this.btnNone.Text = "None";
            this.btnNone.UseVisualStyleBackColor = true;
            this.btnNone.Click += new System.EventHandler(this.btnNone_Click);
            // 
            // btnAll
            // 
            this.btnAll.Location = new System.Drawing.Point(260, 42);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(47, 20);
            this.btnAll.TabIndex = 12;
            this.btnAll.Text = "All";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnModifySelection
            // 
            this.btnModifySelection.Location = new System.Drawing.Point(260, 16);
            this.btnModifySelection.Name = "btnModifySelection";
            this.btnModifySelection.Size = new System.Drawing.Size(47, 20);
            this.btnModifySelection.TabIndex = 11;
            this.btnModifySelection.Text = "Set";
            this.btnModifySelection.UseVisualStyleBackColor = true;
            this.btnModifySelection.Click += new System.EventHandler(this.btnModifySelection_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(123, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "and";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Select Between:";
            // 
            // txtMaxExact
            // 
            this.txtMaxExact.Location = new System.Drawing.Point(154, 16);
            this.txtMaxExact.Name = "txtMaxExact";
            this.txtMaxExact.Size = new System.Drawing.Size(100, 20);
            this.txtMaxExact.TabIndex = 9;
            // 
            // txtMinExact
            // 
            this.txtMinExact.Location = new System.Drawing.Point(17, 16);
            this.txtMinExact.Name = "txtMinExact";
            this.txtMinExact.Size = new System.Drawing.Size(100, 20);
            this.txtMinExact.TabIndex = 8;
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Location = new System.Drawing.Point(814, 9);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(70, 49);
            this.btnPrint.TabIndex = 3;
            this.btnPrint.Text = "Print Selected";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // tabControlListBoxes
            // 
            this.tabControlListBoxes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControlListBoxes.Controls.Add(this.tabPrint);
            this.tabControlListBoxes.Controls.Add(this.tabReview);
            this.tabControlListBoxes.Controls.Add(this.tabDone);
            this.tabControlListBoxes.Location = new System.Drawing.Point(0, 3);
            this.tabControlListBoxes.Name = "tabControlListBoxes";
            this.tabControlListBoxes.SelectedIndex = 0;
            this.tabControlListBoxes.Size = new System.Drawing.Size(168, 431);
            this.tabControlListBoxes.TabIndex = 1;
            this.tabControlListBoxes.SelectedIndexChanged += new System.EventHandler(this.tabControlListBoxes_SelectedIndexChanged);
            // 
            // tabPrint
            // 
            this.tabPrint.Controls.Add(this.lstPrint);
            this.tabPrint.Location = new System.Drawing.Point(4, 22);
            this.tabPrint.Name = "tabPrint";
            this.tabPrint.Padding = new System.Windows.Forms.Padding(3);
            this.tabPrint.Size = new System.Drawing.Size(160, 405);
            this.tabPrint.TabIndex = 0;
            this.tabPrint.Text = "Print";
            this.tabPrint.UseVisualStyleBackColor = true;
            // 
            // lstPrint
            // 
            this.lstPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstPrint.CheckOnClick = true;
            this.lstPrint.FormattingEnabled = true;
            this.lstPrint.Location = new System.Drawing.Point(0, 6);
            this.lstPrint.Name = "lstPrint";
            this.lstPrint.Size = new System.Drawing.Size(160, 394);
            this.lstPrint.TabIndex = 0;
            this.lstPrint.SelectedIndexChanged += new System.EventHandler(this.lstPrint_SelectedIndexChanged);
            this.lstPrint.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstPrint_ItemCheck);
            // 
            // tabReview
            // 
            this.tabReview.Controls.Add(this.lstReview);
            this.tabReview.Location = new System.Drawing.Point(4, 22);
            this.tabReview.Name = "tabReview";
            this.tabReview.Padding = new System.Windows.Forms.Padding(3);
            this.tabReview.Size = new System.Drawing.Size(160, 421);
            this.tabReview.TabIndex = 1;
            this.tabReview.Text = "Review";
            this.tabReview.UseVisualStyleBackColor = true;
            // 
            // lstReview
            // 
            this.lstReview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstReview.CheckOnClick = true;
            this.lstReview.FormattingEnabled = true;
            this.lstReview.Location = new System.Drawing.Point(3, 6);
            this.lstReview.Name = "lstReview";
            this.lstReview.Size = new System.Drawing.Size(156, 409);
            this.lstReview.TabIndex = 0;
            this.lstReview.SelectedIndexChanged += new System.EventHandler(this.lstReview_SelectedIndexChanged);
            this.lstReview.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstReview_ItemCheck);
            // 
            // tabDone
            // 
            this.tabDone.Controls.Add(this.lstDone);
            this.tabDone.Location = new System.Drawing.Point(4, 22);
            this.tabDone.Name = "tabDone";
            this.tabDone.Padding = new System.Windows.Forms.Padding(3);
            this.tabDone.Size = new System.Drawing.Size(160, 421);
            this.tabDone.TabIndex = 2;
            this.tabDone.Text = "Printed/Sent";
            this.tabDone.UseVisualStyleBackColor = true;
            // 
            // lstDone
            // 
            this.lstDone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstDone.CheckOnClick = true;
            this.lstDone.FormattingEnabled = true;
            this.lstDone.Location = new System.Drawing.Point(1, 6);
            this.lstDone.Name = "lstDone";
            this.lstDone.Size = new System.Drawing.Size(156, 409);
            this.lstDone.TabIndex = 1;
            // 
            // panItemInfo
            // 
            this.panItemInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panItemInfo.Controls.Add(this.txtRptDate);
            this.panItemInfo.Controls.Add(this.label9);
            this.panItemInfo.Controls.Add(this.txtActionType);
            this.panItemInfo.Controls.Add(this.label5);
            this.panItemInfo.Controls.Add(this.txtUpszone);
            this.panItemInfo.Controls.Add(this.label4);
            this.panItemInfo.Controls.Add(this.txtExact);
            this.panItemInfo.Controls.Add(this.label3);
            this.panItemInfo.Controls.Add(this.btnPreviewReport);
            this.panItemInfo.Location = new System.Drawing.Point(180, 79);
            this.panItemInfo.Name = "panItemInfo";
            this.panItemInfo.Size = new System.Drawing.Size(137, 393);
            this.panItemInfo.TabIndex = 2;
            // 
            // txtRptDate
            // 
            this.txtRptDate.Enabled = false;
            this.txtRptDate.Location = new System.Drawing.Point(14, 179);
            this.txtRptDate.Name = "txtRptDate";
            this.txtRptDate.Size = new System.Drawing.Size(100, 20);
            this.txtRptDate.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Report Date:";
            // 
            // txtActionType
            // 
            this.txtActionType.Enabled = false;
            this.txtActionType.Location = new System.Drawing.Point(14, 127);
            this.txtActionType.Name = "txtActionType";
            this.txtActionType.Size = new System.Drawing.Size(100, 20);
            this.txtActionType.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "ActionType:";
            // 
            // btnBackup
            // 
            this.btnBackup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBackup.Location = new System.Drawing.Point(5, 6);
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Size = new System.Drawing.Size(136, 26);
            this.btnBackup.TabIndex = 13;
            this.btnBackup.Text = "Backup All";
            this.btnBackup.UseVisualStyleBackColor = true;
            this.btnBackup.Visible = false;
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // txtUpszone
            // 
            this.txtUpszone.Enabled = false;
            this.txtUpszone.Location = new System.Drawing.Point(14, 77);
            this.txtUpszone.Name = "txtUpszone";
            this.txtUpszone.Size = new System.Drawing.Size(100, 20);
            this.txtUpszone.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "UPS Zone:";
            // 
            // txtExact
            // 
            this.txtExact.Enabled = false;
            this.txtExact.Location = new System.Drawing.Point(14, 26);
            this.txtExact.Name = "txtExact";
            this.txtExact.Size = new System.Drawing.Size(100, 20);
            this.txtExact.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Exact No:";
            // 
            // btnPreviewReport
            // 
            this.btnPreviewReport.Location = new System.Drawing.Point(14, 223);
            this.btnPreviewReport.Name = "btnPreviewReport";
            this.btnPreviewReport.Size = new System.Drawing.Size(107, 24);
            this.btnPreviewReport.TabIndex = 0;
            this.btnPreviewReport.Text = "Preview Report";
            this.btnPreviewReport.UseVisualStyleBackColor = true;
            this.btnPreviewReport.Click += new System.EventHandler(this.btnPreviewReport_Click);
            // 
            // btnPreviewReports
            // 
            this.btnPreviewReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreviewReports.Location = new System.Drawing.Point(0, 483);
            this.btnPreviewReports.Name = "btnPreviewReports";
            this.btnPreviewReports.Size = new System.Drawing.Size(164, 26);
            this.btnPreviewReports.TabIndex = 12;
            this.btnPreviewReports.Text = "Preview Selected Reports";
            this.btnPreviewReports.UseVisualStyleBackColor = true;
            this.btnPreviewReports.Click += new System.EventHandler(this.btnPreviewReports_Click);
            // 
            // panCryRpt
            // 
            this.panCryRpt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panCryRpt.Controls.Add(this.cryRptViewer);
            this.panCryRpt.Location = new System.Drawing.Point(321, 79);
            this.panCryRpt.Name = "panCryRpt";
            this.panCryRpt.Size = new System.Drawing.Size(742, 473);
            this.panCryRpt.TabIndex = 3;
            // 
            // cryRptViewer
            // 
            this.cryRptViewer.ActiveViewIndex = -1;
            this.cryRptViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cryRptViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cryRptViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.cryRptViewer.Location = new System.Drawing.Point(0, -3);
            this.cryRptViewer.Name = "cryRptViewer";
            this.cryRptViewer.SelectionFormula = "";
            this.cryRptViewer.Size = new System.Drawing.Size(742, 476);
            this.cryRptViewer.TabIndex = 0;
            this.cryRptViewer.ViewTimeSelectionFormula = "";
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // lblSelectedItems
            // 
            this.lblSelectedItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSelectedItems.AutoSize = true;
            this.lblSelectedItems.Location = new System.Drawing.Point(1, 437);
            this.lblSelectedItems.Name = "lblSelectedItems";
            this.lblSelectedItems.Size = new System.Drawing.Size(106, 13);
            this.lblSelectedItems.TabIndex = 4;
            this.lblSelectedItems.Text = "0 out of 0 statements";
            // 
            // btnSelectByUps
            // 
            this.btnSelectByUps.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectByUps.Location = new System.Drawing.Point(0, 453);
            this.btnSelectByUps.Name = "btnSelectByUps";
            this.btnSelectByUps.Size = new System.Drawing.Size(164, 23);
            this.btnSelectByUps.TabIndex = 1;
            this.btnSelectByUps.Text = "Select By UPS Zones";
            this.btnSelectByUps.UseVisualStyleBackColor = true;
            this.btnSelectByUps.Click += new System.EventHandler(this.btnSelectByUps_Click);
            // 
            // saveLivPdfBackupDialog
            // 
            this.saveLivPdfBackupDialog.DefaultExt = "pdf";
            this.saveLivPdfBackupDialog.Filter = "pdf files|*.pdf";
            this.saveLivPdfBackupDialog.Title = "Select Liv Backup name";
            // 
            // saveUcPdfBackupDialog
            // 
            this.saveUcPdfBackupDialog.DefaultExt = "pdf";
            this.saveUcPdfBackupDialog.Filter = "pdf files|*.pdf";
            this.saveUcPdfBackupDialog.Title = "Select UC backup name";
            // 
            // saveOtherspdfBackupDialog
            // 
            this.saveOtherspdfBackupDialog.DefaultExt = "pdf";
            this.saveOtherspdfBackupDialog.Filter = "pdf files|*.pdf";
            this.saveOtherspdfBackupDialog.Title = "Select Uncategorised backup name";
            // 
            // saveReviewpdfbackupDialog
            // 
            this.saveReviewpdfbackupDialog.DefaultExt = "pdf";
            this.saveReviewpdfbackupDialog.Filter = "pdf files|*.pdf";
            this.saveReviewpdfbackupDialog.Title = "Select Review backup name";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Database";
            // 
            // cboDatabase
            // 
            this.cboDatabase.FormattingEnabled = true;
            this.cboDatabase.Location = new System.Drawing.Point(61, 5);
            this.cboDatabase.Name = "cboDatabase";
            this.cboDatabase.Size = new System.Drawing.Size(52, 21);
            this.cboDatabase.TabIndex = 15;
            this.cboDatabase.Text = "AU";
            this.cboDatabase.SelectedIndexChanged += new System.EventHandler(this.cboDatabase_SelectedIndexChanged);
            // 
            // btnGet
            // 
            this.btnGet.Location = new System.Drawing.Point(119, 3);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(46, 23);
            this.btnGet.TabIndex = 16;
            this.btnGet.Text = "Get";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // btnBackupSelected
            // 
            this.btnBackupSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBackupSelected.Location = new System.Drawing.Point(5, 40);
            this.btnBackupSelected.Name = "btnBackupSelected";
            this.btnBackupSelected.Size = new System.Drawing.Size(137, 23);
            this.btnBackupSelected.TabIndex = 17;
            this.btnBackupSelected.Text = "Backup Selected Reports";
            this.btnBackupSelected.UseVisualStyleBackColor = true;
            this.btnBackupSelected.Click += new System.EventHandler(this.btnBackupSelected_Click);
            // 
            // saveSelectedFilesDialog
            // 
            this.saveSelectedFilesDialog.DefaultExt = "pdf";
            this.saveSelectedFilesDialog.Filter = "pdf files|*.pdf";
            this.saveSelectedFilesDialog.Title = "Select backup name";
            // 
            // panLeft
            // 
            this.panLeft.Controls.Add(this.btnSelectByUps);
            this.panLeft.Controls.Add(this.btnPreviewReports);
            this.panLeft.Controls.Add(this.lblSelectedItems);
            this.panLeft.Controls.Add(this.tabControlListBoxes);
            this.panLeft.Location = new System.Drawing.Point(3, 38);
            this.panLeft.Name = "panLeft";
            this.panLeft.Size = new System.Drawing.Size(171, 513);
            this.panLeft.TabIndex = 18;
            // 
            // panDbSelect
            // 
            this.panDbSelect.Controls.Add(this.label10);
            this.panDbSelect.Controls.Add(this.btnGet);
            this.panDbSelect.Controls.Add(this.cboDatabase);
            this.panDbSelect.Location = new System.Drawing.Point(5, 3);
            this.panDbSelect.Name = "panDbSelect";
            this.panDbSelect.Size = new System.Drawing.Size(168, 31);
            this.panDbSelect.TabIndex = 19;
            // 
            // panBackup
            // 
            this.panBackup.Controls.Add(this.btnBackupSelected);
            this.panBackup.Controls.Add(this.btnBackup);
            this.panBackup.Location = new System.Drawing.Point(176, 483);
            this.panBackup.Name = "panBackup";
            this.panBackup.Size = new System.Drawing.Size(145, 68);
            this.panBackup.TabIndex = 20;
            // 
            // FormClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 554);
            this.Controls.Add(this.panBackup);
            this.Controls.Add(this.panDbSelect);
            this.Controls.Add(this.panLeft);
            this.Controls.Add(this.panCryRpt);
            this.Controls.Add(this.panItemInfo);
            this.Controls.Add(this.panTop);
            this.Name = "FormClient";
            this.Text = "Monthly Statement Printer v2.0";
            this.Shown += new System.EventHandler(this.FormClient_Shown);
            this.panTop.ResumeLayout(false);
            this.panCount.ResumeLayout(false);
            this.panCount.PerformLayout();
            this.panSetPrinter.ResumeLayout(false);
            this.panSetPrinter.PerformLayout();
            this.panSetSelection.ResumeLayout(false);
            this.panSetSelection.PerformLayout();
            this.tabControlListBoxes.ResumeLayout(false);
            this.tabPrint.ResumeLayout(false);
            this.tabReview.ResumeLayout(false);
            this.tabDone.ResumeLayout(false);
            this.panItemInfo.ResumeLayout(false);
            this.panItemInfo.PerformLayout();
            this.panCryRpt.ResumeLayout(false);
            this.panLeft.ResumeLayout(false);
            this.panLeft.PerformLayout();
            this.panDbSelect.ResumeLayout(false);
            this.panDbSelect.PerformLayout();
            this.panBackup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSetPrinter;
        private System.Windows.Forms.Panel panTop;
        private System.Windows.Forms.TabControl tabControlListBoxes;
        private System.Windows.Forms.TabPage tabPrint;
        private System.Windows.Forms.TabPage tabReview;
        private System.Windows.Forms.CheckedListBox lstPrint;
        private System.Windows.Forms.CheckedListBox lstReview;
        private System.Windows.Forms.TextBox txtReview;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPrint;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Panel panItemInfo;
        private System.Windows.Forms.Button btnPreviewReport;
        private System.Windows.Forms.Panel panCryRpt;
        private System.Windows.Forms.TextBox txtExact;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtActionType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUpszone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panSetSelection;
        private System.Windows.Forms.Button btnModifySelection;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMaxExact;
        private System.Windows.Forms.TextBox txtMinExact;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.TextBox txtPrinterName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panSetPrinter;
        private System.Windows.Forms.Panel panCount;
        private System.Windows.Forms.Label lblSelectedItems;
        private System.Windows.Forms.Button btnNone;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.TextBox txtRptDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnPreviewReports;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer cryRptViewer;
        private System.Windows.Forms.Button btnSelectByUps;
        private System.Windows.Forms.Button btnBackup;
        private System.Windows.Forms.SaveFileDialog saveLivPdfBackupDialog;
        private System.Windows.Forms.SaveFileDialog saveUcPdfBackupDialog;
        private System.Windows.Forms.SaveFileDialog saveOtherspdfBackupDialog;
        private System.Windows.Forms.SaveFileDialog saveReviewpdfbackupDialog;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboDatabase;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.TabPage tabDone;
        private System.Windows.Forms.CheckedListBox lstDone;
        private System.Windows.Forms.Button btnBackupSelected;
        private System.Windows.Forms.SaveFileDialog saveSelectedFilesDialog;
        private System.Windows.Forms.Panel panLeft;
        private System.Windows.Forms.Panel panDbSelect;
        private System.Windows.Forms.Panel panBackup;

    }
}

