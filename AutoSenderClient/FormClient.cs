using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;
using AutoSenderLibrarySupportTester;
using AutoSenderLogin;

namespace AutoSenderClient
{
    public partial class FormClient : Form
    {
        //globals
        private CustomerContactList contacts;
        private string printerToUse;
        private readonly string formname = "Monthly Statement Printer ";
        private DatabaseEnvironment _dbEnvironment = DatabaseEnvironment.DefaultEnvironment;
        private string _username = "";
        private FormLogin _loginForm;
        private bool _authenticated;
        private bool _hasStatementAdminPermission;
        private bool _hasPrintStatementPermission;

        public FormClient()
        {
            PreChecks();
            _loginForm = new FormLogin();
            InitEnvironment();
            
        }

        public FormClient(string username, string password,string dataBase)
        {
            PreChecks();
            _loginForm = new FormLogin(username, password, dataBase);
            InitEnvironment();
        }

        private void PreChecks()
        {
            // Test crystal report support
            if (!MonthlyStatementReportTester.TestCrystalReportSupport())
            {
                MessageBox.Show(MonthlyStatementReportTester.TestCrystalReportSupportFailedMessage());
                System.Environment.Exit(1);
                return;
            }

            // Check if barcode font is installed
            if (!MonthlyStatementReportTester.TestBarcodeFontInstalled())
            {
                MessageBox.Show(MonthlyStatementReportTester.TestBarcodeFontInstalledFailedMessage());
                System.Environment.Exit(1);
                return;
            }
        }

        private void InitEnvironment()
        {
            InitializeComponent();

            DatabaseEnvironment[] environments = DatabaseEnvironments.GetEnvironments();

            // Add permission to Database list (cboDatabase) for selection
            foreach (DatabaseEnvironment environment in environments)
            {
                cboDatabase.Items.Add(environment);
            }

            cboDatabase.SelectedIndex = 0;

            while (_loginForm.AuthenticatedDbs().Length == 0)
            {
                _loginForm.ShowDialog();

                if (_loginForm.DialogResult == DialogResult.Cancel)
                {
                    // break and go to main form
                    break;
                }
            }

            SetFormPermissions();
            SetFormControls();
        }

        // helper to populate Listbox with customer numbers
        private void PopulateTable(CheckedListBox lst, ActionType actiontype, CheckState defaultCheckState)
        {
            lst.Items.Clear(); // clear existing items

            ActionTypeList actions = new ActionTypeList();
            actions.addAction(actiontype);
            
            // populate
            foreach (string customer in contacts.getListOfCustomers(actions))
                lst.Items.Add(customer, defaultCheckState);
        }

        #region events
        // update panItemInfo panel when index changes
        private void lstPrint_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstPrint.SelectedIndex >= 0 && lstPrint.SelectedIndex < lstPrint.Items.Count)            
            {
                CustomerContact contact = contacts.getCustomerContact(lstPrint.Items[lstPrint.SelectedIndex].ToString());

                UpdatePanItemInfo(contact);
            }
        }

        // update panItemInfo panel when index changes
        private void lstReview_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstReview.SelectedIndex >= 0 && lstReview.SelectedIndex < lstReview.Items.Count)
            {
                CustomerContact contact = contacts.getCustomerContact(lstReview.Items[lstReview.SelectedIndex].ToString());

                UpdatePanItemInfo(contact);
            }
        }

        // Helper to list information on customer
        private void UpdatePanItemInfo(CustomerContact contact)
        {
            txtExact.Text = contact.getCustomerNumber();
            txtUpszone.Text = contact.getUpszone();
            txtActionType.Text = ActionTypeList.getStringFromActionType(contact.getActionType());
            txtRptDate.Text = MonthlyStatementReport.getLatestMonthlyStatementDate().ToShortDateString();
        }
        #endregion

        private void btnPreviewReport_Click(object sender, EventArgs e)
        {
            CursorBusyStatus("Generating Report for Preview");

            // Generate statement
            MonthlyStatementReport rpt = new MonthlyStatementReport(txtExact.Text, MonthlyStatementReport.getLatestMonthlyStatementDate(), _dbEnvironment);

            // still not sure what is causing the crystal report viewer to print all statements during first print
            if (cryRptViewer.ReportSource == null)
            {
                cryRptViewer.ReportSource = rpt.getCrystalReport();
            }
            // Set report source
            cryRptViewer.ReportSource = rpt.getCrystalReport();

            CursorDefaultStatus();
        }

        // select boxes between
        private void btnModifySelection_Click(object sender, EventArgs e)
        {
            // Get active listbox
            CheckedListBox lst = GetActiveListBox();

            try
            {
                double minexact = double.Parse(txtMinExact.Text);
                double maxexact = double.Parse(txtMaxExact.Text);
                
                // check items in list
                int i;
                for (i = 0; i < lst.Items.Count; i++ )
                {
                    // trim and parse to double
                    string exact = lst.Items[i].ToString().Trim();
                    double exactno = double.Parse(exact);

                    // check if item is within range
                    if (exactno >= minexact && exactno <= maxexact)
                        lst.SetItemCheckState(i, CheckState.Checked);
                    else
                        lst.SetItemCheckState(i, CheckState.Unchecked);
                }
            }
            catch 
            {
                MessageBox.Show("Invalid Exact Numbers given in search");
            }
        }

        // Helper to return active listbox
        private CheckedListBox GetActiveListBox()
        {
            CheckedListBox lst = null;

            string selectedTab = tabControlListBoxes.SelectedTab.Text.ToString();

            switch (selectedTab)
            {
                case "Print":
                    lst = lstPrint;
                    break;
                case "Review":
                    lst = lstReview;
                    break;
                case "Printed/Sent":
                    lst = lstDone;
                    break;
            }

            return lst;
        }

        // Open Printer dialog box if clicked
        private void btnSetPrinter_Click(object sender, EventArgs e)
        {
            if(SetPrinter())
                MessageBox.Show("Printer has been set to " + printerToUse);
        }

        // Helper for setting printer to use
        private bool SetPrinter()
        {
            // Show printer dialog
            DialogResult printresult = printDialog1.ShowDialog();

            // set printer to use
            printerToUse = printDialog1.PrinterSettings.PrinterName;
            txtPrinterName.Text = printerToUse;

            // return set printer result
            if (printresult == DialogResult.Cancel)
                return false;
            else
                return true;
        }

        // Helper for getting exact nos checked in selected list box
        private string[] GetCheckedItems(CheckedListBox lst)
        {
            // Get items in listbox
            List<string> prints = new List<string>();
            int i;
            for (i = 0; i < lst.CheckedIndices.Count; i++)
            {
                // remove empty spaces from listbox
                prints.Add(lst.CheckedItems[i].ToString().Trim());
            }

            prints.Sort();

            return prints.ToArray();
        }

        // Helper for getting all exact nos in selected list box
        private string[] GetAllItems(CheckedListBox lst)
        {
            // Get items in listbox
            List<string> prints = new List<string>();
            int i;
            for (i = 0; i < lst.Items.Count; i++)
            {
                // remove empty spaces from listbox
                prints.Add(lst.Items[i].ToString().Trim());
            }

            prints.Sort();

            return prints.ToArray();
        }

        // Ask for confirmation then print
        private void btnPrint_Click(object sender, EventArgs e)
        {
            // Show print dialog
            if (SetPrinter())
            {
                // Get active listbox and selected exact nos in listbox
                CheckedListBox lst = GetActiveListBox();
                string[] prints = GetCheckedItems(lst);

                // Confirm selected prints
                int count = prints.Length;
                string msg = string.Format("Do you want to print {0} {1} section report(s) to Printer \"{2}\"?", count, tabControlListBoxes.SelectedTab.Text, printerToUse);
                DialogResult result = MessageBox.Show(msg, "Print Selected Documents", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    #region single document (Not in use, using multiple documents)
                    // For each document
                    /*
                    int i = 1;
                    foreach (string print in prints)
                    {
                        // create report
                        CursorBusyStatus(string.Format("Creating document {0} of {1}", i, count));
                        MonthlyStatementReport rpt = new MonthlyStatementReport(print, MonthlyStatementReport.getLatestMonthlyStatementDate());

                        // print to printer
                        CursorBusyStatus(string.Format("Printing document {0} of {1}", i, count));
                        rpt.PrinterName = printerToUse;
                        bool printsucceed = rpt.printReportToPrinter();

                        // Insert into log
                        if (printsucceed)
                        {
                            // set up print log
                            CustomerContact customer = contacts.getCustomerContact(print);

                            // Insert print log
                            log.InsertPrintLog(customer,false,false);
                        }

                        // dispose of report
                        rpt.Dispose();

                        i++;
                    }*/
                    #endregion

                    #region multiple documents
                    // create report
                    CursorBusyStatus("Creating documents");
                    MonthlyStatementReports rpt = new MonthlyStatementReports(prints, MonthlyStatementReports.getLatestMonthlyStatementDate(), _dbEnvironment);

                    // print to printer
                    CursorBusyStatus("Printing documents");
                    rpt.PrinterName = printerToUse;
                    bool printsucceed = rpt.printReportToPrinter();
                    #endregion

                    // Ask if correctly printed
                    #region Check correctly printed
                    if (printsucceed)
                    {
                        DialogResult printresult = MessageBox.Show("Have the printouts been correctly printed?", "Correctly printed?", MessageBoxButtons.YesNo);

                        if (printresult == DialogResult.Yes)
                        {
                            StatementActionsLogger log = new StatementActionsLogger(_username,_dbEnvironment);
                            CursorBusyStatus("Updating log");

                            // For each document
                            foreach (string print in prints)
                            {
                                // set up and insert print log
                                CustomerContact customer = contacts.getCustomerContact(print);
                                log.InsertPrintLog(customer);

                                // Delete print log from active list and put into done
                                lst.Items.Remove(print);

                                lstDone.Items.Add(print);
                            }

                            CursorDefaultStatus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error printing document(s)");
                    }

                    #endregion

                    CursorDefaultStatus();
                }
            }
        }

        // update checked item count when item is checked/unchecked
        private void lstPrint_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            ChangeCheckedCount(lstPrint, e.CurrentValue);
        }

        // As above
        private void lstReview_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            ChangeCheckedCount(lstReview, e.CurrentValue);
        }

        // helper to update check count
        private void ChangeCheckedCount(CheckedListBox lst, CheckState currentstate)
        { 
            int modifier = 0;
            if (currentstate == CheckState.Checked)
                modifier = -1;
            else if (currentstate == CheckState.Unchecked)
                modifier = 1;

            UpdateCheckedCount(lst, modifier);
        }

        // Helper to write checked count label value
        private void UpdateCheckedCount(CheckedListBox lst, int modifier)
        {
            int count = lst.CheckedIndices.Count + modifier;
            int total = lst.Items.Count;
            lblSelectedItems.Text = count.ToString() + " of " + total.ToString() + " documents selected";
        }

        // update checked count when tab changed
        private void tabControlListBoxes_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateCheckedCount(GetActiveListBox(), 0);
        }

        #region cursors
        private void CursorBusyStatus(string status)
        {
            this.Text = formname + "(" + status + ")";
            this.Cursor = Cursors.WaitCursor;
        }

        private void CursorDefaultStatus()
        {
            this.Text = formname;
            this.Cursor = Cursors.Default;
        }
        #endregion

        // if select all is selected
        private void btnAll_Click(object sender, EventArgs e)
        {
            ChangeAllSelection(GetActiveListBox(), CheckState.Checked);
        }

        // if select none is selected
        private void btnNone_Click(object sender, EventArgs e)
        {
            ChangeAllSelection(GetActiveListBox(), CheckState.Unchecked);
        }

        // helper for BtnAll_click, BtnNone_click
        private void ChangeAllSelection(CheckedListBox lst, CheckState state)
        {
            // For all items in list
            int i;
            for (i = 0; i < lst.Items.Count; i++)
            {
                lst.SetItemCheckState(i, state);
            }
        }

        // Perform loading of information once form client is shown
        private void FormClient_Shown(object sender, EventArgs e)
        {
            //GetCustomers();
        }

        private void GetCustomers()
        {
            CursorBusyStatus("Getting Customer Data from Database");

            // Get Print and Review actions from contact list.
            ActionTypeList actions = new ActionTypeList(new ActionType[] { ActionType.Print, ActionType.Review, ActionType.Sent });
            contacts = new CustomerContactList(actions, _dbEnvironment);

            // Populate Listboxes
            // print
            PopulateTable(lstPrint, ActionType.Print, CheckState.Checked);

            // review
            PopulateTable(lstReview, ActionType.Review, CheckState.Checked);

            // sent
            PopulateTable(lstDone, ActionType.Sent, CheckState.Unchecked);

            // number of entries
            txtPrint.Text = lstPrint.Items.Count.ToString();
            txtReview.Text = lstReview.Items.Count.ToString();

            // number of selected entries
            UpdateCheckedCount(lstPrint, 0);

            // set default printer
            System.Drawing.Printing.PrinterSettings defaultprintersettings = new System.Drawing.Printing.PrinterSettings();
            printerToUse = defaultprintersettings.PrinterName;
            txtPrinterName.Text = printerToUse;

            CursorDefaultStatus();
        }

        private void btnPreviewReports_Click(object sender, EventArgs e)
        {
            CursorBusyStatus("Generating Reports for Preview");

            // Get active listbox and selected exact nos in listbox
            CheckedListBox lst = GetActiveListBox();
            string[] prints = GetCheckedItems(lst);

            // preview report if not empty list
            if (prints.Length > 0)
            {
                // Generate statement
                MonthlyStatementReports rpt = new MonthlyStatementReports(prints, MonthlyStatementReport.getLatestMonthlyStatementDate(), _dbEnvironment);

                // still not sure what is causing the crystal report viewer to print all statements during first print
                if (cryRptViewer.ReportSource == null)
                {
                    cryRptViewer.ReportSource = rpt.getCrystalReport();
                }
                // Set report source
                cryRptViewer.ReportSource = rpt.getCrystalReport();
            }

            CursorDefaultStatus();
        }

        private void btnSelectByUps_Click(object sender, EventArgs e)
        {
            // Get Customer contacts in list
            CheckedListBox lst = GetActiveListBox();
            string[] prints = GetAllItems(lst);

            // Show UPS selection dialog
            FormUpsSelection upsselection = new FormUpsSelection(contacts.GetCustomersContacts(prints));
            DialogResult selectionresult = upsselection.ShowDialog();

            // if OK clicked, make changes to selection
            if (selectionresult == DialogResult.OK)
            {
                List<string> selectedcustomers = upsselection.GetSelectedCustomerNos();

                // check items in selected ups zone, uncheck the rest
                for (int i = 0; i < lst.Items.Count; i++)
                {
                    if (selectedcustomers.Contains(lst.Items[i].ToString()))
                        lst.SetItemCheckState(i, CheckState.Checked);
                    else
                        lst.SetItemCheckState(i, CheckState.Unchecked);
                }
            }
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            string[] prints = GetCheckedItems(lstPrint);
            string[] reviews = GetCheckedItems(lstReview);

            List<string> backupLiv = new List<string>();
            List<string> backupUc = new List<string>();
            List<string> backupOthers = new List<string>();

            string backupPathLiv = "";
            string backupPathUc = "";
            string backupPathOthers = "";
            string backupPathReview = "";
            bool doBackupLiv = false;
            bool doBackupUc = false;
            bool doBackupOthers = false;
            bool doBackupReview = false;

            foreach (string print in prints)
            {
                switch (print.Substring(0,2))
                {
                    case "10" :
                        backupLiv.Add(print);
                        break;
                    case "60" :
                        backupUc.Add(print);
                        break;
                    default:
                        backupOthers.Add(print);
                        break;
                }
            }

            if (backupLiv.Count > 0 && saveLivPdfBackupDialog.ShowDialog() == DialogResult.OK)
            {
                backupPathLiv = saveLivPdfBackupDialog.FileName;
                doBackupLiv = true;
            }

            if (backupUc.Count > 0 && saveUcPdfBackupDialog.ShowDialog() == DialogResult.OK)
            {
                backupPathUc = saveUcPdfBackupDialog.FileName;
                doBackupUc = true;
            }

            if (backupOthers.Count > 0 && saveOtherspdfBackupDialog.ShowDialog() == DialogResult.OK)
            {
                backupPathOthers = saveUcPdfBackupDialog.FileName;
                doBackupOthers = true;
            }

            if (reviews.Length > 0 && saveReviewpdfbackupDialog.ShowDialog() == DialogResult.OK)
            {
                backupPathReview = saveUcPdfBackupDialog.FileName;
                doBackupReview = true;
            }

            if (doBackupLiv)
                BackupCustomers(backupPathLiv, backupLiv.ToArray());
            if (doBackupUc)
                BackupCustomers(backupPathUc, backupUc.ToArray());
            if (doBackupOthers)
                BackupCustomers(backupPathOthers, backupUc.ToArray());
            if (doBackupReview)
                BackupCustomers(backupPathReview, backupUc.ToArray());
        }

        private void btnBackupSelected_Click(object sender, EventArgs e)
        {
            string[] backups = GetCheckedItems(GetActiveListBox());
            string backupPath = "";
            bool doBackup = false;

            if (backups.Length > 0 && saveSelectedFilesDialog.ShowDialog() == DialogResult.OK)
            {
                backupPath = saveSelectedFilesDialog.FileName;
                doBackup = true;
            }

            if (doBackup)
                BackupCustomers(backupPath, backups);
        }

        private void BackupCustomers(string backupPath, string[] backupCustomers)
        {
            if (backupPath != "")
            {
                MonthlyStatementReports backupReport = new MonthlyStatementReports(backupCustomers, MonthlyStatementReports.getLatestMonthlyStatementDate(), _dbEnvironment);
                if (backupReport.isReportSuccessfullyGenerated())
                {
                    // Export file to location, allowing overwrites of existing files
                    string filePath = backupReport.exportToPDF();
                    System.IO.File.Copy(filePath, backupPath, true);
                }
            }
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            _dbEnvironment = GetDatabase();

            if (_hasPrintStatementPermission)
            {
                // has print statement permission
                GetCustomers();
            }
            else
            {
                // Prompt to Login
                _loginForm.SwitchDb(_dbEnvironment);
                _loginForm.ShowDialog();
            }
        }

        private DatabaseEnvironment GetDatabase()
        {
            return DatabaseEnvironments.GetEnvironmentFromString((cboDatabase.SelectedItem == null ? "" : cboDatabase.SelectedItem.ToString()));
        }

        private void cboDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetFormPermissions();
            SetFormControls();
        }

        private void SetFormPermissions()
        {
            _dbEnvironment = GetDatabase();
            _username = _loginForm.GetUsername(_dbEnvironment);

            _authenticated = _loginForm.IsAuthenticated(_dbEnvironment);
            _hasStatementAdminPermission = UserLogin.HasAdministratorPermission(_username, _dbEnvironment);
            _hasPrintStatementPermission = _hasStatementAdminPermission || UserLogin.HasPrintStatementPermission(_username, _dbEnvironment);
        }

        // Enable/disable form controls according to whether user is logged in
        private void SetFormControls()
        {
            if (_authenticated && _hasPrintStatementPermission)
            {
                panDbSelect.Enabled = true;
                btnGet.Text = "Get";
                panTop.Enabled = true;
                panLeft.Enabled = true;
                panItemInfo.Enabled = true;
                panBackup.Enabled = true;
                btnBackup.Visible = _hasStatementAdminPermission;
            }
            else
            {
                if (_authenticated)
                    MessageBox.Show("Your account does not give you permission to print statements, please contact IT or login as a different user");

                panDbSelect.Enabled = true;
                btnGet.Text = "Login";
                panTop.Enabled = false;
                panLeft.Enabled = false;
                panItemInfo.Enabled = false;
                panBackup.Enabled = false;
            }
        }
    }
}