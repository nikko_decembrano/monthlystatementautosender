namespace AutoSendServerTrial
{
    partial class AutoSendServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnJobStart = new System.Windows.Forms.Button();
            this.txtEmailStatus = new System.Windows.Forms.TextBox();
            this.btnJobStop = new System.Windows.Forms.Button();
            this.txtMinExact = new System.Windows.Forms.TextBox();
            this.txtMaxExact = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chkEmail = new System.Windows.Forms.CheckBox();
            this.chkFax = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmailsLeft = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFaxesLeft = new System.Windows.Forms.TextBox();
            this.panLeft = new System.Windows.Forms.Panel();
            this.panSent = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFaxesSent = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEmailsSent = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panFailed = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFaxesFailed = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtEmailsFailed = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.sendProgressBar = new System.Windows.Forms.ProgressBar();
            this.label12 = new System.Windows.Forms.Label();
            this.rdoMonthlyStatement = new System.Windows.Forms.RadioButton();
            this.rdoReminder = new System.Windows.Forms.RadioButton();
            this.txtOverDateForReminder = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.rdoNewTnC = new System.Windows.Forms.RadioButton();
            this.rdoSpecify = new System.Windows.Forms.RadioButton();
            this.rdoRange = new System.Windows.Forms.RadioButton();
            this.txtContNo = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdoTnC2018 = new System.Windows.Forms.RadioButton();
            this.panLeft.SuspendLayout();
            this.panSent.SuspendLayout();
            this.panFailed.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnJobStart
            // 
            this.btnJobStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnJobStart.Location = new System.Drawing.Point(374, 29);
            this.btnJobStart.Name = "btnJobStart";
            this.btnJobStart.Size = new System.Drawing.Size(75, 23);
            this.btnJobStart.TabIndex = 0;
            this.btnJobStart.Text = "Start Job";
            this.btnJobStart.UseVisualStyleBackColor = true;
            this.btnJobStart.Click += new System.EventHandler(this.btnJobStart_Click);
            // 
            // txtEmailStatus
            // 
            this.txtEmailStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailStatus.Location = new System.Drawing.Point(12, 157);
            this.txtEmailStatus.Multiline = true;
            this.txtEmailStatus.Name = "txtEmailStatus";
            this.txtEmailStatus.ReadOnly = true;
            this.txtEmailStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEmailStatus.Size = new System.Drawing.Size(351, 494);
            this.txtEmailStatus.TabIndex = 1;
            this.txtEmailStatus.TextChanged += new System.EventHandler(this.txtEmailStatus_TextChanged);
            // 
            // btnJobStop
            // 
            this.btnJobStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnJobStop.Enabled = false;
            this.btnJobStop.Location = new System.Drawing.Point(374, 58);
            this.btnJobStop.Name = "btnJobStop";
            this.btnJobStop.Size = new System.Drawing.Size(75, 23);
            this.btnJobStop.TabIndex = 2;
            this.btnJobStop.Text = "Stop Job";
            this.btnJobStop.UseVisualStyleBackColor = true;
            this.btnJobStop.Click += new System.EventHandler(this.btnJobStop_Click);
            // 
            // txtMinExact
            // 
            this.txtMinExact.Enabled = false;
            this.txtMinExact.Location = new System.Drawing.Point(157, 68);
            this.txtMinExact.Name = "txtMinExact";
            this.txtMinExact.Size = new System.Drawing.Size(76, 20);
            this.txtMinExact.TabIndex = 3;
            this.txtMinExact.Text = "100000000";
            // 
            // txtMaxExact
            // 
            this.txtMaxExact.Enabled = false;
            this.txtMaxExact.Location = new System.Drawing.Point(270, 68);
            this.txtMaxExact.Name = "txtMaxExact";
            this.txtMaxExact.Size = new System.Drawing.Size(76, 20);
            this.txtMaxExact.TabIndex = 4;
            this.txtMaxExact.Text = "699999999";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Send Between ExactNo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(239, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "and";
            // 
            // chkEmail
            // 
            this.chkEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkEmail.AutoSize = true;
            this.chkEmail.Location = new System.Drawing.Point(369, 99);
            this.chkEmail.Name = "chkEmail";
            this.chkEmail.Size = new System.Drawing.Size(84, 17);
            this.chkEmail.TabIndex = 7;
            this.chkEmail.Text = "Send Emails";
            this.chkEmail.UseVisualStyleBackColor = true;
            // 
            // chkFax
            // 
            this.chkFax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFax.AutoSize = true;
            this.chkFax.Location = new System.Drawing.Point(369, 122);
            this.chkFax.Name = "chkFax";
            this.chkFax.Size = new System.Drawing.Size(82, 17);
            this.chkFax.TabIndex = 8;
            this.chkFax.Text = "Send Faxes";
            this.chkFax.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Left to Send:";
            // 
            // txtEmailsLeft
            // 
            this.txtEmailsLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailsLeft.Enabled = false;
            this.txtEmailsLeft.Location = new System.Drawing.Point(0, 51);
            this.txtEmailsLeft.Name = "txtEmailsLeft";
            this.txtEmailsLeft.Size = new System.Drawing.Size(84, 20);
            this.txtEmailsLeft.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Emails:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Faxes:";
            // 
            // txtFaxesLeft
            // 
            this.txtFaxesLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFaxesLeft.Enabled = false;
            this.txtFaxesLeft.Location = new System.Drawing.Point(0, 90);
            this.txtFaxesLeft.Name = "txtFaxesLeft";
            this.txtFaxesLeft.Size = new System.Drawing.Size(84, 20);
            this.txtFaxesLeft.TabIndex = 12;
            // 
            // panLeft
            // 
            this.panLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panLeft.Controls.Add(this.label5);
            this.panLeft.Controls.Add(this.txtFaxesLeft);
            this.panLeft.Controls.Add(this.label4);
            this.panLeft.Controls.Add(this.txtEmailsLeft);
            this.panLeft.Controls.Add(this.label3);
            this.panLeft.Location = new System.Drawing.Point(369, 523);
            this.panLeft.Name = "panLeft";
            this.panLeft.Size = new System.Drawing.Size(85, 117);
            this.panLeft.TabIndex = 14;
            // 
            // panSent
            // 
            this.panSent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panSent.Controls.Add(this.label6);
            this.panSent.Controls.Add(this.txtFaxesSent);
            this.panSent.Controls.Add(this.label7);
            this.panSent.Controls.Add(this.txtEmailsSent);
            this.panSent.Controls.Add(this.label8);
            this.panSent.Location = new System.Drawing.Point(369, 281);
            this.panSent.Name = "panSent";
            this.panSent.Size = new System.Drawing.Size(85, 118);
            this.panSent.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Faxes:";
            // 
            // txtFaxesSent
            // 
            this.txtFaxesSent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFaxesSent.Enabled = false;
            this.txtFaxesSent.Location = new System.Drawing.Point(0, 92);
            this.txtFaxesSent.Name = "txtFaxesSent";
            this.txtFaxesSent.Size = new System.Drawing.Size(84, 20);
            this.txtFaxesSent.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Emails:";
            // 
            // txtEmailsSent
            // 
            this.txtEmailsSent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailsSent.Enabled = false;
            this.txtEmailsSent.Location = new System.Drawing.Point(0, 53);
            this.txtEmailsSent.Name = "txtEmailsSent";
            this.txtEmailsSent.Size = new System.Drawing.Size(84, 20);
            this.txtEmailsSent.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(2, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Sent:";
            // 
            // panFailed
            // 
            this.panFailed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panFailed.Controls.Add(this.label9);
            this.panFailed.Controls.Add(this.txtFaxesFailed);
            this.panFailed.Controls.Add(this.label10);
            this.panFailed.Controls.Add(this.txtEmailsFailed);
            this.panFailed.Controls.Add(this.label11);
            this.panFailed.Location = new System.Drawing.Point(369, 403);
            this.panFailed.Name = "panFailed";
            this.panFailed.Size = new System.Drawing.Size(85, 115);
            this.panFailed.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Faxes:";
            // 
            // txtFaxesFailed
            // 
            this.txtFaxesFailed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFaxesFailed.Enabled = false;
            this.txtFaxesFailed.Location = new System.Drawing.Point(1, 90);
            this.txtFaxesFailed.Name = "txtFaxesFailed";
            this.txtFaxesFailed.Size = new System.Drawing.Size(84, 20);
            this.txtFaxesFailed.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Emails:";
            // 
            // txtEmailsFailed
            // 
            this.txtEmailsFailed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailsFailed.Enabled = false;
            this.txtEmailsFailed.Location = new System.Drawing.Point(1, 51);
            this.txtEmailsFailed.Name = "txtEmailsFailed";
            this.txtEmailsFailed.Size = new System.Drawing.Size(84, 20);
            this.txtEmailsFailed.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Failed:";
            // 
            // sendProgressBar
            // 
            this.sendProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.sendProgressBar.Location = new System.Drawing.Point(12, 128);
            this.sendProgressBar.Name = "sendProgressBar";
            this.sendProgressBar.Size = new System.Drawing.Size(351, 23);
            this.sendProgressBar.TabIndex = 17;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Progress:";
            // 
            // rdoMonthlyStatement
            // 
            this.rdoMonthlyStatement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdoMonthlyStatement.AutoSize = true;
            this.rdoMonthlyStatement.Location = new System.Drawing.Point(370, 145);
            this.rdoMonthlyStatement.Name = "rdoMonthlyStatement";
            this.rdoMonthlyStatement.Size = new System.Drawing.Size(62, 17);
            this.rdoMonthlyStatement.TabIndex = 19;
            this.rdoMonthlyStatement.TabStop = true;
            this.rdoMonthlyStatement.Text = "Monthly";
            this.rdoMonthlyStatement.UseVisualStyleBackColor = true;
            // 
            // rdoReminder
            // 
            this.rdoReminder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdoReminder.AutoSize = true;
            this.rdoReminder.Location = new System.Drawing.Point(370, 167);
            this.rdoReminder.Name = "rdoReminder";
            this.rdoReminder.Size = new System.Drawing.Size(70, 17);
            this.rdoReminder.TabIndex = 20;
            this.rdoReminder.TabStop = true;
            this.rdoReminder.Text = "Reminder";
            this.rdoReminder.UseVisualStyleBackColor = true;
            // 
            // txtOverDateForReminder
            // 
            this.txtOverDateForReminder.Location = new System.Drawing.Point(377, 246);
            this.txtOverDateForReminder.Name = "txtOverDateForReminder";
            this.txtOverDateForReminder.Size = new System.Drawing.Size(34, 20);
            this.txtOverDateForReminder.TabIndex = 21;
            this.txtOverDateForReminder.Text = "30";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(412, 249);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "days";
            // 
            // rdoNewTnC
            // 
            this.rdoNewTnC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdoNewTnC.AutoSize = true;
            this.rdoNewTnC.Location = new System.Drawing.Point(371, 187);
            this.rdoNewTnC.Name = "rdoNewTnC";
            this.rdoNewTnC.Size = new System.Drawing.Size(69, 17);
            this.rdoNewTnC.TabIndex = 23;
            this.rdoNewTnC.TabStop = true;
            this.rdoNewTnC.Text = "T&&C2012";
            this.rdoNewTnC.UseVisualStyleBackColor = true;
            // 
            // rdoSpecify
            // 
            this.rdoSpecify.AutoSize = true;
            this.rdoSpecify.Location = new System.Drawing.Point(3, 10);
            this.rdoSpecify.Name = "rdoSpecify";
            this.rdoSpecify.Size = new System.Drawing.Size(140, 17);
            this.rdoSpecify.TabIndex = 24;
            this.rdoSpecify.TabStop = true;
            this.rdoSpecify.Text = "Specify Contact Number";
            this.rdoSpecify.UseVisualStyleBackColor = true;
            this.rdoSpecify.CheckedChanged += new System.EventHandler(this.rdoSpecify_CheckedChanged);
            // 
            // rdoRange
            // 
            this.rdoRange.AutoSize = true;
            this.rdoRange.Location = new System.Drawing.Point(3, 51);
            this.rdoRange.Name = "rdoRange";
            this.rdoRange.Size = new System.Drawing.Size(72, 17);
            this.rdoRange.TabIndex = 24;
            this.rdoRange.TabStop = true;
            this.rdoRange.Text = "By Range";
            this.rdoRange.UseVisualStyleBackColor = true;
            this.rdoRange.CheckedChanged += new System.EventHandler(this.rdoRange_CheckedChanged);
            // 
            // txtContNo
            // 
            this.txtContNo.Enabled = false;
            this.txtContNo.Location = new System.Drawing.Point(15, 30);
            this.txtContNo.Name = "txtContNo";
            this.txtContNo.Size = new System.Drawing.Size(333, 20);
            this.txtContNo.TabIndex = 3;
            this.txtContNo.Text = "919775";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdoSpecify);
            this.panel1.Controls.Add(this.rdoRange);
            this.panel1.Controls.Add(this.txtMinExact);
            this.panel1.Controls.Add(this.txtContNo);
            this.panel1.Controls.Add(this.txtMaxExact);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(351, 97);
            this.panel1.TabIndex = 25;
            // 
            // rdoTnC2018
            // 
            this.rdoTnC2018.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdoTnC2018.AutoSize = true;
            this.rdoTnC2018.Location = new System.Drawing.Point(370, 207);
            this.rdoTnC2018.Name = "rdoTnC2018";
            this.rdoTnC2018.Size = new System.Drawing.Size(69, 17);
            this.rdoTnC2018.TabIndex = 23;
            this.rdoTnC2018.TabStop = true;
            this.rdoTnC2018.Text = "T&&C2018";
            this.rdoTnC2018.UseVisualStyleBackColor = true;
            // 
            // AutoSendServerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 663);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.rdoTnC2018);
            this.Controls.Add(this.rdoNewTnC);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtOverDateForReminder);
            this.Controls.Add(this.rdoReminder);
            this.Controls.Add(this.rdoMonthlyStatement);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.sendProgressBar);
            this.Controls.Add(this.panFailed);
            this.Controls.Add(this.panSent);
            this.Controls.Add(this.panLeft);
            this.Controls.Add(this.chkFax);
            this.Controls.Add(this.chkEmail);
            this.Controls.Add(this.btnJobStop);
            this.Controls.Add(this.txtEmailStatus);
            this.Controls.Add(this.btnJobStart);
            this.Name = "AutoSendServerForm";
            this.Text = "Monthly Statement Email/Faxer v6.00";
            this.Load += new System.EventHandler(this.AutoSendServerForm_Load);
            this.panLeft.ResumeLayout(false);
            this.panLeft.PerformLayout();
            this.panSent.ResumeLayout(false);
            this.panSent.PerformLayout();
            this.panFailed.ResumeLayout(false);
            this.panFailed.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnJobStart;
        private System.Windows.Forms.TextBox txtEmailStatus;
        private System.Windows.Forms.Button btnJobStop;
        private System.Windows.Forms.TextBox txtMinExact;
        private System.Windows.Forms.TextBox txtMaxExact;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkEmail;
        private System.Windows.Forms.CheckBox chkFax;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmailsLeft;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFaxesLeft;
        private System.Windows.Forms.Panel panLeft;
        private System.Windows.Forms.Panel panSent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFaxesSent;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtEmailsSent;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panFailed;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFaxesFailed;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtEmailsFailed;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ProgressBar sendProgressBar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton rdoMonthlyStatement;
        private System.Windows.Forms.RadioButton rdoReminder;
        private System.Windows.Forms.TextBox txtOverDateForReminder;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton rdoNewTnC;
        private System.Windows.Forms.RadioButton rdoSpecify;
        private System.Windows.Forms.RadioButton rdoRange;
        private System.Windows.Forms.TextBox txtContNo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdoTnC2018;
    }
}

