/*
 * For Testing purposes
 */
//#define SendToOurselves   // If defined, emails will be sent to ourselves (Qingyou and Patrick)       (used for testing purposes)
//#define NoLog             // If defined, any sends will not be logged to tblAutomaticStatementHistory (used for testing purposes)
//#define SendAllAttachmentTypes // If defined, sends all attachment types for emails


using System;
using System.Collections.Generic;
using System.Text;
using AutoSenderLibrary;

namespace AutoSendServerTrial
{
    /// <summary>Delegate for successful contact send</summary>
    /// <param name="contact">contacts of successful send</param>
    /// <param name="e">empty events arg</param>
    public delegate void AutoSendJobEventHandler(CustomerContact contact, AutoSendAttemptStatus attempt);
    public delegate void AutoSendGeneralEventHandler();

    #region AutoSendAttemptStatus class
    class AutoSendAttemptStatus
    { 
        private int exportAttempt;
        private int emailFaxAttempt;
        private int sendId;

        public AutoSendAttemptStatus()
        {
            exportAttempt = 1;
            emailFaxAttempt = 1;
        }

        public int ExportAttempt
        {
            get
            {
                return exportAttempt;
            }
            set
            {
                exportAttempt = value;
            }
        }

        public int EmailFaxAttempt
        {
            get
            {
                return emailFaxAttempt;
            }
            set
            {
                emailFaxAttempt = value;
            }
        }

        public int SendId
        {
            get
            {
                return sendId;
            }
            set
            {
                sendId = value;
            }
        }
    }
    #endregion

    class AutoSendJob
    {
        // contacts
        private CustomerContactList customersToSend;

        // SendJobType
        private AutoSendType _sendJobType;
        private int reminderDay;

        // Parameters
        private string cont_no;
        private int exportAttemptLimit = 2;
        private int exportRetryWait = 300000; // in ms (5 mins)

        // emailing contacts
        private readonly int firstContactToSend = 0;
        private readonly int maxContactsToSend = 2;  // (send to at most 3 emails to avoid spamming)

        // stop job
        private bool stopJob = false;

        #region events
        public event AutoSendJobEventHandler FailedExport;
        public event AutoSendJobEventHandler SuccessfulSend;
        public event AutoSendJobEventHandler FailedSend;
        public event AutoSendGeneralEventHandler AutoSendComplete;

        // Event handlers for Autosendjob
        private void FailedExportStatusUpdate(CustomerContact contact, AutoSendAttemptStatus attempt)
        {
            if (attempt.ExportAttempt < exportAttemptLimit)
            {
                // wait before retrying
                System.Threading.Thread.Sleep(exportRetryWait);

                // retry
                attempt.ExportAttempt++;
                SendEmailFaxHelper(contact, attempt);
            }
        }

        private void SuccessfulEmailStatusUpdate(CustomerContact contact, AutoSendAttemptStatus attempt)
        {
        }

        private void FailedEmailStatusUpdate(CustomerContact contact, AutoSendAttemptStatus attempt)
        {
        }

        private void FinishedSending()
        {
        }

        private void addEventHandlers()
        {
            FailedExport += new AutoSendJobEventHandler(FailedExportStatusUpdate);
            SuccessfulSend += new AutoSendJobEventHandler(SuccessfulEmailStatusUpdate);
            FailedSend += new AutoSendJobEventHandler(FailedEmailStatusUpdate);
            AutoSendComplete += new AutoSendGeneralEventHandler(FinishedSending);
        }

        private void removeEventHandlers()
        {
            FailedExport -= new AutoSendJobEventHandler(FailedExportStatusUpdate);
            SuccessfulSend -= new AutoSendJobEventHandler(SuccessfulEmailStatusUpdate);
            FailedSend -= new AutoSendJobEventHandler(FailedEmailStatusUpdate);
            AutoSendComplete -= new AutoSendGeneralEventHandler(FinishedSending);
        }
        #endregion

        #region constructors
        /// <summary>Constructor that creates new customer contact list() with email and fax</summary>
        public AutoSendJob()
            : this(new ActionTypeList(new ActionType[] { ActionType.Email, ActionType.Fax }))
        {
        }

        /// <summary>Constructor that creates new customer contact list() with email and fax</summary>
        /// <param name="dataBase">Database to connect to</param>
        public AutoSendJob(DatabaseEnvironment dataBase)
            : this(new ActionTypeList(new ActionType[] { ActionType.Email, ActionType.Fax }))
        {
        }

        /// <summary>Constructor that creates new customer contact list</summary>
        /// <param name="actions">Desired actions (Please only send email or fax types)</param>
        public AutoSendJob(ActionTypeList actions)
            : this(actions, AutoSendType.MonthlyStatement, CustomerContactList.defaultOverdueReminderDay, "")
        { 
        }

        /// <summary>Constructor that creates new customer contact list</summary>
        /// <param name="actions">Desired actions (Please only send email or fax types)</param>
        /// <param name="reminder">False if monthly statement, true if reminder statement</param>
        /// <param name="overdueReminderDay">Send to Customers with overdues greater than stated days</param>
        public AutoSendJob(ActionTypeList actions, AutoSendType sendJobType, int overdueReminderDay, string contactNumber)
        {
            _sendJobType = sendJobType;
            cont_no = contactNumber;

            switch (_sendJobType)
            {
                case AutoSendType.Reminder:
                    reminderDay = DateTime.Now.Day;
                    break;
                case AutoSendType.MonthlyStatement:
                    reminderDay = 0;
                    break;
                case AutoSendType.NewTermsAndConditions2012:
                case AutoSendType.NewTermsAndConditions2018:
                    reminderDay = -1;
                    break;
            }

            customersToSend = new CustomerContactList(actions, _sendJobType, reminderDay, overdueReminderDay, cont_no);
            //emailer = new EmailFax();
        }
        #endregion

        /// <summary>Number of contacts that will be sent to at one time.</summary>
        /// <returns>Number of contacts that will be sent to at one time.</returns>
        public int getNumberOfContactsToSend()
        {
            return maxContactsToSend - firstContactToSend + 1;
        }

        /// <summary>Returns if contact retrieval is successful</summary>
        /// <returns>true if successful</returns>
        public bool isContactsRetrievalSuccessful()
        {
            return customersToSend.isRetrievalSuccessful();
        }

        public int getNumberOfCustomersToSend(ActionTypeList actions)
        {
            return customersToSend.getListOfCustomers(actions).Length;
        }

        public int getNumberOfCustomersToSend(ActionTypeList actions, string minExact, string maxExact)
        {
            int num = 0;
            foreach (string exact in customersToSend.getListOfCustomers(actions))
            {
                long exactn = long.Parse(exact);
                long minExactn = long.Parse(minExact);
                long maxExactn = long.Parse(maxExact);

                if (exactn >= minExactn && exactn <= maxExactn)
                    num++;
            }

            return num;
        }

        /// <summary>For stopping current job</summary>
        public bool StopJob
        {
            get
            {
                return stopJob;
            }
            set
            {
                stopJob = value;
            }
        }

        /// <summary>Setting and getting export attempt limit</summary>
        public int ExportAttemptLimit
        {
            get
            {
                return exportAttemptLimit;
            }
            set
            {
                exportAttemptLimit = value;
            }
        }

        /// <summary>Send email and faxes to customers between min(100000000) and max(699999999) numbers</summary>
        public void SendEmailFaxes()
        {
            SendEmailFaxes("100000000", "699999999");
        }

        /// <summary>Send email and faxes to customers between the min and max numbers</summary>
        /// <param name="minCustomer"></param>
        /// <param name="maxCustomer"></param>
        public void SendEmailFaxes(string minCustomer, string maxCustomer)
        {           
            addEventHandlers();

            double minExact = double.Parse(minCustomer);
            double maxExact = double.Parse(maxCustomer);

            string[] customers = customersToSend.getListOfCustomers();
            foreach (string customer in customers)
            {
                if (stopJob)
                    return;

                double customerno = double.Parse(customer);
                if (customerno >= minExact && customerno <= maxExact)
                {
                    CustomerContact customercontact = customersToSend.getCustomerContact(customer);
                    SendEmailFax(customercontact);
                }
            }

            AutoSendComplete();

            removeEventHandlers();
        }

        /// <summary>Send email and fax to customer</summary>
        /// <param name="customer">customer's exact number</param>
        /// <returns>True if successfully emailed</returns>
        public bool SendEmailFax(CustomerContact customer)
        {
            return SendEmailFaxHelper(customer, new AutoSendAttemptStatus());
        }

        private bool SendEmailFaxHelper(CustomerContact customer, AutoSendAttemptStatus attempt)
        {
            EmailFax emailer = new EmailFax();
            
            // Check which versions of the report will need to be generated
            // 1) is a monthly statement
            // 2) is sent via email and format is required
            bool pdfReportRequired = false;
            bool xlsReportRequired = false;

            if (_sendJobType == AutoSendType.MonthlyStatement)
            {
                if (customer.getActionType() != ActionType.Email)
                {
                    pdfReportRequired = true;
                    xlsReportRequired = false;
                }
                else
                {
                    if (customer.getActionType() == ActionType.Email)
                    {
                        if (customer.SendFormats.HasFormat(EStatementFormat.PDF))
                            pdfReportRequired = true;
                        if (customer.SendFormats.HasFormat(EStatementFormat.EXCEL2003))
                            xlsReportRequired = true;
                    }
                }
            }

#if SendAllAttachmentTypes
            pdfReportRequired = true;
            xlsReportRequired = true;
#endif

            // Create statement
            MonthlyStatementReport rpt = null;
            if (pdfReportRequired)
            {
                // Get Statement for the latest full month
                rpt = new MonthlyStatementReport(customer.getCustomerNumber(), MonthlyStatementReport.getLatestMonthlyStatementDate());
            }

            if (!pdfReportRequired || rpt.isReportSuccessfullyGenerated())
            {                
                // Set Email contents
                if (_sendJobType == AutoSendType.MonthlyStatement)
                    emailer.SetEmailBodySettings(customer.getAccountType(), EmailType.Monthly);
                else if (_sendJobType == AutoSendType.Reminder)
                    emailer.SetEmailBodySettings(customer.getAccountType(), EmailType.Reminder1);
                else // New terms and conditions
                    emailer.SetEmailBodySettings(customer.getAccountType(), EmailType.NewTermsAndConditions);

                // Append customer info to message body if fax
                if (customer.getActionType() == ActionType.Fax)
                    emailer.AppendContactInfoToMessageBody(customer, firstContactToSend, maxContactsToSend);

                // Generate PDF of Monthly statement if required
                string pdfReportPath = null;
                if (pdfReportRequired)
                {
                    pdfReportPath = rpt.exportToPDF();
                    if (pdfReportPath == null)
                    {
                        // Signal export failed
                        FailedExport(customer, attempt);
                        return false;
                    }
                }

                // Generate excel of monthly statement if required
                string xlsReportPath = null;
                if (xlsReportRequired)
                {
                    xlsReportPath = MonthlyStatementReportExport.ExportToExcel(customer.Invoices);
                }

                // check if stopJob has been checked
                if (stopJob)
                {
                    return false;
                }

                #region test code (send to ourselves)
#if (SendToOurselves)
                // Add customer details to body
                emailer.MessageBody += "\n\nThis would have been sent to:";
                foreach (string contact in customer.getDetails(firstContactToSend, maxContactsToSend))
                {
                    emailer.MessageBody += "\n\t" + contact;
                }

                // replace customer
                CustomerInvoices invoices = customer.Invoices;
                customer = new CustomerContact(customer.getCustomerNumber(), customer.getCustomerCode(), ActionType.Email, customer.getUpszone());
                customer.addDetail("qingyou_lu@livingstone.com.au");
                //customer.addDetail("patrick_bezzina@livingstone.com.au");
                //customer.addDetail("jasonl@livingstone.com.au");
                
                //customer.addDetail("qingyou_luu@livingstone.com.au");
                //customer.addDetail("patrick_cezzina@livingstone.com.au");

                //customer = new CustomerContact(customer.getCustomerNumber(), ActionType.Fax, customer.getUpszone());

                //customer = new CustomerContact(customer.getCustomerNumber(), customer.getCustomerCode(), ActionType.Fax, customer.getUpszone());
                //customer.addDetail("93138991");
                //customer.addDetail("1300729729");
                //customer.addDetail("93137977");

                customer.Invoices = invoices;
#endif
                #endregion

                #region writetolog
#if (!NoLog)
                StatementActionsLogger logger = new StatementActionsLogger();
                //int sendid = logger.InsertLog(customer, firstContactToSend, maxContactsToSend, false, false, reminderDay);
                int sendid = logger.InsertSendLog(customer, firstContactToSend, maxContactsToSend, reminderDay);
                attempt.SendId = sendid;

                if (_sendJobType == AutoSendType.NewTermsAndConditions2012)
                {
                    logger.UpdateCustomerAccFlags(customer.getCustomerNumber(), true, 2012);
                }
                else if (_sendJobType == AutoSendType.NewTermsAndConditions2018)
                {
                    logger.UpdateCustomerAccFlags(customer.getCustomerNumber(), true, 2018);
                }
#endif

                // append message subject
#if (NoLog)
                // If not logged in database, give send id = 0
                int sendid = 0;
#endif
                #endregion

                SendStatus sendresult;
                if (_sendJobType == AutoSendType.MonthlyStatement)
                {
                    emailer.Subject += "  Month: " + MonthlyStatementReport.getLatestMonthlyStatementDate().ToString("MMMM yyyy") + string.Format(" Rcpt: {0}", sendid.ToString("X"));
                    sendresult = emailer.SendEmail(customer, new string[] { pdfReportPath, xlsReportPath }, firstContactToSend, maxContactsToSend);
                }
                else if (_sendJobType == AutoSendType.Reminder)
                {
                    emailer.Subject += "  Month: " + DateTime.Now.ToString("MMMM yyyy") + string.Format(" Rcpt: {0}", sendid.ToString("X"));
                    sendresult = emailer.SendReminder(customer, pdfReportPath, firstContactToSend, maxContactsToSend);
                }
                else if (_sendJobType == AutoSendType.NewTermsAndConditions2012)
                {
                    emailer.Subject += string.Format(" Rcpt: {0}", sendid.ToString("X"));
                    sendresult = emailer.SendNewTermsandConditions(customer, new string[] { "G:\\IT\\Qingyou\\newtnc\\new_terms_and_conditions.doc", "G:\\IT\\Qingyou\\newtnc\\customer_information_form.doc" }, firstContactToSend, maxContactsToSend);
                }
                else if (_sendJobType == AutoSendType.NewTermsAndConditions2018)
                {
                    emailer.Subject += string.Format(" Rcpt: {0}", sendid.ToString("X"));
                    sendresult = emailer.SendNewTermsandConditions(customer, new string[] { @"\\livdoc9\Mydocs\IT\Shared\TNC2018.pdf" }, firstContactToSend, maxContactsToSend);
                }
                else
                {
                    sendresult = SendStatus.Failure;
                }

                if (sendresult == SendStatus.Success) // non-testing (send to customers)
                {
                    //reset messageBody and subj
                    emailer.ResetMessageBody();

                    // success event
                    SuccessfulSend(customer, attempt);
                    if (_sendJobType == AutoSendType.MonthlyStatement)
                    {
                        rpt.Dispose();
                    }
                    return true;
                }
                #region update log if failed
#if (!NoLog)
                else
                {
                    // Set as internal error if failed
                    //logger.UpdateLog(sendid, false, true);
                    logger.UpdateSendLog(sendid, false, true);

                    // update customer account flag if error
                    if (_sendJobType == AutoSendType.NewTermsAndConditions2012)
                    {
                        logger.UpdateCustomerAccFlags(customer.getCustomerNumber(), false, 2012);
                    }
                    else if (_sendJobType == AutoSendType.NewTermsAndConditions2018)
                    {
                        logger.UpdateCustomerAccFlags(customer.getCustomerNumber(), false, 2018);
                    }
                }
#endif
                #endregion

                //reset messageBody and subj
                emailer.ResetMessageBody();
            }

            // failed event
            FailedSend(customer, attempt);
            if (rpt != null) rpt.Dispose();
            return false;
        }
    }
}
