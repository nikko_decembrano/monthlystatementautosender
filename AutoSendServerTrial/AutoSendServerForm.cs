using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AutoSenderLibrary;
using AutoSenderLibrarySupportTester;
using System.Threading;
using System.Diagnostics; // for stopwatch
using AutoSenderLogin;

namespace AutoSendServerTrial
{
    public partial class AutoSendServerForm : Form
    {
        private Thread autoSendJobThread;
        private AutoSendJob autoSendJob;
        private Stopwatch sw;

        public AutoSendServerForm()
        {
            if (!MonthlyStatementReportTester.TestCrystalReportSupport())
            {
                MessageBox.Show(MonthlyStatementReportTester.TestCrystalReportSupportFailedMessage());
                System.Environment.Exit(1);
                return;
            }
//#if (!DEBUG) // Check barcode font if not running in debug mode
            if (!MonthlyStatementReportTester.TestBarcodeFontInstalled())
            {
                MessageBox.Show(MonthlyStatementReportTester.TestBarcodeFontInstalledFailedMessage());
                System.Environment.Exit(1);
                return;
            }
//#endif

            // Login (AU only)
            FormLogin loginForm = new FormLogin();
            loginForm.AllowDbChange = false;
            DatabaseEnvironment db = DatabaseEnvironment.AU;
            if (!loginForm.IsAuthenticated(db))
            {
                loginForm.ShowDialog();

                if (loginForm.DialogResult == DialogResult.Cancel)
                {
                    // Exit with failure
                    System.Environment.Exit(1);
                }
            }
            if (!loginForm.HasEsendPermission(db))
            {
                MessageBox.Show("Your account does not give you permission to print statements, please contact IT or login as a different user");
                System.Environment.Exit(1);
            }
            //------------------------------------------

            InitializeComponent();

            // set as monthly statement by default
            rdoMonthlyStatement.Checked = true;
        }

        private void btnJobStart_Click(object sender, EventArgs e)
        {
            double minexact;
            double maxexact;

            if (rdoRange.Checked)
            {
                try
                {
                    minexact = double.Parse(txtMinExact.Text);
                    maxexact = double.Parse(txtMaxExact.Text);
                }
                catch
                {
                    MessageBox.Show("Invalid Exact Nos");
                    return;
                }
            }

            // check email and fax checkboxes
            if (!chkEmail.Checked && !chkFax.Checked)
            {
                MessageBox.Show("Please select autosend types.\n(Sending Emails or Faxes or Both)");
                return;
            }
            ActionTypeList actions = new ActionTypeList();
            if (chkEmail.Checked)
                actions.addAction(ActionType.Email);
            if (chkFax.Checked)
                actions.addAction(ActionType.Fax);

            // check if monthly statement or reminder type
            AutoSendType sendjobtype = AutoSendType.MonthlyStatement;
            if (rdoMonthlyStatement.Checked)
                sendjobtype = AutoSendType.MonthlyStatement;
            else if (rdoReminder.Checked)
                sendjobtype = AutoSendType.Reminder;
            else if (rdoNewTnC.Checked)
                sendjobtype = AutoSendType.NewTermsAndConditions2012;
            else if (rdoTnC2018.Checked)
                sendjobtype = AutoSendType.NewTermsAndConditions2018;
            
            // Check number of days
            int overdueReminderDay = CustomerContactList.defaultOverdueReminderDay;
            if (sendjobtype == AutoSendType.Reminder)
            {
                try
                {
                    overdueReminderDay = int.Parse(txtOverDateForReminder.Text);
                }
                catch
                {
                    MessageBox.Show("integer values only for reminder day");
                    return;
                }
            }

            btnJobStart.Enabled = false;
            btnJobStop.Enabled = true;

            sw = new Stopwatch();
            sw.Start();

            txtEmailStatus.Text += System.Environment.NewLine + "Getting customers";

            this.Cursor = Cursors.WaitCursor;
            autoSendJob = new AutoSendJob(actions, sendjobtype, overdueReminderDay, rdoSpecify.Checked ? txtContNo.Text : "");
            this.Cursor = Cursors.Default;

            // check if success
            if (!autoSendJob.isContactsRetrievalSuccessful())
            {
                sw.Stop();
                txtEmailStatus.Text += System.Environment.NewLine + "Database retrieval Failed";
                btnJobStart.Enabled = true;
                btnJobStop.Enabled = false;
                return;
            }

            // Add event Handlers
            autoSendJob.FailedExport += new AutoSendJobEventHandler(FailedExportStatusUpdate);
            autoSendJob.SuccessfulSend += new AutoSendJobEventHandler(SuccessfulEmailStatusUpdate);
            autoSendJob.FailedSend += new AutoSendJobEventHandler(FailedEmailStatusUpdate);
            autoSendJob.AutoSendComplete += new AutoSendGeneralEventHandler(FinishedSending);

            txtEmailStatus.Text += System.Environment.NewLine + string.Format("\tTime taken(hh:mm:ss:mss): {0}:{1:D2}:{2:D2}:{3:D3}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds, sw.Elapsed.Milliseconds);
            sw.Reset();
            sw.Start();

            // initialise counter
            InitSendCounter();

            //single thread test
            //autoSendJob.SendEmailFaxes();

            //autoSendJobThread = new Thread(new ThreadStart(autoSendJob.SendEmailFaxes));
            autoSendJobThread = new Thread(new ThreadStart(SendEmailFaxesHelper));
            autoSendJob.StopJob = false;
            autoSendJobThread.Start();
        }

        // helper for thread start
        private void SendEmailFaxesHelper()
        {
            autoSendJob.SendEmailFaxes(txtMinExact.Text, txtMaxExact.Text);
        }
        
        #region event handlers
        // Event handlers for Autosendjob
        private void FailedExportStatusUpdate(CustomerContact contact, AutoSendAttemptStatus attempt)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AutoSendJobEventHandler(FailedExportStatusUpdate), new object[] { contact, attempt });
            }
            else
            {
                txtEmailStatus.Text += System.Environment.NewLine + "Export failed for " + contact.getCustomerNumber() + string.Format(" (attempt {0})", attempt.ExportAttempt.ToString());
                txtEmailStatus.Text += System.Environment.NewLine + string.Format("\tTime taken(hh:mm:ss:mss): {0}:{1:D2}:{2:D2}:{3:D3}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds, sw.Elapsed.Milliseconds);
                sw.Reset();
                sw.Start();

                // if attempt limit reached, update count as failed
                if (attempt.ExportAttempt == autoSendJob.ExportAttemptLimit)
                {
                    UpdateCount(contact.getActionType(), false);
                }
            }
        }

        private void SuccessfulEmailStatusUpdate(CustomerContact contact, AutoSendAttemptStatus attempt)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AutoSendJobEventHandler(SuccessfulEmailStatusUpdate), new object[] { contact, attempt });
            }
            else
            {
                txtEmailStatus.Text += System.Environment.NewLine + "Successfully Emailed to contacts of " + contact.getCustomerNumber();
                foreach (string email in contact.getDetails(0,autoSendJob.getNumberOfContactsToSend()-1))
                    txtEmailStatus.Text += System.Environment.NewLine + "\t" + email;
                txtEmailStatus.Text += System.Environment.NewLine + string.Format("\tTime taken(hh:mm:ss:mss): {0}:{1:D2}:{2:D2}:{3:D3}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds, sw.Elapsed.Milliseconds);
                txtEmailStatus.Text += System.Environment.NewLine + string.Format("\tSendID: {0}({1})", attempt.SendId, attempt.SendId.ToString("X"));
                sw.Reset();
                sw.Start();

                // update successful email/fax count
                UpdateCount(contact.getActionType(), true);
            }
        }

        private void FailedEmailStatusUpdate(CustomerContact contact, AutoSendAttemptStatus attempt)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AutoSendJobEventHandler(FailedEmailStatusUpdate), new object[] { contact, attempt });
            }
            else
            {
                txtEmailStatus.Text += System.Environment.NewLine + "Failed to send to contacts of " + contact.getCustomerNumber();
                foreach (string email in contact.getDetails(0, autoSendJob.getNumberOfContactsToSend() - 1))
                    txtEmailStatus.Text += System.Environment.NewLine + "\t" + email;
                txtEmailStatus.Text += System.Environment.NewLine + string.Format("\tTime taken(hh:mm:ss:mss): {0}:{1:D2}:{2:D2}:{3:D3}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds, sw.Elapsed.Milliseconds);
                sw.Reset();
                sw.Start();

                // update failed email/fax count
                UpdateCount(contact.getActionType(), false);
            }
        }

        private void FinishedSending()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new AutoSendGeneralEventHandler(FinishedSending));
            }
            else
            {
                FinishedSendingCleanup();

                txtEmailStatus.Text += System.Environment.NewLine + "Complete" + System.Environment.NewLine + System.Environment.NewLine;
            }
        }
        #endregion

        private void btnJobStop_Click(object sender, EventArgs e)
        {
            // stop job
            //autoSendJobThread.Abort();
            autoSendJob.StopJob = true;
            timer1.Start();

            /*while (autoSendJobThread.IsAlive)
            {
                //wait
            }*/
            
            //FinishedSendingCleanup();

            txtEmailStatus.Text += System.Environment.NewLine + "Attempting to cancel" + System.Environment.NewLine + System.Environment.NewLine;
        }

        private void FinishedSendingCleanup()
        {
            sw.Reset();
            btnJobStart.Enabled = true;
            btnJobStop.Enabled = false;

            if (autoSendJob != null)
            {
                autoSendJob.FailedExport -= new AutoSendJobEventHandler(FailedExportStatusUpdate);
                autoSendJob.SuccessfulSend -= new AutoSendJobEventHandler(SuccessfulEmailStatusUpdate);
                autoSendJob.FailedSend -= new AutoSendJobEventHandler(FailedEmailStatusUpdate);
                autoSendJob.AutoSendComplete -= new AutoSendGeneralEventHandler(FinishedSending);
            }

            autoSendJob = null;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!autoSendJobThread.IsAlive)
            {
                timer1.Stop();
                FinishedSendingCleanup();
                txtEmailStatus.Text += System.Environment.NewLine + "Cancelled" + System.Environment.NewLine + System.Environment.NewLine;
            }
        }

        private void InitSendCounter()
        {
            //int Emails = autoSendJob.getNumberOfCustomersToSend(new ActionTypeList(new ActionType[] { ActionType.Email }));
            //int Faxes  = autoSendJob.getNumberOfCustomersToSend(new ActionTypeList(new ActionType[] { ActionType.Fax }));
            int Emails = autoSendJob.getNumberOfCustomersToSend(new ActionTypeList(new ActionType[] { ActionType.Email }), txtMinExact.Text, txtMaxExact.Text);
            int Faxes = autoSendJob.getNumberOfCustomersToSend(new ActionTypeList(new ActionType[] { ActionType.Fax }), txtMinExact.Text, txtMaxExact.Text);

            txtEmailsSent.Text   = "0";
            txtEmailsFailed.Text = "0";
            txtEmailsLeft.Text   = Emails.ToString();

            txtFaxesSent.Text    = "0";
            txtFaxesFailed.Text  = "0";
            txtFaxesLeft.Text    = Faxes.ToString();

            sendProgressBar.Minimum = 0;
            sendProgressBar.Maximum = Emails + Faxes;
            sendProgressBar.Step = 1;
            sendProgressBar.Value = 0;
        }

        private void UpdateCount(ActionType action, bool success)
        {
            if (action == ActionType.Email)
            {
                if (success)
                {
                    txtEmailsSent.Text = string.Format("{0}", (int.Parse(txtEmailsSent.Text) + 1));
                }
                else
                {
                    txtEmailsFailed.Text = string.Format("{0}", (int.Parse(txtEmailsFailed.Text) + 1));
                }

                txtEmailsLeft.Text = string.Format("{0}", (int.Parse(txtEmailsLeft.Text) - 1));
            }

            else if (action == ActionType.Fax)
            {
                if (success)
                {
                    txtFaxesSent.Text = string.Format("{0}", (int.Parse(txtFaxesSent.Text) + 1));
                }
                else
                {
                    txtFaxesFailed.Text = string.Format("{0}", (int.Parse(txtFaxesFailed.Text) + 1));
                }

                txtFaxesLeft.Text = string.Format("{0}", (int.Parse(txtFaxesLeft.Text) - 1));
            }

            // update send progress bar
            sendProgressBar.PerformStep();
        }

        private void txtEmailStatus_TextChanged(object sender, EventArgs e)
        {
            //txtEmailStatus.Select(0, txtEmailStatus.Text.Length);
            txtEmailStatus.SelectionStart = txtEmailStatus.Text.Length;
            txtEmailStatus.ScrollToCaret();
        }

        private void rdoRange_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoRange.Checked)
            {
                txtMinExact.Enabled = true;
                txtMaxExact.Enabled = true;
            }
            else
            {
                txtMinExact.Enabled = false;
                txtMaxExact.Enabled = false;
            }
        }

        private void AutoSendServerForm_Load(object sender, EventArgs e)
        {
            rdoRange.Checked = true;
        }

        private void rdoSpecify_CheckedChanged(object sender, EventArgs e)
        {
            txtContNo.Enabled = (rdoSpecify.Checked);
        }
    }
}